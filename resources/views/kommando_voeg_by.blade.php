@extends('layouts.master')

@section('head')
@parent
<title>Kommando</title>
@stop


@section('content')
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <a href={{URL('kommando/tabel')}}><button type="submit" class="btn btn-primary "><i class="fa fa-arrow-circle-left"></i></button></a> Kommando
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">

                @if($errors->has())
                   @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Waarskuwing!</b> {{$error}}
                        </div>
                  @endforeach
                @endif


                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- form start -->
                           <form role="form" action="{{URL('kommando/voegby/nuut')}}" method="POST">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><b>Kommando Inligting</b></h3>
                                </div><!-- /.box-header -->
                                
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                
                                    <div class="box-body">

                                    <!-- Naam -->
                                        <div class="row form-group">
                                            <div class="col-xs-2">
                                                <label>Kommando Naam <span>*</span></label>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="kommando_naam" value="{{Input::old('kommando_naam')}}"> 
                                            </div>   
                                        </div>

                                    <!-- Stad/Dorp -->
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Stad/Dorp</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="stad_dorp" value="{{Input::old('stad_dorp')}}" > 
                                            </div>   
                                        </div>

                                    <!-- Provinsie -->
                                        <div class="row form-group">
                                            <div class="col-xs-2">
                                                <label>Provinsie</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <select name="provinsie" class="form-control">
                                                    
                                                    <option></option>
                                                    @if(Input::old('provinsie') === "Gauteng")
                                                        <option selected>Gauteng</option>
                                                    @else 
                                                        <option>Gauteng</option>
                                                    @endif

                                                    @if(Input::old('provinsie') === "KwaZulu-Natal")
                                                        <option selected>KwaZulu-Natal</option>
                                                    @else
                                                        <option>KwaZulu-Natal</option>
                                                    @endif

                                                    @if(Input::old('provinsie') === "Limpopo")
                                                        <option selected>Limpopo</option>
                                                    @else
                                                        <option>Limpopo</option>
                                                    @endif

                                                    @if(Input::old('provinsie') === "Mpumalanga")
                                                        <option selected>Mpumalanga</option>
                                                    @else
                                                        <option>Mpumalanga</option>
                                                    @endif

                                                    @if(Input::old('provinsie') === "Noordwes")
                                                        <option selected>Noordwes</option>
                                                    @else
                                                        <option>Noordwes</option>
                                                    @endif

                                                    @if(Input::old('provinsie') === "Noord-Kaap")
                                                        <option selected>Noord-Kaap</option>
                                                    @else
                                                        <option>Noord-Kaap</option>
                                                    @endif

                                                    @if(Input::old('provinsie') === "Oos-Kaap")
                                                        <option selected>Oos-Kaap</option>
                                                    @else
                                                        <option>Oos-Kaap</option>
                                                    @endif

                                                    @if(Input::old('provinsie') === "Wes-Kaap")
                                                        <option selected>Wes-Kaap</option>
                                                    @else
                                                        <option>Wes-Kaap</option>
                                                    @endif

                                                    @if(Input::old('provinsie') === "Vrystaat")
                                                        <option selected>Vrystaat</option>
                                                    @else
                                                        <option>Vrystaat</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                            </div>
                                
                            </div><!-- /.box --> 

                            <!-- Die Voortrekkers --> 
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><b>Kontak Persoon</b></h3>
                                </div><!-- /.box-header -->

                                <div class="box-body">



                                    <!-- Kontak Persoon Naam & Van -->
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Kontak Persoon Naam & Van</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="kontak_persoon_naam_van" value="{{Input::old('kontak_persoon_naam_van')}}" > 
                                            </div>   
                                        </div>            
                                    

                                        <!-- Kontak Persoon Epos Adres -->
                                        <div class="row form-group">
                                                 <div class="col-xs-2">
                                                    <label>Kontak Persoon Epos Adres</label>
                                                </div>
                                                <div class="col-xs-4">
                                                    <input type="text" class="form-control" name="kontak_persoon_epos" value="{{Input::old('kontak_persoon_epos')}}"> 
                                                </div>              
                                        </div>

                                        <!-- Kontak Persoon Telefoon Nommer -->
                                        <div class="row form-group">
                                                 <div class="col-xs-2">
                                                    <label>Kontak Persoon Telefoon Nommer</label>
                                                </div>
                                                <div class="col-xs-4">
                                                    <input type="text" class="form-control" name="kontak_persoon_telefoon" value="{{Input::old('kontak_persoon_telefoon')}}"> 
                                                </div>       
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>

                            <div class="box-footer">
                                        <button type="submit" class="btn bg-olive pull-right" style="margin-bottom: 15px;"><i class="fa fa-paper-plane"></i> Stoor</button>
                                        </form>
                                        <a href={{URL('kommando/tabel')}}><button class="btn btn-primary pull-left" style="margin-bottom: 15px;">Kanselleer</button></a>
                            </div>
                            
                            </form>
                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>
        <!-- InputMask -->
        <script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.date.extensions.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.extensions.js')}}" type="text/javascript"></script>


        <script type="text/javascript">
            //Datemask
                $("#datemask").inputmask("yyyy/mm/dd", {"placeholder": "yyyy/mm/dd"});
        </script>



@stop
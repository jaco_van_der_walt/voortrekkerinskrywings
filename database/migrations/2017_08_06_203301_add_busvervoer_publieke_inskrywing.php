<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBusvervoerPubliekeInskrywing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('publieke_inskrywings', function ($table) {
            $table->char('busvervoer', 1 )->nullable();
        });

        Schema::table('inskrywings', function ($table) {
            $table->char('busvervoer', 1)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('publieke_inskrywings', function ($table) {
            $table->dropColumn('busvervoer');
        });

        Schema::table('inskrywings', function ($table) {
            $table->dropColumn('busvervoer');
        });
    }
}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VoortrekkerBetrokkenheid extends Model {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'voortrekker_betrokkenheid';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

    protected $guarded = ['id'];

    protected $hidden = ['created_at', 'updated_at'];

    public function rolle()
    {
    	return $this->belongsToMany('App\Rol', 'rolle_betrokkenheid', 'voortrekker_betrokkenheid_id', 'rolle_id');
    }
}

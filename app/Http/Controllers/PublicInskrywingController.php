<?php namespace App\Http\Controllers;

use App\Persoon;
use App\Kamp;
use App\Groep;
use App\Inskrywing;
use App\Kommando;
use App\PubliekeInskrywing;
use App\VoortrekkerBetrokkenheid;
use App\Opsie;
use Redirect;
use Session;
use Input;
use Validator;
use Mail;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App;
use Log;
use DB;


class PublicInskrywingController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| PublicInskrywing Controller
	|--------------------------------------------------------------------------
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */

	/**
	 *
	 * @return Response
	 */

	//Everything always directs here, and this function decides what step needs to happen next based on information stored in the Session.
	public function inskrywingControl()
	{
		//Step 1 - select Kamp
		if(!Session::has('publiek_inskrywing_kamp'))
		{
			return $this->getInskrywingHome();
		}
		else if(!Session::has('publiek_inskrywing_bevestig_volwassene')) //Step 2 - view Kamp Beskrywing
		{
			return view('publicinskrywing.inskrywing_kamp_beskrywing')->with(['kamp'=>Session::get('publiek_inskrywing_kamp')]);
		} else if(!Session::has('publiek_inskrywing_soort')) //Step 3 - get inskrywing soort
		{
			return view('publicinskrywing.inskrywing_soort')->with(['kamp'=>Session::get('publiek_inskrywing_kamp')]);
		} else if ((!Session::has('publiek_inskrywing_persoon_id')) && (!Session::has('publieke_inskrywing_id')))
		{

			if(Session::has('publiek_inskrywing_matches'))
			{
				return view('publicinskrywing.inskrywing_moontlike_persone')->with(['kamp'=>Session::get('publiek_inskrywing_kamp'), 'persone'=>Session::get('publiek_inskrywing_matches')]);
			}
			else
			{
				//We don't have a match in the DB, so we must create a person from scratch
				//Decide which form to return depenging on Session
				if(Session::get('publiek_inskrywing_soort') == 'jeuglid')
				{
					return view('publicinskrywing.inskrywing_edit_jeuglid')->with(['kamp'=>Session::get('publiek_inskrywing_kamp'), 'kommando' => Kommando::orderBy('kommando_naam')->get(), 'persoon'=>null]);
				}
				else
				{
					return view('publicinskrywing.inskrywing_edit_volwassene')->with(['kamp'=>Session::get('publiek_inskrywing_kamp'), 'kommando' => Kommando::orderBy('kommando_naam')->get(), 'persoon'=>null]);
				}

			}
		}
		else if(!Session::has('publieke_inskrywing_id'))
		{
			//We have identified a Person already in the DB - so now we show Prepopulated Jeug or Volwassene page
			if(Session::get('publiek_inskrywing_soort') == 'jeuglid')
			{
				return view('publicinskrywing.inskrywing_edit_jeuglid')->with(['kamp'=>Session::get('publiek_inskrywing_kamp'), 'kommando' => Kommando::orderBy('kommando_naam')->get(), 'persoon' => Persoon::where('id', Session::get('publiek_inskrywing_persoon_id'))->with('kommando')->get()]);
			}
			else
			{
				return view('publicinskrywing.inskrywing_edit_volwassene')->with(['kamp'=>Session::get('publiek_inskrywing_kamp'), 'kommando' => Kommando::orderBy('kommando_naam')->get(), 'persoon' => Persoon::where('id', Session::get('publiek_inskrywing_persoon_id'))->with('kommando')->get()]);
			}

		}
		else if(!Session::has('publiek_inskrywing_betrokkenheid_id')) //we have in session publieke_inskrywing_id - the person was created
		{
			$betrokkenhede = null;
			//Find either Volwassene or Jeuglid Betrokkenhede
			if(Session::get('publiek_inskrywing_soort') === 'jeuglid')
			{
				$betrokkenhede = VoortrekkerBetrokkenheid::where('jeuglid', true)->get();
			}
			else
			{
				$betrokkenhede = VoortrekkerBetrokkenheid::where('volwassene', true)->get();
			}

			return view('publicinskrywing.inskrywing_voortrekker_betrokkenheid')->with(['kamp'=>Session::get('publiek_inskrywing_kamp'), 'persoon' => PubliekeInskrywing::where('id', Session::get('publieke_inskrywing_id'))->get(), 'betrokkenhede' => $betrokkenhede]);
		}
		elseif (!Session::has('publiek_inskrywing_groep_id'))//We have already set the Betrokkenheid Id in the Session and save it to the DB - can now choose a group
		{
			$kamp = Session::get('publiek_inskrywing_kamp');
			$groepe = $kamp->groepe;

			$possible_groepe = array();
			$inskrywing_betrokkenheid_id = Session::get('publiek_inskrywing_betrokkenheid_id');

			foreach($groepe as $groep)
			{
				$groep_beperking = 0;
				$groep_reeds_ingeskryf = 0;
				$rolle = $groep->rolle;
				$i = 0;
				foreach($rolle as $rol)
				{
					$betrokkenhede = $rol->voortrekker_betrokkenhede;
					foreach($betrokkenhede as $betrokkenheid)
					{
						if($betrokkenheid->id == $inskrywing_betrokkenheid_id)
						{
							$possible_groepe[$groep->id]['groep_id'] = $groep->id;
							$possible_groepe[$groep->id]['groep_naam'] = $groep->groep_naam;
							$possible_groepe[$groep->id]['groep_beskrywing'] = $groep->beskrywing;
							$possible_groepe[$groep->id]['groep_koste'] = $rol->inskrywing_koste;

							//update groep_beperking
							$groep_beperking = $groep_beperking + $rol->inskrywing_beperking;
							$possible_groepe[$groep->id]['groep_beperking'] = $groep_beperking;


							//update reeds_ingeskryf

							//The $i is here because we only work on Group level for Public. We only need to count the group once; not once for every roll.
							if($i == 0){
								$count = PubliekeInskrywing::where('groep_id', $groep->id)
															->where('voortrekker_betrokkenheid_id', $inskrywing_betrokkenheid_id)
															->where('inskrywing_voltooi', true)
															->where('admin_verwerk', false)
															->count();
								$i++;
							} else {
								$count = 0;
							}

							$count_verwerk = DB::table('persone_rolle')->where('rol_id',$rol->id)->count();

							// The logic is: The available spots is (max) - (public waiting) - (accepted)
							$groep_reeds_ingeskryf = $groep_reeds_ingeskryf + $count + $count_verwerk;
							$possible_groepe[$groep->id]['reeds_ingeskryf'] = $groep_reeds_ingeskryf;
						}
					}
				}
			}

			//Now update the available slots
			$beskikbaar = array();
			foreach($possible_groepe as $groep)
			{
				$beskikbaar[$groep['groep_id']] = $groep['groep_beperking'] - $groep['reeds_ingeskryf'];
			}


			//This will be used later to check the costs of the selected group - we don't wanna look it up again and then have a mismatch that was shown to user
			Session::set('publiek_inskrywing_possible_groepe', $possible_groepe);
			Log::info($possible_groepe);

			return view('publicinskrywing.inskrywing_groepe')->with(['kamp'=>Session::get('publiek_inskrywing_kamp'), 'persoon' => PubliekeInskrywing::where('id', Session::get('publieke_inskrywing_id'))->get(), 'groepe' => $possible_groepe, 'beskikbaar'=>$beskikbaar]);
		} elseif(!Session::has('show_ekstras')) //Show ekstras
		{
			$kamp = Session::get('publiek_inskrywing_kamp');
			//Check if Kamp actually has ekstras
			if($kamp->ekstras->count() > 0)
			{
				//We have Ekstras - show
				return view('publicinskrywing.inskrywing_ekstras')->with(['kamp'=>Session::get('publiek_inskrywing_kamp'), 'persoon' => PubliekeInskrywing::where('id', Session::get('publieke_inskrywing_id'))->get()]);
			}
			else
			{
				//No Ekstras, skip.
				Session::set('show_ekstras', 'true');
				return Redirect::to('inskrywing');
			}
			//return view('publicinskrywing.inskrywing_ekstras')->with(['kamp'=>Session::get('publiek_inskrywing_kamp'), 'persoon' => PubliekeInskrywing::where('id', Session::get('publieke_inskrywing_id'))->get()]);
		}
		elseif (!Session::has('publiek_inskrywing_vrywaring')) //Vrywaring sign
		{
			return view('publicinskrywing.inskrywing_vrywaring')->with(['kamp'=>Session::get('publiek_inskrywing_kamp'), 'persoon' => PubliekeInskrywing::where('id', Session::get('publieke_inskrywing_id'))->get()]);
		}
		else //Vrywaring signed, now just confirm inskrywing
		{
			$persoon = PubliekeInskrywing::where('id', Session::get('publieke_inskrywing_id'))->firstOrFail();
			$opsies = $persoon->opsies;
			$kommando = '';
			if($persoon->kommando_id != null)
			{
				$kommando = Kommando::find($persoon->kommando_id)->kommando_naam;
			}
			$betrokkenheid = VoortrekkerBetrokkenheid::find($persoon->voortrekker_betrokkenheid_id);
			$groep = Groep::find($persoon->groep_id);
			return view('publicinskrywing.inskrywing_bevestig')->with(['kamp'=>Session::get('publiek_inskrywing_kamp'), 'persoon' => $persoon, 'kommando' => $kommando, 'betrokkenheid' => $betrokkenheid, 'groep' => $groep ]);
		}
	}

	//Cancel Inskrywing - clear all Session information!
	public function getKanseleerInskrywing()
	{
		if(Session::has('publieke_inskrywing_id'))
		{
			$publieke_inskrywing = PubliekeInskrywing::where('id', Session::get('publieke_inskrywing_id'))->firstOrFail();
			$publieke_inskrywing->delete();

		}
		Session::flush();
		return Redirect::to('inskrywing');
	}

	public function getInskrywingHome()
	{
		$kampe_inskrywings = Kamp::where('status', '4')->get();
		return view('frontend.home')->with(['kampe_inskrywings'=>$kampe_inskrywings]);
	}

	//Stap 1 - Result - Kies Kamp
	public function getInskrywingKamp($id)
	{
		$kamp = Kamp::find($id);
		if((!$kamp) || ($kamp->status != 'AanvaarInskrywings'))
		{
			return Redirect::back()->withErrors("Daar was 'n probleem met die Kamp wat jy gekies het!");
		}
		else
		{
			Session::set('publiek_inskrywing_kamp', $kamp);
			return Redirect::to('inskrywing');
		}
	}

	//Stap 2 - Result - Bevestig Volwassene
	public function getBevestigVolwassene()
	{
		Session::set('publiek_inskrywing_bevestig_volwassene', true);
		return Redirect::to('inskrywing');
	}

	public function postSoort()
	{


		$rules = array(
				'inskrywing_soort' => 'Required',
		        'geboortedatum' => 'Required|date'
		);

		$messages = [
			    'inskrywing_soort.required' => 'Kies asseblief Jeuglid of Volwassene!',
			    'geboortedatum.required' => 'Ons benodig jou Geboortedatum!',
			    'geboortedatum.date' => 'Jou Geboortedatum is nie geldig nie!',
		];

		$v = Validator::make(Input::all(), $rules, $messages);

		if( $v->passes() ) {

			Session::set('publiek_inskrywing_soort', Input::get('inskrywing_soort'));
			Session::set('publiek_inskrywing_geboortedatum', Input::get('geboortedatum'));
			if(Input::has('voortrekker_registrasie_nommer'))
			{
				Session::set('publiek_inskrywing_vrn', strtoupper(Input::get('voortrekker_registrasie_nommer')));
			}

			//Now try and match a person on the input
			if(Input::has('voortrekker_registrasie_nommer'))
			{
				$persone = Persoon::where(function ($query) {
							    $query->where('geboorte_datum', '=', Input::get('geboortedatum'))
							          ->Where('voortrekker_registrasie_nommer', '=', Input::get('voortrekker_registrasie_nommer'));
							})->with('kommando')->get();
			}
			else
			{
				Session::set('publiek_inskrywing_no_matches', true);
				return Redirect::to('inskrywing');
			}


			if (count($persone) == 0)
			{
				Session::set('publiek_inskrywing_no_matches', true);
				return Redirect::to('inskrywing');
			}
			else
			{
				Session::set('publiek_inskrywing_matches', $persone);
				return Redirect::to('inskrywing');
			}


		}
		else
		{
			return Redirect::to('inskrywing')->withInput()->withErrors($v);
		}

	}

	public function getKiesPersoon($id)
	{
		$persone = Session::get('publiek_inskrywing_matches');

		//Maak seker dat die persoon wat terug gestuur word 'n moontlike opsie was
		$check = false;
		foreach($persone as $persoon)
		{
			if($persoon->id == $id)
			{
				$check = true;
			}
		}

		if($check)
		{
			Session::set('publiek_inskrywing_persoon_id', $id);
			return Redirect::to('inskrywing');
		}
		else
		{
			return Redirect::to('inskrywing')->withInput()->withErrors("Mmm... Dit was nie 'n geldige opsie nie");
		}

		return $id;
	}

	//The form posts the Person's data. We will now create the Inskrywing and just update the rows of this inskrywing with future information.
	public function postPersoonData()
	{
		$rules = array(
                        'van' => 'Required',
                        'noemnaam' => 'Required',
                        'geboorte_datum' => 'Required|date',
                        'geslag' => 'Required|in:m,f',
                        'id_nommer' => 'digits:13',
                        'kontak_nommer_1' => 'numeric',
                        'kontak_nommer_2' => 'numeric',
                        'epos_adres' => 'email|Required',
                        'ouer_voog_id_nommer_1' => 'digits:13',
                        'ouer_voog_id_nommer_2' => 'digits:13',
                        'ouer_voog_kontak_nommer_1' => 'numeric',
                        'ouer_voog_kontak_nommer_2' => 'numeric',
                        'ouer_voog_epos_adres_1' => 'email',
                        'ouer_voog_epos_adres_2' => 'email',
                        'huisdokter_kontak_nommer_1' => 'numeric',
                        'huisdokter_kontak_nommer_2' => 'numeric',
                        'skoolgraad' => 'in:voorskool,r,1,2,3,4,5,6,7,8,9,10,11,12',

                );

			$messages = [
			    'van.required' => 'Ons benodig jou Van!',
			    'noemnaam.required' => 'Ons benodig jou Noemnaam!',
			    'geboorte_datum.required' => 'Ons benodig jou Geboortedatum!',
			    'geboorte_datum.date' => 'Jou Geboortedatum is nie geldig nie!',
			    'id_nommer.digits' => 'Jou ID Nommer is nie geldig nie!',
			    'geslag.required' => 'Ons benodig jou Geslag!',
			    'kontak_nommer_1.numeric' => 'Jou Kontak Nommer is nie geldig nie. Verwyder asseblief enige spasies uit die nommer!',
			    'kontak_nommer_2.numeric' => 'Jou Alternatiewe Kontak Nommer is nie geldig nie. Verwyder asseblief enige spasies uit die nommer!',
			    'epos_adres.email' => 'Jou Epos Adres is nie geldig nie!',
			 	'epos_adres.required' => 'Ons benodig jou Epos Adres!',
			    'ouer_voog_id_nommer_1.digits' => 'Ouer/Voog 1 se ID Nommer is nie geldig nie!',
			    'ouer_voog_id_nommer_2.digits' => 'Ouer/Voog 2 se ID Nommer is nie geldig nie!',
			    'ouer_voog_kontak_nommer_1.digits' => 'Ouer/Voog 1 se Kontak Nommer is nie geldig nie. Verwyder asseblief enige spasies uit die nommer!',
			    'ouer_voog_kontak_nommer_2.digits' => 'Ouer/Voog 2 se Kontak Nommer is nie geldig nie. Verwyder asseblief enige spasies uit die nommer!',
			    'ouer_voog_epos_adres_1.email' => 'Ouer/Voog 1 se Epos Adres is nie geldig nie!',
			    'ouer_voog_epos_adres_2.email' => 'Ouer/Voog 2 se Epos Adres is nie geldig nie!',
			    'huisdokter_kontak_nommer_1.numeric' => 'Jou Huisdokter se Kontak Nommer is nie geldig nie. Verywyder asseblief enige spasies uit die nommer!',
			    'huisdokter_kontak_nommer_2.numeric' => 'Jou Huisdokter se Alternatiewe Kontak Nommer is nie geldig nie. Verywyder asseblief enige spasies uit die nommer!',
			    'skoolgraad.in' => "Kies asseblief 'n geldige skoolgraad!",
			];


		$v = Validator::make(Input::all(), $rules, $messages);

        if( $v->passes() ) {


        	$publieke_inskrywing = new PubliekeInskrywing;

        	//Check if we have an existing record in the DB to which we want to link this Inskrywing
        	if(Session::has('publiek_inskrywing_persoon_id'))
        		{
        			$publieke_inskrywing->matched_persoon_id = Session::get('publiek_inskrywing_persoon_id');
        		}
        	//Saving some of the known Session info to the record
        	$publieke_inskrywing->kamp_id = Session::get('publiek_inskrywing_kamp')->id;
        	$publieke_inskrywing->inskrywing_soort = Session::get('publiek_inskrywing_soort');

        	if(Session::has('publiek_inskrywing_bevestig_volwassene'))
        	{
        		$publieke_inskrywing->bevestig_ouderdom = true;
        	}

        	//Now we can enter the posted details
        	$publieke_inskrywing->van = Input::get('van');
        	$publieke_inskrywing->voorname = Input::get('voorname');
        	$publieke_inskrywing->noemnaam = Input::get('noemnaam');
        	$publieke_inskrywing->geboorte_datum = Input::get('geboorte_datum');
        	$publieke_inskrywing->geslag = Input::get('geslag');
        	$publieke_inskrywing->id_nommer = Input::get('id_nommer');
        	$publieke_inskrywing->kontak_nommer_1 = Input::get('kontak_nommer_1');
        	$publieke_inskrywing->kontak_nommer_2 = Input::get('kontak_nommer_2');
        	$publieke_inskrywing->epos_adres = Input::get('epos_adres');
        	$publieke_inskrywing->woonadres = Input::get('woonadres');
        	$publieke_inskrywing->posadres = Input::get('posadres');
        	$publieke_inskrywing->voertuig_registrasie_nommer = Input::get('voertuig_registrasie_nommer');

        	if(Input::get('geregistreerde_voortrekker') === 'on')
                {
                  $publieke_inskrywing->voortrekker = true;
                }
            else $publieke_inskrywing->voortrekker = false;

            $publieke_inskrywing->voortrekker_registrasie_nommer = strtoupper(Input::get('voortrekker_registrasie_nommer'));
            $publieke_inskrywing->voortrekker_rang = Input::get('voortrekker_rang');


            if(Input::get('kommando') != null)
            {
               $kommando = Kommando::where('id', Input::get('kommando'))->firstOrFail();
               $publieke_inskrywing->kommando_id = $kommando->id;
            };

            if((Input::get('skoolgraad') === 'voorskool') || (Input::get('skoolgraad') === 'r'))
            {
            	$publieke_inskrywing->skoolgraad = null;
            }
            else
            {
            	$publieke_inskrywing->skoolgraad = Input::get('skoolgraad');
            };


            $publieke_inskrywing->ouer_voog_naam_van_1 = Input::get('ouer_voog_naam_van_1');
            $publieke_inskrywing->ouer_voog_id_nommer_1 = Input::get('ouer_voog_id_nommer_1');
            $publieke_inskrywing->ouer_voog_kontak_nommer_1 = Input::get('ouer_voog_kontak_nommer_1');
            $publieke_inskrywing->ouer_voog_epos_adres_1 = Input::get('ouer_voog_epos_adres_1');

            $publieke_inskrywing->ouer_voog_naam_van_2 = Input::get('ouer_voog_naam_van_2');
            $publieke_inskrywing->ouer_voog_id_nommer_2 = Input::get('ouer_voog_id_nommer_2');
            $publieke_inskrywing->ouer_voog_kontak_nommer_2 = Input::get('ouer_voog_kontak_nommer_2');
            $publieke_inskrywing->ouer_voog_epos_adres_2 = Input::get('ouer_voog_epos_adres_2');

            if(Input::get('mediese_fonds') === 'on')
            {
                $publieke_inskrywing->mediese_fonds = true;
            }
            else $publieke_inskrywing->mediese_fonds = false;

            $publieke_inskrywing->mediese_fonds_naam = Input::get('mediese_fonds_naam');
            $publieke_inskrywing->mediese_fonds_lid_nommer = Input::get('mediese_fonds_lid_nommer');
            $publieke_inskrywing->huis_dokter_naam_van = Input::get('huis_dokter_naam_van');
            $publieke_inskrywing->huis_dokter_kontak_nommer_1 = Input::get('huis_dokter_kontak_nommer_1');
            $publieke_inskrywing->huis_dokter_kontak_nommer_2 = Input::get('huis_dokter_kontak_nommer_2');
            $publieke_inskrywing->siekte_allergie = Input::get('siekte_allergie');

        	$publieke_inskrywing->save();


        	Session::set('publieke_inskrywing_id', $publieke_inskrywing->id);

        	return Redirect::to('inskrywing');

        }
        else
        {
        	//dd(redirect()->back()->withInput()->withErrors($v));
        	return redirect()->back()->withInput()->withErrors($v);
        }
	}

	public function getBetrokkenheid($id)
	{
		//Check if valid betrokkenheid
		$betrokkenheid = VoortrekkerBetrokkenheid::find($id);

		if($betrokkenheid === null)
		{
			return Redirect::to('inskrywing')->withErrors("Mmm... Dit is nie 'n geldige opsie nie");
		}

		if((Session::get('publiek_inskrywing_soort') === 'jeuglid') && (!$betrokkenheid->jeuglid == true))
		{
			return Redirect::to('inskrywing')->withErrors("Mmm... Dit is nie 'n geldige opsie nie");
		}

		if((Session::get('publiek_inskrywing_soort') === 'volwassene') && (!$betrokkenheid->volwassene == true))
		{
			return Redirect::to('inskrywing')->withErrors("Mmm... Dit is nie 'n geldige opsie nie");
		}

		//Good - looks like we have a valid Betrokkenheid returned. Save to Session and our Inskrywing
		Session::set('publiek_inskrywing_betrokkenheid_id', $id);
		$publieke_inskrywing = PubliekeInskrywing::find(Session::get('publieke_inskrywing_id'));
		$publieke_inskrywing->voortrekker_betrokkenheid_id = $id;
		$publieke_inskrywing->save();

		//Shap Shap Mieliepap. We can now return to Inskrywing to move on to the next step
		return Redirect::to('inskrywing');
	}

	public function getGroep($id)
	{

		//Let's check if it's a Valid Groep

		$groep = Groep::find($id);
		$kamp = Session::get('publiek_inskrywing_kamp');

		if(($groep === null) || ($groep->kamp_id != $kamp->id))
		{
			return Redirect::to('inskrywing')->withErrors("Mmm... Dit is nie 'n geldige opsie nie");
		}

		$check = false;
		foreach($groep->rolle as $rol)
		{
			$betrokkenhede = $rol->voortrekker_betrokkenhede;
			foreach($betrokkenhede as $betrokkenheid)
			{
				if($betrokkenheid->id == Session::get('publiek_inskrywing_betrokkenheid_id'))
				{
					$check = true;
				}
			}
		}

		//Die groep het nie 'n rol wat die inskrywing se betrokkenheid voor mag inskryf nie!
		if(!$check)
		{
			return Redirect::to('inskrywing')->withErrors("Mmm... Dit is nie 'n geldige opsie nie");
		}
		else
		{
			$possible_groepe = Session::get('publiek_inskrywing_possible_groepe');
			$koste = $possible_groepe[$id]['groep_koste'];
			$publieke_inskrywing = PubliekeInskrywing::find(Session::get('publieke_inskrywing_id'));
			$publieke_inskrywing->groep_id = $id;
			$publieke_inskrywing->koste = $koste;
			$publieke_inskrywing->save();

			Session::set('publiek_inskrywing_groep_id', $id);

			//Coolio Boolio - now return to Inskrywing route to handle next step
			return Redirect::to('inskrywing');
		}
	}

	public function postVrywaring()
	{
		$rules = array(
                        'geteken_deur' => 'Required',
                        'geteken_deur_id_nommer' => 'Required|digits:13',
                        'geteken_deur_email' => 'Required|email'

                );

		$messages = [
			    'geteken_deur.required' => 'Teken asseblief die Vrywaring deur jou Volle Naam en Van in te tik!',
			    'geteken_deur_id_nommer.required' => 'Vul asseblief jou ID Nommer in!',
			    'geteken_deur_id_nommer.digits' => 'Jou ID Nommer is nie geldig nie!',
			    'geteken_deur_email.required' => 'Vul asseblief jou Epos Adres in!',
			    'geteken_deur_email.email' => 'Jou Epos Adres is nie geldig nie!',

		];

		$v = Validator::make(Input::all(), $rules, $messages);

        if( $v->passes() ) {
        	$publieke_inskrywing = PubliekeInskrywing::find(Session::get('publieke_inskrywing_id'));
			$publieke_inskrywing->vrywaring_geteken_deur = Input::get('geteken_deur');
			$publieke_inskrywing->vrywaring_geteken_deur_id = Input::get('geteken_deur_id_nommer');
			$publieke_inskrywing->vrywaring_geteken_deur_epos = Input::get('geteken_deur_email');
			$publieke_inskrywing->teken_vrywaring = true;
			$publieke_inskrywing->save();

			Session::set('publiek_inskrywing_vrywaring', true);
			return Redirect::to('inskrywing');

        }
        else
        {
        	return redirect()->back()->withInput()->withErrors($v);
        }

	}

	//Hierdie is die laaste funksie wat geroep gaan word. Dit merk die inskrywing as voltooi, maak 'n verwysing en email die Inskrywing
	public function getBevestig()
	{
		if(!Session::has('publieke_inskrywing_id'))
		{
			return Redirect::to('inskrywing');
		}

		$publieke_inskrywing = PubliekeInskrywing::find(Session::get('publieke_inskrywing_id'));
		$publieke_inskrywing->inskrywing_voltooi = true;

		$verwysing = strtoupper($publieke_inskrywing->noemnaam).'-'.strtoupper(bin2hex(openssl_random_pseudo_bytes(2)));
		$publieke_inskrywing->verwysing = $verwysing;
		$groep_naam = Groep::find($publieke_inskrywing->groep_id)->groep_naam;
		$publieke_inskrywing->save();

		$kamp = Session::get('publiek_inskrywing_kamp');

		//Let's fire the emails!
		//Need to send to $persoon->epos_adres and $persoon->vrywaring_geteken_deur_epos, but not to both if it's the same Email Adres
		if($publieke_inskrywing->epos_adres === $publieke_inskrywing->vrywaring_geteken_deur_epos)
		{
			Mail::send('emails.inskrywing_confirm', ['persoon' => $publieke_inskrywing, 'kamp' =>$kamp], function ($m) use ($publieke_inskrywing, $kamp) {
            $m->from('hello@voortrekkers.co.za', 'Die Voortrekkers');
            $m->to($publieke_inskrywing->epos_adres, $publieke_inskrywing->noemnaam." ".$publieke_inskrywing->van)->subject($kamp->kamp_naam.': Inskrywing ontvang!');
        	});
		}
		else
		{
			//Aright - two different people, send to both!
			Mail::send('emails.inskrywing_confirm', ['persoon' => $publieke_inskrywing, 'kamp' =>$kamp], function ($m) use ($publieke_inskrywing, $kamp) {
            $m->from('hello@voortrekkers.co.za', 'Die Voortrekkers');
            $m->to($publieke_inskrywing->epos_adres, $publieke_inskrywing->noemnaam." ".$publieke_inskrywing->van)->subject($kamp->kamp_naam.': Inskrywing ontvang!');
        	});

        	Mail::send('emails.inskrywing_confirm', ['persoon' => $publieke_inskrywing, 'kamp' =>$kamp], function ($m) use ($publieke_inskrywing, $kamp) {
            $m->from('hello@voortrekkers.co.za', 'Die Voortrekkers');
            $m->to($publieke_inskrywing->vrywaring_geteken_deur_epos, $publieke_inskrywing->vrywaring_geteken_deur)->subject($kamp->kamp_naam.': Inskrywing ontvang!');
        	});
		}

		//Send some SMS!
		$sms = new \App\Libraries\MyMobileAPI();
		$sms->sendSms($publieke_inskrywing->kontak_nommer_1, "Jou inskrywing vir ".$kamp->kamp_naam." is ontvang met verwysing ".$publieke_inskrywing->verwysing. ". Instruksies vir betaling per e-pos gestuur");

		if($publieke_inskrywing->ouer_voog_kontak_nommer_1 != null)
		{
			$sms->sendSms($publieke_inskrywing->ouer_voog_kontak_nommer_1, $publieke_inskrywing->noemnaam." se inskrywing vir ".$kamp->kamp_naam." is ontvang met verwysing ".$publieke_inskrywing->verwysing. ". Instruksies vir betaling per e-pos gestuur");
		}

		Log::info($publieke_inskrywing->noemnaam." ".$publieke_inskrywing->van." het ingeskryf vir ".$groep_naam." by ".$kamp->kamp_naam);

		/* Just an IFTTT trigger ;)
		$client = new Client();
        $res = $client->request('POST', 'https://maker.ifttt.com/trigger/nuwe_inskrywing/with/key/bmJvZbR7nx9x5Jv65dJk4Kzzv9T2QAdzT7TBKOwPlGp', [
            'form_params' => [
                'value1' => $publieke_inskrywing->noemnaam." ".$publieke_inskrywing->van,
                'value2' => $groep_naam,
            ]
        ]);
        */
	    Session::flush();
		return view('publicinskrywing.inskrywing_voltooi')->with(['kamp'=> $kamp, 'persoon' => $publieke_inskrywing]);


	}

	public function postEkstras()
	{
		$ekstras = Input::get('ekstras');
		$arr = array();
		$publieke_inskrywing = PubliekeInskrywing::find(Session::get('publieke_inskrywing_id'));
		foreach($ekstras as $e)
		{
			if($e != 'null')
			{
				array_push($arr,$e);
				$opsie = Opsie::find($e);
				$publieke_inskrywing->opsies()->attach($opsie);
				$publieke_inskrywing->koste = $publieke_inskrywing->koste + $opsie->prys;
				$publieke_inskrywing->save();
			}
		}
		Session::set('show_ekstras', $arr);
		return Redirect::to('inskrywing');
	}

	public function kanseleerMatches()
	{
		Session::forget('publiek_inskrywing_matches');
		return Redirect::to('inskrywing');
	}

	public function terug()
	{
		//Means we are at the Bevestig page
		if(Session::has('publiek_inskrywing_vrywaring'))
		{
			$publieke_inskrywing = PubliekeInskrywing::find(Session::get('publieke_inskrywing_id'));
			$publieke_inskrywing->vrywaring_geteken_deur = null;
			$publieke_inskrywing->vrywaring_geteken_deur_id = null;
			$publieke_inskrywing->vrywaring_geteken_deur_epos = null;
			$publieke_inskrywing->teken_vrywaring = false;
			$publieke_inskrywing->save();

			Session::forget('publiek_inskrywing_vrywaring');
			return Redirect::to('inskrywing');
		}
		elseif(Session::has('show_ekstras')) //We're at the Vrywaring and need to reselect the Extras
		{
			$publieke_inskrywing = PubliekeInskrywing::find(Session::get('publieke_inskrywing_id'));
			foreach($publieke_inskrywing->opsies as $o)
			{
				$publieke_inskrywing->koste = $publieke_inskrywing->koste - $o->prys;
				$publieke_inskrywing->opsies()->detach($o);
				$publieke_inskrywing->save();
			}
			Session::forget('show_ekstras');
			return Redirect::to('inskrywing');
		}
		elseif(Session::has('publiek_inskrywing_groep_id')) //We're at Ekstras and need to reselect the Group
		{
			$publieke_inskrywing = PubliekeInskrywing::find(Session::get('publieke_inskrywing_id'));
			$publieke_inskrywing->groep_id = null;
			$publieke_inskrywing->koste = null;
			$publieke_inskrywing->save();


			Session::forget('publiek_inskrywing_groep_id');
			return Redirect::to('inskrywing');
		}elseif(Session::has('publiek_inskrywing_betrokkenheid_id')) //We're at the Select Groep step, need to rechoose Betrokkenheid
		{
			$publieke_inskrywing = PubliekeInskrywing::find(Session::get('publieke_inskrywing_id'));
			$publieke_inskrywing->voortrekker_betrokkenheid_id = null;
			$publieke_inskrywing->save();

			Session::forget('publiek_inskrywing_betrokkenheid_id');
			return Redirect::to('inskrywing');
		}elseif(Session::has('publieke_inskrywing_id')) //We're now at Betrokkenheid Step
		{
			$publieke_inskrywing = PubliekeInskrywing::find(Session::get('publieke_inskrywing_id'));
			$persoon[0] = $publieke_inskrywing;
			$publieke_inskrywing->delete();
			Session::forget('publieke_inskrywing_id');

			if(Session::get('publiek_inskrywing_soort') == 'jeuglid')
			{
				return view('publicinskrywing.inskrywing_edit_jeuglid')->with(['kamp'=>Session::get('publiek_inskrywing_kamp'), 'kommando' => Kommando::orderBy('kommando_naam')->get(), 'persoon' => $persoon]);
			}
			else
			{
				return view('publicinskrywing.inskrywing_edit_volwassene')->with(['kamp'=>Session::get('publiek_inskrywing_kamp'), 'kommando' => Kommando::orderBy('kommando_naam')->get(), 'persoon' => $persoon]);
			}
		}
		return Redirect::to('inskrywing');
	}

	//Shortcode URLs to Kamp
	public function setKamp($id)
	{
		$kamp = Kamp::where('shortcode',$id)->first();
		if($kamp == '')
		{
			return Redirect::to('/');
		}
		else {
			Session::flush();
			Session::set('publiek_inskrywing_kamp', $kamp);
			return Redirect::to('inskrywing');
		}

	}


}

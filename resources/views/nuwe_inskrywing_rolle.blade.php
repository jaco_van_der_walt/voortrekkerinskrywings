@extends('layouts.master')

@section('head')
@parent
<title>Nuwe Inskrywing</title>
@stop


@section('content')
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><a href={{URL('inskrywing/kanseleer')}}><button type="submit" class="btn btn-primary "><i class="fa fa-arrow-circle-left"></i></button></a> Nuwe Inskrywing: {{$kamp->kamp_naam}}</h1>
    </section>

    <!-- Main content -->
    <section class="content">
    @if($errors->has())
        @foreach ($errors->all() as $error)
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Waarskuwing!</b> {{$error}}
        </div>
        @endforeach
    @endif
    
    @if(Session::has('success'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Sukses!</b> {{Session::get('success')}}
    </div>
    @endif

    <!-- Instruksies -->
    <div class="row">
        <div class="col-md-12">
            <div class="callout callout-info">
                <h4>Stap 4: Rolle</h4>
                <p>Waar wil {{$persoon[0]->noemnaam}} {{$persoon[0]->van}} inskakel tydens {{$kamp->kamp_naam}}?</p>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="progress">
                <div class="progress-bar progress-bar-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                    <span class="sr-only">60% Voltooi</span>
                </div>
            </div>
        </div>  
    </div>
    <!-- Instruksies -->
    
    <!-- Main Row --> 
    <div class="row"> 
        <div class="col-md-12">

            @foreach($groepe as $groep)
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title"><b>{{$groep['groep_naam']}}</b></h3>
                        </div>

                        <div class="box-body">
                            <div class="row">
                                @foreach($groep['rolle'] as $rol)
                                <a href="{{ url('inskrywing/rol', $rol['rol_id']) }}">
                                    <div class="col-md-3">
                                        <!-- Primary tile -->
                                        <div class="box box-solid bg-light-blue">
                                            <div class="box-header">
                                                <h3 class="box-title">{{$rol['rol_naam']}}</h3>
                                            </div>
                                        </div><!-- /.box -->
                                    </div><!-- /.col -->
                                </a>
                                @endforeach
                                        
                            </div><!-- /.row -->
                        </div>
                    </div><!-- /.box --> 
                </div>
            </div>
            @endforeach
        

        <!-- footer --> 
		<div class="box-footer col-md-12">
			<div class="row">
				<div class="col-md-6">
					<a href={{URL('inskrywing/kanseleer')}} class="btn btn-danger"><i class="fa fa-times"></i> Kanseleer Inskrywing</a>
				</div>
			</div>
		</div>
			<!-- /footer --> 
	</div>
	<!-- End Main Row -->

	</section>
</aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>

        <!-- page script -->
@stop
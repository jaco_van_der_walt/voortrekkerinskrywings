@extends('layouts.master')

@section('head')
@parent
<title>Nuwe Inskrywing</title>
@stop


@section('content')
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><a href={{URL('inskrywing/kanseleer')}}><button type="submit" class="btn btn-primary "><i class="fa fa-arrow-circle-left"></i></button></a> Nuwe Inskrywing: {{$kamp->kamp_naam}}</h1>
    </section>

    <!-- Main content -->
    <section class="content">
    @if($errors->has())
        @foreach ($errors->all() as $error)
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Waarskuwing!</b> {{$error}}
        </div>
        @endforeach
    @endif
    
    @if(Session::has('success'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Sukses!</b> {{Session::get('success')}}
    </div>
    @endif

    <!-- Instruksies -->
    <div class="row">
        <div class="col-md-12">
            <div class="callout callout-info">
                <h4>Rol</h4>
                <p>Waar wil {{$persoon->noemnaam}} {{$persoon->van}} inskakel tydens {{$kamp->kamp_naam}}?<br>Die persoon se voorkeur word aangedui in groen maar jy kan die persone in enige rol en groep plaas.</p>
            </div>
        </div>
    </div>
    
    <!-- Instruksies -->
    
    <!-- Main Row --> 
    <div class="row"> 
        <div class="col-md-12">

            @foreach($groepe as $groep)
            <div class="row">
                <div class="col-md-12">
                    @if($groep['groep_id'] == $inskrywing->groep_id)
                    <div class="box box-inskrywing">
                    @else
                    <div class="box box-primary">
                    @endif
                    
                        <div class="box-header">
                            <h3 class="box-title"><b>{{$groep['groep_naam']}}</b></h3>
                        </div>

                        <div class="box-body">
                            <div class="row">
                                @foreach($groep['rolle'] as $rol)
                                <a href="{{ url('publieke/inskrywing/admin/kies/rol', $rol['rol_id']) }}">
                                    <div class="col-md-3">
                                        <!-- Primary tile -->
                                        <div class="box box-solid bg-light-blue">
                                            <div class="box-header">
                                                <h3 class="box-title">{{$rol['rol_naam']}}</h3>
                                            </div>
                                        </div><!-- /.box -->
                                    </div><!-- /.col -->
                                </a>
                                @endforeach
                                        
                            </div><!-- /.row -->
                        </div>
                    </div><!-- /.box --> 
                </div>
            </div>
            @endforeach
        

        <!-- footer --> 
		<div class="box-footer col-md-12">
			<div class="row">
				<div class="col-md-6">
					<a href={{URL('inskrywing/kanseleer')}} class="btn btn-danger"><i class="fa fa-times"></i> Kanseleer Inskrywing</a>
				</div>
			</div>
		</div>
			<!-- /footer --> 
	</div>
	<!-- End Main Row -->

	</section>
</aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>

        <!-- page script -->
@stop
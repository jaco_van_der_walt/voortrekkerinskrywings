<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class dropColumnStatusKamp extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::table('kampe', function ($table) {
	    $table->dropColumn('status');
		});


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('kampe', function ($table) {
		    $table->enum('status', ['Oop', 'Gesluit']);
		});
	}

}
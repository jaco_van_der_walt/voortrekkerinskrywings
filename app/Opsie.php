<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Opsie extends Model
{
    protected $table = 'opsies';

    public function ekstra()
    {
       return $this->belongsTo('App\Ekstra', 'ekstra_id');
    }

    public function publieke_inskrywings(){
      return $this->belongsToMany('App\Opsie', 'publieke_inskrywing_ekstras', 'opsie_id', 'publieke_inskrywing_id' );
    }
}

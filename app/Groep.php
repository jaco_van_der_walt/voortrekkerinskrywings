<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Groep extends Model {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'groepe';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

    protected $guarded = ['id'];

    protected $hidden = ['created_at', 'updated_at'];

    public function kamp()
    {
    	 return $this->belongsTo('App\Kamp', 'kamp_id');
    }

    public function rolle()
    {
    	return $this->hasMany('App\Rol');
    }

}

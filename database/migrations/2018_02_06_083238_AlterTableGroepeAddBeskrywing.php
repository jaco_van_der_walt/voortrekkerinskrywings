<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableGroepeAddBeskrywing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('groepe', function (Blueprint $table) {
          $table->string('beskrywing')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('groepe', function (Blueprint $table) {
          $table->dropColumn('beskrywing');
      });
    }
}

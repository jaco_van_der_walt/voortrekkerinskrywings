<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<style>
			table, td, th {
			    border: 0px solid black;
			    border-collapse: collapse;
			    width: 100%;
			    padding: 2px;
			    word-wrap:break-word;
			    text-align: left;
			}

			table.fixed { table-layout:fixed; }
			table.fixed td { overflow: hidden; }

			.heading {
				text-align: left;
			}

			body { 
				font-family: DejaVu Sans, sans-serif;
				font-size:6px; 
			}

			.page-break {
			    page-break-after: always;
			}

		</style>
	</head>
	<body>
		@foreach($inskrywings as $i)


		<table >
			<tr>
				<td> 
					<h1>{{$i->persoon->van}}, {{$i->persoon->noemnaam}}</h1>
				</td>
				<td style="text-align:right;">
					<h1>{{$i->persoon->voortrekker_registrasie_nommer}}</h1>
				</td>
				<td>
					{{ QrCode::format('png')->size(120)->encoding('UTF-8')->generate('https://inskrywings.voortrekkers.co.za/persone/beskou/'.$i->persoon->registrasie_nommer, 'QRCodes/code_'.$i->persoon->registrasie_nommer.'.png') }}
					<img src={{url('QRCodes/code_'.$i->persoon->registrasie_nommer.'.png')}}>
				</td>
				

			</tr>
		</table>
		<div>
			<table class="fixed">
				<col width="30px" />
    			<col width="30px" />
				<tr>
					<td colspan=2>
						<label><b>Persoonlike Inligting</b></label>
					</td>
				</tr>
	
				<tr>
					<td>
						<label>Van</label>
					</td>
					<td>
						<label>{{$i->persoon->van}}</label>
					</td>
				</tr>

				<tr>
					<td>
						<label>Voorname</label>
					</td>
					<td>
						<label>{{$i->persoon->voorname}}</label>
					</td>
				</tr>

				<tr>
					<td>
						<label>Noemnaam</label>
					</td>
					<td>
						<label>{{$i->persoon->noemnaam}}</label>
					</td>
				</tr>

				<tr>
					<td>
						<label>Geboortedatum</label>
					</td>
					<td>
						<label>{{$i->persoon->geboorte_datum}}</label>
					</td>
				</tr>
				
				<tr>
					<td>
						<label>Geslag</label>
					</td>
					<td>
						<label>{{$i->persoon->geslag}}</label>
					</td>
				</tr>

				<tr>
					<td>
						<label>Kontak Nommer</label>
					</td>
					<td>
						<label>{{$i->persoon->kontak_nommer_1}}</label>
					</td>
				</tr>

				<tr>
					<td>
						<label>Alternatiewe Kontak Nommer</label>
					</td>
					<td>
						<label>{{$i->persoon->kontak_nommer_2}}</label>
					</td>
				</tr>

				<tr>
					<td>
						<label>E-posadres</label>
					</td>
					<td>
						<label>{{$i->persoon->epos_adres}}</label>
					</td>
				</tr>

				<tr>
					<td>
						<label>Woonadres</label>
					</td>
					<td>
						<label>{{$i->persoon->woonadres}}</label>
					</td>
				</tr>

				<tr>
					<td>
						<label>Posadres</label>
					</td>
					<td>
						<label>{{$i->persoon->posadres}}</label>
					</td>
				</tr>

			</table>
		</div>
		<br>
		<div>
			<table class="fixed">
				<col width="30px" />
    			<col width="30px" />
				<tr>
					<td colspan=2>
						<label><b>Die Voortrekkers</b></label>
					</td>
				</tr>
	
				<tr>
					<td>
						<label>Voortrekker Registrasie Nommer</label>
					</td>
					<td>
						<label>{{$i->persoon->voortrekker_registrasie_nommer}}</label>
					</td>
				</tr>

				@if($i->persoon->kommando)
				<tr>
					<td>
						<label>Kommando</label>
					</td>
					<td>
						<label>{{$i->persoon->kommando->kommando_naam}}</label>
					</td>
				</tr>
				@else
				<tr>
					<td>
						<label>Kommando</label>
					</td>
					<td>
						<label></label>
					</td>
				</tr>
				@endif

			</table>
		</div>
		<br>
		<div>
			<table class="fixed">
				<col width="30px" />
    			<col width="30px" />
				<tr>
					<td>
						<label><b>Ouer/Voog #1</b></label>
					</td>
					<td>
						<label><b>Ouer/Voog #2</b></label>
					</td>
				</tr>
				<tr>
					<td>
						<label>{{$i->persoon->ouer_voog_naam_van_1}}</label>
					</td>
					<td>
						<label>{{$i->persoon->ouer_voog_naam_van_2}}</label>
					</td>
				</tr>
	
				<tr>
					<td>
						<label>{{$i->persoon->ouer_voog_id_nommer_1}}</label>
					</td>
					<td>
						<label>{{$i->persoon->ouer_voog_id_nommer_2}}</label>
					</td>
				</tr>

				<tr>
					<td>
						<label>{{$i->persoon->ouer_voog_kontak_nommer_1}}</label>
					</td>
					<td>
						<label>{{$i->persoon->ouer_voog_kontak_nommer_2}}</label>
					</td>
				</tr>

				<tr>
					<td>
						<label>{{$i->persoon->ouer_voog_epos_adres_1}}</label>
					</td>
					<td>
						<label>{{$i->persoon->ouer_voog_epos_adres_2}}</label>
					</td>
				</tr>
			</table>
		</div>
		<br>
		<div>
			<table class="fixed">
				<col width="30px" />
    			<col width="30px" />
					<td colspan=2>
						<label><b>Mediese Besonderhede</b></label>
					</td>
				</tr>
				<tr>
					<td>
						<label>Mediese Fonds</label>
					</td>
					<td>
						@if($i->persoon->mediese_fonds == '1')<label>Ja</label>@else<label>Nee</label>@endif
					</td>
				</tr>
				<tr>
					<td>
						<label>Mediese Fonds Naam</label>
					</td>
					<td>
						<label>{{$i->persoon->mediese_fonds_naam}}</label>
					</td>
				</tr>
				<tr>
					<td>
						<label>Mediese Fonds Lidmaatskap</label>
					</td>
					<td>
						<label>{{$i->persoon->mediese_fonds_lid_nommer}}</label>
					</td>
				</tr>
				<tr>
					<td>
						<label>Huisdokter Naam & Van</label>
					</td>
					<td>
						<label>{{$i->persoon->huis_dokter_naam_van}}</label>
					</td>
				</tr>
	
				<tr>
					<td>
						<label>Huisdokter Kontak Nommer</label>
					</td>
					<td>
						<label>{{$i->persoon->huis_dokter_kontak_nommer_1}}</label>
					</td>
				</tr>

				<tr>
					<td>
						<label>Siektetoestande of Allergieë</label>
					</td>
					<td>
						<label>{{$i->persoon->siekte_allergie}}</label>
					</td>
				</tr>

			</table>
		</div>
		<br>
		<div>
			<table class="fixed page-break">
				<col width="30px" />
    			<col width="30px" />
					<td colspan=2>
						<label><b>{{$kamp->kamp_naam}}</b></label>
					</td>
				</tr>
				<tr>
					<td>
						<label>Betrokkenheid</label>
					</td>
					<td>
						<label>{{$i->voortrekkerBetrokkenheid->betrokkenheid}}</label>
					</td>
				</tr>
				@if($i->skoolgraad != "")
				<tr>
					<td>
						<label>Skoolgraad</label>
					</td>
					<td>
						<label>{{$i->skoolgraad}}</label>
					</td>
				</tr>
				@endif
				<tr>
					<td>
						<label>Groep</label>
					</td>
					<td>
						@foreach($i->persoon->rolle as $rol)
							@if($rol->groep->kamp_id === $kamp->id)
								<label>{{$rol->groep->groep_naam}}: {{$rol->rol_naam}}</label>
							@endif
						@endforeach
						
					</td>
				</tr>
				<tr>
					<td>
						<label>Verwysing</label>
					</td>
					<td>
						<label>{{$i->verwysing}}</label>
					</td>
				</tr>
				<tr>
					<td>
						<label>Volledige Betaling ontvang</label>
					</td>
					<td>
						@if($i->betaal == 1)<label>Ja</label>@else<label>Nee</label>@endif
					</td>
				</tr>
				<tr>
					<td>
						<label>Nota</label>
					</td>
					<td>
						<label>{{$i->nota}}</label>
					</td>
				</tr>

			</table>
		</div>

		@endforeach

	</body>

</html>
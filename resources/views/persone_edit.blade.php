    @extends('layouts.master')

    @section('head')
    @parent
    <title>Persone</title>
    @stop


    @section('content')
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Wysig {{{$persoon[0]->noemnaam}}} {{{$persoon[0]->van}}} <small class="pull-right">{{$persoon[0]->registrasie_nommer}}</small>
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">

            @if($errors->has())
            @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Waarskuwing!</b> {{$error}}
            </div>
            @endforeach
            @endif


            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-md-12">
                    <!-- form start -->
                    <form role="form" action="{{URL('persone/edit')}}" method="POST">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title"><b>Persoonlike Inligting</b></h3>
                            </div><!-- /.box-header -->

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="registrasie_nommer" value="{{$persoon[0]->registrasie_nommer}}">

                            <div class="box-body">

                                <!-- Van -->
                                <div class="row form-group">
                                    <div class="col-xs-2">
                                        <label>Van <span>*</span></label>
                                    </div>
                                    <div class="col-xs-4">

                                        @if(Input::old('van'))
                                            <input type="text" class="form-control" name="van" value="{{{Input::old('van')}}}"> 
                                        @else
                                            <input type="text" class="form-control" name="van" value="{{{$persoon[0]->van}}}">
                                        @endif
                                    </div>   
                                </div>

                                <!-- Voorname -->
                                <div class="row form-group">
                                   <div class="col-xs-2">
                                    <label>Voorname</label>
                                </div>
                                <div class="col-xs-4">
                                    @if(Input::old('voorname'))
                                        <input type="text" class="form-control" name="voorname" value="{{{Input::old('voorname')}}}" > 
                                    @else
                                        <input type="text" class="form-control" name="voorname" value="{{{$persoon[0]->voorname}}}" > 
                                    @endif
                                </div>   
                            </div>

                            <!-- Noemnaam -->
                            <div class="row form-group">
                               <div class="col-xs-2">
                                <label>Noemnaam <span>*</span></label>
                            </div>
                            <div class="col-xs-4">
                                    @if(Input::old('noemnaam'))
                                        <input type="text" class="form-control" name="voorname" value="{{{Input::old('noemnaam')}}}" > 
                                    @else
                                        <input type="text" class="form-control" name="noemnaam" value="{{{$persoon[0]->noemnaam}}}">
                                    @endif
                            </div>   
                        </div>

                        <!-- Date dd/mm/yyyy -->
                        <div class="row form-group">
                            <div class="col-xs-2">
                                <label>Geboortedatum <span>*</span></label>
                            </div>
                            <div class="col-xs-4">
                                 @if(Input::old('geboorte_datum'))
                                    <input id="datemask" name="geboorte_datum" type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask value="{{{Input::old('geboorte_datum')}}}"/>
                                 @else
                                    <input id="datemask" name="geboorte_datum" type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask value="{{{$persoon[0]->geboorte_datum}}}"/>
                                 @endif
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <!-- Geslag -->
                        <!-- radio -->
                        <div class=" row form-group"> 

                            <div class="col-xs-2">
                                <label>Geslag <span>*</span></label>
                            </div>    
                            <div class="col-xs-1">
                                <label>
                                @if(Input::old('geslag'))
                                    @if(Input::old('geslag') === 'm')
                                        <input type="radio" name="geslag" id="optionsRadios1" value="m" checked>
                                    @else
                                        <input type="radio" name="geslag" id="optionsRadios1" value="m">
                                    @endif
                                @else
                                    @if($persoon[0]->geslag === 'm')
                                        <input type="radio" name="geslag" id="optionsRadios1" value="m" checked>
                                    @else
                                        <input type="radio" name="geslag" id="optionsRadios1" value="m">
                                    @endif
                                @endif
                                    Manlik
                                </label>
                            </div>
                            <div class="col-xs-1">
                                <label>
                                @if(Input::old('geslag'))
                                    @if(Input::old('geslag') === 'f')
                                        <input type="radio" name="geslag" id="optionsRadios2" value="f" checked>
                                    @else
                                        <input type="radio" name="geslag" id="optionsRadios2" value="f">
                                    @endif
                                @else
                                     @if($persoon[0]->geslag === 'f')
                                        <input type="radio" name="geslag" id="optionsRadios2" value="f" checked>
                                    @else
                                        <input type="radio" name="geslag" id="optionsRadios2" value="f">
                                    @endif
                                @endif
                                    Vroulik
                                </label>
                            </div>
                        </div>

                        <!-- ID Nommer -->
                        <div class="row form-group">
                           <div class="col-xs-2">
                            <label>ID Nommer</label>
                        </div>
                        <div class="col-xs-4">
                            @if(Input::old('id_nommer'))
                                <input type="text" class="form-control" name="id_nommer" value="{{{Input::old('id_nommer')}}}" >
                            @else
                                <input type="text" class="form-control" name="id_nommer" value="{{{$persoon[0]->id_nommer}}}" > 
                            @endif
                        </div>   
                    </div>

                    <!-- Kontak Nommer -->
                    <div class="row form-group">
                       <div class="col-xs-2">
                        <label>Kontak Nommer</label>
                    </div>
                    <div class="col-xs-4">
                        @if(Input::old('kontak_nommer_1'))
                            <input type="text" class="form-control" name="kontak_nommer_1" value="{{{Input::old('kontak_nommer_1')}}}"> 
                        @else
                            <input type="text" class="form-control" name="kontak_nommer_1" value="{{{$persoon[0]->kontak_nommer_1}}}">
                        @endif  
                    </div>

                    <div class="col-xs-1">
                        <label>Alternatiewe Kontak Nommer</label>
                    </div>
                    <div class="col-xs-4">
                        @if(Input::old('kontak_nommer_2'))
                            <input type="text" class="form-control" name="kontak_nommer_2" value="{{{Input::old('kontak_nommer_2')}}}"> 
                        @else
                            <input type="text" class="form-control" name="kontak_nommer_2" value="{{{$persoon[0]->kontak_nommer_2}}}"> 
                        @endif
                    </div>     
                </div>

                <!-- Epos Adres -->
                <div class="row form-group">
                   <div class="col-xs-2">
                    <label>Epos Adres</label>
                </div>
                <div class="col-xs-4">
                    @if(Input::old('epos_adres'))
                         <input type="text" class="form-control" name="epos_adres" value="{{{Input::old('epos_adres')}}}" > 
                    @else
                        <input type="text" class="form-control" name="epos_adres" value="{{{$persoon[0]->epos_adres}}}" > 
                    @endif
                    
                </div>   
            </div>


            <!-- Adres -->
            <div class="row form-group">
                <div class="col-xs-2">
                    <label>Woonadres</label>
                </div>

                <div class="col-xs-4">
                    @if(Input::old('woonadres'))
                        <textarea class="form-control" rows="4" name="woonadres">{{{Input::old('woonadres')}}}</textarea>
                    @else
                        <textarea class="form-control" rows="4" name="woonadres">{{{$persoon[0]->woonadres}}}</textarea>
                    @endif
                    
                </div>

                <div class="col-xs-1">
                    <label>Posadres</label>
                </div>
                <div class="col-xs-4">
                    @if(Input::old('posadres'))
                        <textarea class="form-control" rows="4" name="posadres">{{{Input::old('posadres')}}}</textarea>
                    @else
                        <textarea class="form-control" rows="4" name="posadres">{{{$persoon[0]->posadres}}}</textarea>
                    @endif
                </div>
            </div>      

            <!-- Voertuig Registrasie Nommer -->
            <div class="row form-group">
               <div class="col-xs-2">
                    <label>Voertuig Registrasie Nommer</label>
                </div>
                <div class="col-xs-4">
                    @if(Input::old('voertuig_registrasie_nommer'))
                        <input type="text" class="form-control" name="voertuig_registrasie_nommer" value="{{{Input::old('voertuig_registrasie_nommer')}}}" > 
                    @else
                        <input type="text" class="form-control" name="voertuig_registrasie_nommer" value="{{{$persoon[0]->voertuig_registrasie_nommer}}}" > 
                    @endif
                </div>
            </div>
        </div>

</div><!-- /.box --> 

<!-- Die Voortrekkers --> 
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title"><b>Die Voortrekkers</b></h3>
    </div><!-- /.box-header -->

    <div class="box-body"> 

                                        <div class="row form-group">
                                            <div class="col-xs-2">
                                                <label>Geregistreerde Voortrekker?</label>
                                            </div>

                                            <div class="col-xs-3">
                                                <label>
                                                @if(Input::old('geregistreerde_voortrekker'))
                                                    @if(Input::old('geregistreerde_voortrekker') === 'on')
                                                        <input type="checkbox" checked="true" name="geregistreerde_voortrekker" checked/>
                                                    @else
                                                        <input type="checkbox" checked="true" name="geregistreerde_voortrekker"/>
                                                    @endif
                                                @else
                                                    @if($persoon[0]->geregistreerde_voortrekker === 'on')
                                                        <input type="checkbox" checked="true" name="geregistreerde_voortrekker" checked/>
                                                    @else
                                                        <input type="checkbox" checked="true" name="geregistreerde_voortrekker"/>
                                                    @endif
                                                @endif
                                                    Ja
                                                </label>                                                
                                            </div>
                                        </div>



                                        <!-- Voortrekker Registrasie Nommer -->
                                        <div class="row form-group">
                                           <div class="col-xs-2">
                                            <label>Voortrekker Registrasie Nommer</label>
                                        </div>
                                        <div class="col-xs-4">
                                        @if(Input::old('voortrekker_registrasie_nommer'))
                                            <input type="text" class="form-control" name="voortrekker_registrasie_nommer" value="{{{Input::old('voortrekker_registrasie_nommer')}}}" >
                                        @else
                                            <input type="text" class="form-control" name="voortrekker_registrasie_nommer" value="{{{$persoon[0]->voortrekker_registrasie_nommer}}}" > 
                                        @endif
                                        </div>   
                                    </div>            


                                    <!-- Rang -->
                                    <div class="row form-group">
                                       <div class="col-xs-2">
                                        <label>Voortrekker Rang</label>
                                    </div>
                                    <div class="col-xs-4">
                                    @if(Input::old('voortrekker_rang'))
                                        <input type="text" class="form-control" name="voortrekker_rang" value="{{{Input::old('voortrekker_rang')}}}"> 
                                    @else
                                        <input type="text" class="form-control" name="voortrekker_rang" value="{{{$persoon[0]->voortrekker_rang}}}"> 
                                    @endif
                                    </div>              
                                </div>

                                <!-- Kommando-->
                                <div class="row form-group">
                                   <div class="col-xs-2">
                                    <label>Kommando</label>
                                </div>
                                <div class="col-xs-4">


                                 <select name="kommando" class="form-control">
                                    <option></option>
                                    @foreach($kommando as $k)
                                        @if(Input::old('kommando') == $k->id)
                                            <option value="{{{$k->id}}}" selected>{{{$k->kommando_naam}}}</option>
                                        @elseif($persoon[0]->kommando_id == $k->id)
                                            <option value="{{{$k->id}}}" selected>{{{$k->kommando_naam}}}</option>
                                        @else
                                            <option value="{{{$k->id}}}">{{{$k->kommando_naam}}}</option>
                                        @endif
                                    @endforeach
                                 </select>
                                </div>  

                            </div>
                        </div>
                    </div>

                    <!-- Ouers/Voog --> 
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title"><b>Ouers/Voog</b></h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <!-- Ouer/Voog #1 -->
        <h4><b>Ouer/Voog #1</b></h4>
            <div class="row form-group">
                <div class="col-xs-2">
                    <label>Ouer/Voog Naam & Van</label>
                </div>
                <div class="col-xs-4">
                    @if(Input::old('ouer_voog_naam_van_1'))
                        <input type="text" class="form-control" name="ouer_voog_naam_van_1" value="{{{Input::old('ouer_voog_naam_van_1')}}}"> 
                    @else
                        <input type="text" class="form-control" name="ouer_voog_naam_van_1" value="{{{$persoon[0]->ouer_voog_naam_van_1}}}"> 
                    @endif
                </div>       
            </div>

            <div class="row form-group">
                <div class="col-xs-2">
                    <label>Ouer/Voog ID Nommer</label>
                </div>
                <div class="col-xs-4">
                    @if(Input::old('ouer_voog_id_nommer_1'))
                         <input type="text" class="form-control" name="ouer_voog_id_nommer_1" value="{{{Input::old('ouer_voog_id_nommer_1')}}}"> 
                    @else
                        <input type="text" class="form-control" name="ouer_voog_id_nommer_1" value="{{{$persoon[0]->ouer_voog_id_nommer_1}}}">
                    @endif
                </div>       
                    </div>

                    <div class="row form-group">
                       <div class="col-xs-2">
                        <label>Ouer/Voog Kontak Nommer</label>
                    </div>
                    <div class="col-xs-4">
                        @if(Input::old('ouer_voog_kontak_nommer_1'))
                            <input type="text" class="form-control" name="ouer_voog_kontak_nommer_1" value="{{{Input::old('ouer_voog_kontak_nommer_1')}}}">
                        @else
                            <input type="text" class="form-control" name="ouer_voog_kontak_nommer_1" value="{{{$persoon[0]->ouer_voog_kontak_nommer_1}}}">
                        @endif
                    </div>

                    <div class="col-xs-2">
                        <label>Ouer/Voog Epos Adres</label>
                    </div>
                    <div class="col-xs-4">
                        @if(Input::old('ouer_voog_epos_adres_1'))
                            <input type="text" class="form-control" name="ouer_voog_epos_adres_1" value="{{{Input::old('ouer_voog_epos_adres_1')}}}"> 
                        @else
                            <input type="text" class="form-control" name="ouer_voog_epos_adres_1" value="{{{$persoon[0]->ouer_voog_epos_adres_1}}}"> 
                        @endif
                    </div>         
                </div>

                <!-- Ouer/Voog #2 -->
                <h4><b>Ouer/Voog #2</b></h4>
                <div class="row form-group">
                   <div class="col-xs-2">
                    <label>Ouer/Voog Naam & Van</label>
                </div>
                <div class="col-xs-4">
                    @if(Input::old('ouer_voog_naam_van_2'))
                        <input type="text" class="form-control" name="ouer_voog_naam_van_2" value="{{{Input::old('ouer_voog_naam_van_2')}}}">
                    @else
                        <input type="text" class="form-control" name="ouer_voog_naam_van_2" value="{{{$persoon[0]->ouer_voog_naam_van_2}}}"> 
                    @endif
                </div>       
            </div>

            <div class="row form-group">
               <div class="col-xs-2">
                <label>Ouer/Voog ID Nommer</label>
            </div>
            <div class="col-xs-4">
                @if(Input::old('ouer_voog_id_nommer_2'))
                    <input type="text" class="form-control" name="ouer_voog_id_nommer_2" value="{{{Input::old('ouer_voog_id_nommer_2')}}}"> 
                @else
                    <input type="text" class="form-control" name="ouer_voog_id_nommer_2" value="{{{$persoon[0]->ouer_voog_id_nommer_2}}}"> 
                @endif
            </div>       
        </div>

        <div class="row form-group">
           <div class="col-xs-2">
            <label>Ouer/Voog Kontak Nommer</label>
        </div>
        <div class="col-xs-4">
            @if(Input::old('ouer_voog_kontak_nommer_2'))
                <input type="text" class="form-control" name="ouer_voog_kontak_nommer_2" value="{{{Input::old('ouer_voog_kontak_nommer_2')}}}">
            @else
                <input type="text" class="form-control" name="ouer_voog_kontak_nommer_2" value="{{{$persoon[0]->ouer_voog_kontak_nommer_2}}}"> 
            @endif
        </div>

        <div class="col-xs-2">
            <label>Ouer/Voog Epos Adres</label>
        </div>
        <div class="col-xs-4">
            @if(Input::old('ouer_voog_epos_adres_2'))
                <input type="text" class="form-control" name="ouer_voog_epos_adres_2" value="{{{Input::old('ouer_voog_epos_adres_2')}}}"> 
            @else
                <input type="text" class="form-control" name="ouer_voog_epos_adres_2" value="{{{$persoon[0]->ouer_voog_epos_adres_2}}}"> 
            @endif
        </div>         
    </div>



</div>
</div>


<!-- Mediese Inligting --> 
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title"><b>Mediese Inligting</b></h3>
    </div><!-- /.box-header -->

    <div class="box-body"> 

        <!-- Mediese Fonds Check -->
        <div class="row form-group">
            <div class="col-xs-2">
                <label>Mediese Fonds?</label>
            </div>

            <div class="col-xs-3">
                <label>
                @if(Input::old('mediese_fonds'))
                    @if(Input::old('mediese_fonds') === 'on')
                        <input type="checkbox" checked="true" name="mediese_fonds" checked/>
                    @else
                        <input type="checkbox" checked="true" name="mediese_fonds"/>
                    @endif
                @else
                    @if($persoon[0]->mediese_fonds === 'on')
                        <input type="checkbox" checked="true" name="mediese_fonds" checked/>
                    @else
                        <input type="checkbox" checked="true" name="mediese_fonds"/>
                    @endif
                @endif
                    Ja
                </label>                                                
            </div>
        </div>

        <!-- Mediese Fonds Naam -->
        <div class="row form-group">
           <div class="col-xs-2">
            <label>Mediese Fonds Naam</label>
        </div>
        <div class="col-xs-4">
            @if(Input::old('mediese_fonds_naam'))
                <input type="text" class="form-control" name="mediese_fonds_naam" value="{{{Input::old('mediese_fonds_naam')}}}"> 
            @else
                <input type="text" class="form-control" name="mediese_fonds_naam" value="{{{$persoon[0]->mediese_fonds_naam}}}"> 
            @endif
        </div>              
    </div>

    <!-- Mediese Fonds Lidmaat Nommer -->
    <div class="row form-group">
       <div class="col-xs-2">
        <label>Mediese Fonds Lidmaat Nommer</label>
    </div>
    <div class="col-xs-4">
        @if(Input::old('mediese_fonds_lid_nommer'))
            <input type="text" class="form-control" name="mediese_fonds_lid_nommer" value="{{{Input::old('mediese_fonds_lid_nommer')}}}">
        @else
            <input type="text" class="form-control" name="mediese_fonds_lid_nommer" value="{{{$persoon[0]->mediese_fonds_lid_nommer}}}">
        @endif
    </div>              
</div>


<!-- Huisdokter -->
<h4><b>Huisdokter</b></h4>
<div class="row form-group">
   <div class="col-xs-2">
    <label>Huisdokter Naam & Van</label>
</div>
<div class="col-xs-4">
    @if(Input::old('huis_dokter_naam_van'))
        <input type="text" class="form-control" name="huis_dokter_naam_van" value="{{{Input::old('huis_dokter_naam_van')}}}"> 
    @else
        <input type="text" class="form-control" name="huis_dokter_naam_van" value="{{{$persoon[0]->huis_dokter_naam_van}}}"> 
    @endif
</div>       
</div>


<div class="row form-group">
   <div class="col-xs-2">
    <label>Huisdokter Kontak Nommer</label>
</div>
<div class="col-xs-4">
    @if(Input::old('huis_dokter_kontak_nommer_1'))
        <input type="text" class="form-control" name="huis_dokter_kontak_nommer_1" value="{{{Input::old('huis_dokter_kontak_nommer_1')}}}"> 
    @else
        <input type="text" class="form-control" name="huis_dokter_kontak_nommer_1" value="{{{$persoon[0]->huis_dokter_kontak_nommer_1}}}"> 
    @endif
</div>

<div class="col-xs-2">
    <label>Huisdokter Alternatiewe Kontak Nommer</label>
</div>
<div class="col-xs-4">
    @if(Input::old('huis_dokter_kontak_nommer_2'))
        <input type="text" class="form-control" name="huis_dokter_kontak_nommer_2" value="{{{Input::old('huis_dokter_kontak_nommer_2')}}}"> 
    @else
        <input type="text" class="form-control" name="huis_dokter_kontak_nommer_2" value="{{{$persoon[0]->huis_dokter_kontak_nommer_2}}}"> 
    @endif
</div>         
</div>

<!-- Mediese Toestand & Allergie -->
<div class="row form-group">
    <div class="col-xs-2">
        <label>Siektetoestande of Allergieë</label>
    </div>
    <div class="col-xs-4">
        @if(Input::old('siekte_allergie'))
            <textarea class="form-control" rows="4" name="siekte_allergie">{{{Input::old('siekte_allergie')}}}</textarea>
        @else
            <textarea class="form-control" rows="4" name="siekte_allergie">{{{$persoon[0]->siekte_allergie}}}</textarea>
        @endif
    </div>

</div>  



</div>
</div>

</div>

<div class="box-footer col-md-12">
    <div class="col-md-4">
        <a href={{URL('persone')}}><button class="btn btn-primary">Kanseleer</button></a>
    </div>
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <button type="submit" class="btn btn-warning pull-right"><i class="fa fa-edit"></i>  Opdateer</button>
        <button class="btn btn-danger pull-right" style="margin-right: 5px;" data-toggle="modal" data-target="#deleteModal"><i class="fa  fa-trash-o"></i>  Skrap</button>   
    </div>
</div>
</form>

<div id="deleteModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Skrap {{{$persoon[0]->noemnaam}}} {{{$persoon[0]->van}}}</h4>
      </div>
      <div class="modal-body">
        <p>Skrap {{{$persoon[0]->noemnaam}}} {{{$persoon[0]->van}}}?</p>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary pull-left" data-dismiss="modal">Kanseleer</button>
        <a href={{URL('persone/delete')}}><button class="btn btn-danger pull-right"><i class="fa  fa-trash-o"></i>  Skrap</button></a>
      </div>
    </div>

  </div>
</div>


</div><!-- /.row (main row) -->

</section><!-- /.content -->
</aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
<!-- AdminLTE App -->
<script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>
<!-- InputMask -->
<script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.date.extensions.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.extensions.js')}}" type="text/javascript"></script>


<script type="text/javascript">
    //Datemask
    $("#datemask").inputmask("yyyy/mm/dd", {"placeholder": "yyyy/mm/dd"});
</script>



@stop
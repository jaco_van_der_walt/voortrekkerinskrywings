<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInskrywingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inskrywings', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('kamp_id')->unsigned();
			$table->integer('persoon_id')->unsigned();
			$table->boolean('betaal');
			$table->integer('betaal_bedrag')->unsigned();
			$table->text('nota');

			$table->foreign('kamp_id')->references('id')->on('kampe');
			$table->foreign('persoon_id')->references('id')->on('persone');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('inskrywings');
	}

}
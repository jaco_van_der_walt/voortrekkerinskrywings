<?php namespace App\Http\Controllers;

use App\Persoon;
use App\Kamp;
use App\Inskrywing;
use Illuminate\Http\RedirectResponse;
use \Rap2hpoutre\LaravelLogViewer;
use PDF;
use Session;
use App\Organisasie;
use Auth;
use Log;


class AdminController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function logs()
    {
        return view('logs');
    }

	public function index()
	{
		$persoon_count = Persoon::all()->count();
		$oop_kampe_count = Kamp::where('status', '2')->count();
		$gesluit_kampe_count = Kamp::where('status', '3')->count();
		$inskrywings_count = Inskrywing::all()->count();
		$oop_kampe = array();

		$organisasies = Auth::user()->organisasies;

        foreach($organisasies as $o)
        {
        	array_push($oop_kampe, $o->kampe);
        }

		//Nie meer op 'n spesifieke paneel nie!
		if(Session::has('paneel_kamp'))
		{
			Session::forget('paneel_kamp');
		}
		return view('dashboard')->with(['persoon_count' => $persoon_count, 'oop_kampe_count' => $oop_kampe_count, 'gesluit_kampe_count'=> $gesluit_kampe_count, 'inskrywings_count'=> $inskrywings_count, 'oop_kampe'=>$oop_kampe,]);
	}

	public function pdf()
	{
		$pdf = PDF::loadView('pdfs.register')->setPaper("A4", "landscape");
		return $pdf->stream();
	}

}
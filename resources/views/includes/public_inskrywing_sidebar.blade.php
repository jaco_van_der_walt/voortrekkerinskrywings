                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="row">
                            <div class="col-md-12">
                                <h3>Inskrywing</h3>
                            </div>
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="active">
                            <a href="#">
                                <h4>Benodig hulp?</h4>
                                <div class="row">
                                    <div class="col-md-2">
                                        <i class="fa fa-user"></i>
                                        <i class="fa fa-envelope-o"></i>
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <div class="col-md-10">
                                        <b>{{$kamp->organisasies[0]->admin_naam}}</b><br>
                                        {{$kamp->organisasies[0]->admin_epos}}<br>
                                        {{$kamp->organisasies[0]->admin_tel}}<br>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href={{URL('publiek/inskrywing/kanseleer')}} class="text-red"><i class="fa fa-times"></i> Kanselleer Inskrywing</a>

                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->

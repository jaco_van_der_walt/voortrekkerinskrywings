@extends('layouts.public_inskrywing')

@section('head')
@parent
<title>Inskrywing</title>



@stop


@section('content')
<aside class="right-side stretch">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Inskrywing: {{$kamp->kamp_naam}}
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">

                    @if($errors->has())
                       @foreach ($errors->all() as $error)
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Waarskuwing!</b> {{$error}}
                            </div>
                      @endforeach
                    @endif

                    <div class="row">
                        <div class="col-md-12">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="callout callout-info">
                                    <h4>Persone gevind</h4>
                                    <p>Die sisteem het moontlik reeds inligting oor die Persoon wat jy wil inskryf vir {{$kamp->kamp_naam}}. Kies asseblief die regte persoon of indien geen van hierdie persone korrek is nie kliek 'skep nuwe persoon'.</p>
                                </div>
                            </div>
                        </div>

                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title"><b>Is een van hierdie persone die persoon wat jy wil inskryf?</b></h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        @foreach($persone as $persoon)
                                            @if($persoon->kommando)
                                                <a href="{{URL('publiek/inskrywing/kies',[$persoon->id])}}"><button class="btn btn-default btn-block">{{$persoon->noemnaam}} {{$persoon->van}} - {{$persoon->kommando->kommando_naam}}</button></a>
                                            @else
                                                <a href="{{URL('publiek/inskrywing/kies',[$persoon->id])}}"><button class="btn btn-default btn-block">{{$persoon->noemnaam}} {{$persoon->van}}</button></a>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- footer --> 
                        <div class="box-footer col-md-12">
                            <div class="row">
                                    <a href="{{URL('publiek/inskrywing/kanseleer')}}" class="btn btn-warning pull-left" style="margin-right: 5px;"><i class="fa fa-arrow-circle-left"></i> Terug </a>
                                    <a href="{{URL('publiek/inskrywing/matches/kanseleer')}}" class="btn btn-primary pull-right" style="margin-right: 5px;">Nee, ek wil 'n ander persoon inskryf </a>
                            </div>
                        </div>
                        <!-- /footer -->

                    </div> 




                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>


@stop
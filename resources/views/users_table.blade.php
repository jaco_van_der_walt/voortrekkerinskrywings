@extends('layouts.master')

@section('head')
@parent
	<title>Stelsel Gebruikers</title>
	<script src="{{URL('//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css')}}" type="text/css"></script>
@stop

@section('content')
	<aside class="right-side">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>Stelsel Gebruikers</h1>
		</section>
		<!-- Main content -->
		<section class="content">
			@if($errors->has())
				@foreach ($errors->all() as $error)
					<div class="alert alert-danger alert-dismissable">
						<i class="fa fa-ban"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<b>Waarskuwing!</b> {{$error}}
					</div>
				@endforeach
			@endif
			@if(Session::has('success'))
				<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<b>Sukses!</b> {{Session::get('success')}}
				</div>
			@endif
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Stelsel Gebruikers</h3> 
					<a href="{{URL('users/voegby')}}"><button class="btn btn-success pull-right" style="margin-right: 5px; margin-top: 5px;"><i class="fa fa-plus"></i>&nbsp Voeg by</button></a>                                    
				</div><!-- /.box-header -->
				<div class="box-body table-responsive">
					<table id="users_tabel" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Naam</th>
								<th>Epos</th>
								<th>Gebruiker Status</th>
								<th>Organisasies</th>
								<th>Aksies</th>
							</tr>
						</thead>
						<tbody>

						</tbody>
							<tfoot>
							<tr>
								<th>Naam</th>
								<th>Epos</th>
								<th>Gebruiker Status</th>
								<th>Organisasies</th>
								<th>Aksies</th>
							</tr>
						</tfoot>
					</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->


			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Organisasies</h3> 
					<a href="{{URL('users/voegby')}}"><button class="btn btn-success pull-right" style="margin-right: 5px; margin-top: 5px;"><i class="fa fa-plus"></i>&nbsp Voeg by</button></a>                                    
				</div><!-- /.box-header -->
				<div class="box-body table-responsive">
					<table id="organisasies_table" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Naam</th>
								<th>Aksies</th>
							</tr>
						</thead>
						<tbody>

						</tbody>
							<tfoot>
							<tr>
								<th>Naam</th>
								<th>Aksies</th>
							</tr>
						</tfoot>
					</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->



		</section><!-- /.content -->
	</aside><!-- /.right-side -->


	<!-- Skrap User Modal -->
    <div class="modal fade" id="skrap_user" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" id="skrap_user_header"></h4>
            </div>
            <div class="modal-body">
                    <div class="box-body table-responsive">
                        <p>Is jy seker jy wil die gebruiker skrap?</p>
                    </div><!-- /.box-body -->
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal"> Kanseleer</button>
              <a href={{URL('user/skrap')}}><button class="btn btn-danger pull-left"><i class="fa  fa-trash-o"></i>  Skrap</button></a>
            </div>
          </div>
        </div>
    </div>
    <!-- End Skrap User Modal -->

    <!-- Hernoem Groep Modal -->
    <div class="modal fade" id="herstel_wagwoord" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 id="herstel_wagwoord_header" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="box-body table-responsive">
                    {!! Form::open(array('url'=>'user/herstel','method'=>'POST')) !!}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row form-group">
                        <div class="col-xs-3">
                            <label>Wagwoord <span>*</span></label>
                        </div>
                        <div class="col-xs-9">
                            <input type="password" id="password" class="form-control" name="password" value="" required> 
                        </div> 
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-3">
                            <label>Hertik Wagwoord <span>*</span></label>
                        </div>
                        <div class="col-xs-9">
                            <input type="password" id="password_confirmation" class="form-control" name="password_confirmation" value="" required> 
                        </div>   
                    </div>
                </div><!-- /.box-body -->
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary pull-left" data-dismiss="modal"> Kanseleer</button>
              <a href={{URL('user/herstel')}}><button type="submit" class="btn bg-yellow "><i class="fa  fa-edit"></i>  Herstel Wagwoord</button></a>
              {!! Form::close() !!}
            </div>
          </div>
        </div>
    </div>
    <!-- Hernoem Groep Modal -->
    
@stop
@section('plugins')
@parent
	<!-- AdminLTE App -->
	<script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>
	<!-- DATA TABES SCRIPT -->
	<script src="{{URL('//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
	<script src="{{URL::asset('assets/js/plugins/datatables/dataTables.bootstrap.js')}}" type="text/javascript"></script>
	<!-- AdminLTE App -->
	<script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>
	<script type="text/javascript">
		$(function() {
			$('#users_tabel').DataTable( {
				"sAjaxSource": "{{{URL('ajax/users')}}}",
				"sServerMethod": "GET"
			} );
		});

		$(function() {
			$('#organisasies_table').DataTable( {
				"sAjaxSource": "{{{URL('ajax/organisasies')}}}",
				"sServerMethod": "GET"
			} );
		});

		function openSkrapUser(id)
        {
            $.ajax({
              method: "POST",
              url: "{{URL('ajax/user/skrap')}}",
              data: { user_id: id,
                      '_token': '{!! csrf_token() !!}'}
            })
              .done(function(result) {
                $('#skrap_user_header').html("Skrap "+result.user.name+"?");
                
                $('#skrap_user').modal('show');
              });
        }

        function openHerstelWagwoord(id)
        {
            $.ajax({
              method: "POST",
              url: "{{URL('ajax/user/herstel')}}",
              data: { user_id: id,
                      '_token': '{!! csrf_token() !!}'}
            })
	          .done(function( result ) {
	            $('#herstel_wagwoord_header').html("Herstel wagwoord vir " + result.user.name + "?");
	            $('#herstel_wagwoord').modal('show');
	          });
        }

        function openOrganisasies(id)
        {
            $.ajax({
              method: "POST",
              url: "{{URL('ajax/organisasies/user')}}",
              data: { user_id: id,
                      '_token': '{!! csrf_token() !!}'}
            })
	          .done(function( result ) {
	            $('#herstel_wagwoord_header').html("Herstel wagwoord vir " + result.user.name + "?");
	            
	            console.log(result);
	            $('#herstel_wagwoord').modal('show');
	          });
        }
	</script>
@stop
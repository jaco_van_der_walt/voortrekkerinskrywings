<?php namespace App\Http\Controllers;

use App\Persoon;
use App\Kamp;
use App\User;
use Hash;
use Auth;
use Redirect;
use App\Inskrywing;
use Illuminate\Http\RedirectResponse;
use \Rap2hpoutre\LaravelLogViewer;
use Input;
use Validator;
use Session;
use Log;


class SuperAdminController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function users_table()
	{
		if(Auth::user()->super_admin)
		{
			return view('users_table');
		}
		else 
		{
			return Redirect::to('admin')->withErrors("U het nie die nodige toeganregte tot die skakel nie!");
		}
	}

	public function user_voeg_by()
	{
		if(Auth::user()->super_admin)
		{
			return view('user_voeg_by');
		}
		else 
		{
			return Redirect::to('admin')->withErrors("U het nie die nodige toeganregte tot die skakel nie!");
		}
	}
	
	public function ajax_users()
    {
    	$super_user = Auth::user();
    	if($super_user->super_admin)
		{
	        $return['recordsTotal'] = User::count();

	        $flashdata = [];
	        $return['data'] = [];
	        $i = 0;
	        foreach (User::orderBy('name')->get() as $user) {
	            $return['data'][$i] = [$user->name];
	            $return['data'][$i][] = [$user->email];
	            $return['data'][$i][] = [($user->super_admin) ? "Stelsel Administrateur" : "Gebruiker"];
	            $toggle_button = "";
	            if ($super_user->id !== $user->id) {
		            if (!$user->super_admin) {
		            	$toggle_button = '<a href="user/toggle/'.$user->id.'" class="btn btn-xs bg-blue"><i class="fa fa-thumbs-up"></i> Opgradeer User</a>';
		            } else {
		            	$toggle_button = '<a href="user/toggle/'.$user->id.'" class="btn btn-xs bg-blue"><i class="fa fa-thumbs-down"></i> Degradeer User</a>';
		            }
	            }
	            $organisasies = '';
	            foreach($user->organisasies as $o)
	            {
	            	$organisasies = $organisasies.$o->organisasie_naam."<br>";
	            }
	            $return['data'][$i][] = [$organisasies];
	            $return['data'][$i][] =  '<button onclick="openOrganisasies('.$user->id.')" class="btn btn-xs bg-green"><i class="fa fa-building-o"></i> Organisasies </button> <button onclick="openHerstelWagwoord('.$i.')" class="btn btn-xs bg-yellow"><i class="fa fa-refresh"></i> Herstel Wagwoord</button> <button onclick="openSkrapUser('.$i.')" class="btn btn-xs bg-red"><i class="fa fa-trash"></i> Skrap User</button> '. $toggle_button;
	            $flashdata[$i] = $user->id;
	            $i++;
	        }

	        Session::set('user', $flashdata);

	        return response()->json($return);
        }
		else 
		{
			return Redirect::to('admin')->withErrors("U het nie die nodige toeganregte tot die skakel nie!");
		}
    }

	public function add_user()
	{
		if(Auth::user()->super_admin)
		{
			    $rules = array(
	                'name' => 'Required',
	                'email' => 'Required|email',
	                'password' => 'Required',
	                'password_confirmation' => 'Required'
                );
                //Check if Input passes the rules
                $v = Validator::make(Input::all(), $rules);
                if( $v->passes() ) {

                	if(Input::get('password') != Input::get('password_confirmation'))
                	{
                		return Redirect::back()->withErrors("Die wagwoord is nie korrek oorgetik nie!");
                	}
                	
                	$user = new User();
                	$user->name = Input::get('name');
                	$user->email = Input::get('email'); //NEED TO CHECK IF UNIQUE!!
                	$user->password = Hash::make(Input::get('password'));
                	$user->save();

                	return Redirect::to('superadmin')->with('success', $user->name." is geskep!");
                }
                else {
                    //Invalid input! Redirecting back...
                    return redirect()->back()->withInput()->withErrors($v);
                }
		}
		else 
		{
			return Redirect::to('admin')->withErrors("U het nie die nodige toeganregte tot die skakel nie!");
		}

	}

	public function user_herstel_wagwoord()
	{
		if (Auth::user()->super_admin) {
			$real_id = Session::get("user_herstel", false);
        	Session::forget("user_herstel");

	        if ($real_id === false) {
	            Log::warning("2105 || Kon nie 'n user in die flashdata vind nie! |", ['veroorsaak_deur' => Auth::user()->toArray(), "opsoek_na_skuil_id" => $id]);
	            return Redirect::to('superadmin')->withErrors("Kon nie die versoek voltooi nie");
	        }

	        $pass = Input::get("password");
	        $pass_conf = Input::get("password_confirmation");

	        if ($pass === $pass_conf) {
	        	$user = User::find($real_id);
				$user->password = Hash::make($pass);
	        	$user->save();
	        	return Redirect::to('superadmin')->with('success', $user->name." se wagwoord is geherstel!");
	        } else {
	        	return Redirect::to('superadmin')->withErrors("Kon nie die versoek voltooi nie, want die wagwoord en die hertik van die wagwoord het nie ooreen gestem nie.");
	        }
		}
		else 
		{
			return Redirect::to('admin')->withErrors("U het nie die nodige toeganregte tot die skakel nie!");
		}
	}

	public function user_skrap()
	{
		if (Auth::user()->super_admin) {
			$real_id = Session::get("user_delete", false);
        	Session::forget("user_delete");
	        if ($real_id === false) {
	            Log::warning("2105 || Kon nie 'n user in die flashdata vind nie! |", ['veroorsaak_deur' => Auth::user()->toArray(), "opsoek_na_skuil_id" => $id]);
	            return Redirect::to('superadmin')->withErrors("Kon nie die versoek voltooi nie");
	        }
			
	        $user = User::find($real_id);
        	$user->delete();
        	return Redirect::to('superadmin')->with('success', $user->name." is geskrap!");
		}
		else 
		{
			return Redirect::to('admin')->withErrors("U het nie die nodige toeganregte tot die skakel nie!");
		}
	}

	public function ajax_user_herstel()
	{
		if (Auth::user()->super_admin) {
			$fake_id = Input::get('user_id');
			$real_id = Session::get("user.$fake_id", false);

	        if ($real_id === false) {
	            Log::warning("2105 || Kon nie 'n user in die flashdata vind nie! |", ['veroorsaak_deur' => Auth::user()->toArray(), "opsoek_na_skuil_id" => $id]);
	            return Redirect::to('superadmin')->withErrors("Kon nie die versoek voltooi nie");
	        }
			
	        $return['user'] = User::find($real_id);
	        Session::set('user_herstel', $real_id);

        	return response()->json($return);
		}
		else 
		{
			return Redirect::to('admin')->withErrors("U het nie die nodige toeganregte tot die skakel nie!");
		}
	}

	public function ajax_user_skrap()
	{
		if (Auth::user()->super_admin) {
			$fake_id = Input::get('user_id');
			$real_id = Session::get("user.$fake_id", false);

	        if ($real_id === false) {
	            Log::warning("2105 || Kon nie 'n user in die flashdata vind nie! |", ['veroorsaak_deur' => Auth::user()->toArray(), "opsoek_na_skuil_id" => $id]);
	            return Redirect::to('superadmin')->withErrors("Kon nie die versoek voltooi nie");
	        }
			
	        $return['user'] = User::find($real_id);
	        Session::set('user_delete', $real_id);

        	return response()->json($return);
		}
		else 
		{
			return Redirect::to('admin')->withErrors("U het nie die nodige toeganregte tot die skakel nie!");
		}
	}

	public function user_toggle_status($id)
	{
		if (Auth::user()->super_admin) {
			$real_id = $id;

	        if ($real_id === false) {
	            Log::warning("2105 || Kon nie 'n user in die flashdata vind nie! |", ['veroorsaak_deur' => Auth::user()->toArray(), "opsoek_na_skuil_id" => $id]);
	            return Redirect::to('superadmin')->withErrors("Kon nie die versoek voltooi nie");
	        }
			
	        $user = User::find($real_id);
	        $user->super_admin = !$user->super_admin;
        	$user->save();
        	return Redirect::to('superadmin')->with('success', $user->name." is nou ".($user->super_admin)?'super admin':'admin'."!");
		}
		else 
		{
			return Redirect::to('admin')->withErrors("U het nie die nodige toeganregte tot die skakel nie!");
		}
	}
}
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBetrokkenheidColor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('voortrekker_betrokkenheid', function (Blueprint $table) {
            $table->string('kleur', 7)->default("#808080");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('voortrekker_betrokkenheid', function (Blueprint $table) {
            $table->dropColumn('kleur');
        });
    }
}

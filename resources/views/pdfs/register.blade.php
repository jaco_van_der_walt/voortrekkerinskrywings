<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<style>
			table, td, th {
			    border: 1px solid black;
			    width: 100%;
			    border-collapse: collapse;
			    padding: 4px;
			    word-wrap:break-word;
			}

			.heading {
				text-align: left;
			}

			body { 
				font-family: DejaVu Sans, sans-serif;
				font-size:11px; 
			}
		</style>
	</head>
	<body>
		<div>
		<h2>Register: {{$kamp->kamp_naam}}</h2>
			<table>
				<tr> 
					<td class="heading">
						<b>Voortrekker Nr.</b>
					</td>
					<td class="heading">
						<b>Van</b>
					</td>
					<td class="heading">
						<b>Naam</b>
					</td>
					<td class="heading">
						<b>Betrokkenheid</b>
					</td>
					<td class="heading">
						<b>Kommando</b>
					</td>
					<td class="heading">
						<b>Groep</b>
					</td>
					<td class="heading">
						<b>Siekte/Allergië</b>
					</td>
					<td class="heading" style="text-align: centre;">
						<b> ☑</b>
					</td>
				</tr>
				@foreach($inskrywings as $key => $value)
					<tr>
						<td >
							{{$inskrywings[$key]['persoon']->voortrekker_registrasie_nommer}}
						</td>
						<td>
							{{$inskrywings[$key]['persoon']->van}}
						</td>
						<td>
							{{$inskrywings[$key]['persoon']->noemnaam}}
						</td>
						<td>
						<?php echo ucfirst($inskrywings[$key]['betrokkenheid']) ?>
						</td>
						<td>
							@if($inskrywings[$key]['persoon']->kommando)
								{{$inskrywings[$key]['persoon']->kommando->kommando_naam}}
							@endif
						</td>
						<td>
							@if($inskrywings[$key]['groep'])
								{{$inskrywings[$key]['groep']}}
							@endif
						</td>
						<td>
							{{$inskrywings[$key]['persoon']->siekte_allergie}}
						</td>
						<td>
						</td>
					</tr>
				@endforeach
			</table>
			<h4>Totaal: {{$count}}</h4>
		</div>
	</body>
</html>
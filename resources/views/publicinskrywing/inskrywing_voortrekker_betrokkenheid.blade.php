@extends('layouts.public_inskrywing')

@section('head')
@parent
<title>Inskrywing</title>



@stop


@section('content')
<aside class="right-side stretch">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                    <a href={{URL('publiek/inskrywing/terug')}} class="btn-sm btn-primary pull-left" style="margin-left: 5px; margin-bottom: 5px;"><i class="fa fa-arrow-left"></i></a> &nbsp;
                        Inskrywing: {{$kamp->kamp_naam}} - {{$persoon[0]->noemnaam}} {{$persoon[0]->van}} 
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">

                    @if($errors->has())
                       @foreach ($errors->all() as $error)
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Waarskuwing!</b> {{$error}}
                            </div>
                      @endforeach
                    @endif

                    <div class="row">
                        <div class="col-md-12">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="callout callout-info">
                                    <h4>Voortrekker Betrokkenheid</h4>
                                    <p>Hoe is {{$persoon[0]->noemnaam}} {{$persoon[0]->van}} betrokke by Die Voortrekkers?</p>
                                </div>
                            </div>
                        </div>

                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        @foreach($betrokkenhede as $betrokkenheid)
                                                <a href="{{URL('publiek/inskrywing/betrokkenheid',[$betrokkenheid->id])}}"><button class="btn btn-default btn-block" style="margin-bottom: 5px;">{{$betrokkenheid->betrokkenheid}}</button></a>
                                        @endforeach
                                    </div>
                                </div>

                            </div>
                        </div>



                        <!-- footer --> 
                        <div class="box-footer col-md-12">
                            <div class="row">
                                <a href="{{URL('publiek/inskrywing/terug')}}" class="btn btn-warning pull-left" style="margin-right: 5px;"><i class="fa fa-arrow-circle-left"></i> Terug </a>
                            </div>
                        </div>
                        <!-- /footer -->

                    </div> 




                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>


@stop
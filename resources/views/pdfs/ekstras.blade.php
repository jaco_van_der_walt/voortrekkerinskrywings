<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<style>
			table, td, th {
			    border: 1px solid black;
			    width: 15cm;
			    border-collapse: collapse;
			    padding: 5px;
			    word-wrap:break-word;
			    text-align: center;
			}

			table {
				page-break-after: always;
			}

			.heading {
				text-align: left;
			}

			body {
				font-family: DejaVu Sans, sans-serif;
				font-size:12px;
			}
		</style>
	</head>
	<body>

	@foreach($kamp->ekstras as $e)
	<h1>{{$e->naam}}</h1>

	@foreach($e->opsies as $o)
	<h3>{{$o->naam}}</h3>
	<div>
		<table>
			@foreach($inskrywings as $i)
				@foreach($i->opsies as $io)
					@if($io->id == $o->id)
					<tr>
						<td>{{$i->persoon->noemnaam}}</td>
						<td>{{$i->persoon->van}}</td>
						<td>{{$i->persoon->geboorte_datum}}</td>
						<td>{{$i->persoon->geboorte_datum}}</td>
						<td>{{$io->naam}}</td>
					</tr>
					@endif
				@endforeach
			@endforeach
		</table>
	</div>
	@endforeach

	@endforeach


	</body>
</html>

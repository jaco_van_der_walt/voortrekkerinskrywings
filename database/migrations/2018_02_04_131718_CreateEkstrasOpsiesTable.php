<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEkstrasOpsiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ekstras', function(Blueprint $table)
      {
          $table->increments('id');
          $table->integer('kamp_id')->unsigned();
          $table->string("naam");
          $table->text("beskrywing");

          $table->foreign('kamp_id')->references('id')->on('kampe');
      });

      Schema::create('opsies', function(Blueprint $table)
      {
          $table->increments('id');
          $table->integer('ekstra_id')->unsigned();
          $table->string("naam");
          $table->text("beskrywing");
          $table->integer("prys");

          $table->foreign('ekstra_id')->references('id')->on('ekstras');
      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('opsies');
        Schema::drop('ekstras');
    }
}

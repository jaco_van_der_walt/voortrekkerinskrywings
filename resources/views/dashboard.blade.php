@extends('layouts.master')

@section('head')
@parent
<title>Paneelbord</title>
@stop


@section('content')
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Paneelbord
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">

                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>
                                        {{$persoon_count}}
                                    </h3>
                                    <p>
                                        Persone in databasis
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-users"></i>
                                </div>
                                <a href="{{URL('persone/tabel')}}" class="small-box-footer">
                                    Meer Inligting <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>
                                        {{$oop_kampe_count}}
                                    </h3>
                                    <p>
                                        @if($oop_kampe_count == 1)
                                        Oop Kamp
                                        @else
                                        Oop Kampe
                                        @endif
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-unlock"></i>
                                </div>
                                <a href="kamp/tabel" class="small-box-footer">
                                    Meer inligting <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3>
                                        {{$gesluit_kampe_count}}
                                    </h3>
                                    <p>
                                            Geslote Kampe
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-lock"></i>
                                </div>
                                <a href="kamp/tabel" class="small-box-footer">
                                    Meer inligting <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>
                                        {{$inskrywings_count}}
                                    </h3>
                                    <p>
                                        Inskrywings gestoor
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-file-text-o"></i>
                                </div>
                                <a href="#" class="small-box-footer">
                                    Meer inligting <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                    </div><!-- /.row -->

                    @foreach($oop_kampe as $org)
                        @foreach($org as $kamp)
                            @if($kamp->status == "Oop")
                                <!-- top row -->
                                <div class="row">
                                        
                                    <div class="col-lg-12">
                                        <!-- small box -->
                                        <div class="small-box bg-green">
                                            <div class="inner">
                                                <h3>
                                                    {{$kamp->kamp_naam}}
                                                </h3>
                                                <p>
                                                    {{$kamp->begin}}
                                                </p>
                                            </div>
                                            <div class="icon">
                                                <i class="fa fa-tree"></i>
                                            </div>
                                            <a href={{URL('kamp/paneel/'.$kamp->id)}} class="small-box-footer">
                                                Meer Inligting <i class="fa fa-arrow-circle-right"></i>
                                            </a>
                                        </div>
                                    </div><!-- ./col -->
                                </div>
                                <!-- /.row -->
                            @elseif($kamp->status == "AanvaarInskrywings")
                                <div class="row">
                            
                                    <div class="col-lg-12">
                                        <!-- small box -->
                                        <div class="small-box bg-yellow">
                                            <div class="inner">
                                                <h3>
                                                    {{$kamp->kamp_naam}}
                                                </h3>
                                                <p>
                                                    Aanvaar Inskrywings
                                                </p>
                                            </div>
                                            <div class="icon">
                                                <i class="fa fa-tree"></i>
                                            </div>
                                            <a href={{URL('kamp/paneel/'.$kamp->id)}} class="small-box-footer">
                                                Meer Inligting <i class="fa fa-arrow-circle-right"></i>
                                            </a>
                                        </div>
                                    </div><!-- ./col -->
                                </div>
                                <!-- /.row -->
                            @endif
                        @endforeach
                    @endforeach

                    <!-- Main row -->
                    <div class="row">
                        <!-- Left col -->
                        <section class="col-lg-12"> 


                            
                        </section><!-- /.Left col -->
                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>


@stop
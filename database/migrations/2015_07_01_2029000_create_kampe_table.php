<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKampeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kampe', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('kamp_naam');
			$table->enum('status', ['Oop', 'Gesluit']);
			$table->date('begin');
			$table->date('eindig');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('kampe');
	}

}
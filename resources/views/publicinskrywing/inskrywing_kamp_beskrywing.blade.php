@extends('layouts.public_inskrywing')

@section('head')
@parent
<title>Inskrywing</title>
@stop


@section('content')
<aside class="right-side stretch">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Inskrywing: {{$kamp->kamp_naam}}
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">

                    @if($errors->has())
                       @foreach ($errors->all() as $error)
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Waarskuwing!</b> {{$error}}
                            </div>
                      @endforeach
                    @endif

                    <div class="row">
                        <div class="col-md-12">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="callout callout-warning">
                                    <h4>Kennisgewing</h4>
                                    <p>Hierdie inskrywingsproses mag slegs voltooi word deur 'n ouer of wettige voog van 'n jeuglid <i>of</i> deur 'n volwassene wat self {{$kamp->kamp_naam}} wil bywoon.<br>
                                    <b>Geen persoon onder die ouderdom van 18 jaar mag hulself inskryf vir {{$kamp->kamp_naam}} nie!</b></p>
                                </div>
                            </div>
                        </div>

                        <!-- Betrokkenhede -->
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        {!! $kamp->kamp_beskrywing !!}
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div id="bevestigModal" class="modal fade" role="dialog">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Bevestiging</h4>
                              </div>
                              <div class="modal-body">
                                <p>Ek bevestig dat ek 'n ouer of wettige voog is van die jeuglid wat ek wil inskryf vir {{$kamp->kamp_naam}}
                                <i>of</i> ek is 'n Volwassene wat self vir {{$kamp->kamp_naam}} wil inskryf.
                                </p>
                              </div>
                              <div class="modal-footer">
                                <button class="btn btn-primary pull-left" data-dismiss="modal">Kanselleer</button>
                                <a href={{URL('publiek/inskrywing/bevestigvolwassene')}}><button class="btn btn-success pull-right"><i class="fa  fa-check"></i>  Bevestig</button></a>
                              </div>
                            </div>
                          </div>
                        </div>

                        <!-- footer --> 
                        <div class="box-footer col-md-12">
                            <div class="row">
                                    <a href="{{URL('publiek/inskrywing/kanseleer')}}" class="btn btn-warning pull-left" style="margin-right: 5px;"><i class="fa fa-arrow-circle-left"></i> Terug </a> 
                                    <button class="btn btn-success pull-right" style="margin-right: 5px;" data-toggle="modal" data-target="#bevestigModal"> Skryf In <i class="fa fa-arrow-circle-right"></i></button> 
                            </div>
                        </div>
                        <!-- /footer -->
                        </div>
                    </div> 


                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>


@stop
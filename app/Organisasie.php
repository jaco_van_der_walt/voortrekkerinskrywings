<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Organisasie extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'organisasies';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

    protected $guarded = ['id'];

    public function kampe()
    {
    	return $this->belongsToMany('App\Kamp', 'kampe_organisasies', 'organisasie_id', 'kamp_id');
    }
}

<?php namespace App\Http\Controllers;

use Input;
use Validator;
use Redirect;
use Session;
use Response;
use Log;
use Auth;
use View;
use App\Kamp;
use App\Groep;
use App\Rol;
use App\Persoon;
use App\Inskrywing;
use App\Vorm;
use App\VoortrekkerBetrokkenheid;
use App\PubliekeInskrywing;

class KampController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function view_kamp_table()
	{
		//Ensure that the Session forgets about the "edited kommando"
        if (Session::has('paneel_kamp'))
        {
            Session::forget('paneel_kamp');
        }

		return view('kampe_table');
	}

    public function view_voeg_kamp_by()
    {
        return view('kampe_voeg_by');
    }

    public function view_wysig_beskrywing()
    {
        $kamp = Kamp::find(Session::get('paneel_kamp'));
        return view('kampe_wysig_beskrywing')->with(['kamp'=>$kamp]);
    }

    public function wysig_beskrywing()
    {
        $kamp = Kamp::find(Session::get('paneel_kamp'));
        $kamp->kamp_beskrywing = Input::get("beskrywing_inhoud");
        $kamp->save();
        return Redirect::to('kampbeskrywing')->with('success', $kamp->kamp_naam." se beskrywing is opgedateer");
    }

    public function view_wysig_vrywaring()
    {
        $kamp = Kamp::find(Session::get('paneel_kamp'));
        return view('kampe_wysig_vrywaring')->with(['kamp'=>$kamp]);
    }

    public function wysig_vrywaring_volwasse()
    {
        $kamp = Kamp::find(Session::get('paneel_kamp'));
        $kamp->kamp_vrywaring_volwasse = Input::get("vrywaring_inhoud_volwasse");
        $kamp->save();
        return Redirect::to('kampvrywaring')->with('success', $kamp->kamp_naam." se vrywaring vir volwassenes is opgedateer");
    }

    public function wysig_vrywaring_jeug()
    {
        $kamp = Kamp::find(Session::get('paneel_kamp'));
        $kamp->kamp_vrywaring_jeug = Input::get("vrywaring_inhoud_jeug");
        $kamp->save();
        return Redirect::to('kampvrywaring')->with('success', $kamp->kamp_naam." se vrywaring vir jeuglede is opgedateer");
    }

    public function submit_voeg_by()
    {
    	//validation rules
    	$rules = array(
                'kamp_naam' => 'Required',
                'begin' => 'Required|date',
                'eindig' => 'Required|date'
            );
    	$v = Validator::make(Input::all(), $rules);

    	if( $v->passes() ) {
            
            $kamp = new Kamp;

            $kamp->kamp_naam = Input::get('kamp_naam');
            $kamp->begin = Input::get('begin');
            $kamp->eindig = Input::get('eindig');

            $kamp->save();

            $groep_name = Input::get('groep_name');
            $groep_rolle = Input::get('rolle_name');

            if($groep_name != "")
            {
                $i = 0;
                foreach($groep_name as $g)
                {
                    $groep = new Groep;
                    $groep->groep_naam = $g;
                    $groep->kamp_id = $kamp->id;
                    $groep->save();

                    //$rolle = explode(',', $groep_rolle[$i],0);
                    $rolle = preg_split('/([,])+/', $groep_rolle[$i], -1, PREG_SPLIT_NO_EMPTY);
                    foreach($rolle as $r)
                    {
                        $rol = new Rol;
                        $rol->rol_naam = $r;
                        $rol->groep_id = $groep->id;
                        $rol->save();
                        
                    }
                    $i++;

                }
            }
            return Redirect::to('kamp/tabel')->with('success', $kamp->kamp_naam." is bygevoeg");

    	}
    		else {
                //Invalid input! Redirecting back...
                return redirect()->back()->withInput()->withErrors($v);
        }

    }

    public function ajax_kampe()
    {
        
        $return['recordsTotal'] = Kamp::count();

        $return['data'] = [];
        $i = 0;
        $organisasies = Auth::user()->organisasies;

        foreach($organisasies as $o)
        {

            foreach($o->kampe as $kamp)
            {
                $return['data'][$i][] = '<a href="' . url("kamp/paneel/$kamp->id") . '" style="cursor: pointer;">'.$kamp->kamp_naam.'</a>';
                if($kamp->status === 'Oop')
                {
                    $return['data'][$i][] =  '<button class="btn btn-xs bg-olive disabled"><i class="fa fa-unlock"></i></button>';
                }
                else if ($kamp->status === 'AanvaarInskrywings')
                {
                     $return['data'][$i][] =  '<button class="btn btn-xs bg-yellow disabled"><i class="fa fa-pencil-square-o"></i></button>';
                }
                else 
                {
                    $return['data'][$i][] =  '<button class="btn btn-xs bg-red-pink disabled"><i class="fa fa-lock"></i></button>';
                }
                $return['data'][$i][] = $o->organisasie_naam;
                $return['data'][$i][] = $kamp->begin;
                $return['data'][$i][] = $kamp->eindig;
                
                $return['data'][$i][] =  $kamp->groepe->count();

                $return['data'][$i][] = Inskrywing::where('kamp_id', $kamp->id)->count();
                
                $i++; 
            }
        }

        return response()->json($return);
    }

    public function view_kamp_paneel($id)
    {
        $real_id = $id;

        if ($real_id === false) {
            Log::warning("2605 || Kon nie 'n kamp in die flashdata vind nie! |", ['veroorsaak_deur' => Auth::user()->toArray(), "opsoek_na_skuil_id" => $id]);
            return Redirect::to('kamp/tabel')->withErrors("Kon nie die versoek voltooi nie");
        }

        Session::set('paneel_kamp', $real_id);

        $kamp = Kamp::where('id', $real_id)->first();

        $inskrywings_count = Inskrywing::where('kamp_id', $kamp->id)->count();
        $inskrywings = Inskrywing::where('kamp_id', $kamp->id)->get();

        $betrokke = VoortrekkerBetrokkenheid::all();

        $betrokkenheid_teller = array();
        foreach ($betrokke as $betrokkenheid) {
            $betrokkenheid_teller[$betrokkenheid->id] = 0;
        }

        $groepe = Groep::where('kamp_id', $kamp->id)->orderBy('groep_naam', 'ASC')->get();

        // ** Create an array, with each Groep, and under each groep each betrokkenheid, and a count of how many of that betrokkenheid in that groep
        $groep_betrokkenheid = array();
        foreach($groepe as $g)
        {
            //Add Groep name to array
            $groep_betrokkenheid[$g->groep_naam]= array();
            //For each groep, add all the betrokkenheid
            foreach ($betrokke as $betrokkenheid) {
                $groep_betrokkenheid[$g->groep_naam][$betrokkenheid->betrokkenheid] = 0;
            }
        }

        //Looping through each inskrywing
        foreach($inskrywings as $inskrywing)
        {
            $betrokkenheid_teller[$inskrywing->voortrekkerBetrokkenheid->id]++;

            //Each person can have many rolle
            foreach($inskrywing->persoon->rolle as $rol)
            {   
                //This roll is part of this kamp
                if($rol->groep->kamp_id === $kamp->id){
                    //Because we have initialized the array correctly, the indexes will exist
                    $groep_betrokkenheid[$rol->groep->groep_naam][$inskrywing->voortrekkerBetrokkenheid->betrokkenheid]++;
                }
            }
        }

        $publiekeInskrywingsTeller = PubliekeInskrywing::where('kamp_id', $kamp->id)->where('admin_verwerk', false)->where('inskrywing_voltooi',true)->count();

        return View::make('kamp_paneel')->with(['kamp' => $kamp, 'groepe' => $groepe, 'inskrywings' => $inskrywings_count, 'betrokkenhede' => $betrokke, 'betrokkenheid_stats' => $betrokkenheid_teller, 'publiekeInskrywingsTeller' => $publiekeInskrywingsTeller, 'groep_betrokkenheid' => $groep_betrokkenheid]);
    }

    public function ajax_kamp_persone()
    {
        $return['recordsTotal'] = Persoon::count();

        $flashdata = [];
        $return['data'] = [];
        $i = 0;
        $kamp_id = Session::get('paneel_kamp');

        foreach(Inskrywing::where('kamp_id', Session::Get('paneel_kamp'))->get() as $inskrywing)
        {
            $return['data'][$i] = [$inskrywing->persoon->registrasie_nommer];
            $return['data'][$i][] = [$inskrywing->persoon->noemnaam];
            $return['data'][$i][] = [$inskrywing->persoon->van];
            $return['data'][$i][] = [$inskrywing->voortrekkerBetrokkenheid->betrokkenheid];
            if($inskrywing->persoon->kommando)
                {
                    $return['data'][$i][] = [$inskrywing->persoon->kommando->kommando_naam];
                }
            else {$return['data'][$i][] = [];}

            $rolle = $inskrywing->persoon->rolle;
            $groepe_string = "";
            $rolle_string = "";
            foreach($rolle as $rol)
            {   
                //Wys net die rolle wat aan die kamp gebind is
                if($rol->groep->kamp_id == $kamp_id)
                {
                    $groepe_string .= $rol->groep->groep_naam."<br>";
                    $rolle_string .= $rol->rol_naam."<br>";
                }
            }
            $return['data'][$i][] = [$groepe_string];
            $return['data'][$i][] = [$rolle_string];

            if($inskrywing->betaal == '1')
            {
                $return['data'][$i][] = '<button class="btn btn-xs bg-olive disabled"><i class="fa fa-check"></i></button>';
            }
            else
            {
                $return['data'][$i][] = '<button class="btn btn-xs bg-red-pink disabled"><i class="fa fa-times"></i></button>';
            }


            $return['data'][$i][] =  '<a href="' . url("persone/beskou/".$inskrywing->persoon->registrasie_nommer) . '" class="btn btn-xs bg-blue"><i class="fa fa-eye"></i> Beskou</a> <a href="' . url("inskrywing/view/$inskrywing->id") . '" class="btn btn-xs bg-yellow"><i class="fa fa-user-plus"></i> Inskrywing</a>';
            $flashdata[$i] = $inskrywing->persoon->id;
            $i++;
        }

        Session::set('kamp_persoon', $flashdata);

        return response()->json($return);
    }

    public function sluit_open_kamp($id)
    {
            $real_id = $id;

            if ($real_id === false) {
                Log::warning("2605 || Kon nie 'n kamp in die flashdata vind nie! |", ['veroorsaak_deur' => Auth::user()->toArray(), "opsoek_na_skuil_id" => $id]);
                return Redirect::to('kamp/tabel')->withErrors("Kon nie die versoek voltooi nie");
            }

            $kamp = Kamp::find($real_id);
            if ($kamp->status === "Oop" || $kamp->status === "AanvaarInskrywings")
            {
                $kamp->status = "3";
                $status = "gesluit";
            }
            else
            {
                $kamp->status = "2";
                $status = "heropen";
            }

            $kamp->save();

            return Redirect::back()->with('success', 'Die kamp was suksesvol '. $status .'!');
    }

    public function aanvaar_stop_inskrywings($id)
    {
            $real_id = $id;

            if ($real_id === false) {
                Log::warning("2605 || Kon nie 'n kamp in die flashdata vind nie! |", ['veroorsaak_deur' => Auth::user()->toArray(), "opsoek_na_skuil_id" => $id]);
                return Redirect::to('kamp/tabel')->withErrors("Kon nie die versoek voltooi nie");
            }

            $kamp = Kamp::find($real_id);
            if ($kamp->status === "AanvaarInskrywings")
            {
                $kamp->status = "2";
                $status = "gestop";
            }
            else if($kamp->status === "Gesluit")
            {
                return Redirect::back()->withErrors('Die kamp is gesluit!');
            }
            else
            {
                $kamp->status = "4";
                $status = "begin";
            }

            $kamp->save();

            return Redirect::back()->with('success', 'Die kamp se inskrywings is suksesvol '. $status .'!'); 
    }

    public function skrapKamp()
    {
        if (Auth::user()->super_admin) 
        {
            $kamp = Kamp::find(Session::get('paneel_kamp'));
            if ($kamp->status != "AanvaarInskrywings") 
            {
                $bevestigging_string = Input::get('bevestigging');
                if ($bevestigging_string === "SKRAP") 
                {
                    $inskrywings = Inskrywing::where('kamp_id', $kamp->id)->get();
                    foreach ($inskrywings as $inskrywing) {
                        //inskrywing - vorms
                        $vorms = Vorm::where('inskrywing_id', $inskrywing->id)->get();
                        foreach($vorms as $vorm)
                        {
                            $vorm->delete();
                        }

                        //inskrywing
                        $inskrywing->delete();
                    }

                    $groepe = $kamp->groepe;

                    //Delete Publieke Inskrywings
                    $publieke_inskrywings = PubliekeInskrywing::where('kamp_id',$kamp->id)->withTrashed()->get();
                    foreach($publieke_inskrywings as $p)
                    {
                        $p->forceDelete();
                    }


                    foreach ($groepe as $groep) {
                        $rolle = $groep->rolle;
                        foreach ($rolle as $rol) {
                            //persone rolle
                            $persone = $rol->persone;
                            foreach($persone as $persoon)
                            {
                                $persoon->rolle()->detach($rol->id);
                            }

                            //rolle betrokkenheid
                            $betrokkenhede = $rol->voortrekker_betrokkenhede;
                            foreach($betrokkenhede as $betrokkenheid)
                            {
                                $betrokkenheid->rolle()->detach($rol->id);
                            }

                            //rolle
                            $rol->delete();
                        }
                        //groepe
                        $groep->delete();
                    }

                    $organisasies = $kamp->organisasies;
                    foreach($organisasies as $o)
                    {
                        $kamp->organisasies()->detach($o->id);
                    }

                    //kamp
                    $kamp->delete();

                    return Redirect::to('kamp/tabel')->with('success', $kamp->kamp_naam ." is geskrap!");
                }
                else 
                {
                    return Redirect::to('kamp/paneel/'.$kamp->id)->withErrors("Die bevestiggings woord wat jy ingetik het was verkeerd!");
                } 
            }
            else 
            {
                return Redirect::to('kamp/paneel/'.$kamp->id)->withErrors("'n Kamp kan nie geskrap word terwyl hy inskrywings aanvaar nie!");
            } 
        }
        else 
        {
            return Redirect::to('kamp/paneel/'.$kamp->id)->withErrors("Jy het nie die nodige toeganregte om 'n kamp te skrap nie!");
        } 
    }
}

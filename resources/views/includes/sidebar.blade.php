                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="{{URL::asset('assets/img/avatar_wapen.png')}}" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>{{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Aanlyn</a>
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="active">
                            <a href="{{URL('admin')}}">
                                <i class="fa fa-dashboard"></i> <span>Paneelbord</span>
                            </a>
                        </li>


                        <li @if($active = Request::is('persone/*')) class="active" @endif>
                            <a href="{{{URL('persone/tabel')}}}">
                                <i class="fa fa-users"></i>
                                <span>Persone</span>
                                
                            </a>
                        </li>

                        @if(Auth::user()->super_admin)
                        <li @if($active = Request::is('kommando/*')) class="active" @endif>
                            <a href="{{{URL('kommando/tabel')}}}">
                                <i class="fa fa-bullseye"></i>
                                <span>Kommandos</span>
                                
                            </a>
                        </li>
                        @endif

                        <li @if($active = Request::is('kamp/*')) class="active" @endif>
                            <a href="{{{URL('kamp/tabel')}}}">
                                <i class="fa fa-tree"></i>
                                <span>Kampe</span>
                                
                            </a>
                        </li>

                        @if(Auth::user()->super_admin)
                            <li @if($active = Request::is('logs')) class="active" @endif>
                                <a href="{{{URL('logs/view')}}}">
                                    <i class="fa fa-file-text-o"></i>
                                    <span>Logs</span>
                                </a>
                            </li>
                        @endif


                    </ul>
                </section>
                <!-- /.sidebar -->
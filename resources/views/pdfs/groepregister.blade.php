<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<style>
			table, td, th {
			    border: 1px solid black;
			    width: 100%;
			    border-collapse: collapse;
			    padding: 4px;
			    word-wrap:break-word;
			}

			.heading {
				text-align: left;
			}

			body {
				font-family: DejaVu Sans, sans-serif;
				font-size:11px;
			}
		</style>
	</head>
	<body>
		<div>
		<row>
			<h2>{{$groep->kamp->kamp_naam}}: {{$groep->groep_naam}}</h2>
		</row>
			<table>
				<tr>
					<td class="heading">
						<b>Van</b>
					</td>
					<td class="heading">
						<b>Noemnaam</b>
					</td>
					<td class="heading">
						<b>Geboortedatum</b>
					</td>
					<td class="heading">
						<b>Geslag</b>
					</td>
					<td class="heading">
						<b>Kommando</b>
					</td>
					<td class="heading">
						<b>Siekte/Allergië</b>
					</td>
					<td class="heading" style="text-align: centre;">
						<b>☑</b>
					</td>

				</tr>
				@foreach($rolle as $rol)
					@foreach($rol as $persoon)
					<tr>
						<td>
							{{$persoon->van}}
						</td>
						<td>
							{{$persoon->noemnaam}}
						</td>
						<td>
							{{$persoon->geboorte_datum}}
						</td>
						<td>
							@if($persoon->geslag == 'm')
							 ♂
							@elseif($persoon->geslag == 'f')
							 ♀
							@else
								{{$persoon->geslag}}
							@endif
						</td>
						<td>
							@if($persoon->kommando)
								{{$persoon->kommando->kommando_naam}}
							@endif
						</td>
						<td>
							{{$persoon->siekte_allergie}}
						</td>
						<td>
						</td>

					</tr>
					@endforeach
				@endforeach
			</table>
			<h4>Totaal: {{$count}}</h4>
		</div>
	</body>
</html>

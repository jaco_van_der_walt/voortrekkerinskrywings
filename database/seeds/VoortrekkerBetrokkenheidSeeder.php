<?php

use Illuminate\Database\Seeder;
use App\VoortrekkerBetrokkenheid;

class VoortrekkerBetrokkenheidSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
		VoortrekkerBetrokkenheid::create(array('betrokkenheid' => 'Geen'));
		VoortrekkerBetrokkenheid::create(array('betrokkenheid' => 'Ontdekker'));
		VoortrekkerBetrokkenheid::create(array('betrokkenheid' => 'Penkop en Drawwertjie'));
		VoortrekkerBetrokkenheid::create(array('betrokkenheid' => 'Verkenner'));
		VoortrekkerBetrokkenheid::create(array('betrokkenheid' => 'Staatmaker'));
		VoortrekkerBetrokkenheid::create(array('betrokkenheid' => 'Offisier'));
		VoortrekkerBetrokkenheid::create(array('betrokkenheid' => 'Heemraad'));
		VoortrekkerBetrokkenheid::create(array('betrokkenheid' => 'Jeugvriend'));

		 $this->command->info("VoortrekkerBetrokkenheid table seeded!");
    }
}

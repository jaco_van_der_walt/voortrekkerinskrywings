@extends('layouts.master')

@section('head')
@parent
<title>Publieke Inskrywing</title>
@stop


@section('content')
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <a href={{URL('publiek/inskrywing/admin/tabel')}}><button type="submit" class="btn btn-primary "><i class="fa fa-arrow-circle-left"></i></button></a> Smelt inskrywing saam bestaande Persoon
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">

                @if($errors->has())
                   @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Waarskuwing!</b> {{$error}}
                        </div>
                  @endforeach
                @endif

                <div class="callout callout-info">
                    <p>Hierdie bladsy is in twee gedeel. Die linkerkant wys die Inskrywing soos ontvang deur die aanlyn proses en die regterkant wys die data wat in die Databasis gestoor sal word.<br>Enige veld waar 'n wanaanpassing plaasvind word in rooi gemerk.Kontroleer asseblief die inskrywing en die inskrywing in die databasis.<br><br><strong> - Maak seker dat geen data in HOOFLETTERS ingelees word nie <br> - Indien die persoon geen mediese probleme of allergieë het nie verwyder asseblief "Geen" of "NVT" uit die nodige velde.</p>
                </div>



                    <form role="form" action="{{URL('publiek/inskrywing/admin/updatepersoon')}}" method="POST">
                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-md-12">
                                                            <!-- HEADER -->
                                        <div class="row form-group">
                                            <div class="col-xs-2">
                                            </div>
                                            <div class="col-xs-4">
                                                <h3>Inskrywing</h3>
                                            </div>
                                            <div class="col-xs-4">
                                                <h3>Databasis</h3>
                                            </div>
                                        </div>
                                        <hr>

                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><b>Persoonlike Inligting</b></h3>
                                </div>

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="selected_persoon" value="{{$persoon->id}}">

                                    <div class="box-body">



                                    <!-- Van -->
                                        @if($inskrywing->van != $persoon->van)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                            <div class="col-xs-2">
                                                <label>Van</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->van}}</p>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="van" value="{{$persoon->van}}">
                                            </div>
                                        </div>

                                    <!-- Voorname -->
                                        @if($inskrywing->voorname != $persoon->voorname)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                             <div class="col-xs-2">
                                                <label>Voorname</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->voorname}}</p>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="voorname" value="{{$persoon->voorname}}">
                                            </div>
                                        </div>

                                    <!-- Noemnaam -->
                                        @if($inskrywing->noemnaam != $persoon->noemnaam)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                             <div class="col-xs-2">
                                                <label>Noemnaam</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->noemnaam}}</p>
                                            </div>
                                             <div class="col-xs-4">
                                                <input type="text" class="form-control" name="noemnaam" value="{{$persoon->noemnaam}}">
                                            </div>
                                        </div>

                                    <!-- Date dd/mm/yyyy -->
                                    @if($inskrywing->geboorte_datum != $persoon->geboorte_datum)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                        <div class="col-xs-2">
                                                <label>Geboortedatum</label>
                                         </div>
                                        <div class="col-xs-4">
                                                <p>{{$inskrywing->geboorte_datum}}</p>
                                        </div>
                                        <div class="col-xs-4">
                                         @if($persoon->geboorte_datum)
                                            <input id="datemask" name="geboorte_datum" type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask value="{{$persoon->geboorte_datum}}"/>
                                         @else
                                            <input id="datemask" name="geboorte_datum" type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask/>
                                         @endif
                                        </div><!-- /.input group -->
                                    </div>

                                    <!-- Geslag -->
                                        @if($inskrywing->geslag != $persoon->geslag)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif

                                            <div class="col-xs-2">
                                                <label>Geslag</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->geslag}}</p>
                                            </div>
                                            <div class="col-xs-1">
                                                <label>
                                                @if($persoon->geslag)
                                                    @if($persoon->geslag === 'm')
                                                        <input type="radio" name="geslag" id="optionsRadios1" value="m" checked>
                                                    @else
                                                        <input type="radio" name="geslag" id="optionsRadios1" value="m">
                                                    @endif
                                                @endif
                                                    Manlik
                                                </label>
                                            </div>
                                            <div class="col-xs-1">
                                                <label>
                                                @if($persoon->geslag)
                                                    @if($persoon->geslag === 'f')
                                                        <input type="radio" name="geslag" id="optionsRadios2" value="f" checked>
                                                    @else
                                                        <input type="radio" name="geslag" id="optionsRadios2" value="f">
                                                    @endif
                                                @endif
                                                    Vroulik
                                                </label>
                                            </div>
                                        </div>

                                    <!-- ID Nommer -->
                                        @if($inskrywing->id_nommer != $persoon->id_nommer)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                             <div class="col-xs-2">
                                                <label>ID Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->id_nommer}}</p>
                                            </div>
                                             <div class="col-xs-4">
                                                <input type="text" class="form-control" name="id_nommer" value="{{$persoon->id_nommer}}">
                                            </div>
                                        </div>

                                    <!-- Kontak Nommer -->
                                        @if($inskrywing->kontak_nommer_1 != $persoon->kontak_nommer_1)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                             <div class="col-xs-2">
                                                <label>Kontak Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->kontak_nommer_1}}</p>
                                            </div>
                                             <div class="col-xs-4">
                                                <input type="text" class="form-control" name="kontak_nommer_1" value="{{$persoon->kontak_nommer_1}}">
                                            </div>
                                        </div>

                                    <!-- Alternatiewe Kontak Nommer -->
                                        @if($inskrywing->kontak_nommer_2 != $persoon->kontak_nommer_2)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                            <div class="col-xs-2">
                                                <label>Alternatiewe Kontak Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->kontak_nommer_2}}</p>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="kontak_nommer_2" value="{{$persoon->kontak_nommer_2}}">
                                            </div>
                                        </div>

                                    <!-- Epos Adres -->
                                        @if($inskrywing->epos_adres != $persoon->epos_adres)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                             <div class="col-xs-2">
                                                <label>Epos Adres</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->epos_adres}}</p>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="epos_adres" value="{{$persoon->epos_adres}}">
                                            </div>
                                        </div>

                                        <!-- Woonadres -->
                                        @if($inskrywing->woonadres != $persoon->woonadres)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                            <div class="col-xs-2">
                                                <label>Woonadres</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->woonadres}}</p>
                                            </div>
                                            <div class="col-xs-4">
                                                @if($persoon->woonadres)
                                                    <textarea class="form-control" rows="4" name="woonadres">{{{$persoon->woonadres}}}</textarea>
                                                @else
                                                    <textarea class="form-control" rows="4" name="woonadres"></textarea>
                                                @endif

                                            </div>
                                        </div>

                                        <!-- Posadres -->
                                        @if($inskrywing->posadres != $persoon->posadres)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                            <div class="col-xs-2">
                                                <label>Posadres</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->posadres}}</p>
                                            </div>
                                            <div class="col-xs-4">
                                                @if($persoon->posadres)
                                                    <textarea class="form-control" rows="4" name="posadres">{{{$persoon->posadres}}}</textarea>
                                                @else
                                                    <textarea class="form-control" rows="4" name="posadres"></textarea>
                                                @endif

                                            </div>
                                        </div>

                                    <!-- Voertuig Registrasie Nommer -->
                                        @if($inskrywing->voertuig_registrasie_nommer != $persoon->voertuig_registrasie_nommer)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                             <div class="col-xs-2">
                                                <label>Voertuig Registrasie Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->voertuig_registrasie_nommer}}</p>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="voertuig_registrasie_nommer" value="{{$persoon->voertuig_registrasie_nommer}}">
                                            </div>
                                        </div>
                                    </div>
                            </div><!-- /.box -->

                            <!-- Die Voortrekkers -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><b>Die Voortrekkers</b></h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                        @if($inskrywing->geregistreerde_voortrekker != $persoon->geregistreerde_voortrekker)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                            <div class="col-xs-2">
                                                <label>Geregistreerde Voortrekker?</label>
                                            </div>
                                            <div class="col-xs-4">
                                                @if($inskrywing->voortrekker == '1')
                                                    <p>Ja</p>
                                                @else
                                                    <p>Nee</p>
                                                @endif
                                            </div>
                                            <div class="col-xs-3">
                                                <label>
                                                @if($persoon->geregistreerde_voortrekker)
                                                    @if(Input::old('geregistreerde_voortrekker') === 'on')
                                                        <input type="checkbox" checked="true" name="geregistreerde_voortrekker"/>
                                                    @endif
                                                @else
                                                        <input type="checkbox" checked="true" name="geregistreerde_voortrekker"/>
                                                @endif
                                                    Ja
                                                </label>
                                            </div>
                                        </div>

                                    <!-- Voortrekker Registrasie Nommer -->
                                        @if($inskrywing->voortrekker_registrasie_nommer != $persoon->voortrekker_registrasie_nommer)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                             <div class="col-xs-2">
                                                <label>Voortrekker Registrasie Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->voortrekker_registrasie_nommer}}</p>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="voortrekker_registrasie_nommer" value="{{$persoon->voortrekker_registrasie_nommer}}">
                                            </div>
                                        </div>

                                        <!-- Rang -->
                                        @if($inskrywing->voortrekker_rang != $persoon->voortrekker_rang)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                             <div class="col-xs-2">
                                                <label>Voortrekker Rang</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->voortrekker_rang}}</p>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="voortrekker_rang" value="{{$persoon->voortrekker_rang}}">
                                            </div>
                                        </div>

                                        <!-- Kommando -->
                                        @if($inskrywing->kommando_id != $persoon->kommando_id)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                             <div class="col-xs-2">
                                                <label>Kommando</label>
                                            </div>
                                            <div class="col-xs-4">
                                                @foreach($kommando as $k)
                                                    @if($inskrywing->kommando_id == $k->id)
                                                        <p>{{$k->kommando_naam}}</p>
                                                    @endif
                                                @endforeach

                                            </div>
                                            <div class="col-xs-4">
                                                <select name="kommando" class="form-control">
                                                    <option></option>
                                                    @foreach($kommando as $k)
                                                        @if($persoon->kommando_id == $k->id)
                                                            <option value="{{{$k->id}}}" selected>{{{$k->kommando_naam}}}</option>
                                                        @else
                                                            <option value="{{{$k->id}}}">{{{$k->kommando_naam}}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <!-- Ouers/Voog -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><b>Ouers/Voog</b></h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                        <!-- Ouer/Voog #1 -->
                                        <h4><b>Ouer/Voog #1</b></h4>

                                        @if($inskrywing->ouer_voog_naam_van_1 != $persoon->ouer_voog_naam_van_1)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                             <div class="col-xs-2">
                                                <label>Ouer/Voog Naam & Van</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->ouer_voog_naam_van_1}}</p>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="ouer_voog_naam_van_1" value="{{$persoon->ouer_voog_naam_van_1}}">
                                            </div>
                                        </div>

                                        @if($inskrywing->ouer_voog_id_nommer_1 != $persoon->ouer_voog_id_nommer_1)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                             <div class="col-xs-2">
                                                <label>Ouer/Voog ID Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->ouer_voog_id_nommer_1}}</p>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="ouer_voog_id_nommer_1" value="{{$persoon->ouer_voog_id_nommer_1}}">
                                            </div>
                                        </div>

                                        @if($inskrywing->ouer_voog_kontak_nommer_1 != $persoon->ouer_voog_kontak_nommer_1)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                             <div class="col-xs-2">
                                                <label>Ouer/Voog Kontak Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->ouer_voog_kontak_nommer_1}}</p>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="ouer_voog_kontak_nommer_1" value="{{$persoon->ouer_voog_kontak_nommer_1}}">
                                            </div>
                                        </div>

                                        @if($inskrywing->ouer_voog_epos_adres_1 != $persoon->ouer_voog_epos_adres_1)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                             <div class="col-xs-2">
                                                <label>Ouer/Voog Epos Adres</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->ouer_voog_epos_adres_1}}</p>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="ouer_voog_epos_adres_1" value="{{$persoon->ouer_voog_epos_adres_1}}">
                                            </div>
                                        </div>

                                        <!-- Ouer/Voog #2 -->
                                        <h4><b>Ouer/Voog #2</b></h4>
                                        @if($inskrywing->ouer_voog_naam_van_2 != $persoon->ouer_voog_naam_van_2)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                             <div class="col-xs-2">
                                                <label>Ouer/Voog Naam & Van</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->ouer_voog_naam_van_2}}</p>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="ouer_voog_naam_van_2" value="{{$persoon->ouer_voog_naam_van_2}}">
                                            </div>
                                        </div>

                                        @if($inskrywing->ouer_voog_id_nommer_2 != $persoon->ouer_voog_id_nommer_2)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                             <div class="col-xs-2">
                                                <label>Ouer/Voog ID Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->ouer_voog_id_nommer_2}}</p>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="ouer_voog_id_nommer_2" value="{{$persoon->ouer_voog_id_nommer_2}}">
                                            </div>
                                        </div>

                                        @if($inskrywing->ouer_voog_kontak_nommer_2 != $persoon->ouer_voog_kontak_nommer_2)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                             <div class="col-xs-2">
                                                <label>Ouer/Voog Kontak Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->ouer_voog_kontak_nommer_2}}</p>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="ouer_voog_kontak_nommer_2" value="{{$persoon->ouer_voog_kontak_nommer_2}}">
                                            </div>
                                        </div>

                                        @if($inskrywing->ouer_voog_epos_adres_2 != $persoon->ouer_voog_epos_adres_2)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                             <div class="col-xs-2">
                                                <label>Ouer/Voog Epos Adres</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->ouer_voog_epos_adres_2}}</p>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="ouer_voog_epos_adres_2" value="{{$persoon->ouer_voog_epos_adres_2}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <!-- Mediese Inligting -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><b>Mediese Inligting</b></h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <!-- Mediese Fonds Check -->
                                    @if($inskrywing->mediese_fonds != $persoon->mediese_fonds)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                            <div class="col-xs-2">
                                                <label>Mediese Fonds?</label>
                                            </div>
                                            <div class="col-xs-4">
                                                @if($inskrywing->mediese_fonds == '1')
                                                    <p>Ja</p>
                                                @else
                                                    <p>Nee</p>
                                                @endif
                                            </div>
                                            <div class="col-xs-3">
                                                <label>
                                                @if($persoon->mediese_fonds)
                                                    @if(Input::old('mediese_fonds') === 'on')
                                                        <input type="checkbox" checked="true" name="mediese_fonds" checked/>
                                                    @else
                                                        <input type="checkbox" checked="true" name="mediese_fonds"/>
                                                    @endif
                                                @else
                                                        <input type="checkbox" checked="true" name="mediese_fonds"/>
                                                @endif
                                                    Ja
                                                </label>
                                            </div>
                                        </div>

                                        <!-- Mediese Fonds Naam -->
                                        @if($inskrywing->mediese_fonds_naam != $persoon->mediese_fonds_naam)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                             <div class="col-xs-2">
                                                <label>Mediese Fonds Naam</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->mediese_fonds_naam}}</p>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="mediese_fonds_naam" value="{{$persoon->mediese_fonds_naam}}">
                                            </div>
                                        </div>

                                        <!-- Mediese Fonds Lidmaat Nommer -->
                                        @if($inskrywing->mediese_fonds_lid_nommer != $persoon->mediese_fonds_lid_nommer)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                             <div class="col-xs-2">
                                                <label>Mediese Fonds Lidmaat Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->mediese_fonds_lid_nommer}}</p>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="mediese_fonds_lid_nommer" value="{{$persoon->mediese_fonds_lid_nommer}}">
                                            </div>
                                        </div>

                                        <!-- Huisdokter -->
                                        <h4><b>Huisdokter</b></h4>
                                        @if($inskrywing->huis_dokter_naam_van != $persoon->huis_dokter_naam_van)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                             <div class="col-xs-2">
                                                <label>Huisdokter Naam & Van</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->huis_dokter_naam_van}}</p>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="huis_dokter_naam_van" value="{{$persoon->huis_dokter_naam_van}}">
                                            </div>
                                        </div>

                                       @if($inskrywing->huis_dokter_kontak_nommer_1 != $persoon->huis_dokter_kontak_nommer_1)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                             <div class="col-xs-2">
                                                <label>Huisdokter Kontak Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->huis_dokter_kontak_nommer_1}}</p>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="huis_dokter_kontak_nommer_1" value="{{$persoon->huis_dokter_kontak_nommer_1}}">
                                            </div>
                                        </div>

                                       @if($inskrywing->huis_dokter_kontak_nommer_2 != $persoon->huis_dokter_kontak_nommer_2)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                             <div class="col-xs-2">
                                                <label>Huisdokter Alternatiewe Kontak Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->huis_dokter_kontak_nommer_2}}</p>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="huis_dokter_kontak_nommer_2" value="{{$persoon->huis_dokter_kontak_nommer_2}}">
                                            </div>
                                        </div>

                                        <!-- Mediese Toestand & Allergie -->
                                        @if($inskrywing->siekte_allergie != $persoon->siekte_allergie)
                                        <div class="row form-group alert-merge">
                                        @else
                                        <div class="row form-group">
                                        @endif
                                            <div class="col-xs-2">
                                                <label>Siektetoestande of Allergieë</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->siekte_allergie}}</p>
                                            </div>
                                            <div class="col-xs-4">
                                                @if($persoon->siekte_allergie)
                                                    <textarea class="form-control" rows="4" name="siekte_allergie">{{{$persoon->siekte_allergie}}}</textarea>
                                                @else
                                                    <textarea class="form-control" rows="4" name="siekte_allergie"></textarea>
                                                @endif
                                            </div>
                                        </div>
                                </div>
                            </div>

                             <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><b>Kampinligting</b></h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">

                                        @if($inskrywing->inskrywing_soort == 'jeuglid')
                                        <!-- Skoolgraad -->
                                        <div class="row form-group alert-merge">
                                             <div class="col-xs-2">
                                                <label>Skoolgraad</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->skoolgraad}}</p>
                                            </div>
                                            <div class="col-xs-4">
                                                <select name="skoolgraad" class="form-control">
                                                    <option value="voorskool">Voorskool</option>
                                                    <option value="voorskool">Voorskool</option>
                                                    <option value="r">Graad R</option>
                                                    @if($inskrywing->skoolgraad == '1')<option value="1" selected>Graad 1</option> @else <option value="1">Graad 1</option> @endif
                                                    @if($inskrywing->skoolgraad == '2')<option value="2" selected>Graad 2</option> @else <option value="2">Graad 2</option> @endif
                                                    @if($inskrywing->skoolgraad == '3')<option value="3" selected>Graad 3</option> @else <option value="3">Graad 3</option> @endif
                                                    @if($inskrywing->skoolgraad == '4')<option value="4" selected>Graad 4</option> @else <option value="4">Graad 4</option> @endif
                                                    @if($inskrywing->skoolgraad == '5')<option value="5" selected>Graad 5</option> @else <option value="5">Graad 5</option> @endif
                                                    @if($inskrywing->skoolgraad == '6')<option value="6" selected>Graad 6</option> @else <option value="6">Graad 6</option> @endif
                                                    @if($inskrywing->skoolgraad == '7')<option value="7" selected>Graad 7</option> @else <option value="7">Graad 7</option> @endif
                                                    @if($inskrywing->skoolgraad == '8')<option value="8" selected>Graad 8</option> @else <option value="8">Graad 8</option> @endif
                                                    @if($inskrywing->skoolgraad == '9')<option value="9" selected>Graad 9</option> @else <option value="9">Graad 9</option> @endif
                                                    @if($inskrywing->skoolgraad == '10')<option value="10" selected>Graad 10</option> @else <option value="10">Graad 10</option> @endif
                                                    @if($inskrywing->skoolgraad == '11')<option value="11" selected>Graad 11</option> @else <option value="11">Graad 11</option> @endif
                                                    @if($inskrywing->skoolgraad == '12')<option value="12" selected>Graad 12</option> @else <option value="12">Graad 12</option> @endif
                                                </select>
                                            </div>
                                        </div>
                                        @endif

                                        @foreach($kamp->ekstras as $e)
                                        <div class="row form-group">
                                          <div class="col-xs-6">
                                             <label>{{$e->naam}}</label>
                                         </div>

                                         <div class="col-xs-4">
                                             <select name="ekstras[]" class="form-control">
                                               <option value="null"></option>
                                              @foreach($e->opsies as $o)
                                                @if($inskrywing->opsies()->where('opsie_id',$o->id)->count() == 1)
                                                  <option value="{{$o->id}}" selected>{{$o->naam}}</option>
                                                @else
                                                  <option value="{{$o->id}}">{{$o->naam}}</option>
                                                @endif
                                              @enforeach

                                       @endforeach

                                             </select>
                                          </div>
                                        </div>
                                        @endforeach
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="row form-group">
                            <div class = "col-md-12">
                                <a href={{URL('publiek/inskrywing/admin/tabel')}}><button class="btn btn-primary" style="margin-bottom: 15px;">Kanselleer</button></a>
                                <button id="submit" type="submit" class="btn btn-success pull-right">Aanvaar Inskrywing <i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                        </div>
                    </form>
                    </div><!-- /.row (main row) -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
    <!-- AdminLTE App -->
    <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>
    <!-- InputMask -->
    <script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.date.extensions.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.extensions.js')}}" type="text/javascript"></script>

    <script type="text/javascript">
        //Datemask
        $("#datemask").inputmask("yyyy/mm/dd", {"placeholder": "yyyy/mm/dd"});
    </script>


    <script type="text/javascript">
        var selected = -1;
        function load_persoon($id)
        {
            $('#'+$id).toggleClass('btn-success');
            $('#'+selected).toggleClass('btn-success');
            selected = $id;

            $.get("{{URL('ajax/persoon')}}/"+$id,
                function( data )
                {
                    $('#van').html(data.van);
                    $('#voorname').html(data.voorname);
                    $('#noemnaam').html(data.noemnaam);
                    $('#geboorte_datum').html(data.geboorte_datum);
                    $('#geslag').html(data.geslag);
                    $('#id_nommer').html(data.id_nommer);
                    $('#kontak_nommer_1').html(data.kontak_nommer_1);
                    $('#kontak_nommer_2').html(data.kontak_nommer_2);
                    $('#epos_adres').html(data.epos_adres);
                    $('#woonadres').html(data.woonadres);
                    $('#posadres').html(data.posadres);
                    $('#voertuig_registrasie_nommer').html(data.voertuig_registrasie_nommer);
                    $('#voortrekker').html(data.voortrekker);
                    $('#voortrekker_registrasie_nommer').html(data.voortrekker_registrasie_nommer);
                    $('#voortrekker_rang').html(data.voortrekker_rang);
                    $('#kommando_naam').html(data.kommando_naam);
                    $('#ouer_voog_naam_van_1').html(data.ouer_voog_naam_van_1);
                    $('#ouer_voog_id_nommer_1').html(data.ouer_voog_id_nommer_1);
                    $('#ouer_voog_kontak_nommer_1').html(data.ouer_voog_kontak_nommer_1);
                    $('#ouer_voog_pos_adres_1').html(data.ouer_voog_pos_adres_1);
                    $('#ouer_voog_naam_van_2').html(data.ouer_voog_naam_van_2);
                    $('#ouer_voog_id_nommer_2').html(data.ouer_voog_id_nommer_2);
                    $('#ouer_voog_kontak_nommer_2').html(data.ouer_voog_kontak_nommer_2);
                    $('#ouer_voog_epos_adres_2').html(data.ouer_voog_epos_adres_2);
                    $('#mediese_fonds').html(data.mediese_fonds);
                    $('#mediese_fonds_naam').html(data.mediese_fonds_naam);
                    $('#mediese_fonds_lid_nommer').html(data.mediese_fonds_lid_nommer);
                    $('#huis_dokter_naam_van').html(data.huis_dokter_naam_van);
                    $('#huis_dokter_kontak_nommer_1').html(data.huis_dokter_kontak_nommer_1);
                    $('#huis_dokter_kontak_nommer_2').html(data.huis_dokter_kontak_nommer_2);
                    $('#siekte_allergie').html(data.siekte_allergie);

                    document.getElementById("selected_persoon").value = data.id;
                });
        }

    </script>





@stop

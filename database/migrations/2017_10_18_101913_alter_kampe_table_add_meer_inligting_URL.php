<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterKampeTableAddMeerInligtingURL extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kampe', function ($table) {
            $table->string('meer_inligting_URL')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kampe', function ($table) {
            $table->dropColumn('meer_inligting_URL');
        });
    }
}

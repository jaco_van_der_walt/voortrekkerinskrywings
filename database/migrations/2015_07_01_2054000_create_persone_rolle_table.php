<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersoneRolleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('persone_rolle', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('rol_id')->unsigned();
			$table->integer('persoon_id')->unsigned();
			$table->timestamps();

			$table->foreign('rol_id')->references('id')->on('rolle');
			$table->foreign('persoon_id')->references('id')->on('persone');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('persone_rolle');
	}

}
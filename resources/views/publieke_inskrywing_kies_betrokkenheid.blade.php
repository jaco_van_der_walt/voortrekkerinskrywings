@extends('layouts.master')

@section('head')
@parent
<title>Publieke Inskrywing</title>
@stop


@section('content')
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><a href={{URL('inskrywing/kanseleer')}}><button type="submit" class="btn btn-primary "><i class="fa fa-arrow-circle-left"></i></button></a> Nuwe Inskrywing: {{$kamp->kamp_naam}}</h1>
    </section>

    <!-- Main content -->
    <section class="content">
    @if($errors->has())
        @foreach ($errors->all() as $error)
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Waarskuwing!</b> {{$error}}
        </div>
        @endforeach
    @endif
    
    @if(Session::has('success'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Sukses!</b> {{Session::get('success')}}
    </div>
    @endif

    <!-- Instruksies -->
    <div class="row">
        <div class="col-md-12">
            <div class="callout callout-info">
                <h4>Betrokkenheid</h4>
                <p>Hoe is {{$persoon->noemnaam}} {{$persoon->van}} betrokke by Die Voortrekkers en {{$kamp->kamp_naam}}?<br>Die persoon se voorkeur is in groen aangedui maar jy kan 'n ander betrokkenheid kies. Maak asseblief seker dat jy die korrekte betrokkenheid kies volgens die persoon se lidmaatskap op SAS.</p>
            </div>
        </div>
    </div>
    
    <!-- Instruksies -->
    
    <!-- Form Start -->
    <form role="form" action="{{URL('inskrywing/opdateer/persoon')}}" method="POST">

    <!-- Main Row --> 
    <div class="row"> 
        <div class="col-md-12"> 

			<!-- Betrokkenhede -->
			<div class="box box-primary">
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							@foreach($betrokkenhede as $betrokkenheid)
								@if($inskrywing->voortrekker_betrokkenheid_id == $betrokkenheid->id)
								<a href="{{URL('publieke/inskrywing/admin/kies/betrokkenheid',[$betrokkenheid->id])}}" class="btn btn-success btn-lg btn-block">{{$betrokkenheid->betrokkenheid}}</a>
								@else
								<a href="{{URL('publieke/inskrywing/admin/kies/betrokkenheid',[$betrokkenheid->id])}}" class="btn btn-default btn-lg btn-block">{{$betrokkenheid->betrokkenheid}}</a>
								@endif
							
							@endforeach
						</div>
					</div>
				</div>
			</div>

			<!-- footer --> 
			<div class="box-footer col-md-12">
				<div class="row">
					<div class="col-md-6">
						<a href={{URL('inskrywing/kanseleer')}} class="btn btn-danger"><i class="fa fa-times"></i> Kanseleer Inskrywing</a>
					</div>
				</div>
			</div>
			<!-- /footer --> 

		</div>
	</div>
	<!-- End Main Row -->

	</section>
</aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
    <!-- AdminLTE App -->
    <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>

@stop
<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inskrywing extends Model {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'inskrywings';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

    protected $guarded = ['id'];

    protected $hidden = ['created_at', 'updated_at'];

    protected $fillable = ['voortrekker_betrokkenheid_id'];


    public function kamp()
    {
    	 return $this->belongsTo('App\Kamp', 'kamp_id');
    }

    public function persoon()
    {
    	return $this->belongsTo('App\Persoon', 'persoon_id');
    }

    public function voortrekkerBetrokkenheid(){
        return $this->belongsTo('App\VoortrekkerBetrokkenheid');
    }

		public function opsies(){
      return $this->belongsToMany('App\Opsie', 'inskrywing_ekstras', 'inskrywing_id', 'opsie_id');
    }


}

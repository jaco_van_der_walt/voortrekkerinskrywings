<?php namespace App\Http\Controllers;

use Input;
use Form;
use Validator;
use Redirect;
use Session;
use Response;
use Log;
use Auth;
use View;
use App\Groep;
use App\Kamp;
use App\Rol;
use App\Persoon;
use App\Inskrywing;
use App\VoortrekkerBetrokkenheid;
use App\PubliekeInskrywing;

class GroepController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function view_groep($id)
	{
        //This id is stored in the session when we're interacting with that rol. Needs to be cleared when the page reloads.
        if(Session::has('rol_id'))
        {
          Session::forget('rol_id');
        }
        $groep = Groep::find($id);

        Session::set('groep_id', $groep->id);

        $betrokke = VoortrekkerBetrokkenheid::all();
        $betrokkenheid_teller = array();
        foreach ($betrokke as $betrokkenheid) {
            $betrokkenheid_teller[$betrokkenheid->id] = 0;
        }

        foreach($groep->rolle as $rol)
        {
            foreach($rol->persone as $persoon)
            {
                foreach ($persoon->inskrywings as $inskrywing)
                {
                    if ($inskrywing->kamp_id === $groep->kamp_id)
                    {
                        $betrokkenheid_teller[$inskrywing->voortrekkerBetrokkenheid->id]++;
                    }
                }
            }
        }

		return View::make('groep')->with(['kamp' => $groep->kamp, 'groep' => $groep, 'rolle' => $groep->rolle, 'betrokkenhede' => $betrokke, 'betrokkenheid_stats' => $betrokkenheid_teller]);
	}

    public function laai_logo()
    {
        var_dump(Input::file('logo'));
        if(Input::has('logo'))
        {
            $file = Input::file('logo');

            if (Input::file('logo')->isValid()) {
              $destinationPath = 'uploads'; // upload path
              $extension = Input::file('logo')->getClientOriginalExtension(); // getting image extension
              $fileName = rand(11111,99999).'.'.$extension; // renameing image
              Input::file('logo')->move($destinationPath, $fileName); // uploading file to given path
              // sending back with message
              Session::flash('success', 'Upload successfully');
              return Redirect::back()->with('success', "Logo updated");
            }

        }
        else return Redirect::back()->withErrors("Kon nie die versoek voltooi nie");

    }

    public function ajax_groep_rolle()
    {
      $rol = Rol::find(Input::get('rol_id'));

      //This is important! We're going to check this when we get further ajax posts to know which rol we're manipulating.
      Session::set('rol_id', $rol->id);

      $return['rol_naam'] = $rol->rol_naam;
      $return['inskrywing_beperking'] = $rol->inskrywing_beperking;
      $return['inskrywing_koste'] = $rol->inskrywing_koste;
      $return['voortrekker_betrokkenheid'] = VoortrekkerBetrokkenheid::all();
      $return['rol_betrokkenhede'] = $rol->voortrekker_betrokkenhede;


      return response()->json($return);
    }

    public function vervul_rol($id)
    {
       $real_id = Session::get("groep_persoon.$id", false);

       if ($real_id === false) {
            Log::warning("2605 || Kon nie 'n persoon in die flashdata vind om die rol te vervul nie! |", ['veroorsaak_deur' => Auth::user()->toArray(), "opsoek_na_skuil_id" => $id]);
            return Redirect::back()->withErrors("Kon nie die versoek voltooi nie");
        }

        $persoon = Persoon::find($real_id);

        //Hierdie rol is in die session gestoor in ajax_groep_rolle() en ons gebruik dit nou hier om te weet watse rol die gebruiker na verwys.
        $rol_id = Session::get('rol_id', false);

        if ($rol_id === false) {
            Log::warning("2606 || Kon nie die rol in die flashdata vind wat die persoon probeer vervul nie! |", ['veroorsaak_deur' => Auth::user()->toArray(), "opsoek_na_skuil_id" => $id]);
            return Redirect::back()->withErrors("Kon nie die versoek voltooi nie");
        }

        $nuwe_rol = Rol::find($rol_id);

        $exists = $persoon->rolle->contains($rol_id);

        if($exists)
        {
          return Redirect::back()->withErrors($persoon->noemnaam." ".$persoon->van." het reeds die rol ".$nuwe_rol->rol_naam);
        }
        else
        {
          $persoon->rolle()->attach($rol_id);

          //Check if Inskrywing exists
          $check_inskrywing = Inskrywing::where('persoon_id', $persoon->id)->where('kamp_id', $nuwe_rol->groep->kamp_id)->first();
          if($check_inskrywing === null)
          {
            //Skep die Inskrywing
            $inskrywing = new Inskrywing;
            $inskrywing->persoon_id = $persoon->id;
            $inskrywing->kamp_id = $nuwe_rol->groep->kamp_id;
            $inskrywing->save();
          }

          return Redirect::back()->with('success', $persoon->noemnaam." ".$persoon->van." vervul nou die rol ".$nuwe_rol->rol_naam);
        }

    }

    //This will detach a role
    public function verwyder_rol($id)
    {
        $real_id = Session::get("groep_persone_rolle.$id", false);
        if ($real_id === false) {
            Log::warning("2615 || Kon nie 'n persoon in die flashdata vind wie se rol moet verwyder word nie! |", ['veroorsaak_deur' => Auth::user()->toArray(), "opsoek_na_skuil_id" => $id]);
            return Redirect::back()->withErrors("Kon nie die versoek voltooi nie");
        }

        $persoon = Persoon::find($real_id);

        $groep = Groep::find(Session::get('groep_id'));
        if ($groep === false) {
            Log::warning("2616 || Kon nie 'n groep in die flashdata vind om van die persoon te verwyder nie! |", ['veroorsaak_deur' => Auth::user()->toArray(), "opsoek_na_skuil_id" => $id]);
            return Redirect::back()->withErrors("Kon nie die versoek voltooi nie");
        }

        foreach($groep->rolle as $rol)
        {
            $persoon->rolle()->detach($rol->id);
        }

        return Redirect::back()->with('success', $persoon->noemnaam." ".$persoon->van." se rolle van die groep ".$groep->groep_naam." is verwyder!");
    }

    //This is inside the modal when adding a new person to a role
    public function ajax_groep_persone()
    {

        $return['recordsTotal'] = Persoon::count();

        $flashdata = [];
        $return['data'] = [];
        $i = 0;
        foreach (Persoon::orderBy('van')->with('kommando')->get() as $persoon) {
            $return['data'][$i] = [$persoon->voortrekker_registrasie_nommer];
            $return['data'][$i][] = [$persoon->noemnaam];
            $return['data'][$i][] = [$persoon->van];
            $return['data'][$i][] = [$persoon->geboorte_datum];

            if($persoon->kommando)
            {
               $return['data'][$i][] = [$persoon->kommando->kommando_naam];
            }
            else
            {
                $return['data'][$i][] ="";
            }
            $return['data'][$i][] = Inskrywing::where('persoon_id', $persoon->id)->count();
            $return['data'][$i][] =  '<a href="' . url("groep/rol/vervul/persoon/$i") . '" class="btn btn-xs bg-olive"><i class="fa fa-user-plus"></i> Vervul Rol</a>';
            $flashdata[$i] = $persoon->id;
            $i++;
        }

        Session::set('groep_persoon', $flashdata);

        return response()->json($return);
    }

    public function skrapRol()
    {
        $rol_id = Session::get('rol_id', false);

        if ($rol_id === false) {
            Log::warning("2656 || Kon nie die rol in die flashdata vind wat ons probeer skrap nie! |", ['veroorsaak_deur' => Auth::user()->toArray(), "opsoek_na_skuil_id" => $id]);
            return Redirect::back()->withErrors("Kon nie die versoek voltooi nie");
        }

        $rol = Rol::find($rol_id);
        $persone = $rol->persone;

				$betrokkenheid = $rol->voortrekker_betrokkenhede;

				foreach($betrokkenheid as $b)
				{
					$rol->voortrekker_betrokkenhede()->detach($b->id);
				}

        foreach($persone as $persoon)
        {
            $persoon->rolle()->detach($rol->id);
        }

        $rol->delete();

        return Redirect::back()->with('success', $rol->rol_naam." is verwyder van die groep ".$rol->groep->groep_naam);
    }

    //This is for the "Main" Page of each group
    public function ajax_groep_rolle_persone()
    {

        $groep = Groep::find(Session::get('groep_id'));
        $count = 0;
        $flashdata = [];
        $return['data'] = [];
        $i = 0;
        foreach($groep->rolle as $rol)
        {
          foreach($rol->persone as $persoon)
          {
            $return['data'][$i] = [$persoon->voortrekker_registrasie_nommer];
            $return['data'][$i][] = [$persoon->noemnaam];
            $return['data'][$i][] = [$persoon->van];
            $return['data'][$i][] = [$persoon->geboorte_datum];

            if($persoon->kommando)
            {
               $return['data'][$i][] = [$persoon->kommando->kommando_naam];
            }
            else
            {
                $return['data'][$i][] ="";
            }

            $return['data'][$i][] = [$rol->rol_naam];

            $return['data'][$i][] =  '<a href="' . url("groep/rol/verwyder/persoon/$i") . '" class="btn btn-xs bg-pink-red"><i class="fa fa-user-plus"></i> Verwyder</a>';
            $flashdata[$i] = $persoon->id;
            $i++;
          }
        }

        $return['recordsTotal'] = $i;

        Session::set('groep_persone_rolle', $flashdata);

        return response()->json($return);
    }

    public function skrapGroep(){
        $groep = Groep::find(Session::get('groep_id'));
        //Groep se Rolle
        $rolle = $groep->rolle;
        $kamp_id = Session::get('paneel_kamp');

        foreach($rolle as $rol)
        {
            $persone = $rol->persone;

            foreach($persone as $persoon)
            {
                $persoon->rolle()->detach($rol->id);
            }

            $voortrekker_betrokkenhede = $rol->voortrekker_betrokkenhede;

            foreach($voortrekker_betrokkenhede as $betrokkenheid)
            {
                $betrokkenheid->rolle()->detach($rol->id);
            }

            $rol->delete();
        }

				//Null any Publieke Inskrywings to this group
				$publieke_inskrywings = PubliekeInskrywing::where('groep_id',$groep->id)->get();
				foreach($publieke_inskrywings as $p)
				{
					$p->groep_id = null;
					$p->save();
				}

        $groep->delete();

        return Redirect::to('kamp/paneel/'.$kamp_id)->with('success', $groep->groep_naam." is verwyder van die kamp");
    }

    public function nuweGroep()
    {
        $kamp = Kamp::find(Session::get('paneel_kamp'));

        $rolle_string = Input::get('groep_rolle');

        $rolle = explode("\r\n", $rolle_string);

        $groep = new Groep;

        $groep->groep_naam = Input::get('groep_naam');
        $groep->kamp_id = $kamp->id;
        $groep->save();


        for($x = 0; $x < count($rolle); $x++)
        {
            $rol = new Rol;
            $rol->rol_naam = trim($rolle[$x]);
            $rol->groep_id = $groep->id;
            $rol->save();
        }

        return Redirect::to('kamp/paneel/'.$kamp->id)->with('success', $groep->groep_naam." is bygevoeg!");
    }

    public function nuweRol()
    {

        //validation rules
        $rules = array(
                        'rol_naam' => 'Required'
                );
        $v = Validator::make(Input::all(), $rules);

        if( $v->passes() ) {
            $rol = new Rol;
            $rol->rol_naam = trim(Input::get('rol_naam'));

            $groep = Groep::find(Session::get('groep_id'));
            $rol->groep_id = $groep->id;

            $rol->save();

            return Redirect::back()->with('success', $rol->rol_naam." is geskep in ".$groep->groep_naam);

        }
        else{
            //Invalid input! Redirecting back...
            return redirect()->back()->withInput()->withErrors($v);
        }


    }

    public function hernoemRol()
    {
        $rules = array(
                        'hernoem_rol_naam' => 'Required',
                        'rol_inskrywing_beperking' => 'numeric',
                        'rol_inskrywing_koste' => 'numeric'
                );
        $v = Validator::make(Input::all(), $rules);

        if( $v->passes() ) {
            $groep = Groep::find(Session::get('groep_id'));
            $rol = Rol::find(Session::get('rol_id', false));
            $rol->rol_naam = Input::get('hernoem_rol_naam');
            $rol->inskrywing_beperking = Input::get('rol_inskrywing_beperking');
            $rol->inskrywing_koste = Input::get('rol_inskrywing_koste');

            $rol->voortrekker_betrokkenhede()->detach(); //Remove all
            foreach (Input::get('voortrekker_betrokkenheid') as $betrokkenheid) { //Add all new ones
                $betrokkenheid = VoortrekkerBetrokkenheid::find($betrokkenheid);
                $rol->voortrekker_betrokkenhede()->attach($betrokkenheid);
            }
            $rol->save();

            return Redirect::back()->with('success', $rol->rol_naam." in ".$groep->groep_naam." is opgedateer!");
        }else{
            //Invalid input! Redirecting back...
            return redirect()->back()->withInput()->withErrors($v);
        }

    }

    public function hernoemGroep()
    {
        $rules = array(
                        'hernoem_groep_naam' => 'Required',
												'groep_beskrywing' => 'string',
                );
        $v = Validator::make(Input::all(), $rules);

        if( $v->passes() ) {
            $groep = Groep::find(Session::get('groep_id'));
            $groep->groep_naam = Input::get('hernoem_groep_naam');
						$groep->beskrywing = Input::get('groep_beskrywing');
            $groep->save();

            return Redirect::back()->with('success', "Groep is hernoem na ".$groep->groep_naam);
        }else{
            //Invalid input! Redirecting back...
            return redirect()->back()->withInput()->withErrors($v);
        }

    }


}

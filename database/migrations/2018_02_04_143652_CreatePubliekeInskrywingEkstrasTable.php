<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePubliekeInskrywingEkstrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('publieke_inskrywing_ekstras', function(Blueprint $table)
      {
          $table->integer('publieke_inskrywing_id')->unsigned();
          $table->integer('opsie_id')->unsigned();

          $table->foreign('publieke_inskrywing_id')->references('id')->on('publieke_inskrywings');
          $table->foreign('opsie_id')->references('id')->on('opsies');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('publieke_inskrywing_ekstras');
    }
}

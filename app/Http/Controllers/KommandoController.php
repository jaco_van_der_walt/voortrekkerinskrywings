<?php namespace App\Http\Controllers;

use Input;
use Validator;
use Redirect;
use App\Persoon;
use Session;
use Response;
use Log;
use Auth;
use View;
use App\Kommando;

class KommandoController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function view_kommando_table()
	{

		//Ensure that the Session forgets about the "edited kommando"
        if (Session::has('edit_kommando'))
            {
                Session::forget('edit_kommando');
            }
            

		return view('kommando_table');
	}

    public function view_voeg_by()
    {
        return view('kommando_voeg_by');
    }

    public function submit_voeg_by()
    {
    	//validation rules
    	$rules = array(
                        'kommando_naam' => 'Required',
                        'provinsie' => 'in:Gauteng,KwaZulu-Natal,Limpopo,Mpumalanga,Noordwes,Noord-Kaap,Oos-Kaap,Vrystaat',
                        'kontak_persoon_epos' => 'email'
                );
    	$v = Validator::make(Input::all(), $rules);

    	if( $v->passes() ) {
    		$kommando = new Kommando;

			$kommando->kommando_naam = Input::get('kommando_naam');
			$kommando->stad_dorp = Input::get('stad_dorp');
			$kommando->provinsie = Input::get('provinsie');
			$kommando->kontak_persoon_naam_van = Input::get('kontak_persoon_naam_van');
			$kommando->kontak_persoon_epos = Input::get('kontak_persoon_epos');
			$kommando->kontak_persoon_telefoon = Input::get('kontak_persoon_telefoon');

			$kommando->save();


            Log::notice('2500 || Nuwe Kommando geskep |', ['veroorsaak_deur' => Auth::user()->toArray(), 'kommando' => $kommando->toArray()]);

            return Redirect::to('kommando/tabel')->with('success', $kommando->kommando_naam." is gestoor");
    	}
    		else {
                    //Invalid input! Redirecting back...
                    return redirect()->back()->withInput()->withErrors($v);
                }


    	return Input::all();
    }

    public function postEdit()
    {
    	if (Session::has('edit_kommando'))
    	{
    		$rules = array(
                        'kommando_naam' => 'Required',
                        'provinsie' => 'in:Gauteng,KwaZulu-Natal,Limpopo,Mpumalanga,Noordwes,Noord-Kaap,Oos-Kaap,Vrystaat',
                        'kontak_persoon_epos' => 'email'
                );
    		$v = Validator::make(Input::all(), $rules);
    		if ($v->passes())
    		{
    			$id = Session::get('edit_kommando');
    			$kommando = Kommando::find($id);
    			$kommando_backup = $kommando;

    			$kommando->kommando_naam = Input::get('kommando_naam');
				$kommando->stad_dorp = Input::get('stad_dorp');
				$kommando->provinsie = Input::get('provinsie');
				$kommando->kontak_persoon_naam_van = Input::get('kontak_persoon_naam_van');
				$kommando->kontak_persoon_epos = Input::get('kontak_persoon_epos');
				$kommando->kontak_persoon_telefoon = Input::get('kontak_persoon_telefoon');

				$kommando->save();

				Log::notice('2515 || Kommando suksesfol opgedateer |', ['veroorsaak_deur' => Auth::user()->toArray(), 'ou_kommando' => $kommando_backup->toArray(), 'nuew_kommando' => $kommando->toArray()]);

            	return Redirect::to('kommando/tabel')->with('success', $kommando->kommando_naam." is opgedateer");
    		}	
    		else{
    			 //Invalid input! Redirecting back...
                 return redirect()->back()->withInput()->withErrors($v);
    		}	
    	}
    	else
    	{
    		 Log::warning("2510 || Kon nie 'n kommando in die flashdata vind om te edit nie! |", ['veroorsaak_deur' => Auth::user()->toArray()]);
            return Redirect::to('kommando/tabel')->withErrors("Kon nie die versoek voltooi nie");
    	}
    	return Input::all();
    }


    public function ajax_kommando()
    {
        
        $return['recordsTotal'] = Kommando::count();

        $flashdata = [];
        $return['data'] = [];
        $i = 0;
        foreach (Kommando::orderBy('kommando_naam')->get() as $kommando) {
            $return['data'][$i] = [$kommando->kommando_naam];
            $return['data'][$i][] = [$kommando->stad_dorp];
            $return['data'][$i][] = [$kommando->provinsie];

            $return['data'][$i][] =  '<a href="' . url("kommando/edit/$i") . '" class="btn btn-xs blue"><i class="fa fa-wrench"></i> Edit</a>';
            $flashdata[$i] = $kommando->id;
            $i++;
        }

        Session::set('kommando', $flashdata);

        return response()->json($return);
    }

    public function getEdit($id)
    {
       $real_id = Session::get("kommando.$id", false);

        if ($real_id === false) {
            Log::warning("2505 || Kon nie 'n kommando in die flashdata vind nie! |", ['veroorsaak_deur' => Auth::user()->toArray(), "opsoek_na_skuil_id" => $id]);
            return Redirect::to('kommando/tabel')->withErrors("Kon nie die versoek voltooi nie");
        }

        Session::set('edit_kommando', $real_id);

        return View::make('kommando_edit')->with(['kommando' => Kommando::find($real_id)]);

    }


    //Deletes the Kommando currently edited in the Session
    public function getDelete()
    {
        if (Session::has('edit_kommando'))
            {
                $id = Session::get('edit_kommando');
                $kommando = Kommando::find($id);
                $kommando->delete();
                Log::notice("2520 || ".$kommando->kommando_naam." is uit die databases sag verwyder |", ['veroorsaak_deur' => Auth::user()->toArray(), 'kommando' => $kommando->toArray()]);
                return Redirect::to('kommando/tabel')->with('success', $kommando->kommando_naam." is verwyder");
            }
        else {
            Log::warning("2505 || Kon nie 'n kommando in die flashdata vind om te verwyder nie! |", ['veroorsaak_deur' => Auth::user()->toArray(), "opsoek_na_skuil_id" => $id]);
            return Redirect::to('kommando/tabel')->withErrors("Kon nie die versoek voltooi nie");
        }
        
    }

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMergeIdToPublicInskrywing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('publieke_inskrywings', function ($table) {
            $table->integer('admin_merge_persoon_id')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('publieke_inskrywings', function ($table) {
            $table->dropColumn('admin_merge_persoon_id');
        });
    }
}

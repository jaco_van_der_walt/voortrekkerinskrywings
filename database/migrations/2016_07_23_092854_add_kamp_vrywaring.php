<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKampVrywaring extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kampe', function (Blueprint $table) {
            $table->text('kamp_vrywaring')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kampe', function (Blueprint $table) {
            $table->dropColumn('kamp_vrywaring');
        });
    }
}

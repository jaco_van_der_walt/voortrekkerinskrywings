@extends('layouts.public_inskrywing')

@section('head')
@parent
<title>Inskrywing</title>

@stop


@section('content')
<aside class="right-side stretch">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                    <a href={{URL('publiek/inskrywing/terug')}} class="btn-sm btn-primary pull-left" style="margin-left: 5px; margin-bottom: 5px;"><i class="fa fa-arrow-left"></i></a> &nbsp;
                        Inskrywing: {{$kamp->kamp_naam}} - {{$persoon[0]->noemnaam}} {{$persoon[0]->van}}
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">

                    @if($errors->has())
                       @foreach ($errors->all() as $error)
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Waarskuwing!</b> {{$error}}
                            </div>
                      @endforeach
                    @endif

                    <div class="row">
                        <div class="col-md-12">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="callout callout-info">
                                    <h4>Kies Ekstras</h4>
                                    <p>{{$kamp->kamp_naam}} bied die volgende opsionele ekstras. Kies asseblief die opsies wat jy by jou inskrywing wil byvoeg.</p>
                                </div>
                            </div>
                        </div>

                        <form role="form" action="{{URL('publiek/inskrywing/ekstras')}}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        @foreach($kamp->ekstras as $ekstra)
                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title"><b>{{$ekstra->naam}}</b></h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                  <div class="col-md-12">
                                    {{$ekstra->beskrywing}}
                                  </div>
                                </div>
                                <br>
                                <div class="row">
                                  <div class="col-md-4">
                                    <div class="form-group">
                                            <select name="ekstras[]" class="form-control">
                                              <option value="null"><em>Geen</em></option>
                                              @foreach($ekstra->opsies as $opsie)
                                                <option value="{{$opsie->id}}">{{$opsie->naam}} - R{{$opsie->prys}}</option>
                                              @endforeach
                                            </select>
                                        </div>
                                  </div>
                                </div>

                            </div>
                        </div>
                        @endforeach


                        <!-- footer -->
                        <div class="box-footer col-md-12">
                            <div class="row">
                                <a href="{{URL('publiek/inskrywing/terug')}}" class="btn btn-warning pull-left" style="margin-right: 5px;"><i class="fa fa-arrow-circle-left"></i> Terug </a>
                                <button id="submit" type="submit" class="btn btn-success pull-right" style="margin-right: 5px;">Volgende <i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                        </div>
                        <!-- /footer -->
                        </form>
                    </div>




                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>


@stop

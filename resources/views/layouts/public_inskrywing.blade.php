<!DOCTYPE html>
<html>
    <head>
            @section('head')
            @include('includes.head')
            @show
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            @include('includes.public_inskrywing_navbar')
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <aside class="left-side sidebar-offcanvas">
                @include('includes.public_inskrywing_sidebar')
            </aside>


            <!-- Right side column. Contains the navbar and content of the page -->
            @yield('content')

            
        </div><!-- ./wrapper -->

        @section('plugins')
        @include('includes.plugins')
        @show

    </body>
</html>

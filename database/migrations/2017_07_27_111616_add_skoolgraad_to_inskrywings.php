<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSkoolgraadToInskrywings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('publieke_inskrywings', function ($table) {
            $table->integer('skoolgraad')->nullable();
        });

        Schema::table('inskrywings', function ($table) {
            $table->integer('skoolgraad')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('publieke_inskrywings', function ($table) {
            $table->dropColumn('skoolgraad');
        });

        Schema::table('inskrywings', function ($table) {
            $table->dropColumn('skoolgraad');
        });
    }
}

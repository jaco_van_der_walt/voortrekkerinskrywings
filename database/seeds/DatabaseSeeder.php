<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Persoon;
use App\Counter;



class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		//$this->call('UserTableSeeder');

        //$this->command->info('User table seeded!');

        //$this->call('PersoneTableSeeder');

        //$this->command->info('Persoon table seeded!');

        //$this->call('CounterTableSeeder');
        //$this->command->info("Counter table seeded!");

	}

}

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create(array('name' => 'Jaco van der Walt', 'super_admin'=> true, 'password'=>'$2y$10$Q3zcpOloQU2AVnBELzfTEep0j4Lgrd3HpYZCaciXhZD4/AsN16GlW', 'email'=>'jaco.vn.dr.walt@gmail.com'));
    }
}

class PersoneTableSeeder extends Seeder { 

	public function run()
	{
		DB::table('persone')->delete();

		Persoon::create(array('registrasie_nommer' => 'V15000','van' => 'van der Walt', 'noemnaam' => 'Jaco', 'geboorte_datum' => '1989-12-13', 'geslag' => 'm', 'betrokkenheid'=>'staatmaker'));
		Persoon::create(array('registrasie_nommer' => 'V15001','van' => 'van der Walt', 'noemnaam' => 'Nelis', 'geboorte_datum' => '1990-12-13', 'geslag' => 'm', 'betrokkenheid'=>'staatmaker'));
		Persoon::create(array('registrasie_nommer' => 'B15000','van' => 'Beinike', 'noemnaam' => 'Werner', 'geboorte_datum' => '1999-12-13', 'geslag' => 'm', 'betrokkenheid'=>'staatmaker'));
		Persoon::create(array('registrasie_nommer' => 'S15000','van' => 'Swart', 'noemnaam' => 'Christiaan', 'geboorte_datum' => '1989-01-23', 'geslag' => 'm', 'betrokkenheid'=>'staatmaker'));
		Persoon::create(array('registrasie_nommer' => 'S15001','van' => 'Stoffberg', 'noemnaam' => 'Karin', 'geboorte_datum' => '1975-08-13', 'geslag' => 'f', 'betrokkenheid'=>'offisier'));
		Persoon::create(array('registrasie_nommer' => 'V15002','van' => 'van der Merwe', 'noemnaam' => 'Kobus', 'geboorte_datum' => '1978-05-30', 'geslag' => 'm', 'betrokkenheid'=>'offisier'));
		Persoon::create(array('registrasie_nommer' => 'V15003','van' => 'van der Merwe', 'noemnaam' => 'Marilie', 'geboorte_datum' => '1987-05-15', 'geslag' => 'f', 'betrokkenheid'=>'offisier'));
		Persoon::create(array('registrasie_nommer' => 'W15000','van' => 'Wentzel', 'noemnaam' => 'Magda', 'geboorte_datum' => '1969-08-13', 'geslag' => 'f', 'betrokkenheid'=>'offisier'));
		Persoon::create(array('registrasie_nommer' => 'G15000','van' => 'Grobler', 'noemnaam' => 'Louis', 'geboorte_datum' => '1976-10-23', 'geslag' => 'm', 'betrokkenheid'=>'offisier'));
		Persoon::create(array('registrasie_nommer' => 'G15001','van' => 'Grobler', 'noemnaam' => 'Christelle', 'geboorte_datum' => '1978-05-08', 'geslag' => 'f', 'betrokkenheid'=>'ouer'));
		Persoon::create(array('registrasie_nommer' => 'V15004','van' => 'van Eden', 'noemnaam' => 'Jasper', 'geboorte_datum' => '1978-06-18', 'geslag' => 'm', 'betrokkenheid'=>'offisier'));
		Persoon::create(array('registrasie_nommer' => 'V15005','van' => 'van Eden', 'noemnaam' => 'Maria', 'geboorte_datum' => '1988-05-23', 'geslag' => 'f', 'betrokkenheid'=>'offisier'));
		Persoon::create(array('registrasie_nommer' => 'S15002','van' => 'Stroh', 'noemnaam' => 'Francois', 'geboorte_datum' => '1968-11-18', 'geslag' => 'm', 'betrokkenheid'=>'offisier'));
		Persoon::create(array('registrasie_nommer' => 'F15000','van' => 'Fourie', 'noemnaam' => 'Wiehann', 'geboorte_datum' => '1998-05-08', 'geslag' => 'm', 'betrokkenheid'=>'heemraad'));
		Persoon::create(array('registrasie_nommer' => 'F15001','van' => 'Fourie', 'noemnaam' => 'Maricelle', 'geboorte_datum' => '1989-08-18', 'geslag' => 'f', 'betrokkenheid'=>'heemraad'));
		
		
	}

}

class CounterTableSeeder extends Seeder {
	public function run()
	{
		DB::table('counters')->delete();

		Counter::create(array('alpha' => 'A', 'counter'=> 1));
		Counter::create(array('alpha' => 'B', 'counter'=> 1));
		Counter::create(array('alpha' => 'C', 'counter'=> 1));
		Counter::create(array('alpha' => 'D', 'counter'=> 1));
		Counter::create(array('alpha' => 'E', 'counter'=> 1));
		Counter::create(array('alpha' => 'F', 'counter'=> 1));
		Counter::create(array('alpha' => 'G', 'counter'=> 1));
		Counter::create(array('alpha' => 'H', 'counter'=> 1));
		Counter::create(array('alpha' => 'I', 'counter'=> 1));
		Counter::create(array('alpha' => 'J', 'counter'=> 1));
		Counter::create(array('alpha' => 'K', 'counter'=> 1));
		Counter::create(array('alpha' => 'L', 'counter'=> 1));
		Counter::create(array('alpha' => 'M', 'counter'=> 1));
		Counter::create(array('alpha' => 'N', 'counter'=> 1));
		Counter::create(array('alpha' => 'O', 'counter'=> 1));
		Counter::create(array('alpha' => 'P', 'counter'=> 1));
		Counter::create(array('alpha' => 'Q', 'counter'=> 1));
		Counter::create(array('alpha' => 'R', 'counter'=> 1));
		Counter::create(array('alpha' => 'S', 'counter'=> 1));
		Counter::create(array('alpha' => 'T', 'counter'=> 1));
		Counter::create(array('alpha' => 'U', 'counter'=> 1));
		Counter::create(array('alpha' => 'V', 'counter'=> 1));
		Counter::create(array('alpha' => 'W', 'counter'=> 1));
		Counter::create(array('alpha' => 'X', 'counter'=> 1));
		Counter::create(array('alpha' => 'Y', 'counter'=> 1));
		Counter::create(array('alpha' => 'Z', 'counter'=> 1));

	}
}

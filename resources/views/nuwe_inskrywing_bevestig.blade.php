@extends('layouts.master')

@section('head')
@parent
<title>Nuwe Inskrywing</title>
@stop


@section('content')
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><a href={{URL('inskrywing/kanseleer')}}><button type="submit" class="btn btn-primary "><i class="fa fa-arrow-circle-left"></i></button></a> Nuwe Inskrywing: {{$kamp->kamp_naam}} - {{$persoon[0]->noemnaam}} {{$persoon[0]->van}}</h1>
    </section>

    <!-- Main content -->
    <section class="content">
    @if($errors->has())
        @foreach ($errors->all() as $error)
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Waarskuwing!</b> {{$error}}
        </div>
        @endforeach
    @endif
    
    @if(Session::has('success'))
    <div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Sukses!</b> {{Session::get('success')}}
    </div>
    @endif

    <!-- Instruksies -->
    <div class="row">
        <div class="col-md-12">
            <div class="callout callout-info">
                <h4>Stap 5: Bevestig Inskrywing</h4>
                <p>Bevestig asseblief die inskrywing vir {{$persoon[0]->noemnaam}} {{$persoon[0]->van}} vir {{$kamp->kamp_naam}}.</p>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="progress">
                <div class="progress-bar progress-bar-green" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%">
                    <span class="sr-only">90% Voltooi</span>
                </div>
            </div>
        </div>  
    </div>
    <!-- Instruksies -->

    <!-- Main Row --> 
    <div class="row"> 
        <div class="col-md-12">

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><b>Persoon Inligting</b></h3>
                </div><!-- /.box-header -->
                <div class="box-body">

                    <div class="row form-group">
                        <div class="col-xs-2">
                            <label>Naam en Van</label>
                        </div>
                        <div class="col-xs-4">
                            <label>{{$persoon[0]->noemnaam}} {{$persoon[0]->van}}</label>    
                        </div>   
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-2">
                            <label>Kamp ID</label>
                        </div>
                        <div class="col-xs-4">
                            <label>{{$persoon[0]->registrasie_nommer}}</label>    
                        </div>   
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-2">
                            <label>Voortrekker Registrasie Nommer</label>
                        </div>
                        <div class="col-xs-4">
                            <label>{{$persoon[0]->voortrekker_registrasie_nommer}}</label>    
                        </div>   
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-2">
                            <label>Geslag</label>
                        </div>
                        <div class="col-xs-4">
                            <label>{{$persoon[0]->geslag}}</label>    
                        </div>   
                    </div>

                     <div class="row form-group">
                        <div class="col-xs-2">
                            <label>Geboortedatum</label>
                        </div>
                        <div class="col-xs-4">
                            <label>{{$persoon[0]->geboorte_datum}}</label>    
                        </div>   
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-2">
                            <label>Kommando</label>
                        </div>
                        <div class="col-xs-4">
                            
                            @if($persoon[0]->kommando != '')
                            <label>{{$persoon[0]->kommando->kommando_naam}}</label>
                            @endif    
                        </div>   
                    </div>


                    
                </div>
            </div>

            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><b>{{$kamp->kamp_naam}}</b></h3>
                </div><!-- /.box-header -->
                <div class="box-body">

                    <div class="row form-group">
                        <div class="col-xs-2">
                            <label>Betrokkenheid</label>
                        </div>
                        <div class="col-xs-4">
                            <label>{{$betrokkenheid->betrokkenheid}}</label>    
                        </div>   
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-2">
                            <label>Groep</label>
                        </div>
                        <div class="col-xs-4">
                            <label>{{$groep->groep_naam}}</label>    
                        </div>   
                    </div>

                    <div class="row form-group">
                        <div class="col-xs-2">
                            <label>Rol</label>
                        </div>
                        <div class="col-xs-4">
                            <label>{{$rol->rol_naam}}</label>    
                        </div>   
                    </div>
                </div>
            </div>



        
        

        

        <!-- footer --> 
		<div class="box-footer col-md-12">
			<div class="row">
				<div class="col-md-6">
                    <a href={{URL('inskrywing/kanseleer')}} class="btn btn-danger"><i class="fa fa-times"></i> Kanseleer Inskrywing</a>
                </div>
                <div class="col-md-6">
                    <a href="{{URL('inskrywing/bevestig')}}" class="btn btn-success pull-right"><i class="fa fa-check"></i> Skryf In </a>
                </div>
			</div>
		</div>
			<!-- /footer --> 
	</div>
	<!-- End Main Row -->

	</section>
</aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>

        <!-- page script -->
@stop
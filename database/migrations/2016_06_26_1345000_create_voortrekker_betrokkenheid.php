<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoortrekkerBetrokkenheid extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('voortrekker_betrokkenheid', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('betrokkenheid');
			$table->timestamps();

		});

		Artisan::call('db:seed', ['--class' => 'VoortrekkerBetrokkenheidSeeder', '--force' => true]); //Must seed else next migrations will fail because all new foreign keys will be null! 

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('voortrekker_betrokkenheid');
	}

}
@extends('layouts.public_inskrywing')

@section('head')
@parent
<title>Inskrywing</title>
@stop


@section('content')
<aside class="right-side stretch">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                        <h1>
                        <a href={{URL('publiek/inskrywing/terug')}} class="btn-sm btn-primary pull-left" style="margin-left: 5px; margin-bottom: 5px;"><i class="fa fa-arrow-left"></i></a> &nbsp;
                         Inskrywing: {{$kamp->kamp_naam}} - {{$persoon->noemnaam}} {{$persoon->van}}
                        </h1>
                </section>

                <!-- Main content -->
                <section class="content">

                    @if($errors->has())
                       @foreach ($errors->all() as $error)
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Waarskuwing!</b> {{$error}}
                            </div>
                      @endforeach
                    @endif

                    <div class="row">
                        <div class="col-md-12">


                        <div class="row">
                            <div class="col-md-12">
                                <div class="callout callout-info">
                                    <h4>Bevestig Inskrywing</h4>
                                    <p>Bevestig asseblief dat die inskrywing vir <strong>{{$persoon->noemnaam}} {{$persoon->van}}</strong> korrek is vir {{$kamp->kamp_naam}}.</p>
                                    <p><i>Hierdie is slegs 'n oorsig van die Inskrywing en al die velde word nie vertoon nie.</i></p>
                                </div>
                            </div>
                        </div>


                        <div class="box box-primary">
                        <div class="box-header">
                           <h4 class="box-title">Persoonlike Inligting</h4>
                        </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-xs-2">
                                            <label><strong>Naam en Van</strong></label>
                                        </div>
                                            <div class="col-xs-10">
                                                <p>{{$persoon->noemnaam}} {{$persoon->van}}</p>
                                            </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-2">
                                            <label><strong>Geboortedatum</strong></label>
                                        </div>
                                            <div class="col-xs-10">
                                                <p>{{$persoon->geboorte_datum}}</p>
                                            </div>
                                    </div>

                                    @if((Session::get('publiek_inskrywing_soort') === 'volwassene') && ($persoon->id_nommer != ''))
                                    <div class="row">
                                        <div class="col-xs-2">
                                            <label><strong>ID Nommer</strong></label>
                                        </div>
                                            <div class="col-xs-10">
                                                <p>{{$persoon->id_nommer}}</p>
                                            </div>
                                    </div>
                                    @endif

                                    @if($persoon->kontak_nommer_1 != '')
                                    <div class="row">
                                        <div class="col-xs-2">
                                            <label><strong>Kontak Nommer</strong></label>
                                        </div>
                                            <div class="col-xs-10">
                                                <p>{{$persoon->kontak_nommer_1}}</p>
                                            </div>
                                    </div>
                                    @endif

                                    @if($persoon->epos_adres != '')
                                    <div class="row">
                                        <div class="col-xs-2">
                                            <label><strong>Epos Adres</strong></label>
                                        </div>
                                            <div class="col-xs-10">
                                                <p>{{$persoon->epos_adres}}</p>
                                            </div>
                                    </div>
                                    @endif

                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="box box-primary">
                        <div class="box-header">
                           <h4 class="box-title">Die Voortrekkers</h4>
                        </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">

                                    @if($persoon->voortrekker_registrasie_nommer != '')
                                    <div class="row">
                                        <div class="col-xs-2">
                                            <label><strong>Voortrekker Registrasie Nommer</strong></label>
                                        </div>
                                            <div class="col-xs-10">
                                                <p>{{$persoon->voortrekker_registrasie_nommer}}</p>
                                            </div>
                                    </div>
                                    @endif

                                    @if($kommando != '')
                                    <div class="row">
                                        <div class="col-xs-2">
                                            <label><strong>Kommando</strong></label>
                                        </div>
                                            <div class="col-xs-10">
                                                <p>{{$kommando}}</p>
                                            </div>
                                    </div>
                                    @endif

                                    </div>
                                </div>
                            </div>
                        </div>

                        @if(Session::get('publiek_inskrywing_soort') === 'jeuglid')
                        <div class="box box-primary">
                        <div class="box-header">
                           <h4 class="box-title">Ouer of Voog</h4>
                        </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">

                                    <div class="row">
                                        <div class="col-xs-2">
                                            <label><strong>Naam en Van</strong></label>
                                        </div>
                                            <div class="col-xs-10">
                                                <p>{{$persoon->ouer_voog_naam_van_1}}</p>
                                            </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-2">
                                            <label><strong>Kontak Nommer</strong></label>
                                        </div>
                                            <div class="col-xs-10">
                                                <p>{{$persoon->ouer_voog_kontak_nommer_1}}</p>
                                            </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-2">
                                            <label><strong>Epos Adres</strong></label>
                                        </div>
                                            <div class="col-xs-10">
                                                <p>{{$persoon->ouer_voog_epos_adres_1}}</p>
                                            </div>
                                    </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif

                        <div class="box box-primary">
                        <div class="box-header">
                           <h4 class="box-title">Mediese Besonderhede</h4>
                        </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">

                                    <div class="row">
                                        <div class="col-xs-2">
                                            <label><strong>Mediese Fonds</strong></label>
                                        </div>
                                            <div class="col-xs-10">
                                                <p>{{$persoon->mediese_fonds_naam}}</p>
                                            </div>
                                    </div>

                                    @if($persoon->mediese_fonds_lid_nommer != '')
                                    <div class="row">
                                        <div class="col-xs-2">
                                            <label><strong>Mediese Fonds Lid Nommer</strong></label>
                                        </div>
                                            <div class="col-xs-10">
                                                <p>{{$persoon->mediese_fonds_lid_nommer}}</p>
                                            </div>
                                    </div>
                                    @endif

                                    <div class="row">
                                        <div class="col-xs-2">
                                            <label><strong>Siekte/Allergie</strong></label>
                                        </div>
                                            <div class="col-xs-10">
                                                <p>{{$persoon->siekte_allergie}}</p>
                                            </div>
                                    </div>


                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box box-warning">
                        <div class="box-header">
                           <h4 class="box-title">{{$kamp->kamp_naam}}</h4>
                        </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">

                                    <div class="row">
                                        <div class="col-xs-3">
                                            <label><strong>Voortrekker Betrokkenheid</strong></label>
                                        </div>
                                            <div class="col-xs-9">
                                                <p>{{$betrokkenheid->betrokkenheid}}</p>
                                            </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-3">
                                            <label><strong>Kursus, Divisie of Groep</strong></label>
                                        </div>
                                            <div class="col-xs-9">
                                                <p>{{$groep->groep_naam}}</p>
                                            </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-3">
                                            <label><strong>Vrywaring Geteken Deur</strong></label>
                                        </div>
                                            <div class="col-xs-9">
                                                <p>{{$persoon->vrywaring_geteken_deur}} ({{$persoon->vrywaring_geteken_deur_id}})</p>
                                            </div>
                                    </div>

                                    @if($kamp->ekstras->count() > 0)
                                    @foreach($persoon->opsies as $o)
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <label><strong>{{$o->ekstra->naam}}</strong></label>
                                        </div>
                                            <div class="col-xs-9">
                                              <p>{{$o->naam}} (R{{$o->prys}}.00)</p>
                                            </div>
                                    </div>
                                    @endforeach
                                    @endif

                                    <div class="row">
                                        <div class="col-xs-3">
                                            <label><strong>Totaal Inskrywingskoste</strong></label>
                                        </div>
                                            <div class="col-xs-9">
                                              <p><strong>R{{$persoon->koste}}.00</strong></p>
                                            </div>
                                    </div>

                                    </div>
                                </div>
                            </div>
                        </div>



                        <!-- footer -->
                        <div class="box-footer col-md-12">
                            <div class="row">
                                    <a href="{{URL('publiek/inskrywing/terug')}}" class="btn btn-warning pull-left" style="margin-right: 5px;"><i class="fa fa-arrow-circle-left"></i> Terug </a>
                                     <a href={{URL('publiek/inskrywing/bevestiginskrywing')}} class="btn btn-success pull-right" style="margin-right: 5px;"><i class="fa fa-paper-plane-o"></i> &nbsp  Voltooi Inskrywing </a>
                            </div>
                        </div>
                        <!-- /footer -->
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>


@stop

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKampVrywaringJeugVolwasse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kampe', function (Blueprint $table) {
            $table->text('kamp_vrywaring_jeug')->nullable();
            $table->renameColumn('kamp_vrywaring', 'kamp_vrywaring_volwasse');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kampe', function (Blueprint $table) {
            $table->dropColumn('kamp_vrywaring_jeug');
            $table->renameColumn('kamp_vrywaring_volwasse', 'kamp_vrywaring');
        });
    }
}

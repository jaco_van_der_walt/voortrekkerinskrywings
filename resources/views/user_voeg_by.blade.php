@extends('layouts.master') @section('head') @parent
<title>Stelsel Administrateur</title>@stop @section('content')
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <a href={{URL('superadmin')}}><button type="submit" class="btn btn-primary "><i class="fa fa-arrow-circle-left"></i></button></a> Voeg Stelsel Gebruiker By
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        @if($errors->has()) 
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Waarskuwing!</b> {{$error}}
                </div>
            @endforeach 
        @endif
        @if(Session::has('success'))
            <div class="alert alert-success alert-dismissable">
	            <i class="fa fa-check"></i>
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	            <b>Sukses!</b> {{Session::get('success')}}
            </div>
        @endif
        <!-- Small boxes (Stat box) -->
		<div class="row">
			<div class="col-md-12">
				<!-- form start -->
				<form role="form" method="POST" action="{{ url('users/voegby/nuut') }}">
					<!-- general form elements -->
					<div class="box box-primary">
						<div class="box-header">
							<h3 class="box-title"><b>Voeg Stelsel Gebruiker By</b></h3>
						</div><!-- /.box-header -->

						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="box-body">

							<!-- Naam en Van -->
							<div class="row form-group">
								<div class="col-xs-2">
									<label>Naam en Van <span>*</span></label>
								</div>
								<div class="col-xs-4">
									<input type="text" class="form-control" name="name" value="{{Input::old('name')}}" required> 
								</div>   
							</div>

							<!-- Epos -->
							<div class="row form-group">
								<div class="col-xs-2">
									<label>Epos Address <span>*</span></label>
								</div>
								<div class="col-xs-4">
									<input type="text" class="form-control" name="email" value="{{Input::old('email')}}" required> 
								</div>   
							</div>

							<!-- Wagwoord -->
							<div class="row form-group">
								<div class="col-xs-2">
									<label>Wagwoord <span>*</span></label>
								</div>
								<div class="col-xs-4">
									<input type="password" class="form-control" name="password" required> 
								</div>   
							</div>

							<!-- Wagwoord Confirmation -->
							<div class="row form-group">
								<div class="col-xs-2">
									<label>Hertik Wagwoord <span>*</span></label>
								</div>
								<div class="col-xs-4">
									<input type="password" class="form-control" name="password_confirmation" required> 
								</div>
							</div>

							<div class="row form-group">
				                <div class="footer col-xs-6">                    
				                    <button type="submit" class="btn bg-olive btn-block">Skep Nuwe Gebruiker</button>
				                </div>
			                </div>

						</div>
					</div>
				</form>
			</div>
		</div>
    </div><!-- /.box --> 
        <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
</aside>
<!-- /.right-side -->@stop @section('plugins') @parent
<!-- AdminLTE App -->
<script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>
<!-- InputMask -->
<script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.date.extensions.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.extensions.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    //Datemask
    $("#datemask").inputmask("yyyy/mm/dd", {
        "placeholder": "yyyy/mm/dd"
    });
</script>@stop
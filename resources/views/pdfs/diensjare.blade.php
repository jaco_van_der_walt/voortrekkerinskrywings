<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<style>
			table, td, th {
			    border: 1px solid black;
			    width: 100%;
			    border-collapse: collapse;
			    padding: 5px;
			    word-wrap:break-word;
			}

			.heading {
				text-align: left;
			}

			body { 
				font-family: DejaVu Sans, sans-serif;
				font-size:14px; 
			}
		</style>
	</head>
	<body>
		<div>
		<h2>Diensjare: {{$kamp->kamp_naam}}</h2>
			<table>
				<tr> 
					<td class="heading">
						<b>Kamp ID</b>
					</td>
					<td class="heading">
						<b>Voortrekker Nr.</b>
					</td>
					<td class="heading">
						<b>Van</b>
					</td>
					<td class="heading">
						<b>Naam</b>
					</td>
					<td class="heading">
						<b>Betrokkenheid</b>
					</td>
					<td class="heading">
						<b>Kommando</b>
					</td>
					<td class="heading">
						<b>Diensjare</b>
					</td>
				</tr>
				@foreach($persone as $key => $value)
					<tr>
						<td >
							{{$persone[$key]['persoon']->registrasie_nommer}}
						</td>
						<td >
							{{$persone[$key]['persoon']->voortrekker_registrasie_nommer}}
						</td>
						<td>
							{{$persone[$key]['persoon']->van}}
						</td>
						<td>
						{{$persone[$key]['persoon']->noemnaam}}
						</td>
						<td>
						<?php echo ucfirst($persone[$key]['betrokkenheid']) ?>
						</td>
						<td>
							@if($persone[$key]['persoon']->kommando)
								{{$persone[$key]['persoon']->kommando->kommando_naam}}
							@endif
						</td>
						<td>
							{{$persone[$key]['diensjare']}}
						</td>
					</tr>	
				@endforeach		
			</table>
		</div>
	</body>
</html>
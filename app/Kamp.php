<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\KampStatus;
use App\Organisasie;
use Log;

class Kamp extends Model {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'kampe';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

    protected $guarded = ['id'];

    protected $hidden = ['created_at', 'updated_at'];

    public function groepe()
    {
    	return $this->hasMany('App\Groep');
    }

    public function rolle()
    {
        return $this->hasManyThrough(
            'App\Rol', 'App\Groep',
            'kamp_id', 'groep_id', 'id'
        );
    }

    //This is a Mutator that will automatically look up the Kamp Status and not just return the id..
    public function getStatusAttribute($value)
    {
        $status = KampStatus::find($value);
        return ucfirst($status->status);
    }

    public function organisasies()
    {
        return $this->belongsToMany('App\Organisasie', 'kampe_organisasies', 'kamp_id', 'organisasie_id');
     }

		 public function ekstras()
     {
        return $this->hasMany('App\Ekstra');
     }

}

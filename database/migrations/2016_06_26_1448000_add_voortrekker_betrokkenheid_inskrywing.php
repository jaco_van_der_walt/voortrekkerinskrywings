<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVoortrekkerBetrokkenheidInskrywing extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{


		//If this fails ensure that voortrekker_betrokkenheid is properly seeded with at least one row that has the id of '1' as set as default! 
		Schema::table('inskrywings', function ($table) {
		    $table->integer('voortrekker_betrokkenheid_id')->unsigned()->default(1);
		    $table->foreign('voortrekker_betrokkenheid_id')->references('id')->on('voortrekker_betrokkenheid');
		});

		


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('inskrywings', function ($table) {
		    $table->dropColumn('voortrekker_betrokkenheid_id');
		});
	}

}
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPubliekeInskrywingAddInskrywingVoltooi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('publieke_inskrywings', function ($table) {
            $table->boolean('inskrywing_voltooi')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('publieke_inskrywings', function ($table) {
            $table->dropColumn('inskrywing_voltooi');
        });
    }
}

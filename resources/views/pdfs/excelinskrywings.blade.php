<table>
    <tbody>
    <tr>
        <td><b>Verwysing</b></td>
        <td><b>Groep</b></td>
        <td><b>Rol</b></td>
        <td><b>Van</b></td>
        <td><b>Noemnaam</b></td>
        <td><b>Voorname</b></td>
        <td><b>Skoolgraad</b></td>
        <td><b>Geboortedatum</b></td>
        <td><b>Geslag</b></td>
        <td><b>ID Nommer</b></td>
        <td><b>Kontak Nommer</b></td>
        <td><b>Alternatiewe Kontak Nommer</b></td>
        <td><b>E-posadres</b></td>
        <td><b>Voertuig Registrasie Nommer</b></td>
        <td><b>VRN</b></td>
        <td><b>Voortrekker Betrokkenheid</b></td>
        <td><b>Kommando</b></td>
        <td><b>Ouer/Voog #1</b></td>
        <td><b>Ouer/Voog #1 ID Nommer</b></td>
        <td><b>Ouer/Voog #1 Kontak Nommer</b></td>
        <td><b>Ouer/Voog #1 E-posadres</b></td>
        <td><b>Ouer/Voog #2</b></td>
        <td><b>Ouer/Voog #2 ID Nommer</b></td>
        <td><b>Ouer/Voog #2 Kontak Nommer</b></td>
        <td><b>Ouer/Voog #2 E-posadres</b></td>
        <td><b>Posadres</b></td>
        <td><b>Woonadres</b></td>
        <td><b>Mediese Fonds Naam</b></td>
        <td><b>Mediese Fonds Lidmaatskapnommer</b></td>
        <td><b>Huisdokter</b></td>
        <td><b>Huisdokter Kontak Nommer</b></td>
        <td><b>Siekte/Allergië</b></td>
        <td><b>Groep Koste</b></td>
        <td><b>Ekstra Kostes</b></td>
        <td><b>Berekende Kampkoste</b></td>
        <td><b>Nota</b></td>
        @foreach($kamp->ekstras as $e)
        <td><b>{{$e->naam}}</b></td>
        @endforeach
    </tr>
    @foreach($inskrywings as $i)
    <tr>
        <td>{{$i->verwysing}}</td>
        @foreach($i->persoon->rolle as $rol)
            @if($rol->groep->kamp_id == $kamp_id)
                <td>{{$rol->groep->groep_naam}}</td>
                <td>{{$rol->rol_naam}}</td>
            @endif
        @endforeach
        <td>{{$i->persoon->van}}</td>
        <td>{{$i->persoon->noemnaam}}</td>
        <td>{{$i->persoon->voorname}}</td>
        <td>{{$i->skoolgraad}}</td>
        <td>{{$i->persoon->geboorte_datum}}</td>
        <td>{{$i->persoon->geslag}}</td>
        <td>{{$i->persoon->id_nommer}}</td>
        <td>{{$i->persoon->kontak_nommer_1}}</td>
        <td>{{$i->persoon->kontak_nommer_2}}</td>
        <td>{{$i->persoon->epos_adres}}</td>
        <td>{{$i->persoon->voertuig_registrasie_nommer}}</td>
        <td>{{$i->persoon->voortrekker_registrasie_nommer}}</td>
        <td>{{$i->voortrekkerBetrokkenheid->betrokkenheid}}</td>
        @if($i->persoon->kommando)
            <td>{{$i->persoon->kommando->kommando_naam}}</td>
        @else
            <td></td>
        @endif
        <td>{{$i->persoon->ouer_voog_naam_van_1}}</td>
        <td>{{$i->persoon->ouer_voog_id_nommer_1}}</td>
        <td>{{$i->persoon->ouer_voog_kontak_nommer_1}}</td>
        <td>{{$i->persoon->ouer_voog_epos_adres_1}}</td>
        <td>{{$i->persoon->ouer_voog_naam_van_2}}</td>
        <td>{{$i->persoon->ouer_voog_id_nommer_2}}</td>
        <td>{{$i->persoon->ouer_voog_kontak_nommer_2}}</td>
        <td>{{$i->persoon->ouer_voog_epos_adres_2}}</td>
        <td>{{$i->persoon->posadres}}</td>
        <td>{{$i->persoon->woonadres}}</td>
        <td>{{$i->persoon->mediese_fonds_naam}}</td>
        <td>{{$i->persoon->mediese_fonds_lid_nommer}}</td>
        <td>{{$i->persoon->huis_dokter_naam_van}}</td>
        <td>{{$i->persoon->huis_dokter_kontak_nommer_1}}</td>
        <td>{{$i->persoon->siekte_allergie}}</td>

        <?php $koste = 0; ?>
        @foreach($i->persoon->rolle as $rol)
            @if($rol->groep->kamp_id == $kamp_id)
                <?php $koste = $koste + $rol->inskrywing_koste; ?>
                <td>{{$rol->inskrywing_koste}}</td>
            @endif
        @endforeach
          <?php $ekstra = 0; ?>
        @foreach($i->opsies as $o)
          <?php $koste = $koste + $o->prys; ?>
          <?php $ekstra = $ekstra + $o->prys; ?>
        @endforeach
        <td>{{$ekstra}}</td>
        <td>{{$koste}}</td>

        <td>{{$i->nota}}</td>

        @foreach($kamp->ekstras as $e)
          <?php $flag = 'false'; ?>
          @foreach($i->opsies as $o)
            @if($o->ekstra->id == $e->id)
              <td>{{$o->naam}}</td>
              <?php $flag = 'true'; ?>
            @endif
          @endforeach
          @if($flag == 'false')
            <td> </td>
          @endif
        @endforeach


    </tr>
    @endforeach
    </tbody>
</table>

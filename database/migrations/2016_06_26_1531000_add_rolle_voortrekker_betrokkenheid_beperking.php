<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRolleVoortrekkerBetrokkenheidBeperking extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::table('rolle', function ($table) {
		    $table->integer('inskrywing_beperking')->unsigned();
		    $table->float('inskrywing_koste');
		});

		Schema::create('rolle_betrokkenheid', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('rolle_id')->unsigned();
			$table->integer('voortrekker_betrokkenheid_id')->unsigned();
			$table->timestamps();

			$table->foreign('rolle_id')->references('id')->on('rolle');
			$table->foreign('voortrekker_betrokkenheid_id')->references('id')->on('voortrekker_betrokkenheid');

		});


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rolle', function ($table) {
		    $table->dropColumn('inskrywing_beperking');
		    $table->dropColumn('inskrywing_koste');
		});

		Schema::drop('rolle_betrokkenheid');
	}

}
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrganisasieAddNaamkaartjieHeaderImg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('organisasies', function ($table) {
            $table->string('naamkaartjie_header_img')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('organisasies', function ($table) {
            $table->dropColumn('naamkaartjie_header_img');
        });
    }
}

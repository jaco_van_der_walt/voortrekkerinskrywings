@extends('layouts.public_inskrywing')

@section('head')
@parent
<title>Inskrywing</title>



@stop


@section('content')
<aside class="right-side stretch">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                    <a href={{URL('publiek/inskrywing/kanseleer')}} class="btn-sm btn-primary pull-left" style="margin-left: 5px; margin-bottom: 5px;"><i class="fa fa-arrow-left"></i></a> &nbsp;
                        Inskrywing: {{$kamp->kamp_naam}}
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">

                    @if($errors->has())
                       @foreach ($errors->all() as $error)
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Waarskuwing!</b> {{$error}}
                            </div>
                      @endforeach
                    @endif

                    <div class="row">
                        <div class="col-md-12">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="callout callout-info">
                                    <h4>Inskrywingsvorm</h4>
                                    <p>Voltooi asseblief hierdie inskrywingsvorm</p>
                                </div>
                            </div>
                        </div>

                        <!-- Form Start -->
                        <form role="form" action="{{URL('publiek/inskrywing/persoon/data')}}" method="POST">

                        <!-- Main Row --> 
                        <div class="row"> 
                            <div class="col-md-12"> 

                                <!-- Persoonlike Inligting -->
                                <div class="box box-primary">
                                    <div class="box-header">
                                        <h3 class="box-title"><b>Persoonlike Inligting van Jeuglid</b></h3>
                                    </div><!-- /.box-header -->
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="box-body">

                                        <!-- Van -->
                                        <div class="row form-group">
                                            <div class="col-xs-3">
                                                <label>Van <span>*</span></label>
                                            </div>
                                            <div class="col-xs-6">
                                            @if(Input::old('van'))
                                                <input type="text" class="form-control" name="van" value="{{{Input::old('van')}}}"> 
                                            @elseif($persoon)
                                                <input type="text" class="form-control" name="van" value="{{$persoon[0]->van}}">
                                            @else
                                                <input type="text" class="form-control" name="van" value="">
                                            @endif
                                            </div>   
                                        </div>
                                        
                                        <!-- Voorname -->
                                        <div class="row form-group">
                                            <div class="col-xs-3">
                                                <label>Voorname</label>
                                            </div>
                                            <div class="col-xs-6">
                                            @if(Input::old('voorname'))
                                                <input type="text" class="form-control" name="voorname" value="{{{Input::old('voorname')}}}" > 
                                            @elseif($persoon)
                                                <input type="text" class="form-control" name="voorname" value="{{$persoon[0]->voorname}}" > 
                                            @else
                                                <input type="text" class="form-control" name="voorname" value="" > 
                                            @endif
                                            </div>   
                                        </div>

                                        <!-- Noemnaam -->
                                        <div class="row form-group">
                                            <div class="col-xs-3">
                                                <label>Noemnaam <span>*</span></label>
                                            </div>
                                            <div class="col-xs-6">
                                            @if(Input::old('noemnaam'))
                                                <input type="text" class="form-control" name="noemnaam" value="{{{Input::old('noemnaam')}}}" > 
                                            @elseif($persoon)
                                                <input type="text" class="form-control" name="noemnaam" value="{{$persoon[0]->noemnaam}}" > 
                                            @else
                                                <input type="text" class="form-control" name="noemnaam" value="">
                                            @endif
                                            </div>   
                                        </div>

                                        <!-- Date dd/mm/yyyy -->
                                        <div class="row form-group">
                                            <div class="col-xs-3">
                                                <label>Geboortedatum <span>*</span></label>
                                            </div>
                                            <div class="col-xs-6">
                                                <input id="datemask" name="geboorte_datum" type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask value="{{Session::get('publiek_inskrywing_geboortedatum')}}" disabled/>
                                                <input name="geboorte_datum" value="{{Session::get('publiek_inskrywing_geboortedatum')}}" hidden/>

                                            </div>
                                        </div>

                                        <!-- Geslag -->
                                        <div class=" row form-group"> 
                                            <div class="col-xs-3">
                                                <label>Geslag <span>*</span></label>
                                            </div>    
                                            <div class="col-xs-3">
                                                <label>
                                                @if(Input::old('geslag'))
                                                    @if(Input::old('geslag') === 'm')
                                                        <input type="radio" name="geslag" id="optionsRadios1" value="m" checked>
                                                    @else
                                                        <input type="radio" name="geslag" id="optionsRadios1" value="m">
                                                    @endif
                                                @elseif($persoon)
                                                    @if($persoon[0]->geslag === 'm')
                                                        <input type="radio" name="geslag" id="optionsRadios1" value="m" checked>
                                                    @else
                                                        <input type="radio" name="geslag" id="optionsRadios1" value="m">
                                                    @endif
                                                @else
                                                        <input type="radio" name="geslag" id="optionsRadios1" value="m">
                                                @endif
                                                    Manlik
                                                </label>
                                            </div>
                                            <div class="col-xs-3">
                                                <label>
                                                @if(Input::old('geslag'))
                                                    @if(Input::old('geslag') === 'f')
                                                        <input type="radio" name="geslag" id="optionsRadios2" value="f" checked>
                                                    @elseif($persoon)
                                                        @if($persoon[0]->geslag === 'f')
                                                            <input type="radio" name="geslag" id="optionsRadios2" value="f" checked>
                                                        @else
                                                            <input type="radio" name="geslag" id="optionsRadios2" value="f">
                                                        @endif
                                                    @else
                                                        <input type="radio" name="geslag" id="optionsRadios2" value="f">
                                                    @endif
                                                @else
                                                        <input type="radio" name="geslag" id="optionsRadios2" value="f">
                                                @endif
                                                    Vroulik
                                                </label>
                                            </div>
                                        </div>

                                        <!-- Kontak Nommers -->
                                        <div class="row form-group">
                                            <div class="col-xs-3">
                                                <label>Kontak Nommer</label>
                                            </div>
                                            <div class="col-xs-6">
                                            @if(Input::old('kontak_nommer_1'))
                                                <input type="text" class="form-control" name="kontak_nommer_1" value="{{{Input::old('kontak_nommer_1')}}}">
                                            @elseif($persoon)
                                                <input type="text" class="form-control" name="kontak_nommer_1" value="{{$persoon[0]->kontak_nommer_1}}">
                                            @else
                                                <input type="text" class="form-control" name="kontak_nommer_1" value="">
                                            @endif  
                                            </div>    
                                        </div>

                                        <!-- Epos Adres -->
                                        <div class="row form-group">
                                            <div class="col-xs-3">
                                                <label>Epos Adres <span>*</span></label>
                                            </div>
                                            <div class="col-xs-6">
                                            @if(Input::old('epos_adres'))
                                                <input type="text" class="form-control" name="epos_adres" value="{{{Input::old('epos_adres')}}}" > 
                                            @elseif($persoon)
                                                <input type="text" class="form-control" name="epos_adres" value="{{$persoon[0]->epos_adres}}" > 
                                            @else
                                                <input type="text" class="form-control" name="epos_adres" value="" > 
                                            @endif
                                            </div>   
                                        </div>

                                        <!-- Adres -->
                                        <div class="row form-group">


                                            <div class="col-xs-3">
                                                <label>Woonadres</label>
                                            </div>
                                             <div class="col-xs-3">
                                            @if(Input::old('woonadres'))
                                                <textarea class="form-control" rows="4" name="woonadres">{{{Input::old('woonadres')}}}</textarea>
                                            @elseif($persoon)
                                                <textarea class="form-control" rows="4" name="woonadres">{{$persoon[0]->woonadres}}</textarea>
                                            @else
                                                <textarea class="form-control" rows="4" name="woonadres"></textarea>
                                            @endif
                                            </div>


                                            <div class="col-xs-3">
                                                <label>Posadres</label>
                                            </div>
                                            <div class="col-xs-3">

                                            @if(Input::old('posadres'))
                                                <textarea class="form-control" rows="4" name="posadres">{{{Input::old('posadres')}}}</textarea>
                                            @elseif($persoon)
                                                <textarea class="form-control" rows="4" name="posadres">{{$persoon[0]->posadres}}</textarea>
                                            @else
                                                <textarea class="form-control" rows="4" name="posadres"></textarea>
                                            @endif

                                            </div>
                                        </div>      
                                    </div>
                                </div> 

                                <!-- Die Voortrekkers --> 
                                <div class="box box-primary">
                                    <div class="box-header">
                                        <h3 class="box-title"><b>Die Voortrekkers</b></h3>
                                    </div><!-- /.box-header -->
                                    
                                    <div class="box-body"> 
                                        <div class="row form-group">
                                            <!-- Gerigistreerde Voortrekker? -->
                                            <div class="col-xs-3">
                                                <label>Geregistreerde Voortrekker?</label>
                                            </div>

                                            <div class="col-xs-6">
                                                <label>
                                                @if(Input::old('geregistreerde_voortrekker'))
                                                    @if(Input::old('geregistreerde_voortrekker') === 'on')
                                                    <input type="checkbox" checked="true" name="geregistreerde_voortrekker" checked/>
                                                    @else
                                                    <input type="checkbox" checked="true" name="geregistreerde_voortrekker"/>
                                                    @endif
                                                @elseif($persoon)
                                                    @if($persoon[0]->voortrekker == '1')
                                                        <input type="checkbox" checked="true" name="geregistreerde_voortrekker" checked/>
                                                    @else
                                                        <input type="checkbox" checked="true" name="geregistreerde_voortrekker"/>
                                                    @endif
                                                @else
                                                    <input type="checkbox" name="geregistreerde_voortrekker"/>
                                                @endif
                                                Ja </label>                                                
                                            </div>
                                        </div>

                                        <!-- Voortrekker Registrasie Nommer -->
                                        <div class="row form-group">
                                            <div class="col-xs-3">
                                                <label>Voortrekker Registrasie Nommer</label>
                                            </div>
                                            <div class="col-xs-6">
                                                @if(Input::old('voortrekker_registrasie_nommer'))
                                                    <input type="text" class="form-control" name="voortrekker_registrasie_nommer" value="{{{Input::old('voortrekker_registrasie_nommer')}}}" disabled>
                                                    <input name="voortrekker_registrasie_nommer" value="{{{Input::old('voortrekker_registrasie_nommer')}}}" hidden/>
                                                @elseif(Session::get('publiek_inskrywing_vrn') != null)
                                                    <input type="text" class="form-control" name="voortrekker_registrasie_nommer" value="{{Session::get('publiek_inskrywing_vrn')}}" disabled>
                                                    <input name="voortrekker_registrasie_nommer" value="{{Session::get('publiek_inskrywing_vrn')}}" hidden/>
                                                @else
                                                    <input type="text" class="form-control" name="voortrekker_registrasie_nommer" value="" > 
                                                @endif
                                            </div>   
                                        </div>            

                                        <!-- Kommando-->
                                        <div class="row form-group">
                                            <div class="col-xs-3">
                                                <label>Kommando</label>
                                            </div>
                                            <div class="col-xs-6">
                                                <select name="kommando" class="form-control">
                                                    <option></option>
                                                    @foreach($kommando as $k)
                                                        @if(Input::old('kommando') == $k->id)
                                                            <option value="{{{$k->id}}}" selected>{{{$k->kommando_naam}}}</option>
                                                        @elseif(($persoon) && ($persoon[0]->kommando))
                                                            @if($persoon[0]->kommando->id == $k->id)
                                                            <option value="{{{$k->id}}}" selected>{{{$k->kommando_naam}}}</option>
                                                            @else
                                                            <option value="{{{$k->id}}}">{{{$k->kommando_naam}}}</option>
                                                            @endif
                                                        @else
                                                            <option value="{{{$k->id}}}">{{{$k->kommando_naam}}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>  
                                        </div>

                                        <!-- Skoolgraad -->
                                        <div class="row form-group">
                                            <div class="col-xs-3">
                                                <label>Huidige Skoolgraad</label>
                                            </div>
                                            <div class="col-xs-6">
                                                <select name="skoolgraad" class="form-control">
                                                    <option value="voorskool">Voorskool</option>
                                                    <option value="r">Graad R</option>
                                                    <option value="1">Graad 1</option>
                                                    <option value="2">Graad 2</option>
                                                    <option value="3">Graad 3</option>
                                                    <option value="4">Graad 4</option>
                                                    <option value="5">Graad 5</option>
                                                    <option value="6">Graad 6</option>
                                                    <option value="7">Graad 7</option>
                                                    <option value="8">Graad 8</option>
                                                    <option value="9">Graad 9</option>
                                                    <option value="10">Graad 10</option>
                                                    <option value="11">Graad 11</option>
                                                    <option value="12">Graad 12</option>
                                                </select>
                                            </div>  
                                        </div>
                                    </div>
                                </div>

                                <!-- Ouers/Voog --> 
                                <div class="box box-primary">
                                    <div class="box-header">
                                        <h3 class="box-title"><b>Ouers/Voog</b></h3>
                                    </div><!-- /.box-header -->
                                    <div class="box-body">
                                        <!-- Ouer/Voog #1 -->
                                        <h4><b>Ouer/Voog #1</b></h4>
                                        <div class="row form-group">
                                            <div class="col-xs-3">
                                                <label>Ouer/Voog Naam & Van</label>
                                            </div>
                                            <div class="col-xs-6">
                                                @if(Input::old('ouer_voog_naam_van_1'))
                                                <input type="text" class="form-control" name="ouer_voog_naam_van_1" value="{{{Input::old('ouer_voog_naam_van_1')}}}"> 
                                                @elseif($persoon)
                                                <input type="text" class="form-control" name="ouer_voog_naam_van_1" value="{{{$persoon[0]->ouer_voog_naam_van_1}}}">
                                                @else 
                                                <input type="text" class="form-control" name="ouer_voog_naam_van_1" value=""> 
                                                @endif
                                            </div>       
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-xs-3">
                                                <label>Ouer/Voog ID Nommer</label>
                                            </div>
                                            <div class="col-xs-6">
                                                @if(Input::old('ouer_voog_id_nommer_1'))
                                                <input type="text" class="form-control" name="ouer_voog_id_nommer_1" value="{{{Input::old('ouer_voog_id_nommer_1')}}}"> 
                                                @elseif($persoon)
                                                <input type="text" class="form-control" name="ouer_voog_id_nommer_1" value="{{{$persoon[0]->ouer_voog_id_nommer_1}}}">
                                                @else
                                                <input type="text" class="form-control" name="ouer_voog_id_nommer_1" value="">
                                                @endif
                                            </div>       
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-xs-3">
                                                <label>Ouer/Voog Kontak Nommer</label>
                                            </div>
                                            <div class="col-xs-3">
                                                @if(Input::old('ouer_voog_kontak_nommer_1'))
                                                <input type="text" class="form-control" name="ouer_voog_kontak_nommer_1" value="{{{Input::old('ouer_voog_kontak_nommer_1')}}}">
                                                @elseif($persoon)
                                                <input type="text" class="form-control" name="ouer_voog_kontak_nommer_1" value="{{{$persoon[0]->ouer_voog_kontak_nommer_1}}}">
                                                @else
                                                 <input type="text" class="form-control" name="ouer_voog_kontak_nommer_1" value="">
                                                @endif
                                            </div>

                                            <div class="col-xs-3">
                                                <label>Ouer/Voog Epos Adres</label>
                                            </div>
                                            <div class="col-xs-3">
                                                @if(Input::old('ouer_voog_epos_adres_1'))
                                                <input type="text" class="form-control" name="ouer_voog_epos_adres_1" value="{{{Input::old('ouer_voog_epos_adres_1')}}}"> 
                                                @elseif($persoon)
                                                <input type="text" class="form-control" name="ouer_voog_epos_adres_1" value="{{{$persoon[0]->ouer_voog_epos_adres_1}}}">
                                                @else
                                                <input type="text" class="form-control" name="ouer_voog_epos_adres_1" value="">
                                                @endif
                                            </div>         
                                        </div>

                                        <!-- Ouer/Voog #2 -->
                                        <h4><b>Ouer/Voog #2</b></h4>
                                        <div class="row form-group">
                                            <div class="col-xs-3">
                                                <label>Ouer/Voog Naam & Van</label>
                                            </div>
                                            <div class="col-xs-6">
                                                @if(Input::old('ouer_voog_naam_van_2'))
                                                <input type="text" class="form-control" name="ouer_voog_naam_van_2" value="{{{Input::old('ouer_voog_naam_van_2')}}}">
                                                @elseif($persoon)
                                                <input type="text" class="form-control" name="ouer_voog_naam_van_2" value="{{{$persoon[0]->ouer_voog_naam_van_2}}}">
                                                @else
                                                <input type="text" class="form-control" name="ouer_voog_naam_van_2" value="">
                                                @endif
                                            </div>       
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-xs-3">
                                                <label>Ouer/Voog ID Nommer</label>
                                            </div>
                                            <div class="col-xs-6">
                                                @if(Input::old('ouer_voog_id_nommer_2'))
                                                <input type="text" class="form-control" name="ouer_voog_id_nommer_2" value="{{{Input::old('ouer_voog_id_nommer_2')}}}"> 
                                                @elseif($persoon)
                                                <input type="text" class="form-control" name="ouer_voog_id_nommer_2" value="{{{$persoon[0]->ouer_voog_id_nommer_2}}}"> 
                                                @else
                                                <input type="text" class="form-control" name="ouer_voog_id_nommer_2" value=""> 
                                                @endif
                                            </div>       
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-xs-3">
                                                <label>Ouer/Voog Kontak Nommer</label>
                                            </div>
                                            <div class="col-xs-3">
                                                @if(Input::old('ouer_voog_kontak_nommer_2'))
                                                <input type="text" class="form-control" name="ouer_voog_kontak_nommer_2" value="{{{Input::old('ouer_voog_kontak_nommer_2')}}}">
                                                @elseif($persoon)
                                                <input type="text" class="form-control" name="ouer_voog_kontak_nommer_2" value="{{{$persoon[0]->ouer_voog_kontak_nommer_2}}}"> 
                                                @else
                                                <input type="text" class="form-control" name="ouer_voog_kontak_nommer_2" value=""> 
                                                @endif
                                            </div>

                                            <div class="col-xs-3">
                                                <label>Ouer/Voog Epos Adres</label>
                                            </div>
                                            <div class="col-xs-3">
                                                @if(Input::old('ouer_voog_epos_adres_2'))
                                                <input type="text" class="form-control" name="ouer_voog_epos_adres_2" value="{{{Input::old('ouer_voog_epos_adres_2')}}}"> 
                                                @elseif($persoon)
                                                <input type="text" class="form-control" name="ouer_voog_epos_adres_2" value="{{{$persoon[0]->ouer_voog_epos_adres_2}}}"> 
                                                @else
                                                <input type="text" class="form-control" name="ouer_voog_epos_adres_2" value=""> 
                                                @endif
                                            </div>         
                                        </div>
                                    
                                    </div>
                                </div>


                                <!-- Mediese Inligting --> 
                                <div class="box box-primary">
                                    <div class="box-header">
                                        <h3 class="box-title"><b>Mediese Inligting</b></h3>
                                    </div><!-- /.box-header -->

                                    <div class="box-body"> 
                                        <!-- Mediese Fonds Naam -->
                                        <div class="row form-group">
                                            <div class="col-xs-3">
                                                <label>Mediese Fonds Naam</label>
                                            </div>
                                            <div class="col-xs-6">
                                                @if(Input::old('mediese_fonds_naam'))
                                                <input type="text" class="form-control" name="mediese_fonds_naam" value="{{{Input::old('mediese_fonds_naam')}}}"> 
                                                @elseif($persoon)
                                                    <input type="text" class="form-control" name="mediese_fonds_naam" value="{{$persoon[0]->mediese_fonds_naam}}"> 
                                                @else
                                                <input type="text" class="form-control" name="mediese_fonds_naam" value=""> 
                                                @endif
                                            </div>              
                                        </div>

                                        <!-- Mediese Fonds Lidmaat Nommer -->
                                        <div class="row form-group">
                                            <div class="col-xs-3">
                                                <label>Mediese Fonds Lidmaat Nommer</label>
                                            </div>
                                            <div class="col-xs-6">
                                                @if(Input::old('mediese_fonds_lid_nommer'))
                                                <input type="text" class="form-control" name="mediese_fonds_lid_nommer" value="{{{Input::old('mediese_fonds_lid_nommer')}}}">
                                                @elseif($persoon)
                                                <input type="text" class="form-control" name="mediese_fonds_lid_nommer" value="{{$persoon[0]->mediese_fonds_lid_nommer}}">
                                                @else
                                                <input type="text" class="form-control" name="mediese_fonds_lid_nommer" value="">
                                                @endif
                                            </div>              
                                        </div>

                                        <!-- Huisdokter -->
                                        <h4><b>Huisdokter</b></h4>
                                        <div class="row form-group">
                                            <div class="col-xs-3">
                                                <label>Huisdokter Naam & Van</label>
                                            </div>
                                            <div class="col-xs-6">
                                                @if(Input::old('huis_dokter_naam_van'))
                                                <input type="text" class="form-control" name="huis_dokter_naam_van" value="{{{Input::old('huis_dokter_naam_van')}}}">
                                                @elseif($persoon)
                                                 <input type="text" class="form-control" name="huis_dokter_naam_van" value="{{$persoon[0]->huis_dokter_naam_van}}">
                                                @else
                                                <input type="text" class="form-control" name="huis_dokter_naam_van" value=""> 
                                                @endif
                                            </div>       
                                        </div>

                                        <!-- Huisdokter Kontak Nommer -->
                                        <div class="row form-group">
                                            <div class="col-xs-3">
                                                <label>Huisdokter Kontak Nommer</label>
                                            </div>
                                            <div class="col-xs-3">
                                                @if(Input::old('huis_dokter_kontak_nommer_1'))
                                                <input type="text" class="form-control" name="huis_dokter_kontak_nommer_1" value="{{{Input::old('huis_dokter_kontak_nommer_1')}}}"> 
                                                @elseif($persoon)
                                                <input type="text" class="form-control" name="huis_dokter_kontak_nommer_1" value="{{$persoon[0]->huis_dokter_kontak_nommer_1}}"> 
                                                @else
                                                <input type="text" class="form-control" name="huis_dokter_kontak_nommer_1" value=""> 
                                                @endif
                                            </div>

                                            <div class="col-xs-3">
                                                <label>Huisdokter Alternatiewe Kontak Nommer</label>
                                            </div>
                                            <div class="col-xs-3">
                                                @if(Input::old('huis_dokter_kontak_nommer_2'))
                                                <input type="text" class="form-control" name="huis_dokter_kontak_nommer_2" value="{{{Input::old('huis_dokter_kontak_nommer_2')}}}"> 
                                                @elseif($persoon)
                                                <input type="text" class="form-control" name="huis_dokter_kontak_nommer_2" value="{{$persoon[0]->huis_dokter_kontak_nommer_2}}"> 
                                                @else
                                                <input type="text" class="form-control" name="huis_dokter_kontak_nommer_2" value=""> 
                                                @endif
                                            </div>         
                                        </div>

                                        <!-- Mediese Toestand & Allergie -->
                                        <div class="row form-group">
                                            <div class="col-xs-3">
                                                <label>Siektetoestande of Allergieë</label>
                                            </div>
                                            <div class="col-xs-6">
                                                @if(Input::old('siekte_allergie'))
                                                <textarea class="form-control" rows="4" name="siekte_allergie">{{{Input::old('siekte_allergie')}}}</textarea>
                                                @elseif($persoon)
                                                <textarea class="form-control" rows="4" name="siekte_allergie">{{$persoon[0]->siekte_allergie}}</textarea>
                                                @else
                                                <textarea class="form-control" rows="4" name="siekte_allergie"></textarea>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                </div>


                                <!-- footer --> 
                        <div class="box-footer col-md-12">
                            <div class="row">
                                    <a href="{{URL('publiek/inskrywing/kanseleer')}}" class="btn btn-warning pull-left" style="margin-right: 5px;"><i class="fa fa-arrow-circle-left"></i> Terug </a>
                                    <button id="submit" type="submit" class="btn btn-success pull-right" style="margin-right: 5px;">Skryf In <i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                        </div>
                        

                            </div>
                        

                       


                        
                        <!-- /footer -->
                         </form> <!-- form -->


                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>


@stop
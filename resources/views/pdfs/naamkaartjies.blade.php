<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<style>
			table, td, th {
			    border: 1px solid black;
			    width: 5cm;
			    border-collapse: collapse;
			    padding: 10px;
			    word-wrap:break-word;
			    text-align: center;
			    page-break-inside: avoid;
			};

			table.fixed { table-layout:fixed; }
			table.fixed td { overflow: hidden; }

			.heading {
				text-align: left;
			}

			.header_img {
				width:45mm;
			}

			body { 
				font-family: DejaVu Sans, sans-serif;
				font-size:14px; 
			}
		</style>
	</head>
	<body>
		@foreach($inskrywings as $inskrywing)
			@foreach($inskrywing->persoon->rolle as $rol)
				@if($rol->groep->kamp_id === $kamp->id)
					
					<div>
						<table>
							<col width="500" />
    						<col width="500" />
							<tr>
								<td>
									<img src="{{asset($kamp->organisasies[0]->naamkaartjie_header_img)}}" class="header_img">
								</td>
								@if($inskrywing->persoon->siekte_allergie)
									<td>
										<img class="header_img">
										{!! QrCode::size(100)->encoding('UTF-8')->generate('http://inskrywings.voortrekkers.co.za/persone/beskou/'.$inskrywing->persoon->registrasie_nommer) !!}
									</td>
								@else
									<td rowspan="4">
										<img class="header_img">
									{!! QrCode::size(100)->encoding('UTF-8')->generate('http://inskrywings.voortrekkers.co.za/persone/beskou/'.$inskrywing->persoon->registrasie_nommer) !!}
									</td>
								@endif
							</tr>
							<tr>
								<td>
									<label style="font-size:18px;"><b>{{$rol->groep->groep_naam}}</b></label><br>
									{{$rol->rol_naam}}<br>
								</td>
								@if($inskrywing->persoon->siekte_allergie)
									<td rowspan="3">
										@if(strlen($inskrywing->persoon->siekte_allergie) > 80)
											<p style="font-size:10px;">{{ $inskrywing->persoon->siekte_allergie }}
											</p><br>
										@else
											<p style="font-size:14px;">{{preg_replace("/\r\n|\r|\n/",'<br>',$inskrywing->persoon->siekte_allergie)}}</p><br>
										@endif
									</td>
								@endif
							</tr>
							<tr>
								<td>
									@if ($inskrywing->persoon->voortrekker_registrasie_nommer === "")
										KID: <label style="font-size:15px;">{{$inskrywing->persoon->registrasie_nommer}}</label>
									@else
										VRN: <label style="font-size:15px;">{{$inskrywing->persoon->voortrekker_registrasie_nommer}}</label>
									@endif
								</td>
							</tr>
							<tr>
								<td>
									<label style="font-size:24px;"><b>{{$inskrywing->persoon->noemnaam}}</b></label><br>
									<label style="font-size:15px;">{{$inskrywing->persoon->van}}</label>
								</td>
							</tr>
						</table>
					</div>
					<br><br>
				@endif
			@endforeach
		@endforeach
	</body>
</html>
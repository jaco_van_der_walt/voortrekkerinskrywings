<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rol extends Model {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'rolle';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

    protected $guarded = ['id'];

    protected $hidden = ['created_at', 'updated_at'];


    public function groep()
    {
    	 return $this->belongsTo('App\Groep', 'groep_id');
    }

    public function persone()
    {
    	return $this->belongsToMany('App\Persoon', 'persone_rolle', 'rol_id', 'persoon_id');
    }

    public function voortrekker_betrokkenhede()
    {
        return $this->belongsToMany('App\VoortrekkerBetrokkenheid', 'rolle_betrokkenheid', 'rolle_id', 'voortrekker_betrokkenheid_id');
    }
}

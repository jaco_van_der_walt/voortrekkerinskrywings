<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVormsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vorms', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('inskrywing_id')->unsigned();
			$table->string('file_name');
			$table->string('file_path');

			$table->foreign('inskrywing_id')->references('id')->on('inskrywings');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vorms');
	}

}
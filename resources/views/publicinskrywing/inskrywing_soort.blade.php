@extends('layouts.public_inskrywing')

@section('head')
@parent
<title>Inskrywing</title>



@stop


@section('content')
<aside class="right-side stretch">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Inskrywing: {{$kamp->kamp_naam}}
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">

                    @if($errors->has())
                       @foreach ($errors->all() as $error)
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Waarskuwing!</b> {{$error}}
                            </div>
                      @endforeach
                    @endif

                    <div class="row">
                        <div class="col-md-12">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="callout callout-info">
                                    <h4>Inskrywing Soort</h4>
                                    <p>Wie wil jy inskryf vir {{$kamp->kamp_naam}}?</p>
                                    <p>Voorsien jou <b>Voortrekker Registrasie Nommer</b> om makliker in te skryf!</p>
                                </div>
                            </div>
                        </div>

                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title"><b>Jeuglid of Volwassene?</b></h3>
                            </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <a href="#" onclick="jeuglid();" id="jeuglid_button" class="col-md-12 btn btn-default">  <h4>Jeuglid</h4>Ek as ouer of voog wil 'n jeuglid inskryf</a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="#" onclick="volwassene();" id="volwassene_button" class="col-md-12 btn btn-default">  <h4>Volwassene</h4>Ek is 'n volwassene en wil myself inskryf</a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <form role="form" action="{{URL('publiek/inskrywing/soort')}}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" id="inskrywing_soort" name="inskrywing_soort" value="">

                        <div class="box box-primary" id="basiese_inligting" style="">
                            <div class="box-header">
                                <h3 class="box-title"><b>Besonderhede van persoon wat Ingeskryf word</b></h3>
                            </div>
                            <div class="box-body">

                                    <!-- Date dd/mm/yyyy -->
                                    <div id="geboortedatum_form" class="row form-group">
                                        <div class="col-xs-3">
                                                <label>Geboortedatum</label>
                                         </div>
                                        <div class="col-xs-6">
                                            <input id="datemask" name="geboortedatum" type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask value="{{Input::old('geboortedatum')}}"/>
                                        </div><!-- /.input group -->
                                    </div><!-- /.form group -->

                                    <!-- Voortrekker Registrasie Nommer -->
                                        <div id="vrn_form" class="row form-group">
                                             <div class="col-xs-3">
                                                <label id="vrn">Voortrekker Registrasie Nommer</label><br>
                                                <span><em>Opsioneel</em><br><em>Gebruik 'n SAS1 of SAS2 registrasienommer<em></span>
                                            </div>
                                            <div class="col-xs-6">
                                                <input id="vrn_input" type="text" class="form-control" name="voortrekker_registrasie_nommer" value="{{Input::old('voortrekker_registrasie_nommer')}}" >
                                            </div>
                                        </div>




                            </div>
                        </div>


                        <!-- footer -->
                        <div class="box-footer col-md-12">
                            <div class="row">
                                    <a href="{{URL('publiek/inskrywing/kanseleer')}}" class="btn btn-warning pull-left" style="margin-right: 5px;"><i class="fa fa-arrow-circle-left"></i> Terug </a>
                                    <button id="submit" type="submit" class="btn btn-success pull-right" style="margin-right: 5px;">Skryf In <i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                        </div>
                        <!-- /footer -->
                        </form>
                    </div>
                </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>
        <!-- InputMask -->
        <script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.date.extensions.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.extensions.js')}}" type="text/javascript"></script>


        <script type="text/javascript">
            //Datemask
                $("#datemask").inputmask("yyyy/mm/dd", {"placeholder": "yyyy/mm/dd"});
        </script>

        <script type="text/javascript">

            function jeuglid(){
                document.getElementById("jeuglid_button").className = "col-md-12 btn btn-success";
                document.getElementById("volwassene_button").className = "col-md-12 btn btn-default";
                document.getElementById("basiese_inligting").style = "display:true;";
                document.getElementById("submit").style = "margin-right: 5px";
                document.getElementById("vrn").innerHTML = "Voortrekker Registrasie Nommer";
                document.getElementById("inskrywing_soort").value = "jeuglid";

            };


            function volwassene(){
                document.getElementById("volwassene_button").className = "col-md-12 btn btn-success";
                document.getElementById("jeuglid_button").className = "col-md-12 btn btn-default";
                document.getElementById("basiese_inligting").style = "display:true;";
                document.getElementById("submit").style = "margin-right: 5px";
                document.getElementById("vrn").innerHTML = "Voortrekker Registrasie Nommer";
                document.getElementById("inskrywing_soort").value = "volwassene";

            };

            $( document ).ready(function() {

                    //Check old input for inskrywing soort in order to fire correct function, simulating a click of the button if there's an erro.

            });

        </script>


@stop

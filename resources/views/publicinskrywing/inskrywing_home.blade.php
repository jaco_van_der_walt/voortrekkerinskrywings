@extends('layouts.public_inskrywing')

@section('head')
@parent
<title>Inskrywing</title>
@stop


@section('content')
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Inskrywing
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">

                    @if($errors->has())
                       @foreach ($errors->all() as $error)
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Waarskuwing!</b> {{$error}}
                            </div>
                      @endforeach
                    @endif

                    <div class="row">
                        <div class="col-md-12">

                         <div class="row">
                            <div class="col-md-12">
                                <div class="callout callout-warning">
                                    <h4>Kennisgewing</h4>
                                    <p>Hierdie inskrywingsproses vertoon die beste op 'n rekenaar. Dit word nie aanbeveel om in te skryf vanaf 'n selfoon nie!</p>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <div class="callout callout-info">
                                    <h4>Kies die Kamp waarvoor jy wil inskryf</h4>
                                    <p>Alle kampe wat tans inskrywings aanvaar word hier vertoon</p>
                                </div>
                            </div>
                        </div>


                        @foreach($kampe_inskrywings as $kamp)
                        <div class="row">
                            <div class="col-md-12">
                            <a href={{URL('publiek/inskrywing/kamp/'.$kamp->id)}}>
                                <div class="small-box bg-green">
                                    <div class="inner">
                                        <h3> {{$kamp->kamp_naam}} </h3>
                                        <p>{{$kamp->begin}} tot {{$kamp->eindig}}</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-tree"></i>
                                    </div>
                                    <div class="small-box-footer">
                                        Skryf In <i class="fa fa-arrow-circle-right"></i>
                                    </div>
                                </div>
                            </a>
                            </div>
                        </div>
                        @endforeach

                        <!-- footer --> 
                        <div class="box-footer col-md-12">
                            <div class="row">
                            </div>
                        </div>
                        <!-- /footer -->
                        </div>
                    </div> 


                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>


@stop
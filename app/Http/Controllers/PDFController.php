<?php namespace App\Http\Controllers;

use App\Persoon;
use App\Kamp;
use App\Inskrywing;
use App\Groep;
use App\VoortrekkerBetrokkenheid;
use Illuminate\Http\RedirectResponse;
use \Rap2hpoutre\LaravelLogViewer;
use PDF;
use QrCode;
use Excel;
use Carbon\Carbon;


class PDFController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function register($id)
	{
		$inskrywings = Inskrywing::where('kamp_id', $id)->get();
		$inskrywing_count = Inskrywing::where('kamp_id', $id)->count();

		//All this crazy shenanigans in this controller is just to format the data either alphabetically or on other critera (diensjare)
		$sort_arr = array();
		foreach($inskrywings as $inskrywing)
		{
			$sort_arr[$inskrywing->persoon->registrasie_nommer] = $inskrywing->persoon->van;
		}
		asort($sort_arr);

		$sort_inskrywings = array();
		foreach($sort_arr as $key => $value)
		{
			$persoon = Persoon::where('registrasie_nommer', $key)->first();
			$groep_ar = '';
			$rol_ar = '';
			foreach($persoon->rolle as $rol)
			{
				if($rol->groep->kamp_id == $id)
				{
					$groep_ar = $rol->groep->groep_naam;
					$rol_ar = $rol->rol_naam;
				}
			}

			$sort_inskrywings[$key] = array('persoon' => $persoon, 'groep' => $groep_ar, 'rol'=>$rol_ar, 'betrokkenheid' => Inskrywing::where('kamp_id',$id)->where('persoon_id', $persoon->id)->first()->voortrekkerBetrokkenheid->betrokkenheid);
		}

		$data = array('kamp'=> Kamp::find($id), 'inskrywings' => $sort_inskrywings, 'count' => $inskrywing_count);
		$pdf = PDF::loadView('pdfs.register', $data)->setPaper("A4", "landscape");
		return $pdf->stream();
	}

	public function groepregister($id)
	{
		$groep = Groep::find($id);
		$count = 0;
		$i = 0;
		$return= array();

		foreach($groep->rolle as $rol)
        {

          if(count($rol->persone) != 0)
          {
          	$count = $count + count($rol->persone);
          	$rol_persone = $rol->persone->sortBy('van', SORT_NATURAL|SORT_FLAG_CASE);
          	$return[$rol->rol_naam] = $rol_persone;
          }

        }
        $data = array('groep'=>$groep, 'rolle'=>$return, 'count'=>$count);
        $pdf = PDF::loadView('pdfs.groepregister', $data)->setPaper("A4", "landscape");

		return $pdf->stream();
	}

	public function inskrywings($id)
	{
		//$id = kamp_id
		$kamp = Kamp::find($id);

		$inskrywings = Inskrywing::where('kamp_id', $kamp->id)->get();

		//$data = array('kamp'=>$kamp, 'inskrywings'=>$inskrywings);
		//$pdf = PDF::loadView('pdfs.inskrywings', $data)->setPaper("A4", "portrait");
		//return $pdf->stream();

		return view('pdfs.inskrywings')->with(['kamp'=>$kamp, 'inskrywings'=>$inskrywings]);
	}

	public function siektesallergie($id)
	{
		$kamp = Kamp::find($id);
		$inskrywings = Inskrywing::where('kamp_id', $kamp->id)->get();
		$sort_inskrywings = $inskrywings->sortBy('voortrekker_betrokkenheid_id', SORT_NATURAL|SORT_FLAG_CASE);
		$siekte_persone = array();
		foreach($sort_inskrywings as $inskrywing)
		{
			if($inskrywing->persoon->siekte_allergie != "")
			{
				array_push($siekte_persone, $inskrywing);
			}
		}
		$data = array('kamp'=>$kamp, 'inskrywings'=>$siekte_persone, 'count'=>count($siekte_persone));
        $pdf = PDF::loadView('pdfs.siekte_persone', $data)->setPaper("A4", "landscape");
		return $pdf->stream();
	}

	public function naamkaartjies($id)
	{
		$inskrywings = Inskrywing::where('kamp_id', $id)->get();
		$kamp = Kamp::find($id);
		//return view('pdfs.naamkaartjies')->with(["kamp"=>$kamp, 'inskrywings'=>$inskrywings]);
		$data = array('kamp'=> Kamp::find($id), 'inskrywings' => $inskrywings);
		//$pdf = PDF::loadView('pdfs.naamkaartjies', $data)->setPaper("A4", "portrait")->setOptions(["dpi", 300]);
		//return $pdf->stream();
		return view('pdfs.naamkaartjies')->with(['kamp'=>Kamp::find($id), 'inskrywings'=>$inskrywings]);
	}

	public function diensjare($id)
	{

		$inskrywings = Inskrywing::where('kamp_id', $id)->get();

		$diens = array();

		foreach($inskrywings as $inskrywing)
		{
			$diens[$inskrywing->persoon->registrasie_nommer] = Inskrywing::where('persoon_id', $inskrywing->persoon->id)->count();
		}

		asort($diens);

		$sorted_diensjare_persoon = array();

		foreach($diens as $key => $value)
		{
			$persoon = Persoon::where('registrasie_nommer', $key)->first();
			$sorted_diensjare_persoon[$key] = array("persoon" => $persoon , "diensjare" => $value, 'betrokkenheid' => Inskrywing::where('kamp_id',$id)->where('persoon_id', $persoon->id)->first()->voortrekkerBetrokkenheid->betrokkenheid);
		}

		$data = array('kamp'=> Kamp::find($id), 'persone' => $sorted_diensjare_persoon);
		$pdf = PDF::loadView('pdfs.diensjare', $data)->setPaper("A4", "landscape");
		return $pdf->stream();
	}

	public function opsomming($id)
	{
		$kamp = Kamp::where('id', $id)->first();

        $inskrywings = Inskrywing::where('kamp_id', $kamp->id)->get();
        $betrokke = VoortrekkerBetrokkenheid::all();

        $groepe = Groep::where('kamp_id', $kamp->id)->orderBy('groep_naam', 'ASC')->get();
        $groep_teller = array();

        foreach ($groepe as $groep) {
        	$groep_teller[$groep->groep_naam] = array();
        }

        $kamp_teller = array();
        $kamp_teller['Totaal'] = Inskrywing::where('kamp_id', $kamp->id)->count();
        foreach ($betrokke as $betrokkenheid) {
        	$kamp_teller[$betrokkenheid->id] = 0;
        	foreach ($groepe as $groep) {
	        	$groep_teller[$groep->groep_naam][$betrokkenheid->id] = 0;
	        }
        }

        $kommando_teller = array();
        //Init the array
        foreach($inskrywings as $inskrywing)
        {
        	if($inskrywing->persoon->kommando)
        	{
        		$kommando_teller[$inskrywing->persoon->kommando->kommando_naam]['naam'] = $inskrywing->persoon->kommando->kommando_naam;
        		$kommando_teller[$inskrywing->persoon->kommando->kommando_naam]['count'] = 0;
        	}
        }

        foreach($inskrywings as $inskrywing)
        {
        	//Inc the the array
        	if($inskrywing->persoon->kommando)
        	{
        		$kommando_teller[$inskrywing->persoon->kommando->kommando_naam]['count']++;
        	}

            $kamp_teller[$inskrywing->voortrekkerBetrokkenheid->id]++;
            foreach($inskrywing->persoon->rolle as $rol){
            	if($rol->groep->kamp_id === $kamp->id){
	            	$groep_teller[$rol->groep->groep_naam][$inskrywing->voortrekkerBetrokkenheid->id]++;
	            }
            }
        }

		$data = array('kamp'=> Kamp::find($id),
					  'betrokkenhede' => $betrokke,
					  'kamp_stats' => $kamp_teller,
					  'groepe' => $groepe,
					  'kommandos' => $kommando_teller,
					  'groepe_stats' => $groep_teller);

		$pdf = PDF::loadView('pdfs.opsomming', $data)->setPaper("A4", "portrait");
		return $pdf->stream();
	}

	public function cert1($rol_id)
	{
		return $rol_id;
	}

	public function getEkstras($id)
	{
		$kamp = Kamp::find($id);
		$inskrywings = Inskrywing::where('kamp_id',$id)->get();
	  return view('pdfs.ekstras')->with(['kamp'=>$kamp, 'inskrywings'=>$inskrywings]);
	}

	public function excelInskrywings($id)
	{
		$kamp = Kamp::find($id);
		Excel::create($kamp->kamp_naam."_".Carbon::now('Africa/Johannesburg'), function($excel) use ($id) {
             $excel->sheet('New sheet', function($sheet) use ($id) {
                $inskrywings = Inskrywing::where('kamp_id',$id)->get();
                $sheet->setColumnFormat(array(
				    'A:AG' => '@',
				    'J' => '0',
				    'S' => '0',
				    'W' => '0',
				));
                $sheet->loadView('pdfs.excelinskrywings', ['inskrywings'=>$inskrywings, 'kamp_id'=>$id, 'kamp'=> Kamp::find($id)]);
             });
         })->download('xls');
         return redirect('kamp/paneel',$id);

	}

}

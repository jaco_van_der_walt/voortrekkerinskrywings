<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroepeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('groepe', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('kamp_id')->unsigned();
			$table->string('groep_naam');
			$table->string('logo');
			$table->string('kleur')->default('#3c8dbc');
			$table->timestamps();

			$table->foreign('kamp_id')->references('id')->on('kampe');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('groepe');
	}

}
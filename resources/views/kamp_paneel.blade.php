@extends('layouts.master')

@section('head')
@parent
<title>{{$kamp->kamp_naam}}</title>
<script src="{{URL('//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css')}}" type="text/css"></script>
@stop


@section('content')
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                       {{$kamp->kamp_naam}}
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">

                @if($errors->has())
                   @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Waarskuwing!</b> {{$error}}
                        </div>
                  @endforeach
                @endif

                @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissable">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Sukses!</b> {{Session::get('success')}}
                        </div>
                @endif

                @if($kamp->status === "AanvaarInskrywings")
                <div class="alert alert-warning ">
                        <i class="fa fa-warning"></i>
                        <b>Aandag!</b> Hierdie kamp aanvaar tans inskrywings!
                </div>
                @elseif($kamp->status === "Gesluit" || $kamp->status === "Geen")
                    <div class="alert alert-danger alert-dismissable">
                        <i class="fa fa-ban"></i>
                        <b>Aandag!</b> Hierdie kamp is gesluit!
                    </div>
                @endif
                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><b>Kamp Inligting</b></h3>
                                    <div class="btn-group pull-right" style="margin: 5px;">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            Opsies&nbsp;&nbsp;<span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href={{URL('register/kamp/'.$kamp->id)}} target="_blank"><i class="fa  fa-check-square-o"> Register</i></a></li>
                                            <li><a href={{URL('inskrywings/kamp/'.$kamp->id)}} target="_blank"><i class="fa  fa-list-alt"> Inskrywings</i></a></li>
                                            <li><a href={{URL('inskrywings/excel/kamp/'.$kamp->id)}} target="_blank"><i class="fa fa-file-excel-o"> Excel Uitvoer</i></a></li>
                                            <li><a href={{URL('siektesallergie/kamp/'.$kamp->id)}} target="_blank"><i class="fa fa-medkit"> Siektes/Allergië</i></a></li>
                                            <li><a href={{URL('ekstras/'.$kamp->id)}} target="_blank"><i class="fa  fa-bus"> Ekstras</i></a></li>
                                            <li><a href={{URL('naamkaartjies/'.$kamp->id)}} target="_blank"><i class="fa  fa-tags"> Naamkaartjies</i></a></li>
                                            <li><a href={{URL('diensjare/'.$kamp->id)}} target="_blank"><i class="fa  fa-trophy"> Diensjare</i></a></li>
                                            <li><a href={{URL('opsomming/'.$kamp->id)}} target="_blank"><i class="fa  fa-book"> Kampopsomming</i></a></li>
                                             <li><a href={{URL('inskrywings/eposse/'.$kamp->id)}} target="_blank"><i class="fa  fa-envelope"> Epos Adresse</i></a></li>
                                            <hr>
                                            <li><a href={{URL('kampbeskrywing')}}><i class="fa  fa-file-text-o"> Kampbeskrywing</i></a></li>
                                            <li><a href={{URL('kampvrywaring')}}><i class="fa  fa-life-ring"> Kampvrywarings</i></a></li>
                                            <hr>
                                                <!-- Sluit kamp oop of toe -->
                                                @if(($kamp->status === "Oop"))
                                                    <li><a href={{URL('kamp/toggleinskrywings/'.$kamp->id)}}><i class="fa fa-pencil-square-o"> Aanvaar Inskrywings</i></a></li>
                                                    <li><a href={{URL('kamp/toggle/'.$kamp->id)}}><i class="fa fa-lock"> Sluit Kamp</i></a></li>
                                                    @if(Auth::user()->super_admin)
                                                    <li class="bg-red"><a href='#' onclick="skrapKamp()"><i class="fa fa-trash" style="color: white"> Skrap Kamp</i></a></li>
                                                    @endif
                                                @elseif($kamp->status === "AanvaarInskrywings")
                                                    <li><a href={{URL('kamp/toggleinskrywings/'.$kamp->id)}}><i class="fa fa-times"> Sluit Inskrywings</i></a></li>
                                                    <li><a href={{URL('kamp/toggle/'.$kamp->id)}}><i class="fa fa-lock"> Sluit Kamp</i></a></li>
                                                @else
                                                    @if(Auth::user()->super_admin)
                                                        <li><a href={{URL('kamp/toggle/'.$kamp->id)}}><i class="fa fa-unlock"> Heropen Kamp</i></a></li>
                                                        <li class="bg-red"><a href='#' onclick="skrapKamp()"><i class="fa fa-trash" style="color: white"> Skrap Kamp</i></a></li>
                                                    @endif
                                                @endif
                                        </ul>
                                    </div>
                                </div><!-- /.box-header -->

                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-lg-3 col-xs-6">
                                            <!-- small box -->
                                            <div class="small-box bg-green">
                                                <div class="inner">
                                                    <h3 >
                                                        {{$kamp->begin}}
                                                    </h3>
                                                    <p id="countdown_start">
                                                        Kamp is reeds verby!
                                                    </p>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-flag-o"></i>
                                                </div>
                                                <br>
                                            </div>
                                        </div><!-- ./col -->
                                        <div class="col-lg-3 col-xs-6">
                                            <!-- small box -->
                                            <a href="{{URL('publiek/inskrywing/admin/tabel')}}">
                                                <div class="small-box bg-red">
                                                    <div class="inner">
                                                        <h3>
                                                            {{$publiekeInskrywingsTeller}}
                                                        </h3>
                                                        <p>
                                                            Inskrywings staan tou
                                                        </p>
                                                    </div>
                                                    <div class="icon">
                                                        <i class="fa fa-envelope"></i>
                                                    </div>
                                                    <br>
                                                </div>
                                            </a>
                                        </div><!-- ./col -->
                                        <div class="col-lg-3 col-xs-6">
                                            <!-- small box -->
                                            <div class="small-box bg-yellow">
                                                <div class="inner">
                                                    <h3>
                                                        {{sizeof($groepe)}}
                                                    </h3>
                                                    <p>
                                                        Groepe
                                                    </p>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-sitemap"></i>
                                                </div>
                                                <br>
                                            </div>
                                        </div><!-- ./col -->
                                        <div class="col-lg-3 col-xs-6">
                                            <!-- small box -->
                                            <div class="small-box bg-aqua">
                                                <div class="inner">
                                                    <h3>
                                                        {{$inskrywings}}
                                                    </h3>
                                                    <p>
                                                        Inskrywings
                                                    </p>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-users"></i>
                                                </div>
                                                <br>
                                            </div>
                                        </div><!-- ./col -->
                                    </div><!-- /.row -->
                                </div>
                            </div>
                            </div><!-- /.box -->
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <!-- Bar Chart -->
                                <div class="box box-primary">
                                    <div class="box-header">
                                        <i class="fa fa-bar-chart-o"></i>
                                        <h3 class="box-title">Inskrywings per betrokkenheid</h3>
                                    </div>
                                    <div class="box-body">
                                        <div id="bar-chart" style="height: 300px;"></div>
                                    </div><!-- /.box-body-->
                                </div><!-- /.box -->
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="box box-primary">
                                    <div class="box-header">
                                        <h3 class="box-title">Groepe</h3>
                                        <div class="box-tools">
                                            @if(($kamp->status === "Oop"))
                                            <ul class="pagination pagination-sm no-margin pull-right">
                                                <li><a class="btn bg-olive" onclick="voegGroepBy()">Nuwe Groep</a></li>
                                            </ul>
                                            @endif
                                        </div>
                                    </div><!-- /.box-header -->
                                    <div class="box-body no-padding">
                                        <table class="table">
                                            <tr>
                                                <th>Groep</th>
                                                @foreach($betrokkenhede as $betrokkenheid)
                                                    <th style="width: 40px; text-align: center;">{{$betrokkenheid->betrokkenheid}}</th>
                                                @endforeach
                                            </tr>

                                            @foreach($groepe as $g)
                                                <tr>
                                                    <td><a href="{{ URL::to('groep/view/' . $g->id) }}">{{$g->groep_naam}}</a></td>
                                                </td>
                                                @foreach($betrokkenhede as $betrokkenheid)
                                                        @if($groep_betrokkenheid[$g->groep_naam][$betrokkenheid->betrokkenheid] == 0)
                                                        <td style="text-align: center;"><span class="badge" style="background: {{$betrokkenheid->kleur}}; background-color:rgba(0, 0, 0, 0.1);">{{$groep_betrokkenheid[$g->groep_naam][$betrokkenheid->betrokkenheid]}}</span>
                                                        </td>
                                                        @else
                                                        <td style="text-align: center;"><span class="badge" style="background: {{$betrokkenheid->kleur}};">{{$groep_betrokkenheid[$g->groep_naam][$betrokkenheid->betrokkenheid]}}</span>
                                                        </td>
                                                        @endif

                                                @endforeach
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div><!-- /.box-body -->
                                </div><!-- /.box -->
                            </div>
                        </div>

                         <!-- Persons -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title">Inskrywings</h3>
                                    <div class="box-tools">
                                        <ul class="pagination pagination-sm no-margin pull-right">
                                            @if(($kamp->status === "Oop"))
                                            <li><a class="btn bg-olive" href="{{url('inskrywing/nuut')}}">Nuwe Inskrywing</a></li>
                                            @endif
                                        </ul>
                                    </div>
                                </div><!-- /.box-header -->


                                <div class="box-body">
                                    <div class="box-body table-responsive">
                                    <table id="kamp_persone" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Naam</th>
                                                <th>Van</th>
                                                <th>Betrokkenheid</th>
                                                <th>Kommando</th>
                                                <th>Groep</th>
                                                <th>Rol</th>
                                                <th>Betaal</th>
                                                <th>Aksies</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>#</th>
                                                <th>Naam</th>
                                                <th>Van</th>
                                                <th>Betrokkenheid</th>
                                                <th>Kommando</th>
                                                <th>Groep</th>
                                                <th>Rol</th>
                                                <th>Betaal</th>
                                                <th>Aksies</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                                </div>

                            </div><!-- /.box -->
                            <div class="box-footer">
                            </div>

                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->

<!-- Add Groep Modal -->
<div id="addGroepRolModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Nuwe Groep</h4>
      </div>
      <div class="modal-body">
        {!! Form::open(array('url'=>'groep/nuut','method'=>'POST')) !!}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
         <div class="row form-group">
                <div class="col-xs-4">
                    <label>Groep Naam <span>*</span></label>
                </div>
                <div class="col-xs-8">
                    <input id="new_group_name" name="groep_naam" type="text" class="form-control">
                </div>
         </div>

         <div class="row form-group">
                <div class="col-xs-4">
                    <label>Rolle</label>
                    <br>
                    <p><i>Een rol per lyn</i></p>
                </div>
                <div class="col-xs-8">
                    <textarea name="groep_rolle" id="new_group_rolle" class="form-control" rows="4">Kursusleier&#10;Kursusaanbieder&#10;Kursusganger</textarea>
                </div>
         </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary pull-left" data-dismiss="modal">Kanselleer</button>
        <button type="submit" class="btn btn-success pull-right"><i class="fa  fa-plus"></i>  Voeg By</button>
      </div>
    </div>
    {!! Form::close() !!}

  </div>
</div>


<!-- Skrap Kamp Modal -->
<div id="skrapKampModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Skrap Kamp</h4>
      </div>
      <div class="modal-body">
        {!! Form::open(array('url'=>'kamp/skrap','method'=>'POST')) !!}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
         <div class="row form-group">
                <div class="col-xs-12">
                    <label>As jy die kamp skrap is dit finaal en kan jy nie die kamp terug bring nie.</label>
                </div>
         </div>
         <div class="row form-group">
                <div class="col-xs-12">
                    <label>Tik asseblief "SKRAP" hier onder in om te bevestig dat jy die kamp wil skrap.</label>
                </div>
         </div>
         <div class="row form-group">
                <div class="col-xs-8">
                    <input id="bevestig_skrap" name="bevestigging" type="text" class="form-control">
                </div>
         </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary pull-left" data-dismiss="modal">Kanselleer</button>
        <button type="submit" class="btn btn-danger pull-right"><i class="fa fa-trash"></i>  Skrap Kamp</button>
      </div>
    </div>
    {!! Form::close() !!}

  </div>
</div>



@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>

        <!-- Countdown -->
        <script src="{{URL::asset('assets/js/plugins/countdown/countdown.min.js')}}" type="text/javascript"></script>

         <!-- DATA TABES SCRIPT -->
        <script src="{{URL('//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('assets/js/plugins/datatables/dataTables.bootstrap.js')}}" type="text/javascript"></script>

                <!-- page script -->
        <script type="text/javascript">

                 $(function() {
                    $('#kamp_persone').DataTable( {
                        "sAjaxSource": "{{{URL('ajax/kampe/persone')}}}",
                        "sServerMethod": "GET"
                      });
                });

                var now = new Date();
                var begin = new Date("{{{$kamp->begin}}}");
                var end = new Date("{{{$kamp->eindig}}}");
                if (begin > now) {
                countdown.setLabels(
                    '||| ure| dae',
                    '| sekondes| minute| ure| dae| weke|| jare',
                    ', en ');

                countdown(
                    begin,
                    function(ts) {
                        if(document.getElementById('countdown_start')){
                          document.getElementById('countdown_start').innerHTML = ts.toHTML("strong");
                        }
                    },
                    countdown.DAYS|countdown.HOURS|countdown.MINUTES|countdown.SECONDS,
                    3);
                }

                if (end > now) {
                countdown.setLabels(
                    '||| ure| dae',
                    '| sekondes| minute| ure| dae| weke|| jare',
                    ', en ');

                countdown(
                    end,
                    function(ts) {
                        if(document.getElementById('countdown_end')){
                            document.getElementById('countdown_end').innerHTML = ts.toHTML("strong");
                        }
                    },
                    countdown.DAYS|countdown.HOURS|countdown.MINUTES|countdown.SECONDS,
                    3);
                }

                function voegGroepBy()
                {
                     $('#addGroepRolModal').modal('show');
                }

                function skrapKamp()
                {
                     $('#skrapKampModal').modal('show');
                }

        </script>

        <!-- FLOT CHARTS -->
        <script src="{{URL::asset('assets/js/plugins/flot/jquery.flot.min.js')}}" type="text/javascript"></script>
        <!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
        <script src="{{URL::asset('assets/js/plugins/flot/jquery.flot.resize.min.js')}}" type="text/javascript"></script>
        <!-- FLOT BARNUMBERS PLUGIN -->
        <script src="{{URL::asset('assets/js/plugins/flot/jquery.flot.barnumbers.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('assets/js/plugins/flot/jquery.flot.categories.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('assets/js/plugins/flot/jquery.flot.stack.patched.js')}}" type="text/javascript"></script>

        <!-- Page script -->
        <script type="text/javascript">

            $(function() {

                /*
                 * BAR CHART
                 * ---------
                 */


                var bar_data =[@foreach($betrokkenhede as $betrokkenheid)
                                {data: [["{{$betrokkenheid->betrokkenheid}}", {{$betrokkenheid_stats[$betrokkenheid->id]}}]], color: "{{$betrokkenheid->kleur}}"},
                               @endforeach];

                $.plot("#bar-chart", bar_data, {
                    grid: {
                        borderWidth: 1,
                        borderColor: "#f3f3f3",
                        tickColor: "#f3f3f3"
                    },
                    series: {
                        bars: {
                            show: true,
                            barWidth: 0.5,
                            align: "center",
                            numbers :{
                                show: true,
                                yAlign: function(y) { return y + 2; },
                            },
                        }
                    },
                    xaxis: {
                        mode: "categories",
                        tickLength: 0
                    },
                });
                /* END BAR CHART */

            /*
             * Custom Label formatter
             * ----------------------
             */
            function labelFormatter(label, series) {
                return "<div style='font-size:10px; text-align:center; padding:2px; color: #fff; font-weight: 600;'>"
                        + label
                        + "<br/>"
                        + Math.round(series.percent) + "%</div>";
            };
        });

        </script>
@stop

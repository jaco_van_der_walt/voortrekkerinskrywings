<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrganisasieAddBankbesonderhedeAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('organisasies', function ($table) {
            $table->text('bankbesonderhede')->nullable();
            $table->string('admin_naam')->nullable();
            $table->string('admin_tel')->nullable();
            $table->string('admin_epos')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('organisasies', function ($table) {
            $table->dropColumn('bankbesonderhede');
            $table->dropColumn('admin_naam');
            $table->dropColumn('admin_tel');
            $table->dropColumn('admin_epos');
        });
    }
}

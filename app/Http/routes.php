<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//FRONTEND
Route::get('/', 'PublicInskrywingController@inskrywingControl');
Route::get('skryfin/{id}', 'PublicInskrywingController@setKamp');

//PUBLIC INSKRYWING
Route::get('inskrywing', 'PublicInskrywingController@inskrywingControl');
Route::get('publiek/inskrywing/kanseleer', 'PublicInskrywingController@getKanseleerInskrywing');
Route::get('publiek/inskrywing/kamp/{id}', 'PublicInskrywingController@getInskrywingKamp');
Route::get('publiek/inskrywing/bevestigvolwassene', 'PublicInskrywingController@getBevestigVolwassene');
Route::post('publiek/inskrywing/soort', 'PublicInskrywingController@postSoort');
Route::get('publiek/inskrywing/kies/{id}', 'PublicInskrywingController@getKiesPersoon');
Route::post('publiek/inskrywing/persoon/data', 'PublicInskrywingController@postPersoonData');
Route::get('publiek/inskrywing/betrokkenheid/{id}', 'PublicInskrywingController@getBetrokkenheid');
Route::get('publiek/inskrywing/groep/{id}', 'PublicInskrywingController@getGroep');
Route::post('publiek/inskrywing/vrywaring', 'PublicInskrywingController@postVrywaring');
Route::get('publiek/inskrywing/bevestiginskrywing', 'PublicInskrywingController@getBevestig');
Route::get('publiek/inskrywing/matches/kanseleer', 'PublicInskrywingController@kanseleerMatches');
Route::get('publiek/inskrywing/terug', 'PublicInskrywingController@terug');
Route::post('publiek/inskrywing/ekstras', 'PublicInskrywingController@postEkstras');

//PUBLIC INSKRYWING ADMIN
Route::get('publiek/inskrywing/admin/tabel', 'PublicInskrywingAdminController@view_publieke_inskrywings_tabel');
Route::get('ajax/publiek/inskrywings', 'PublicInskrywingAdminController@ajax_publiek_inskrywings');
Route::get('publiek/inskrywing/admin/kies/{id}', 'PublicInskrywingAdminController@publiek_inskrywing_kies');
Route::post('publiek/inskrywing/admin/merge', 'PublicInskrywingAdminController@mergePersone');
Route::post('publiek/inskrywing/admin/updatepersoon', 'PublicInskrywingAdminController@updatePersoonMerge');
Route::get('publieke/inskrywing/admin/betrokkenheid', 'PublicInskrywingAdminController@kiesBetrokkenheid');
Route::get('publieke/inskrywing/admin/kies/betrokkenheid/{id}', 'PublicInskrywingAdminController@setBetrokkenheid');
Route::get('publieke/inskrywing/admin/rol', 'PublicInskrywingAdminController@kiesRol');
Route::get('publieke/inskrywing/admin/kies/rol/{id}', 'PublicInskrywingAdminController@setRol');
Route::get('publieke/inskrywing/admin/bevestig', 'PublicInskrywingAdminController@getBevestig');
Route::get('publieke/inskrywing/admin/skryfin', 'PublicInskrywingAdminController@skryfIn');
Route::get('publiek/inskrywing/admin/skep/persoon/{id}', 'PublicInskrywingAdminController@skepPersoon');
Route::get('publiek/inskrywing/admin/beskou/{id}', 'PublicInskrywingAdminController@beskouInskrywing');
Route::get('publiek/inskrywing/admin/skrap/{id}', 'PublicInskrywingAdminController@skrapInskrywing');
Route::get('publiek/inskrywing/admin/herverwerk/{id}', 'PublicInskrywingAdminController@herverwerkInskrywing');

//ADMIN
Route::get('admin', 'AdminController@index');
Route::get('logs/view', 'AdminController@logs');

Route::group(['middleware' => 'auth'], function () {
	Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});

//PERSONE
Route::get('persone/voegby', 'PersoneController@view_voeg_persoon_by');
Route::post('persone/voegby/nuut', 'PersoneController@submit_voeg_by');
Route::get('persone/tabel', 'PersoneController@view_persone_table');
Route::get('ajax/persone', 'PersoneController@ajax_persone');
Route::get('ajax/persoon/{id}', 'PersoneController@ajax_persoon');
Route::get('persone/edit/{id}', 'PersoneController@getEdit');
Route::get('persone/delete', 'PersoneController@getDelete');
Route::post('persone/edit', 'PersoneController@postEdit');
Route::get('persone/beskou/{id}', 'PersoneController@getView');

//KOMMANDO
Route::get('kommando/tabel', 'KommandoController@view_kommando_table');
Route::get('kommando/voegby', 'KommandoController@view_voeg_by');
Route::post('kommando/voegby/nuut', 'KommandoController@submit_voeg_by');
Route::get('ajax/kommando', 'KommandoController@ajax_kommando');
Route::get('kommando/edit/{id}', 'KommandoController@getEdit');
Route::post('kommando/edit', 'KommandoController@postEdit');
Route::get('kommando/delete', 'KommandoController@getDelete');

//KAMPE
Route::get('kamp/tabel', 'KampController@view_kamp_table');
Route::get('kamp/voegby', 'KampController@view_voeg_kamp_by');
Route::post('kamp/voegby/nuut', 'KampController@submit_voeg_by');
Route::get('ajax/kampe', 'KampController@ajax_kampe');
Route::get('kamp/paneel/{id}', 'KampController@view_kamp_paneel');
Route::get('kamp/toggle/{id}', 'KampController@sluit_open_kamp');
Route::get('ajax/kampe/persone', 'KampController@ajax_kamp_persone');
Route::get('kamp/toggleinskrywings/{id}', 'KampController@aanvaar_stop_inskrywings');
Route::get('kampbeskrywing', 'KampController@view_wysig_beskrywing');
Route::post('kampbeskrywing/wysig', 'KampController@wysig_beskrywing');
Route::get('kampvrywaring', 'KampController@view_wysig_vrywaring');
Route::post('kampvrywaring/wysig/volwasse', 'KampController@wysig_vrywaring_volwasse');
Route::post('kampvrywaring/wysig/jeug', 'KampController@wysig_vrywaring_jeug');
Route::post('kamp/skrap', 'KampController@skrapKamp');


//GROEPE
Route::get('groep/view/{id}', 'GroepController@view_groep');
Route::post('groep/logo', 'GroepController@laai_logo');
Route::get('ajax/groepe/persone', 'GroepController@ajax_groep_persone');
Route::get('ajax/groep/persone/rolle', 'GroepController@ajax_groep_rolle_persone');
Route::get('groep/rol/vervul/persoon/{id}', 'GroepController@vervul_rol');
Route::get('groep/rol/verwyder/persoon/{id}', 'GroepController@verwyder_rol');
Route::get('groep/rol/skrap', 'GroepController@skrapRol');
Route::post('groep/nuut', 'GroepController@nuweGroep');
Route::get('groep/skrap', 'GroepController@skrapGroep');
Route::post('rol/nuut', 'GroepController@nuweRol');
Route::post('rol/hernoem', 'GroepController@hernoemRol');
Route::post('groep/hernoem', 'GroepController@hernoemGroep');

//INSKRYWINGS
Route::get('inskrywing/view/{id}', 'InskrywingController@view_inskrywing');
Route::post('inskrywing/edit', 'InskrywingController@opdateer_inskrywing');
Route::get('inskrywing/delete', 'InskrywingController@getDelete');
Route::post('ajax/groep/rolle', 'GroepController@ajax_groep_rolle');
Route::get('ajax/persone/inskrywing', 'InskrywingController@ajax_persone_inskrywing');
Route::get('inskrywings/eposse/{id}', 'InskrywingController@getEmails');

//INSKRYWING NUUT
Route::get('inskrywing/nuut', 'InskrywingController@nuwe_inskrywing');
Route::get('inskrywing/kanseleer', 'InskrywingController@kanseleer_inskrywing');
Route::get('inskrywing/nuut/{id}', 'InskrywingController@nuwe_inskrywing_persoon');
Route::post('inskrywing/opdateer/persoon', 'InskrywingController@opdateer_persoon');
Route::get('inskrywing/betrokkenheid/{id}', 'InskrywingController@nuwe_inskrywing_betrokkenheid');
Route::get('inskrywing/rol/{id}', 'InskrywingController@nuwe_inskrywing_rol');
Route::get('inskrywing/bevestig', 'InskrywingController@nuwe_inskrywing_bevestig');

//ORGANISASIES
Route::get('ajax/organisasies', 'OrganisasieController@ajax_organisasies');

//PDF
Route::get('register/kamp/{id}', 'PDFController@register');
Route::get('groepregister/{id}', 'PDFController@groepregister');
Route::get('naamkaartjies/{id}', 'PDFController@naamkaartjies');
Route::get('diensjare/{id}', 'PDFController@diensjare');
Route::get('opsomming/{id}', 'PDFController@opsomming');
Route::get('siektesallergie/kamp/{id}', 'PDFController@siektesallergie');
Route::get('inskrywings/kamp/{id}', 'PDFController@inskrywings');
Route::get('ekstras/{id}', 'PDFController@getEkstras');

//EXCEL
Route::get('inskrywings/excel/kamp/{id}', 'PDFController@excelInskrywings');

//SUPER ADMIN
Route::get('superadmin','SuperAdminController@users_table');
Route::get('ajax/users', 'SuperAdminController@ajax_users');
Route::get('users/voegby', 'SuperAdminController@user_voeg_by');
Route::post('users/voegby/nuut', 'SuperAdminController@add_user');
Route::post('user/herstel', 'SuperAdminController@user_herstel_wagwoord');
Route::get('user/skrap', 'SuperAdminController@user_skrap');
Route::post('ajax/user/skrap', 'SuperAdminController@ajax_user_skrap');
Route::post('ajax/user/herstel', 'SuperAdminController@ajax_user_herstel');
Route::get('user/toggle/{id}', 'SuperAdminController@user_toggle_status');



Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

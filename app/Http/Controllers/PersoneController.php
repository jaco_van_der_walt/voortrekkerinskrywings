<?php namespace App\Http\Controllers;

use Input;
use Validator;
use Redirect;
use App\Persoon;
use App\Kommando;
use App\Rol;
use App\Inskrywing;
use App\Vorm;
use App\Counter;
use Session;
use Response;
use Log;
use Auth;
use View;
use DateTime;

class PersoneController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function view_voeg_persoon_by()
	{
		return view('persone_voeg_by')->with(['kommando' => $kommando = Kommando::orderBy('kommando_naam')->get()]);
	}

    public function view_persone_table()
    {
        //Ensure that the Session forgets about the "edited person"
        if (Session::has('edit_persoon'))
        {
            Session::forget('edit_persoon');
        }

        return view('persone_table');
    }

    public function getEdit($id)
    {
        return View::make('persone_edit')->with(['persoon' => Persoon::where('registrasie_nommer', $id)->with('kommando')->get(), 'kommando' => $kommando = Kommando::orderBy('kommando_naam')->get()]);
    }

    public function getView($id)
    {
        $persoon = Persoon::where('registrasie_nommer', $id)->first();
        $inskrywings = Inskrywing::where('persoon_id', $persoon->id)->get();

        $inskrywing_arr = array();
        $groep_arr = array();
        $rol_arr = array();
        foreach($inskrywings as $inskrywing)
        {
            array_push($inskrywing_arr, $inskrywing->kamp->kamp_naam);
            $rolle = $inskrywing->persoon->rolle;
            foreach($rolle as $rol)
            {
                if($rol->groep->kamp_id == $inskrywing->kamp_id)
                {
                    $groep_arr[$inskrywing->kamp->kamp_naam] = $rol->groep->groep_naam;
                    $rol_arr[$inskrywing->kamp->kamp_naam] = $rol->rol_naam;
                }
            }
        }

        return View::make('persone_view')->with(['persoon' => $persoon, 'inskrywings' => $inskrywing_arr, 'groepe'=>$groep_arr, 'rolle'=>$rol_arr]);
    }

    //The actual update data, and going to apply it
    public function postEdit()
    {
                //Validate the new input
                $rules = array(
                        'van' => 'Required',
                        'noemnaam' => 'Required',
                        'geboorte_datum' => 'Required|date',
                        'geslag' => 'Required|in:m,f',
                        'id_nommer' => 'digits:13',
                        'kontak_nommer_1' => 'numeric',
                        'kontak_nommer_2' => 'numeric',
                        'epos_adres' => 'email',
                        'ouer_voog_id_nommer_1' => 'digits:13',
                        'ouer_voog_id_nommer_2' => 'digits:13',
                        'ouer_voog_kontak_nommer_1' => 'numeric',
                        'ouer_voog_kontak_nommer_2' => 'numeric',
                        'ouer_voog_epos_adres_1' => 'email',
                        'ouer_voog_epos_adres_2' => 'email',
                        'huisdokter_kontak_nommer_1' => 'numeric',
                        'huisdokter_kontak_nommer_2' => 'numeric'
                );
                //Check if Input passes the rules
                $v = Validator::make(Input::all(), $rules);
                if( $v->passes() ) {
                    //All Input Valid. Let's update the record!
                    $persoon = Persoon::where('registrasie_nommer',Input::get('registrasie_nommer'))->firstOrFail();
                    $persoon_backup = $persoon;

                    //begin setting! 
                    $persoon->van = Input::get('van');
                    $persoon->voorname = Input::get('voorname');
                    $persoon->noemnaam = Input::get('noemnaam');
                    $persoon->geboorte_datum = Input::get('geboorte_datum');
                    $persoon->geslag = Input::get('geslag');
                    $persoon->id_nommer = Input::get('id_nommer');
                    $persoon->geslag = Input::get('geslag');
                    $persoon->kontak_nommer_1 = Input::get('kontak_nommer_1');
                    $persoon->kontak_nommer_2 = Input::get('kontak_nommer_2');
                    $persoon->epos_adres = Input::get('epos_adres');
                    $persoon->voertuig_registrasie_nommer = Input::get('voertuig_registrasie_nommer');
                    $persoon->woonadres = Input::get('woonadres');
                    $persoon->posadres = Input::get('posadres');

                    if(Input::get('geregistreerde_voortrekker') === 'on')
                        {
                           $persoon->voortrekker = true;
                        }
                    else $persoon->voortrekker = false;

                    $persoon->voortrekker_registrasie_nommer = Input::get('voortrekker_registrasie_nommer');
                    $persoon->voortrekker_rang = Input::get('voortrekker_rang');
                    
                    if(Input::get('kommando') != null)
                    {
                       $kommando = Kommando::where('id', Input::get('kommando'))->firstOrFail();
                       $persoon->kommando_id = $kommando->id;
                    }

                    $persoon->ouer_voog_naam_van_1 = Input::get('ouer_voog_naam_van_1');
                    $persoon->ouer_voog_id_nommer_1 = Input::get('ouer_voog_id_nommer_1');
                    $persoon->ouer_voog_kontak_nommer_1 = Input::get('ouer_voog_kontak_nommer_1');
                    $persoon->ouer_voog_epos_adres_1 = Input::get('ouer_voog_epos_adres_1');

                    $persoon->ouer_voog_naam_van_2 = Input::get('ouer_voog_naam_van_2');
                    $persoon->ouer_voog_id_nommer_2 = Input::get('ouer_voog_id_nommer_2');
                    $persoon->ouer_voog_kontak_nommer_2 = Input::get('ouer_voog_kontak_nommer_2');
                    $persoon->ouer_voog_epos_adres_2 = Input::get('ouer_voog_epos_adres_2');

                    if(Input::get('mediese_fonds') === 'on')
                    {
                        $persoon->mediese_fonds = true;
                    }
                    else $persoon->mediese_fonds = false;

                    $persoon->mediese_fonds_naam = Input::get('mediese_fonds_naam');
                    $persoon->mediese_fonds_lid_nommer = Input::get('mediese_fonds_lid_nommer');
                    $persoon->huis_dokter_naam_van = Input::get('huis_dokter_naam_van');
                    $persoon->huis_dokter_kontak_nommer_1 = Input::get('huis_dokter_kontak_nommer_1');
                    $persoon->huis_dokter_kontak_nommer_2 = Input::get('huis_dokter_kontak_nommer_2');
                    $persoon->siekte_allergie = Input::get('siekte_allergie');

                    $persoon->save();

                    Log::notice('2000 || Persoon opgedateer! |', ['veroorsaak_deur' => Auth::user()->toArray(), 'nuwe_persoon_rekord' => $persoon->toArray(), 'ou_persoon_rekord' => $persoon_backup->toArray()]);
                     return Redirect::to('persone/tabel')->with('success', $persoon->noemnaam." ".$persoon->van." is opgedateer");
                }
                else {
                    //Invalid input! Redirecting back...
                    return redirect()->back()->withInput()->withErrors($v);
                }

        return view('persone_table');
    }

    //Deletes the Person currently edited in the Session
    public function getDelete()
    {
        if (Session::has('edit_persoon'))
            {
                $id = Session::get('edit_persoon');
                $persoon = Persoon::find($id);

                $rolle = $persoon->rolle;

                //verwyder die persoon van al sy rolle
                foreach($rolle as $rol)
                {
                    $persoon->rolle()->detach($rol->id);
                }

                $inskrywings = Inskrywing::where('persoon_id', $persoon->id)->get();

                foreach($inskrywings as $inskrywing)
                {
                    $vorms = Vorm::where("inskrywing_id", $inskrywing->id)->get();
                    foreach($vorms as $vorm)
                    {
                        $vorm->delete();
                    }

                    $inskrywing->delete();
                }
                

                $persoon->delete();
                Log::notice("2050 || ".$persoon->noemnaam." ".$persoon->van." is uit die databases sag verwyder |", ['veroorsaak_deur' => Auth::user()->toArray(), 'persoon' => $persoon->toArray()]);
                return Redirect::to('persone/tabel')->with('success', $persoon->noemnaam." ".$persoon->van." is verwyder");
            }
        else {
            Log::warning("2110 || Kon nie 'n persoon in die flashdata vind om te delete nie! |", ['veroorsaak_deur' => Auth::user()->toArray()]);
            return Redirect::to('persone/tabel')->withErrors("Kon nie die versoek voltooi nie");
        }
        
    }

    public function ajax_persone()
    {
        
        $return['recordsTotal'] = Persoon::count();
        $return['data'] = [];
        $i = 0;
        foreach (Persoon::orderBy('van')->with('kommando')->get() as $persoon) {
            $return['data'][$i] = [$i];
            $return['data'][$i][] = [$persoon->voortrekker_registrasie_nommer];
            $return['data'][$i][] = [$persoon->noemnaam];
            $return['data'][$i][] = [$persoon->van];
            $return['data'][$i][] = [$persoon->geboorte_datum];
            if($persoon->kommando)
            {
               $return['data'][$i][] = [$persoon->kommando->kommando_naam]; 
            }
            else
            {
                $return['data'][$i][] ="";
            }
            $return['data'][$i][] = Inskrywing::where('persoon_id', $persoon->id)->count();
            $return['data'][$i][] =  '<a href="' . url("persone/beskou/".$persoon->registrasie_nommer) . '" class="btn btn-xs bg-blue"><i class="fa fa-eye"></i> Beskou</a> <a href="' . url("persone/edit/".$persoon->registrasie_nommer) . '" class="btn btn-xs bg-yellow"><i class="fa fa-wrench"></i> Edit</a>';
            $i++;
        }

        return response()->json($return);
    }

    public function ajax_persoon($id)
    {
        $persoon = Persoon::find($id);

        if ($persoon === null)
        {
            return null;
        }

        if ($persoon->geslag === 'm')
        {
            $persoon->geslag = "Manlik";
        }
        elseif ($persoon->geslag === 'v')
        {
            $persoon->geslag = "Vroulik";
        }

        if ($persoon->kommando_id != null)
        {
            $persoon->kommando_naam = Kommando::find($persoon->kommando_id)->kommando_naam;
        }

        if ($persoon->voortrekker === 1)
        {
            $persoon->voortrekker = "Ja";
        }
        else
        {
            $persoon->voortrekker = "Nee";
        }

        if ($persoon->mediese_fonds === 1)
        {
            $persoon->mediese_fonds = "Ja";
        }
        else
        {
            $persoon->mediese_fonds = "Nee";
        }

        return response()->json($persoon);
    }

	public function submit_voeg_by()
	{
        //Input has to be validated according to the following rules:
		$rules = array(
		        'van' => 'Required',
		        'noemnaam' => 'Required',
		        'geboorte_datum' => 'Required|date',
		        'geslag' => 'Required|in:m,f',
		        'id_nommer' => 'digits:13',
		        'kontak_nommer_1' => 'numeric',
		        'kontak_nommer_2' => 'numeric',
		        'epos_adres' => 'email',
                'ouer_voog_id_nommer_1' => 'digits:13',
                'ouer_voog_id_nommer_2' => 'digits:13',
                'ouer_voog_kontak_nommer_1' => 'numeric',
                'ouer_voog_kontak_nommer_2' => 'numeric',
                'ouer_voog_epos_adres_1' => 'email',
                'ouer_voog_epos_adres_2' => 'email',
                'huisdokter_kontak_nommer_1' => 'numeric',
                'huisdokter_kontak_nommer_2' => 'numeric'

		);

        //Check if Input passes the rules. If yes, add to DB else Reject input
		$v = Validator::make(Input::all(), $rules);

		if( $v->passes() ) {

                //Create Registration Number
                $registrasie_nommer_van = strtoupper(Input::get('van')[0]);
                $dt = new DateTime();
                $year = explode("-", $dt->format('Y-m-d H:i:s'));
                $registrasie_nommer_jaar = substr($year[0], -2);
                $counter = Counter::where('alpha', strtoupper($registrasie_nommer_van))->first();
                $registrasie_nommer_counter = sprintf('%03d', $counter->counter);

                $registrasie_nommer = $registrasie_nommer_van.$registrasie_nommer_jaar.$registrasie_nommer_counter;

                $counter->counter++;
                $counter->save();

                if(Persoon::where('registrasie_nommer', $registrasie_nommer)->count() != 0)
                {
                    Log::warning("2126 || Kon nie 'n registrasie nommer skep nie! Duplikaat gevind! |", ['veroorsaak_deur' => Auth::user()->toArray(), 'verlange_registrasie_nommer' => $registrasie_nommer]);
                    return Redirect::to('persone/tabel')->withErrors("Kon nie die versoek voltooi nie. Probeer asseblief weer.");
                }

                $persoon = new Persoon;

                $persoon->registrasie_nommer = $registrasie_nommer;
                $persoon->van = Input::get('van');
                $persoon->voorname = Input::get('voorname');
                $persoon->noemnaam = Input::get('noemnaam');
                $persoon->geboorte_datum = Input::get('geboorte_datum');
                $persoon->geslag = Input::get('geslag');
                $persoon->id_nommer = Input::get('id_nommer');
                $persoon->geslag = Input::get('geslag');
                $persoon->kontak_nommer_1 = Input::get('kontak_nommer_1');
                $persoon->kontak_nommer_2 = Input::get('kontak_nommer_2');
                $persoon->epos_adres = Input::get('epos_adres');
                $persoon->voertuig_registrasie_nommer = Input::get('voertuig_registrasie_nommer');
                $persoon->woonadres = Input::get('woonadres');
                $persoon->posadres = Input::get('posadres');

                if(Input::get('geregistreerde_voortrekker') === 'on')
                    {
                       $persoon->voortrekker = true;
                    }
                else $persoon->voortrekker = false;

                $persoon->voortrekker_registrasie_nommer = Input::get('voortrekker_registrasie_nommer');
                $persoon->voortrekker_rang = Input::get('voortrekker_rang');
                
                if(Input::get('kommando') != null)
                {
                   $kommando = Kommando::where('id', Input::get('kommando'))->firstOrFail();
                   $persoon->kommando_id = $kommando->id;
                }
                

                $persoon->ouer_voog_naam_van_1 = Input::get('ouer_voog_naam_van_1');
                $persoon->ouer_voog_id_nommer_1 = Input::get('ouer_voog_id_nommer_1');
                $persoon->ouer_voog_kontak_nommer_1 = Input::get('ouer_voog_kontak_nommer_1');
                $persoon->ouer_voog_epos_adres_1 = Input::get('ouer_voog_epos_adres_1');

                $persoon->ouer_voog_naam_van_2 = Input::get('ouer_voog_naam_van_2');
                $persoon->ouer_voog_id_nommer_2 = Input::get('ouer_voog_id_nommer_2');
                $persoon->ouer_voog_kontak_nommer_2 = Input::get('ouer_voog_kontak_nommer_2');
                $persoon->ouer_voog_epos_adres_2 = Input::get('ouer_voog_epos_adres_2');

                if(Input::get('mediese_fonds') === 'on')
                {
                    $persoon->mediese_fonds = true;
                }
                else $persoon->mediese_fonds = false;

                $persoon->mediese_fonds_naam = Input::get('mediese_fonds_naam');
                $persoon->mediese_fonds_lid_nommer = Input::get('mediese_fonds_lid_nommer');
                $persoon->huis_dokter_naam_van = Input::get('huis_dokter_naam_van');
                $persoon->huis_dokter_kontak_nommer_1 = Input::get('huis_dokter_kontak_nommer_1');
                $persoon->huis_dokter_kontak_nommer_2 = Input::get('huis_dokter_kontak_nommer_2');
                $persoon->siekte_allergie = Input::get('siekte_allergie');

                $persoon->save();

                Log::notice('2000 || Nuwe Persoon geskep |', ['veroorsaak_deur' => Auth::user()->toArray(), 'persoon' => $persoon->toArray()]);

                return Redirect::to('persone/tabel')->with('success', $persoon->noemnaam." ".$persoon->van." is gestoor");

		} else { 
		      
                Log::warning("2010 || Validasie op die skep van 'n nuwe persoon was onsuksesvol |", ['veroorsaak_deur' => Auth::user()->toArray(), 'probleem' => $v->messages()->all(), 'input' => Input::all()]);
		       return Redirect::to('persone/voegby')->withInput()->withErrors($v);
		}

	}

}

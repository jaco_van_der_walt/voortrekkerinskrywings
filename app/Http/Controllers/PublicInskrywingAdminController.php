<?php namespace App\Http\Controllers;

use App\Groep;
use App\Kommando;
use App\Persoon;
use App\PubliekeInskrywing;
use App\VoortrekkerBetrokkenheid;
use App\Kamp;
use App\Rol;
use App\Opsie;
use App\Inskrywing;
use Illuminate\Http\RedirectResponse;
use Session;
use Input;
use Redirect;
use Validator;
use Log;
use Auth;
use View;
use App\Counter;
use DateTime;

class PublicInskrywingAdminController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function view_publieke_inskrywings_tabel()
	{
		return view('publieke_inskrywings_tabel')->with(['kamp_id' => Session::get('paneel_kamp')]);
	}

	public function ajax_publiek_inskrywings()
    {
        $return['recordsTotal'] = PubliekeInskrywing::where('kamp_id', Session::get('paneel_kamp'))
                                                    ->where('inskrywing_voltooi', true)
                                                    ->count();

        $flashdata = [];
        $return['data'] = [];
        $i = 0;
        foreach (PubliekeInskrywing::where('kamp_id', Session::get('paneel_kamp'))->where('inskrywing_voltooi',true)->orderBy('created_at', 'DESC')->get() as $inskrywing)
        {
            $return['data'][$i] = [$inskrywing->voortrekker_registrasie_nommer];
            $return['data'][$i][] = [$inskrywing->noemnaam];
            $return['data'][$i][] = [$inskrywing->van];
            $return['data'][$i][] = [$inskrywing->geboorte_datum];

            if($inskrywing->kommando_id != null)
            {
                $return['data'][$i][] = [Kommando::find($inskrywing->kommando_id)->kommando_naam];
            }
            else
        	{
        		$return['data'][$i][] = [];
        	}

        	if($inskrywing->voortrekker_betrokkenheid_id != null)
            {
                $return['data'][$i][] = [VoortrekkerBetrokkenheid::find($inskrywing->voortrekker_betrokkenheid_id)->betrokkenheid];
            }
            else
        	{
        		$return['data'][$i][] = [];
        	}

        	if($inskrywing->groep_id != null)
            {
                $return['data'][$i][] = [Groep::find($inskrywing->groep_id)->groep_naam];
            }
            else
        	{
        		$return['data'][$i][] = [];
        	}

            if($inskrywing->admin_verwerk == true)
            {
                $return['data'][$i][] = '<button class="btn btn-xs bg-olive disabled"><i class="fa fa-check"></i></button>';
                $return['data'][$i][] = '<a href="' . url("publiek/inskrywing/admin/beskou/$inskrywing->id") . '" class="btn btn-xs bg-blue"><i class="fa fa-eye"></i> Beskou</a>';
            }
            else
            {
                $return ['data'][$i][] = '';
                $return['data'][$i][] = '<a href="' . url("publiek/inskrywing/admin/kies/$i") . '" class="btn btn-xs btn-success"><i class="fa fa-wrench"></i> Verwerk</a> <a href="' . url("publiek/inskrywing/admin/beskou/$inskrywing->id") . '" class="btn btn-xs bg-blue"><i class="fa fa-eye"></i> Beskou</a>';
            }



            $flashdata[$i] = $inskrywing->id;
            $i++;
        }

        Session::set('publieke_inskrywings', $flashdata);

        return response()->json($return);
    }

    public function publiek_inskrywing_kies($id)
    {
        $real_id = Session::get('publieke_inskrywings')[$id];

        if ($real_id === false) {
            Log::warning("2605 || Kon nie 'n publieke inskrywing in die flashdata vind nie! |", ['veroorsaak_deur' => Auth::user()->toArray(), "opsoek_na_skuil_id" => $id]);
            return Redirect::to('publiek/inskrywing/admin/tabel')->withErrors("Kon nie die versoek voltooi nie");
        }

        Session::set('verwerk_inskrywing', $real_id);

        $inskrywing = PubliekeInskrywing::where('id', $real_id)->first();

        if ($inskrywing === null)
        {
            return Redirect::back()->withErrors("Kon nie die inskrywing kry nie!");
        }

        if ($inskrywing->geslag === 'm')
        {
            $inskrywing->geslag = "Manlik";
        }
        elseif ($inskrywing->geslag === 'v')
        {
            $inskrywing->geslag = "Vroulik";
        }

        if ($inskrywing->kommando_id != null)
        {
            $inskrywing->kommando_naam = Kommando::find($inskrywing->kommando_id)->kommando_naam;
        }

        if ($inskrywing->voortrekker === 1)
        {
            $inskrywing->voortrekker = "Ja";
        }
        else
        {
            $inskrywing->voortrekker = "Nee";
        }

        if ($inskrywing->mediese_fonds === 1)
        {
            $inskrywing->mediese_fonds = "Ja";
        }
        else
        {
            $inskrywing->mediese_fonds = "Nee";
        }

        $moontlike_persone = Persoon::where('voortrekker_registrasie_nommer', $inskrywing->voortrekker_registrasie_nommer)
                                    ->orwhere('van', $inskrywing->van)
                                    ->orwhere('voorname', $inskrywing->voorname)
                                    ->orwhere('noemnaam', $inskrywing->noemnaam)
                                    ->orwhere('geboorte_datum', $inskrywing->geboorte_datum)
                                    ->orwhere('id_nommer', $inskrywing->id_nommer)
                                    ->orwhere('kontak_nommer_1', $inskrywing->kontak_nommer_1)
                                    ->orwhere('epos_adres', $inskrywing->epos_adres)
                                    ->get();

        return view('publieke_inskrywings_kies')->with(['inskrywing' => $inskrywing, 'moontlike_persone' => $moontlike_persone]);
    }

    public function mergePersone()
    {
        if(Input::get('selected_persoon') == null)
        {
            return Redirect::back()->withErrors("Kies asseblief 'n persoon om die inskrywing mee saam te smelt!");
        };

        $inskrywing = PubliekeInskrywing::find(Session::get('verwerk_inskrywing'));
        $persoon = Persoon::find(Input::get('selected_persoon'));

        if(($inskrywing == null) || ($persoon == null))
        {
            return Redirect::back()->withErrors("Whoops! Iets het skeef geloop maar alles sal steeds okay wees...");
        }

        Session::set('merge_persoon_id', $persoon->id);
				$kamp = Kamp::find(Session::get('paneel_kamp'));

        return view('publieke_inskrywing_merge')->with(['kamp' => $kamp, 'inskrywing' => $inskrywing, 'persoon'=>$persoon, 'kommando' => $kommando = Kommando::orderBy('kommando_naam')->get()]);
    }

    public function updatePersoonMerge()
    {

            if ((Session::has('merge_persoon_id')) && (Session::has('verwerk_inskrywing')))
            {
                //Validate the new input
                $rules = array(
                        'selected_persoon' => 'Required|numeric',
                        'van' => 'Required',
                        'noemnaam' => 'Required',
                        'geboorte_datum' => 'Required|date',
                        'geslag' => 'Required|in:m,f',
                        'id_nommer' => 'digits:13',
                        'kontak_nommer_1' => 'numeric',
                        'kontak_nommer_2' => 'numeric',
                        'epos_adres' => 'email',
                        'ouer_voog_id_nommer_1' => 'digits:13',
                        'ouer_voog_id_nommer_2' => 'digits:13',
                        'ouer_voog_kontak_nommer_1' => 'numeric',
                        'ouer_voog_kontak_nommer_2' => 'numeric',
                        'ouer_voog_epos_adres_1' => 'email',
                        'ouer_voog_epos_adres_2' => 'email',
                        'huisdokter_kontak_nommer_1' => 'numeric',
                        'huisdokter_kontak_nommer_2' => 'numeric'
                );
                //Check if Input passes the rules
                $v = Validator::make(Input::all(), $rules);
                if( $v->passes() ) {

                    //All Input Valid. Let's update the record!
                    $id = Session::get('merge_persoon_id');
                    $persoon = Persoon::find($id);
                    $persoon_backup = $persoon;

                    //begin setting!
                    $persoon->van = Input::get('van');
                    $persoon->voorname = Input::get('voorname');
                    $persoon->noemnaam = Input::get('noemnaam');
                    $persoon->geboorte_datum = Input::get('geboorte_datum');
                    $persoon->geslag = Input::get('geslag');
                    $persoon->id_nommer = Input::get('id_nommer');
                    $persoon->geslag = Input::get('geslag');
                    $persoon->kontak_nommer_1 = Input::get('kontak_nommer_1');
                    $persoon->kontak_nommer_2 = Input::get('kontak_nommer_2');
                    $persoon->epos_adres = Input::get('epos_adres');
                    $persoon->voertuig_registrasie_nommer = Input::get('voertuig_registrasie_nommer');
                    $persoon->woonadres = Input::get('woonadres');
                    $persoon->posadres = Input::get('posadres');

                    if(Input::get('geregistreerde_voortrekker') === 'on')
                        {
                           $persoon->voortrekker = true;
                        }
                    else $persoon->voortrekker = false;

                    $persoon->voortrekker_registrasie_nommer = Input::get('voortrekker_registrasie_nommer');
                    $persoon->voortrekker_rang = Input::get('voortrekker_rang');

                    if(Input::get('kommando') != null)
                    {
                       $kommando = Kommando::where('id', Input::get('kommando'))->firstOrFail();
                       $persoon->kommando_id = $kommando->id;
                    }

                    $persoon->ouer_voog_naam_van_1 = Input::get('ouer_voog_naam_van_1');
                    $persoon->ouer_voog_id_nommer_1 = Input::get('ouer_voog_id_nommer_1');
                    $persoon->ouer_voog_kontak_nommer_1 = Input::get('ouer_voog_kontak_nommer_1');
                    $persoon->ouer_voog_epos_adres_1 = Input::get('ouer_voog_epos_adres_1');

                    $persoon->ouer_voog_naam_van_2 = Input::get('ouer_voog_naam_van_2');
                    $persoon->ouer_voog_id_nommer_2 = Input::get('ouer_voog_id_nommer_2');
                    $persoon->ouer_voog_kontak_nommer_2 = Input::get('ouer_voog_kontak_nommer_2');
                    $persoon->ouer_voog_epos_adres_2 = Input::get('ouer_voog_epos_adres_2');

                    if(Input::get('mediese_fonds') === 'on')
                    {
                        $persoon->mediese_fonds = true;
                    }
                    else $persoon->mediese_fonds = false;

                    $persoon->mediese_fonds_naam = Input::get('mediese_fonds_naam');
                    $persoon->mediese_fonds_lid_nommer = Input::get('mediese_fonds_lid_nommer');
                    $persoon->huis_dokter_naam_van = Input::get('huis_dokter_naam_van');
                    $persoon->huis_dokter_kontak_nommer_1 = Input::get('huis_dokter_kontak_nommer_1');
                    $persoon->huis_dokter_kontak_nommer_2 = Input::get('huis_dokter_kontak_nommer_2');
                    $persoon->siekte_allergie = Input::get('siekte_allergie');

                    $persoon->save();

										Session::set('inskrywing_ekstras', Input::get('ekstras'));

                    $inskrywing = PubliekeInskrywing::find(Session::get('verwerk_inskrywing'));
                    $inskrywing->admin_merge_persoon_id = $persoon->id;
                    $inskrywing->save();

                    Session::set('inskrywing_skoolgraad', Input::get('skoolgraad'));

                    Log::notice('2000 || Persoon saamgesmelt! |', ['veroorsaak_deur' => Auth::user()->toArray(), 'nuwe_persoon_rekord' => $persoon->toArray(), 'ou_persoon_rekord' => $persoon_backup->toArray()]);
                     return Redirect::to('publieke/inskrywing/admin/betrokkenheid')->with('success', $persoon->noemnaam." ".$persoon->van." is opgedateer");
                }
                else {
                    //Invalid input! Redirecting back...
                    return redirect()->back()->withInput()->withErrors($v);
                }

            }
        else {
            Log::warning("2110 || Kon nie 'n persoon in die flashdata vind om mee te merge nie! |", ['veroorsaak_deur' => Auth::user()->toArray()]);
            return Redirect::to('kamp/tabel')->withErrors("Kon nie die versoek voltooi nie");
        }

        return Redirect::to('kamp/tabel')->withErrors("Kon nie die versoek voltooi nie");
    }

    //Show the view where the Admin chooses which role the persoon will have
    public function kiesBetrokkenheid()
    {
        if ((Session::has('merge_persoon_id')) && (Session::has('verwerk_inskrywing')))
        {
            $betrokkenhede = VoortrekkerBetrokkenheid::all();

            return View::make('publieke_inskrywing_kies_betrokkenheid')->with(['persoon' => Persoon::find(Session::get('merge_persoon_id')), 'kamp'=> Kamp::find(Session::get('paneel_kamp')), 'inskrywing' => PubliekeInskrywing::find(Session::get('verwerk_inskrywing')), 'betrokkenhede' => $betrokkenhede]);

        }
        else
        {
            return Redirect::to('kamp/tabel')->withErrors("Kon nie die versoek voltooi nie");
        }
    }

    //Admin has chosen the Betrokkenheid, now we set it in the session and move to select a Rol
    public function setBetrokkenheid($id)
    {
        if ((Session::has('merge_persoon_id')) && (Session::has('verwerk_inskrywing')))
        {
                $betrokkenheid = VoortrekkerBetrokkenheid::find($id);
                if($betrokkenheid == false)
                {
                   Log::warning("2110 || Kon nie die betrokkenheid in databasis kry vir 'n inskrywing nie! |", ['veroorsaak_deur' => Auth::user()->toArray()]);
                        return Redirect::to('kamp/tabel')->withErrors("Die Inskrywing was onsuksesvol!");
                }

                Session::set('publieke_inskrywing_betrokkenheid', $betrokkenheid);
                return Redirect::to('publieke/inskrywing/admin/rol');
        }
        else
        {
            return Redirect::to('kamp/tabel')->withErrors("Kon nie die versoek voltooi nie");
        }

    }

    //Display Select Rol View
    public function kiesRol()
    {
        if ((Session::has('merge_persoon_id')) && (Session::has('verwerk_inskrywing')))
        {
            $persoon = Persoon::find(Session::get('merge_persoon_id'));
            $kamp = Kamp::find(Session::get('paneel_kamp'));
            $inskrywing = PubliekeInskrywing::find(Session::get('verwerk_inskrywing'));
            //We have Kamp, Persoon, Persoon edit and Betrokkenheid. Now we need to set the Groep
            $session_betrokkenheid = Session::get('publieke_inskrywing_betrokkenheid');

            //Find all Groepe that has a Rol where the Persoon kan join
            $rolle = Kamp::find(Session::get('paneel_kamp'))->rolle()->get();
            $groepe = array();
            foreach($rolle as $rol)
            {
                $betrokkenheid_beperkings = $rol->voortrekker_betrokkenhede()->get();
                foreach($betrokkenheid_beperkings as $beperking)
                {
                    $beperking_id = $beperking->id;
                    if($beperking_id === $session_betrokkenheid->id)
                    {
                       $groep = $rol->groep()->get();
                       $groepe[$groep[0]->id]['groep_id'] =$groep[0]->id;
                       $groepe[$groep[0]->id]['groep_naam'] =$groep[0]->groep_naam;
                       $groepe[$groep[0]->id]['rolle'][$rol->id]['rol_id'] = $rol->id;
                       $groepe[$groep[0]->id]['rolle'][$rol->id]['rol_naam'] = $rol->rol_naam;
                    }
                }
            }

            if ($groepe === '')
            {
                return "Geen groepe beskikbaar vir die Inskrywing nie!"; //TODO
            }

            return View::make('publieke_inskrywing_kies_rol')->with(['persoon' => $persoon, 'kamp'=> $kamp, 'groepe' => $groepe, 'inskrywing' => $inskrywing]);
        }
        else
        {
             return Redirect::to('kamp/tabel')->withErrors("Kon nie die versoek voltooi nie");
        }

    }

    public function setRol($id)
    {
        if ((Session::has('merge_persoon_id')) && (Session::has('verwerk_inskrywing')) && (Session::has('publieke_inskrywing_betrokkenheid')))
        {

            //Check if flow is valid
            $session_kamp = Session::get('paneel_kamp');
            $session_persoon = Session::get('merge_persoon_id');
            $session_betrokkenheid = Session::get('publieke_inskrywing_betrokkenheid');
            if(($session_kamp == false) || ($session_persoon == false) || ($session_betrokkenheid == false))
            {
                Log::warning("2110 || Inskrywing Proses was nie korrek voltooi nie! |", ['veroorsaak_deur' => Auth::user()->toArray()]);
                        return Redirect::to('inskrywing/kanseleer')->withErrors("Die Inskrywing was onsuksesvol!");
            }
            else
            {
               $rol = Rol::find($id);
               if($rol == false)
                {
                   Log::warning("2110 || Kon nie die rol in databasis kry vir 'n inskrywing nie! |", ['veroorsaak_deur' => Auth::user()->toArray()]);
                        return Redirect::to('inskrywing/kanseleer')->withErrors("Die Inskrywing was onsuksesvol!");
                }
                Session::set('publieke_inskrywing_rol', $rol);
               //Go to Confirm Screen
               return Redirect::to('publieke/inskrywing/admin/bevestig');
            }
        }
        else
        {
             return Redirect::to('kamp/tabel')->withErrors("Kon nie die versoek voltooi nie");
        }
    }

    public function getBevestig()
    {
        $betrokkenheid = Session::get('publieke_inskrywing_betrokkenheid');
        $rol = Session::get('publieke_inskrywing_rol');
        $groep = Groep::find($rol->groep_id);

        $persoon = Persoon::where('id', Session::get('merge_persoon_id'))->with('kommando')->get();
        $kamp = Kamp::find(Session::get('paneel_kamp'));

        $skoolgraad = Session::get('inskrywing_skoolgraad');
				if (Session::has('inskrywing_ekstras'))
				{
					$ekstras = Session::get('inskrywing_ekstras');
					$ekstra_string = array();
					foreach($ekstras as $e)
					{
						if($e != 'null')
						{
							$opsie = Opsie::find($e);
							$opsie_string = $opsie->ekstra->naam." - ".$opsie->naam;
							array_push($ekstra_string, $opsie_string);
						}
					}

					return View::make('publieke_inskrywing_bevestig')->with(['persoon' => $persoon, 'kamp'=> $kamp, 'betrokkenheid' => $betrokkenheid, 'groep' => $groep, 'rol' => $rol, 'skoolgraad' =>$skoolgraad, 'ekstras'=>$ekstra_string]);

				} else {
					return View::make('publieke_inskrywing_bevestig')->with(['persoon' => $persoon, 'kamp'=> $kamp, 'betrokkenheid' => $betrokkenheid, 'groep' => $groep, 'rol' => $rol, 'skoolgraad' =>$skoolgraad, 'ekstras'=>array()]);
				}




    }

    public function skryfIn()
    {
            $persoon = Persoon::find(Session::get('merge_persoon_id'));
            $rol = Session::get('publieke_inskrywing_rol');
            $betrokkenheid = Session::get('publieke_inskrywing_betrokkenheid');

            $exists = $persoon->rolle->contains($rol);

            if($exists)
            {
              return Redirect::back()->withErrors($persoon->noemnaam." ".$persoon->van." het reeds die rol ".$rol->rol_naam);
            }
            else
            {

              //Check if Inskrywing exists
              $check_inskrywing = Inskrywing::where('persoon_id', $persoon->id)->where('kamp_id', $rol->groep->kamp_id)->first();
              if($check_inskrywing === null)
              {
                //Skep die Inskrywing
                $inskrywing = new Inskrywing;
                $inskrywing->persoon_id = $persoon->id;
                $inskrywing->kamp_id = $rol->groep->kamp_id;
                $inskrywing->voortrekker_betrokkenheid_id = $betrokkenheid->id;

                $inskrywing->skoolgraad = Session::get('inskrywing_skoolgraad');

                $publieke_inskrywing = PubliekeInskrywing::find(Session::get('verwerk_inskrywing'));
                $inskrywing->verwysing = $publieke_inskrywing->verwysing;
								$inskrywing->save();

								if(Session::has('inskrywing_ekstras'))
								{
									$ekstras = Session::get('inskrywing_ekstras');
									foreach($ekstras as $e)
									{
										if($e != 'null')
										{
											$opsie = Opsie::find($e);
											$inskrywing->opsies()->attach($opsie);
											$inskrywing->save();
										}
									}
								}


                $persoon->rolle()->attach($rol);
                $inskrywing->save();
              }
              else
              {
                return Redirect::back()->withErrors($persoon->noemnaam." ".$persoon->van." het reeds 'n inskrywing");
              }

              $publieke_inskrywing = PubliekeInskrywing::find(Session::get('verwerk_inskrywing'));
              $publieke_inskrywing->admin_verwerk = true;
              $publieke_inskrywing->save();

                //Forget all Session Variables
                Session::forget('merge_persoon_id');
                Session::forget('publieke_inskrywing_rol');
                Session::forget('publieke_inskrywing_betrokkenheid');
                Session::forget('verwerk_inskrywing');

              return Redirect::to('kamp/paneel/'.$inskrywing->kamp_id)->with('success', $persoon->noemnaam." ".$persoon->van." is nou ingeskryf!");
            }
    }

    public function beskouInskrywing($id)
    {
        $publieke_inskrywing = PubliekeInskrywing::find($id);

        $kamp = Kamp::find($publieke_inskrywing->kamp_id);

        $betrokkenheid = VoortrekkerBetrokkenheid::find($publieke_inskrywing->voortrekker_betrokkenheid_id)->betrokkenheid;

				$g = Groep::find($publieke_inskrywing->groep_id);
				if($g)
				{
					$groep = $g->groep_naam;
				} else {
					$groep = "";
				}

        if($publieke_inskrywing->kommando_id != '')
        {
            $kommando = Kommando::find($publieke_inskrywing->kommando_id)->kommando_naam;
        }
        else
        {
            $kommando = '';
        }

        return view('publieke_inskrywing_beskou')->with(['inskrywing' => $publieke_inskrywing, 'betrokkenheid' => $betrokkenheid, 'kommando' => $kommando, 'groep' => $groep, 'kamp' => $kamp]);
    }

    public function skrapInskrywing($id)
    {
        $publieke_inskrywing = PubliekeInskrywing::find($id);
        $kamp = Kamp::find($publieke_inskrywing->kamp_id);

        $publieke_inskrywing->delete();

        return Redirect::to('kamp/paneel/'.$kamp->id)->with('success', $publieke_inskrywing->noemnaam." ".$publieke_inskrywing->van." se aanlyn inskrywing is geskrap");
    }

		public function herverwerkInskrywing($id)
		{
			$publieke_inskrywing = PubliekeInskrywing::find($id);
			$kamp = Kamp::find($publieke_inskrywing->kamp_id);
			$publieke_inskrywing->admin_verwerk = false;
			$publieke_inskrywing->save();

			return Redirect::to('kamp/paneel/'.$kamp->id)->with('success', $publieke_inskrywing->noemnaam." ".$publieke_inskrywing->van." se aanlyn inskrywing is gereed vir herverwerking");
		}

    public function skepPersoon($id)
    {
        $publieke_inskrywing = PubliekeInskrywing::find($id);
        $persoon = new Persoon();
        $persoon->van = $publieke_inskrywing->van;
        $persoon->voorname = $publieke_inskrywing->voorname;
        $persoon->id_nommer = $publieke_inskrywing->id_nommer;
        $persoon->geslag = $publieke_inskrywing->geslag;
        $persoon->noemnaam = $publieke_inskrywing->noemnaam;
        $persoon->geboorte_datum = $publieke_inskrywing->geboorte_datum;
        $persoon->voortrekker_registrasie_nommer = $publieke_inskrywing->voortrekker_registrasie_nommer;
        $persoon->kontak_nommer_1 = $publieke_inskrywing->kontak_nommer_1;
        $persoon->kontak_nommer_2 = $publieke_inskrywing->kontak_nommer_2;
        $persoon->epos_adres = $publieke_inskrywing->epos_adres;
        $persoon->woonadres = $publieke_inskrywing->woonadres;
        $persoon->posadres = $publieke_inskrywing->posadres;
        $persoon->voertuig_registrasie_nommer = $publieke_inskrywing->voertuig_registrasie_nommer;
        $persoon->voortrekker_rang = $publieke_inskrywing->voortrekker_rang;

        //Ouer/Voog
        $persoon->ouer_voog_naam_van_1 = $publieke_inskrywing->ouer_voog_naam_van_1;
        $persoon->ouer_voog_naam_van_2 = $publieke_inskrywing->ouer_voog_naam_van_2;
        $persoon->ouer_voog_id_nommer_1 = $publieke_inskrywing->ouer_voog_id_nommer_1;
        $persoon->ouer_voog_id_nommer_2 = $publieke_inskrywing->ouer_voog_id_nommer_2;
        $persoon->ouer_voog_kontak_nommer_1 = $publieke_inskrywing->ouer_voog_kontak_nommer_1;
        $persoon->ouer_voog_kontak_nommer_2 = $publieke_inskrywing->ouer_voog_kontak_nommer_2;
        $persoon->ouer_voog_epos_adres_1 = $publieke_inskrywing->ouer_voog_epos_adres_1;
        $persoon->ouer_voog_epos_adres_2 = $publieke_inskrywing->ouer_voog_epos_adres_2;

        //Medies
        $persoon->mediese_fonds = $publieke_inskrywing->mediese_fonds;
        $persoon->mediese_fonds_naam = $publieke_inskrywing->mediese_fonds_naam;
        $persoon->mediese_fonds_lid_nommer = $publieke_inskrywing->mediese_fonds_lid_nommer;
        $persoon->huis_dokter_naam_van = $publieke_inskrywing->huis_dokter_naam_van;
        $persoon->huis_dokter_kontak_nommer_1 = $publieke_inskrywing->huis_dokter_kontak_nommer_1;
        $persoon->huis_dokter_kontak_nommer_2 = $publieke_inskrywing->huis_dokter_kontak_nommer_2;
        $persoon->siekte_allergie = $publieke_inskrywing->siekte_allergie;

        //Skep Registrasie Nommer
        $registrasie_nommer_van = strtoupper($publieke_inskrywing->van[0]);
        $dt = new DateTime();
        $year = explode("-", $dt->format('Y-m-d H:i:s'));
        $registrasie_nommer_jaar = substr($year[0], -2);
        $counter = Counter::where('alpha', strtoupper($registrasie_nommer_van))->first();
        $registrasie_nommer_counter = sprintf('%03d', $counter->counter);

        $registrasie_nommer = $registrasie_nommer_van.$registrasie_nommer_jaar.$registrasie_nommer_counter;

        $counter->counter++;
        $counter->save();

				$kamp = Kamp::find($publieke_inskrywing->kamp_id);

        if(Persoon::where('registrasie_nommer', $registrasie_nommer)->count() != 0)
        {
            Log::warning("2126 || Kon nie 'n registrasie nommer skep nie! Duplikaat gevind! |", ['veroorsaak_deur' => Auth::user()->toArray(), 'verlange_registrasie_nommer' => $registrasie_nommer]);
            return Redirect::to('persone/tabel')->withErrors("Kon nie die versoek voltooi nie. Probeer asseblief weer.");
        }

        $persoon->registrasie_nommer = $registrasie_nommer;
        $persoon->save();

        Session::set('merge_persoon_id', $persoon->id);

        return view('publieke_inskrywing_merge')->with(['kamp'=>$kamp, 'inskrywing' => $publieke_inskrywing, 'persoon'=>$persoon, 'kommando' => $kommando = Kommando::orderBy('kommando_naam')->get()]);
    }

}

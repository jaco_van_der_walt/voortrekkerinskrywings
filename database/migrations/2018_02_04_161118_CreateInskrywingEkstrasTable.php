<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInskrywingEkstrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('inskrywing_ekstras', function(Blueprint $table)
      {
          $table->integer('inskrywing_id')->unsigned();
          $table->integer('opsie_id')->unsigned();

          $table->foreign('inskrywing_id')->references('id')->on('inskrywings');
          $table->foreign('opsie_id')->references('id')->on('opsies');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('publieke_inskrywing_ekstras');
    }
}

@extends('layouts.master')

@section('head')
@parent
    <title>Logs</title>
@stop


@section('content')
    <embed src="{{URL('logs')}}" class="pull-right" style="width: 85%; height: 90vh;"></embed>
@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>
@stop
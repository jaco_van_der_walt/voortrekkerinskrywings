@extends('layouts.master')

@section('head')
@parent
<title>Kampe</title>
@stop


@section('content')
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Kampe
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Kampe</a></li>
                        <li class="active">Voeg By</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                @if($errors->has())
                   @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Waarskuwing!</b> {{$error}}
                        </div>
                  @endforeach
                @endif


                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- form start -->
                            <form role="form" action="{{URL('kamp/voegby/nuut')}}" method="POST">
                                <!-- general form elements -->
                                <div class="box box-primary">
                                    <div class="box-header">
                                        <h3 class="box-title"><b>Kamp Inligting</b></h3>
                                    </div><!-- /.box-header -->
                                    
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    
                                        <div class="box-body">

                                            <!-- Naam -->
                                            <div class="row form-group">
                                                <div class="col-xs-2">
                                                    <label>Kamp Naam <span>*</span></label>
                                                </div>
                                                <div class="col-xs-4">
                                                    <input type="text" class="form-control" name="kamp_naam" value="{{Input::old('kamp_naam')}}"> 
                                                </div>   
                                            </div>
                                        

                                             <!-- Begin dd/mm/yyyy -->
                                            <div class="row form-group">
                                                <div class="col-xs-2">
                                                        <label>Begin Datum <span>*</span></label>
                                                 </div>
                                                <div class="col-xs-4">
                                                    <input id="begin" name="begin" type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask value="{{Input::old('begin')}}"/>
                                                </div><!-- /.input group -->
                                            </div><!-- /.form group -->

                                            <!-- Eindig dd/mm/yyyy -->
                                            <div class="row form-group">
                                                <div class="col-xs-2">
                                                        <label>Eindig Datum <span>*</span></label>
                                                 </div>
                                                <div class="col-xs-4">
                                                    <input id="eindig" name="eindig" type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask value="{{Input::old('eindig')}}"/>
                                                </div><!-- /.input group -->
                                            </div><!-- /.form group -->

                                        </div>


                                </div>

                                <div class="box box-primary">
                                    <div class="box-header">
                                        <h3 class="box-title"><b>Groepe & Rolle</b></h3>
                                    </div><!-- /.box-header -->

                                    <div id="groepe_rolle_box" class="box-body">


                                        <div class="row form-group">
                                            <div class="col-xs-2">
                                            <button class="btn btn-success" style="margin-right: 5px;" data-toggle="modal" data-target="#addGroepRolModal"><i class="fa fa-plus"></i>  Nuwe Groep</button>  
                                            </div>
                                        </div>



                                        <!-- NEW GROUPS ADDED HERE -->






                                    </div>


                                     
                                </div>

                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary pull-right" style="margin-right: 5px;">Stoor</button>
                                </div>

                            </form>
                        </div>
                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->


<!-- MODALS -->
<div id="addGroepRolModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Nuwe Groep</h4>
      </div>
      <div class="modal-body">
         <div class="row form-group">
                <div class="col-xs-4">
                    <label>Groep Naam <span>*</span></label>
                </div>
                <div class="col-xs-8">
                    <input id="new_group_name" type="text" class="form-control"> 
                </div>   
         </div>

         <div class="row form-grouo">
                <div class="col-xs-4">
                    <label>Rolle</label>
                    <br>
                    <p><i>Een rol per lyn</i></p>                   
                </div>
                <div class="col-xs-8">
                    <textarea id="new_group_rolle" class="form-control" rows="4">Kursusleier&#10;Kursusaanbieder&#10;Kursusganger</textarea>
                </div>
         </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary pull-left" data-dismiss="modal">Cancel</button>
        <a href="#" onclick=validate()><button class="btn btn-success pull-right"><i class="fa  fa-plus"></i>  Voeg By</button></a>
      </div>
    </div>

  </div>
</div>



@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>

        <!-- InputMask -->
        <script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.date.extensions.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.extensions.js')}}" type="text/javascript"></script>


        <script type="text/javascript">
            //Datemask
            $("#begin").inputmask("yyyy/mm/dd", {"placeholder": "yyyy/mm/dd"});
            $("#eindig").inputmask("yyyy/mm/dd", {"placeholder": "yyyy/mm/dd"});

            function addGroup()
            {
                var group_name = $("#new_group_name").val();



                var group_rolle = $("#new_group_rolle").val();
                var lines = group_rolle.split(/\n/); 
                var retext = "";
                for(var i in lines) {
                    retext += lines[i] + "<br>";
                }
                $("#new_group_name").val("");
                $("#new_group_rolle").val("Kursusleier\nKursusaanbieder\nKursusganger");
                //Insert the data from the Modal onto the Page, including hidden HTML input tags containing the Form data
                $("#groepe_rolle_box").append("<div class='row form-group'><div class='col-md-4'><div class='box box-solid bg-light-blue'><div class='box-header'><h3 class='box-title'>" + group_name + "</h3><button type='button' style='margin-top: 3px; margin-right: 3px;' class='btn btn-danger pull-right' onclick=deleteAlert(this)><i class='fa  fa-trash-o'></i></button></div><div class='box-body'><p>"+ retext +"</p><input name='groep_name[]' value='"+group_name+"' type='text' hidden><input name='rolle_name[]' value='"+lines+"' type='text' hidden></div></div></div></div>");
            }

            //Delete the entire element (including hidden divs)
            function deleteAlert(_elem)
            {
                $(_elem).closest('.row').remove();
            }

            function validate()
            {
                if($("#new_group_name").val() === "")
                    {return;}
                else{
                    addGroup();
                    $('#addGroepRolModal').modal('toggle');
                };
 
            }
        </script> 


@stop
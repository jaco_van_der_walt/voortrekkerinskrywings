@extends('layouts.public_inskrywing')

@section('head')
@parent
<title>Inskrywing</title>
@stop


@section('content')
<aside class="right-side stretch">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Inskrywing: {{$kamp->kamp_naam}}
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">

                    @if($errors->has())
                       @foreach ($errors->all() as $error)
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Waarskuwing!</b> {{$error}}
                            </div>
                      @endforeach
                    @endif

                    <img source="{{URL::asset('assets/img/inskrywing_voltooi.png')}}">

                    <div class="row">
                        <div class="col-md-12">

                        <div class="row">
                            <div class="col-md-12">
                            <!-- Success box -->
                            <div class="box box-solid bg-green">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-check"></i> Inskrywing Voltooi</h3>
                                </div>
                                <div class="box-body">
                                    <strong> Die Inskrywing vir {{$persoon->noemnaam}} {{$persoon->van}} is suksesvol ontvang! </strong>
                                    <br>
                                    <strong> Die verwysingsnommer vir hierdie inskrywing is {{$persoon->verwysing}} </strong>
                                    <br><br>
                                    <p>
                                        'n Epos met instruksies vir betaling is nou gestuur na die epos adres wat jy voorsien het. Onthou dat 'n inskrywing slegs aanvaar sal word indien die volle kampfooi binne 7 dae betaal is. Maak seker jy betaal so gou as moontlik die kampfooi om jou inskrywing te verseker! Indien jy nie binnekort 'n epos ontvang nie, kyk in jou 'Spam' of 'Promotions' vouer!
                                    </p>

                                    <p>
                                        Indien jy hulp benodig met die inskrywing of 'n alternatiewe reëling wil tref kontak asseblief vir <strong>{{$kamp->organisasies[0]->admin_naam}}</strong> op <strong>{{$kamp->organisasies[0]->admin_tel}}</strong> of <strong>{{$kamp->organisasies[0]->admin_epos}}</strong>
                                    </p>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div><!-- /.col -->
                        </div>


                        <!-- footer -->
                        <div class="box-footer col-md-12">
                            <div class="row">
                                    <a href="{{URL('/')}}" class="btn btn-primary pull-right" style="margin-right: 5px;" ><i class="fa fa-home"></i> Hoofblad </a>
                            </div>
                        </div>
                        <!-- /footer -->
                        </div>
                    </div>


                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>


@stop

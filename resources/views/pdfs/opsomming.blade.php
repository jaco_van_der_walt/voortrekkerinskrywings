<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<style>
			table, td, th {
			    border: 1px solid black;
			    width: 5cm;
			    border-collapse: collapse;
			    padding: 5px;
			    word-wrap:break-word;
			    text-align: center;
			}

			table {
				page-break-after: always;
			}

			.heading {
				text-align: left;
			}

			body {
				font-family: DejaVu Sans, sans-serif;
				font-size:12px;
			}
		</style>
	</head>
	<body>

	<h1>Kamp Opsomming</h1>
	<div>
		<table>
			<tr>
				<td colspan=2>
					<label><b>Kamp Statistiek</b></label>
				</td>
			</tr>
			@foreach($betrokkenhede as $betrokkenheid)
			<tr>
			<td>
				<label>{{$betrokkenheid->betrokkenheid}}</label>
			</td>
			<td>
				<label >{{$kamp_stats[$betrokkenheid->id]}}</label>
			</td>
			@endforeach
			<tr>
				<td>
					<label><b>Totaal:</b></label>
				</td>
				<td>
					<b>{{$kamp_stats['Totaal']}}</b>
				</td>
			</tr>
		</table>
	</div>




	<h2>Groepe</h2>
	<div>
		<table>
			@foreach($groepe as $groep)
			<tr>
			<td colspan=2>
				<label><b>{{$groep->groep_naam}}</b></label>
			</td>
			</tr>
				@foreach($betrokkenhede as $betrokkenheid)
					@if($groepe_stats[$groep->groep_naam][$betrokkenheid->id] > 0)
					<tr>
						<td>
							<label >{{$betrokkenheid->betrokkenheid}}</label>
						</td>
						<td>
							<label >{{$groepe_stats[$groep->groep_naam][$betrokkenheid->id]}}</label>
						</td>
					</tr>
					@endif
				@endforeach
			@endforeach
		</table>
	</div>


	<h2>Kommandos</h2>
	<div>
		<table>
			<tr>
				<td colspan="2" >
					<label><b>Kommandos</b></label>
				</td>

			</tr>
			@foreach($kommandos as $kommando)
			<tr>
				<td>
					{{$kommando['naam']}}
				</td>
				<td>
					{{$kommando['count']}}
				</td>
			</tr>
			@endforeach
		</table>
	</div>

	</body>
</html>

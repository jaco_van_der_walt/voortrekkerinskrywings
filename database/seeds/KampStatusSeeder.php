<?php

use Illuminate\Database\Seeder;
use App\KampStatus;

class KampStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    // NOTE: If this class cannot be found try running 'composer dump-autoload'
    public function run()
    {
        
		KampStatus::create(array('status' => 'Geen'));
		KampStatus::create(array('status' => 'Oop'));
		KampStatus::create(array('status' => 'Gesluit'));
		KampStatus::create(array('status' => 'AanvaarInskrywings'));
		
		$this->command->info("KampStatus table seeded!");
    }
}

@extends('layouts.master')

@section('head')
@parent
<title>{{$kamp->kamp_naam}}: {{$groep->groep_naam}}</title>
<script src="{{URL('//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css')}}" type="text/css"></script>
@stop


@section('content')
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                         <a href={{URL('kamp/paneel/'.$kamp->id)}}><button type="submit" class="btn btn-primary "><i class="fa fa-arrow-circle-left"></i></button></a> {{$kamp->kamp_naam}}: {{$groep->groep_naam}}
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">

                @if($errors->has())
                   @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Waarskuwing!</b> {{$error}}
                        </div>
                  @endforeach
                @endif

                @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissable">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Sukses!</b> {{Session::get('success')}}
                        </div>
                @endif


                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-md-12">

                            <!-- Rolle -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3 class="box-title"><b>{{$groep->groep_naam}} Rolle</b></h3>

                                            <div class="btn-group pull-right" style="margin-right: 15px; margin-top: 5px;">
                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                    Opsies&nbsp;&nbsp;<span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="{{URL('groepregister',$groep->id)}}"><i class="fa  fa-list">  Groepregister</i></a></li>
                                                    <li><a href="#" onclick="nuweRol()"><i class="fa  fa-plus">  Nuwe Rol</i></a></li>
                                                    <li><a href="#" onclick="hernoemGroep()"><i class="fa fa-edit">  Wysig Groep</i></a></li>
                                                    <li><a href="#" onclick="skrapGroep()"><i class="fa  fa-trash-o"></i>Skrap Groep</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-md-12">
                                        <p class="col-md-12"><em>{{$groep->beskrywing}}</em></p>
                                      </div>
                                    </div>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <div class="row">
                                    @foreach($rolle as $rol)
                                        <div class="col-md-4">
                                            <!-- Block buttons -->
                                            <div class="box">
                                                <div id="{{$rol->id}}" class="box-header">
                                                    <h3 class="box-title">{{$rol->rol_naam}}</h3>
                                                    <div class="btn-group pull-right" style="margin-right: 15px; margin-top: 5px;">
                                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                            Opsies&nbsp;&nbsp;<span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" class="hernoem"><i class="fa fa-edit">  Wysig Rol</i></a></li>
                                                            <li><a href="#" class="skrap" onclick="sertifikate()"><i class="fa  fa-trash-o"></i>Skrap Rol</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div id="{{$rol->id}}" class="box-body">
                                                    <button class="btn btn-block btn-flat bg-olive vervul" ><i class="fa fa-user-plus"></i> Vervul Rol</button>
                                                </div>
                                            </div><!-- /.box -->
                                        </div>
                                    @endforeach
                                    </div>
                                </div>
                            </div>
                            <!-- Persons -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><b>Persone</b></h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            @foreach($betrokkenhede as $betrokkenheid)
                                                @if($betrokkenheid_stats[$betrokkenheid->id] > 0)
                                                    <button class="col-md-2 btn disabled" style="background: {{$betrokkenheid->kleur}};color: white; margin-right: 5px; margin-top: 5px;">{{$betrokkenheid->betrokkenheid}}: {{$betrokkenheid_stats[$betrokkenheid->id]}}</button>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="box-body table-responsive">
                                    <table id="groep_persone" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Naam</th>
                                                <th>Van</th>
                                                <th>Geboortedatum</th>
                                                <th>Kommando</th>
                                                <th>Rol</th>
                                                <th>Aksies</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>#</th>
                                                <th>Naam</th>
                                                <th>Van</th>
                                                <th>Geboortedatum</th>
                                                <th>Kommando</th>
                                                <th>Rol</th>
                                                <th>Aksies</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                                </div>

                            </div><!-- /.box -->



                            <div class="box-footer">
                            </div>

                    </div><!-- /.row (main row) -->


                <!-- Vervul Rol Modal -->
                <div class="modal fade" id="vervul_rol" role="dialog">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title" id="vervul_rol_header"></h4>
                        </div>
                        <div class="modal-body">
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Naam</th>
                                                <th>Van</th>
                                                <th>Geboortedatum</th>
                                                <th>Kommando</th>
                                                <th>Diensjare</th>
                                                <th>Aksies</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>#</th>
                                                <th>Naam</th>
                                                <th>Van</th>
                                                <th>Geboortedatum</th>
                                                <th>Kommando</th>
                                                <th>Diensjare</th>
                                                <th>Aksies</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" onclick="destroyDataTable()">Maak Toe</button>
                        </div>
                      </div>
                    </div>
                </div>
                <!-- End Vervul Rol Modal -->


                <!-- Vervul Rol Modal -->
                <div class="modal fade" id="skrap_rol" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title" id="skrap_rol_header"></h4>
                        </div>
                        <div class="modal-body">
                            <div class="box-body table-responsive">
                                <p>Is jy seker jy wil hierdie rol skrap? Alle persone wat tans die rol vervul se inskrywing sal gehou word, maar sal nie meer die rol vervul nie.</p>
                            </div><!-- /.box-body -->
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-primary" onclick="destroySkrapRol()"> Kanseleer</button>
                          <a href={{URL('groep/rol/skrap')}}><button class="btn btn-danger pull-left"><i class="fa  fa-trash-o"></i>  Skrap</button></a>
                        </div>
                      </div>
                    </div>
                </div>
                <!-- End Vervul Rol Modal -->

                <!-- Skrap Groep Modal -->
                <div class="modal fade" id="skrap_groep" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title" id="skrap_groep_header">Skrap {{$kamp->kamp_naam}}: {{$groep->groep_naam}}</h4>
                        </div>
                        <div class="modal-body">
                                <div class="box-body table-responsive">
                                    <p>Is jy seker jy wil die hele Groep {{$groep->groep_naam}} van {{$kamp->kamp_naam}} skrap?</p>
                                    <br>
                                    <p>Alle rolle in die groep sal ook verwyder word</p>
                                </div><!-- /.box-body -->
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-primary" data-dismiss="modal"> Kanseleer</button>
                          <a href="{{URL('groep/skrap/')}}" class="btn btn-danger pull-left"><i class="fa  fa-trash-o"></i>  Skrap</a>
                        </div>
                      </div>
                    </div>
                </div>
                <!-- End Skrap Groep Modal -->

                <!-- Hernoem Groep Modal -->
                <div class="modal fade" id="hernoem_groep" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Wysig {{$groep->groep_naam}}</h4>
                        </div>
                        <div class="modal-body">
                                <div class="box-body table-responsive">
                                    {!! Form::open(array('url'=>'groep/hernoem','method'=>'POST')) !!}
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="row form-group">
                                                <div class="col-xs-3">
                                                    <label>Groep Naam <span>*</span></label>
                                                </div>
                                                <div class="col-xs-9">
                                                    <input type="text" id="hernoem_groep_naam" class="form-control" name="hernoem_groep_naam" value="{{$groep->groep_naam}}">
                                                </div>
                                    </div>
                                    <div class="row form-group">
                                                <div class="col-xs-3">
                                                    <label>Beskrywing</label>
                                                </div>
                                                <div class="col-xs-9">
                                                    <input type="text" id="groep_beskrywing" class="form-control" name="groep_beskrywing" value="{{$groep->beskrywing}}">
                                                </div>
                                    </div>
                                </div><!-- /.box-body -->
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-primary pull-left" data-dismiss="modal"> Kanseleer</button>
                          <a href={{URL('groep/hernoem')}}><button type="submit" class="btn bg-yellow "><i class="fa  fa-edit"></i>  Wysig</button></a>
                          {!! Form::close() !!}
                        </div>
                      </div>
                    </div>
                </div>
                <!-- Hernoem Groep Modal -->

                <!-- Hernoem Rol Modal -->
                <div class="modal fade" id="hernoem_rol" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title" id="hernoem_rol_header">Instellings vir {{$kamp->kamp_naam}}: {{$groep->groep_naam}}</h4>
                        </div>
                        <div class="modal-body">
                                <div class="box-body">
                                {!! Form::open(array('url'=>'rol/hernoem','method'=>'POST')) !!}
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="row form-group">
                                            <div class="col-xs-3">
                                                <label>Rol Naam <span>*</span></label>
                                            </div>
                                            <div class="col-xs-9">
                                                <input type="text" id="hernoem_rol_naam1" class="form-control" name="hernoem_rol_naam" value="{{Input::old('hernoem_rol_naam1')}}">
                                            </div>
                                        </div>
                                    <div class="row form-group">
                                            <div class="col-xs-3">
                                                <label>Inskrywing Beperking <span>*</span></label>
                                            </div>
                                            <div class="col-xs-9">
                                                <input type="number" id="rol_inskrywing_beperking" class="form-control" name="rol_inskrywing_beperking" value="{{Input::old('rol_inskrywing_beperking')}}">
                                            </div>
                                    </div>
                                    <div class="row form-group">
                                            <div class="col-xs-3">
                                                <label>Inskrywing Koste <span>*</span></label>
                                            </div>
                                            <div class="col-xs-9">
                                                <input type="number" id="rol_inskrywing_koste" class="form-control" name="rol_inskrywing_koste" value="{{Input::old('rol_inskrywing_koste')}}" step="any">
                                            </div>
                                    </div>
                                    <div class="row form-group">
                                            <div class="col-xs-3">
                                                <label>Betrokkenheid Beperking</label>
                                            </div>
                                            <div id="betrokkenheid_div" class="col-xs-9">

                                            </div>
                                        </div>
                                </div><!-- /.box-body -->
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-primary pull-left" data-dismiss="modal"> Kanseleer</button>
                          <button type="submit" class="btn bg-yellow"><i class="fa  fa-edit"></i>  Opdateer</button>
                          {!! Form::close() !!}
                        </div>
                      </div>
                    </div>
                </div>
                <!-- End Hernoem Groep Modal -->

                <!-- Nuwe Rol Modal -->
                <div class="modal fade" id="nuwe_rol" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title" id="skrap_groep_header">Nuwe Rol</h4>
                        </div>
                        <div class="modal-body">
                         {!! Form::open(array('url'=>'rol/nuut','method'=>'POST')) !!}
                         <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="box-body">
                                    <div class="row form-group">
                                            <div class="col-xs-3">
                                                <label>Rol Naam <span>*</span></label>
                                            </div>
                                            <div class="col-xs-9">
                                                <input type="text" class="form-control" name="rol_naam" value="{{Input::old('rol_naam')}}">
                                            </div>
                                        </div>
                                </div><!-- /.box-body -->
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-primary pull-left" data-dismiss="modal"> Kanseleer</button>
                          <button type="submit" class="btn pull-right bg-olive"><i class="fa  fa-plus"></i>  Skep Rol</button>
                        </div>
                         {!! Form::close() !!}
                      </div>
                    </div>
                </div>
                <!-- End Skrap Groep Modal -->



                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>

         <!-- DATA TABES SCRIPT -->
        <script src="{{URL('//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('assets/js/plugins/datatables/dataTables.bootstrap.js')}}" type="text/javascript"></script>

        <script type="text/javascript">

            $(function() {
                $('#groep_persone').DataTable( {
                    "sAjaxSource": "{{{URL('ajax/groep/persone/rolle')}}}",
                    "sServerMethod": "GET"
                  });

            });

            $('.vervul').click(function(e){
                e.preventDefault();
                var container_id = $(this).parents('.box-body').attr('id');
                openVervulRol(container_id);
            });

            $('.skrap').click(function(e){
                e.preventDefault();
                var container_id = $(this).parents('.box-header').attr('id');
                openSkrapRol(container_id);
            });

            $('.hernoem').click(function(e){
                e.preventDefault();
                var container_id = $(this).parents('.box-header').attr('id');
                openHernoemRol(container_id);
            });

            function openSkrapRol(id)
            {
                $.ajax({
                  method: "POST",
                  url: "{{URL('ajax/groep/rolle')}}",
                  data: { rol_id: id,
                          '_token': '{!! csrf_token() !!}'}
                })
                  .done(function( result ) {
                    $('#skrap_rol_header').html(" Skrap {{$groep->groep_naam}}: " + result.rol_naam + "?");

                    $('#skrap_rol').modal('show');
                  });

            }

            function openHernoemRol(id)
            {
                $.ajax({
                  method: "POST",
                  url: "{{URL('ajax/groep/rolle')}}",
                  data: { rol_id: id,
                          '_token': '{!! csrf_token() !!}'}
                })
                  .done(function( result ) {
                    $('#hernoem_rol_header').html(" Instellings vir {{$groep->groep_naam}}: " + result.rol_naam + "?");
                    $('#hernoem_rol_naam1').val(result.rol_naam);
                    $('#rol_inskrywing_beperking').val(result.inskrywing_beperking);
                    $('#rol_inskrywing_koste').val(result.inskrywing_koste);
                    $('#betrokkenheid_div').html("");
                    for(var i=0; i<result.voortrekker_betrokkenheid.length;i++)
                    {
                        var check = false;
                        for(var j=0; j<result.rol_betrokkenhede.length; j++)
                        {
                            //Check if already has the role...
                            if(result.rol_betrokkenhede[j].id == result.voortrekker_betrokkenheid[i].id)
                            {
                                check = true;
                            }

                        };

                        if(check)
                            {
                                $('#betrokkenheid_div').append("<div><input type='checkbox' checked  name='voortrekker_betrokkenheid[]' value='"+result.voortrekker_betrokkenheid[i].id+"'/><label> &nbsp"+result.voortrekker_betrokkenheid[i].betrokkenheid+"</label></div>");
                            }
                        else
                            {
                                $('#betrokkenheid_div').append("<div><input type='checkbox'  name='voortrekker_betrokkenheid[]' value='"+result.voortrekker_betrokkenheid[i].id+"'/><label> &nbsp"+result.voortrekker_betrokkenheid[i].betrokkenheid+"</label></div>");
                            }


                    }
                    console.log(result.rol_betrokkenhede);
                    $('#hernoem_rol').modal('show');
                  });
            }

            function openVervulRol(id)
            {

                $.ajax({
                  method: "POST",
                  url: "{{URL('ajax/groep/rolle')}}",
                  data: { rol_id: id,
                          '_token': '{!! csrf_token() !!}'}
                })
                  .done(function( result ) {
                    $('#vervul_rol_header').html("{{$groep->groep_naam}}: " + result.rol_naam);

                    $(function() {
                        $('#example1').DataTable( {
                        "sAjaxSource": "{{{URL('ajax/groepe/persone')}}}",
                        "sServerMethod": "GET",
                        retrieve: true
                        });

                    });
                    $('#vervul_rol').modal('show');
                  });

            }

            function destroyDataTable()
            {
                var table = $('#myTable').DataTable();

                table.destroy();

                $('#vervul_rol').modal('hide');
            }

            function destroySkrapRol()
            {
                $('#skrap_rol').modal('hide');
            }

            function skrapGroep()
            {
                $('#skrap_groep').modal('show');
            }

            function nuweRol()
            {
                $('#nuwe_rol').modal('show');
            }

            function hernoemGroep()
            {
                $('#hernoem_groep').modal('show');
            }


        </script>
@stop

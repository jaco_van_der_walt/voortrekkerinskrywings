<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBetrokkenheidVolwJeug extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    //REMEMBER: The existing DB entries must be updated with correct settings!
    public function up()
    {
        Schema::table('voortrekker_betrokkenheid', function ($table) {
            $table->boolean('volwassene')->default(false);
            $table->boolean('jeuglid')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('voortrekker_betrokkenheid', function ($table) {
            $table->dropColumn('volwassene');
            $table->dropColumn('jeuglid');
        });
    }
}

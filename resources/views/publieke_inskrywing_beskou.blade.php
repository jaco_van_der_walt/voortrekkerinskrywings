@extends('layouts.master') @section('head') @parent
<title>Persone</title>@stop @section('content')
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
         <h1>
            <div class="row">
                <div class="col-md-6">
                    <a href="{{ URL::previous() }}"><button type="submit" class="btn btn-primary "><i class="fa fa-arrow-circle-left"></i></button></a> {{$inskrywing->noemnaam}} {{$inskrywing->van}} <small class="pull-right">{{$inskrywing->registrasie_nommer}}</small>
                </div>
                <div class="col-md-6">
                    <div class="btn-group pull-right" style="margin-right: 15px; margin-top: 5px;">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            Opsies&nbsp;&nbsp;<span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                          @if($inskrywing->admin_verwerk)
                            <li><a href="#" onclick="herverwerk()"><i class="fa fa-refresh"></i>Herverwerk</a></li>
                          @endif
                            <li><a href="#" onclick="skrapInskrywing()"><i class="fa fa-trash"></i>Skrap Inskrywing</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </h1>


    </section>
    <!-- Main content -->
    <section class="content">@if($errors->has()) @foreach ($errors->all() as $error)
        <div class="alert alert-danger alert-dismissable"> <i class="fa fa-ban"></i>

            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <b>Waarskuwing!</b> {{$error}}</div>@endforeach @endif

        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header">
                         <h3 class="box-title"><b>Inskrywing Inligting</b></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Kamp -->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Kamp</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$kamp->kamp_naam}}</label>
                            </div>
                        </div>

                        <!-- Inskrywing Soort -->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Inskrywing Soort</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{ucwords($inskrywing->inskrywing_soort)}}</label>
                            </div>
                        </div>

                        <!-- Betrokkenheid-->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Betrokkenheid</label>
                            </div>
                            <div class="col-xs-7">
                                    <label>{{$betrokkenheid}}</label>
                            </div>
                        </div>

                        @if($inskrywing->inskrywing_soort == "jeuglid")
                        <!-- Inskrywing Soort -->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Skoolgraad</label>
                            </div>
                            <div class="col-xs-7">
                                <label>Graad: {{$inskrywing->skoolgraad}}</label>
                            </div>
                        </div>
                        @endif

                        <!-- Groep -->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Groep</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{ucwords($groep)}}</label>
                            </div>
                        </div>

                        <!-- Ekstras -->
                        @foreach($inskrywing->opsies as $o)
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>{{$o->ekstra->naam}}</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$o->naam}}</label>
                            </div>
                        </div>
                       @endforeach

                        <!-- Vrywaring geteken deur -->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Vrywaring geteken deur</label>
                            </div>
                            <div class="col-xs-7">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>{{$inskrywing->vrywaring_geteken_deur}}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>({{$inskrywing->vrywaring_geteken_deur_id}})</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label><i>{{$inskrywing->vrywaring_geteken_deur_epos}}</i></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>{{$inskrywing->created_at}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                         <h3 class="box-title"><b>Persoonlike Inligting</b></h3>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Registrasie Nommer -->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Registrasie Nommer</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$inskrywing->voortrekker_registrasie_nommer}}</label>
                            </div>
                        </div>
                        <!-- Van -->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Van
                                </label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$inskrywing->van}}</label>
                            </div>
                        </div>
                        <!-- Voorname -->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Voorname</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$inskrywing->voorname}}</label>
                            </div>
                        </div>
                        <!-- Noemnaam -->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Noemnaam
                                </label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$inskrywing->noemnaam}}</label>
                            </div>
                        </div>
                        <!-- Date dd/mm/yyyy -->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Geboortedatum
                                </label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$inskrywing->geboorte_datum}}</label>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                        <!-- Geslag -->
                        <!-- radio -->
                        <div class=" row form-group">
                            <div class="col-xs-5">
                                <label>Geslag
                                </label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$inskrywing->geslag}}</label>
                            </div>
                        </div>
                        <!-- ID Nommer -->
                        @if($inskrywing->id_nommer)
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>ID Nommer</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$inskrywing->id_nommer}}</label>
                            </div>
                        </div>
                        @endif

                        @if($inskrywing->kontak_nommer_1)
                        <!-- Kontak Nommer -->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Kontak Nommer</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$inskrywing->kontak_nommer_1}}</label>
                            </div>
                        </div>
                        @endif

                        @if($inskrywing->kontak_nommer_2)
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Alternatiewe Kontak Nommer</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$inskrywing->kontak_nommer_2}}</label>
                            </div>
                        </div>
                        @endif

                        @if($inskrywing->epos_adres)
                        <!-- Epos Adres -->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Epos Adres</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$inskrywing->epos_adres}}</label>
                            </div>
                        </div>
                        @endif

                        @if($inskrywing->woonadres)
                        <!-- Adres -->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Woonadres</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$inskrywing->woonadres}}</label>
                            </div>
                        </div>
                        @endif

                        @if($inskrywing->posadres)
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Posadres</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$inskrywing->posadres}}</label>
                            </div>
                        </div>
                        @endif

                        <!-- Voertuig Registrasie Nommer -->
                         @if($inskrywing->voertuig_registrasie_nommer)
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Voertuig Registrasie Nommer</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$inskrywing->voertuig_registrasie_nommer}}</label>
                            </div>
                        </div>
                        @endif

                    </div>
                </div>
                <!-- /.box -->
                <!-- Die Voortrekkers -->
                <div class="box box-primary">
                    <div class="box-header">
                         <h3 class="box-title"><b>Die Voortrekkers</b></h3>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Voortrekker Registrasie Nommer -->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Voortrekker Registrasie Nommer</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$inskrywing->voortrekker_registrasie_nommer}}</label>
                            </div>
                        </div>

                        @if($inskrywing->voortrekker_rang)
                        <!-- Rang -->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Voortrekker Rang</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$inskrywing->voortrekker_rang}}</label>
                            </div>
                        </div>
                        @endif

                        <!-- Kommando-->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Kommando</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$kommando}}</label>
                            </div>
                        </div>
                    </div>
                </div>

                @if($inskrywing->inskrywing_soort == "jeuglid")
                <!-- Ouers/Voog -->
                <div class="box box-primary">
                    <div class="box-header">
                         <h3 class="box-title"><b>Ouers/Voog</b></h3>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Ouer/Voog #1 -->
                         <h4><b>Ouer/Voog #1</b></h4>

                        @if($inskrywing->ouer_voog_naam_van_1)
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Ouer/Voog Naam & Van</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$inskrywing->ouer_voog_naam_van_1}}</label>
                            </div>
                        </div>
                        @endif

                        @if($inskrywing->ouer_voog_id_nommer_1)
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Ouer/Voog ID Nommer</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$inskrywing->ouer_voog_id_nommer_1}}</label>
                            </div>
                        </div>
                        @endif

                        @if($inskrywing->ouer_voog_kontak_nommer_1)
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Ouer/Voog Kontak Nommer</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$inskrywing->ouer_voog_kontak_nommer_1}}</label>
                            </div>
                        </div>
                        @endif

                        @if($inskrywing->ouer_voog_epos_adres_1)
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Ouer/Voog Epos Adres</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$inskrywing->ouer_voog_epos_adres_1}}</label>
                            </div>
                        </div>
                        @endif

                         <h4><b>Ouer/Voog #2</b></h4>

                         @if($inskrywing->ouer_voog_naam_van_2)
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Ouer/Voog Naam & Van</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$inskrywing->ouer_voog_naam_van_2}}</label>
                            </div>
                        </div>
                        @endif

                        @if($inskrywing->ouer_voog_id_nommer_2)
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Ouer/Voog ID Nommer</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$inskrywing->ouer_voog_id_nommer_2}}</label>
                            </div>
                        </div>
                        @endif

                        @if($inskrywing->ouer_voog_kontak_nommer_2)
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Ouer/Voog Kontak Nommer</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$inskrywing->ouer_voog_kontak_nommer_2}}</label>
                            </div>
                        </div>
                        @endif

                        @if($inskrywing->ouer_voog_epos_adres_2)
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Ouer/Voog Epos Adres</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$inskrywing->ouer_voog_epos_adres_2}}</label>
                            </div>
                        </div>
                        @endif

                    </div>
                </div>
            @endif
            <!-- Mediese Inligting -->
            <div class="box box-primary">
                <div class="box-header">
                     <h3 class="box-title"><b>Mediese Inligting</b></h3>

                </div>
                <!-- /.box-header -->
                <div class="box-body">


                    <!-- Mediese Fonds Naam -->
                    @if($inskrywing->mediese_fonds_naam)
                    <div class="row form-group">
                        <div class="col-xs-5">
                            <label>Mediese Fonds Naam</label>
                        </div>
                        <div class="col-xs-7">
                            <label>{{$inskrywing->mediese_fonds_naam}}</label>
                        </div>
                    </div>
                    @endif

                    <!-- Mediese Fonds Lidmaat Nommer -->
                    @if($inskrywing->mediese_fonds_lid_nommer)
                    <div class="row form-group">
                        <div class="col-xs-5">
                            <label>Mediese Fonds Lidmaat Nommer</label>
                        </div>
                        <div class="col-xs-7">
                            <label>{{$inskrywing->mediese_fonds_lid_nommer}}</label>
                        </div>
                    </div>
                    @endif

                    <!-- Huisdokter -->
                    <h4><b>Huisdokter</b></h4>

                    @if($inskrywing->huis_dokter_naam_van)
                    <div class="row form-group">
                        <div class="col-xs-5">
                            <label>Huisdokter Naam & Van</label>
                        </div>
                        <div class="col-xs-7">
                            <label>{{$inskrywing->huis_dokter_naam_van}}</label>
                        </div>
                    </div>
                    @endif

                    @if($inskrywing->huis_dokter_kontak_nommer_1)
                    <div class="row form-group">
                        <div class="col-xs-5">
                            <label>Huisdokter Kontak Nommer</label>
                        </div>
                        <div class="col-xs-7">
                            <label>{{$inskrywing->huis_dokter_kontak_nommer_1}}</label>
                        </div>
                    </div>
                    @endif

                    @if($inskrywing->huis_dokter_kontak_nommer_2)
                    <div class="row form-group">
                        <div class="col-xs-5">
                            <label>Huisdokter Alternatiewe Kontak Nommer</label>
                        </div>
                        <div class="col-xs-7">
                            <label>{{$inskrywing->huis_dokter_kontak_nommer_2}}</label>
                        </div>
                    </div>
                    @endif

                    <!-- Mediese Toestand & Allergie -->
                    <div class="row form-group">
                        <div class="col-xs-5">
                            <label>Siektetoestande of Allergieë</label>
                        </div>
                        <div class="col-xs-7">
                            <label>{{$inskrywing->siekte_allergie}}</label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="box-footer col-md-12">

        </div>
        </div>
        <!-- /.row (main row) -->
    </section>
    <!-- /.content -->

    <!-- Skrap Inskrywing Modal -->
    <div class="modal fade" id="skrap_inskrywing" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" id="skrap_groep_header">Skrap Inskrywing: {{$inskrywing->noemnaam}} {{$inskrywing->van}}</h4>
            </div>
            <div class="modal-body">
                    <div class="box-body table-responsive">
                        <p>Is jy seker jy wil die inskrywing vir {{$inskrywing->noemnaam}} {{$inskrywing->van}} skrap?</p>
                        <p>Om hierdie inskrywing te skrap verwyder slegs die inligting soos ontvang deur die aanlyn inskrywingsproses. Dit verwyder nie die persoon of kampinskrywing wat 'n Administrateur verwerk het nie.</p>
                    </div><!-- /.box-body -->
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal"> Kanseleer</button>
              <a href="{{URL('publiek/inskrywing/admin/skrap', $inskrywing->id)}}" class="btn btn-danger pull-left"><i class="fa  fa-trash-o"></i>  Skrap</a>
            </div>
          </div>
        </div>
    </div>
    <!-- End Skrap Groep Modal -->

    <!-- Herverwerk Modal -->
    <div class="modal fade" id="herverwerk" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" id="skrap_groep_header">Herverwerk Inskrywing: {{$inskrywing->noemnaam}} {{$inskrywing->van}}</h4>
            </div>
            <div class="modal-body">
                    <div class="box-body table-responsive">
                        <p>Is jy seker jy wil die inskrywing vir {{$inskrywing->noemnaam}} {{$inskrywing->van}} weer oopmaak vir verwerking?</p>
                        <p>Om hierdie inskrywing oop te maak vir Herverwerking sal jou toelaat om dit weer te verwerk. Maak seker dat jy die bestaande aanvaarde inskrywing eers skrap voordat jy hierdie inskrywing herverwerk.</p>
                    </div><!-- /.box-body -->
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal"> Kanseleer</button>
              <a href="{{URL('publiek/inskrywing/admin/herverwerk', $inskrywing->id)}}" class="btn btn-warning pull-left"><i class="fa  fa-refresh"></i>  Herverwerk</a>
            </div>
          </div>
        </div>
    </div>
    <!-- Herverwerk Modal -->



</aside>
<!-- /.right-side -->@stop @section('plugins') @parent
<!-- AdminLTE App -->
<script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>
<!-- InputMask -->
<script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.date.extensions.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.extensions.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    //Datemask
    $("#datemask").inputmask("yyyy/mm/dd", {
        "placeholder": "yyyy/mm/dd"
    });


    function skrapInskrywing()
    {
      $('#skrap_inskrywing').modal('show');
    };

    function herverwerk()
    {
      $('#herverwerk').modal('show');
    }


</script>@stop

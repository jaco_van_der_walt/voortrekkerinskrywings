@extends('layouts.master')

@section('head')
@parent
<title>Persone</title>

<script src="{{URL('//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css')}}" type="text/css"></script>
@stop


@section('content')
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1><a href={{URL('inskrywing/kanseleer')}}><button type="submit" class="btn btn-primary "><i class="fa fa-arrow-circle-left"></i></button></a> Nuwe Inskrywing: {{$kamp->kamp_naam}}</h1>
                </section>

                <!-- Main content -->
                <section class="content">

                @if($errors->has())
                   @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Waarskuwing!</b> {{$error}}
                        </div>
                  @endforeach
                @endif

                @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissable">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Sukses!</b> {{Session::get('success')}}
                        </div>
                @endif


                        <div class="row">
                            <div class="col-md-12">
                                <div class="callout callout-info">
                                    <h4>Stap 1: Kies Persoon</h4>
                                    <p>Kies assbelief die Persoon wat ingeskryf moet word vir {{$kamp->kamp_naam}}</p>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-green" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%">
                                    <span class="sr-only">10% Voltooi</span>
                                    </div>
                                </div>
                            </div>  
                        </div>
                        <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Persone</h3> 
                                        <a href="{{URL('persone/voegby')}}"><button class="btn btn-success pull-right" style="margin-right: 5px; margin-top: 5px;"><i class="fa fa-plus"></i>&nbsp Nuwe Persoon</button></a> 

                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="persone_tabel" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Kamp ID</th>
                                                <th>Voortrekker Reg Nr.</th>
                                                <th>Naam</th>
                                                <th>Van</th>
                                                <th>Geboortedatum</th>
                                                <th>Kommando</th>
                                                <th>Diensjare</th>
                                                <th>Aksies</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Kamp ID</th>
                                                <th>Voortrekker Reg Nr.</th>
                                                <th>Naam</th>
                                                <th>Van</th>
                                                <th>Geboortedatum</th>
                                                <th>Kommando</th>
                                                <th>Diensjare</th>
                                                <th>Aksies</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>

        <!-- DATA TABES SCRIPT -->
        <script src="{{URL('//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('assets/js/plugins/datatables/dataTables.bootstrap.js')}}" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>

        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $('#persone_tabel').DataTable( {
                    "sAjaxSource": "{{{URL('ajax/persone/inskrywing')}}}",
                    "sServerMethod": "GET"
                } );
                
            });
        </script>


@stop
<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Persoon extends Model {

	use SoftDeletes;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'persone';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

    protected $guarded = ['id'];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = ['deleted_at'];

    public function kommando(){
        return $this->belongsTo('App\Kommando');
    }

    public function inskrywings(){
        return $this->hasMany('App\Inskrywing', 'persoon_id', 'id');
    }

    public function rolle()
    {
    	return $this->belongsToMany('App\Rol', 'persone_rolle', 'persoon_id', 'rol_id');
    }
}

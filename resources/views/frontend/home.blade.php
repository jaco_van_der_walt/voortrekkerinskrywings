<!DOCTYPE html>
<html>
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-113663245-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-113663245-1');
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <meta name="theme-color" content="#2196F3">
    <title>Inskrywings - Die Voortrekkers</title>

    <!-- CSS  -->
    <link href="{{asset('frontend/min/plugin-min.css')}}" type="text/css" rel="stylesheet">
    <link href="{{asset('frontend/min/custom-min.css')}}" type="text/css" rel="stylesheet" >

    <link rel="shortcut icon" type="image/png" href="{{asset('favicon.png')}}"/>
</head>
<body id="top" class="scrollspy">

<!-- Pre Loader -->
<div id="loader-wrapper">
    <div id="loader"></div>

    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>

</div>

<!--Navigation-->
 <div class="navbar-fixed">
    <nav id="nav_f" class="default_color" role="navigation">
        <div class="container">
            <div class="nav-wrapper">
            <a href="#" id="logo-container" class="brand-logo"><img src="{{asset('frontend/img/logo2.svg')}}" height="60px"></a>
                <ul class="right hide-on-med-and-down">
                </ul>
                <ul id="nav-mobile" class="side-nav">
                </ul>
            <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
            </div>
        </div>
    </nav>
</div>

<!--Parallax-->
<div class="parallax-container" style="height:150px;">
    <div class="parallax"><img src="{{asset('frontend/img/parallax_sky.svg')}}"></div>
</div>

<!--Intro and service-->
<div id="intro" class="section scrollspy">
    <div class="container">
        <div class="row">
            <div  class="col s12">
                <h4 class="center header text_h4"> Welkom by die aanlyn inskrywingsportaal van Die Voortrekkers.</h4>
                <h5 class="center header text_h5"> Die volgende kampe aanvaar inskrywings</h5>
            </div>
        </div>
    </div>
</div>

<!--Work-->
<div class="section scrollspy" id="work">
    <div class="container">
        <h3 class="header text_b">Kampe </h3>
        <div class="row">


            @foreach($kampe_inskrywings as $kamp)
            <div class="col s12 m4 l4">
                <div class="card">
                    <div class="card-image waves-effect waves-block waves-light">
                        <a href={{URL('publiek/inskrywing/kamp/'.$kamp->id)}}><img class="activator" src="{{asset($kamp->organisasies[0]->logo_500_500)}}"></a>
                    </div>
                    <div class="card-content">
                        <span class="card-title activator grey-text text-darken-4">{{$kamp->kamp_naam}}</span>
                        <p>{{$kamp->begin}} tot {{$kamp->eindig}}</p>
                        <p>@if($kamp->meer_inligting_URL)<a href="{{$kamp->meer_inligting_URL}}">Meer Inligting</a>@else &nbsp @endif<a class="pull-right" href={{URL('publiek/inskrywing/kamp/'.$kamp->id)}}>Skryf In</a></p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

<!--Parallax-->
<div class="parallax-container">
    <div class="parallax"><img src="{{asset('frontend/img/parallax2.svg')}}"></div>
</div>

<!--Footer-->
<footer id="contact" class="page-footer default_color scrollspy">
    <div class="footer-copyright default_color">
        <div class="container">
            Ontwikkel vir Die Voortrekkers deur Kamp Nakuphi
        </div>
    </div>
</footer>


    <!--  Scripts-->
    <script src="{{asset('frontend/min/plugin-min.js')}}"></script>
    <script src="{{asset('frontend/min/custom-min.js')}}"></script>

    </body>
</html>

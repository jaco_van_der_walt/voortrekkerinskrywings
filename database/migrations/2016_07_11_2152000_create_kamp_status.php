<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKampStatus extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::create('kamp_status', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('status');
			$table->timestamps();
		});

		Artisan::call('db:seed', ['--class' => 'KampStatusSeeder', '--force'=>true]); //Must seed else next migrations will fail because all new foreign keys will be null! 

		Schema::table('kampe', function ($table) {
		    $table->integer('status')->unsigned()->default(1);
		    $table->foreign('status')->references('id')->on('kamp_status');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('kampe', function ($table) {
			$table->dropForeign('kampe_status_foreign');
	    	$table->dropColumn('status');
		});

		Schema::drop('kamp_status');
	}

}
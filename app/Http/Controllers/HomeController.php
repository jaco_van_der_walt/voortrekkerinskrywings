<?php namespace App\Http\Controllers;

use App\Persoon;
use App\Kamp;
use App\Inskrywing;
use Session;


class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	/*
	public function __construct()
	{
		$this->middleware('auth');
	}
	*/

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$persoon_count = Persoon::all()->count();
		$oop_kampe_count = Kamp::where('status_id', '2')->count();
		$gesluit_kampe_count = Kamp::where('status_id', '3')->count();
		$inskrywings_count = Inskrywing::all()->count();

		$oop_kampe = Kamp::where('status_id', '2')->get();

		$inskrywings_aanvaar_kampe = Kamp::where('status_id', '4')->get();

		return view('dashboard')->with(['persoon_count' => $persoon_count, 'oop_kampe_count' => $oop_kampe_count, 'gesluit_kampe_count'=> $gesluit_kampe_count, 'inskrywings_count'=> $inskrywings_count, 'oop_kampe'=>$oop_kampe, 'inskrywings_aanvaar_kampe'=>$inskrywings_aanvaar_kampe]);
	}

	public function getInskrywing()
	{
		return "Skryf in!";
	}

	public function getTuis()
	{
		Session::flush();
		return view('voorblad.tuis');
	}

	public function getKursusse()
	{
		return view('voorblad.kursusse');
	}

	public function getKampdatums()
	{
		return view('voorblad.kampdatums');
	}

	public function getKampbriewe()
	{
		return view('voorblad.kampbriewe');
	}

	public function getTerrein()
	{
		return view('voorblad.terrein');
	}

	public function getKontak()
	{
		return view('voorblad.kontak');
	}

	public function getGallery()
	{
		return view('voorblad.gallery');
	}

	//*****************************//
	//       FOTO FUNCTIONS 	   //		
	//*****************************//
	
	public function getAlgemeen2014()
	{
		return view('voorblad.algemeen_2014');
	}

	public function getAlgemeen2013()
	{
		return view('voorblad.algemeen_2013');
	}

	public function getVarswaterkunde2014()
	{
		return view('voorblad.varswaterkunde_2014');
	}

	public function getVarswaterkunde2013()
	{
		return view('voorblad.varswaterkunde_2013');
	}

	public function getRoeikunde2014()
	{
		return view('voorblad.roeikunde_2014');
	}

	public function getRoeikunde2013()
	{
		return view('voorblad.roeikunde_2013');
	}

	public function getSeilkuns2014()
	{
		return view('voorblad.seilkuns_2014');
	}
	
	public function getSeilkuns2013()
	{
		return view('voorblad.seilkuns_2013');
	}

	public function getSnorkelduik2014()
	{
		return view('voorblad.snorkelduik_2014');
	}

	public function getSnorkelduik2013()
	{
		return view('voorblad.snorkelduik_2013');
	}

	public function getSelfbehoud2013()
	{
		return view('voorblad.selfbehoud_2013');
	}
}

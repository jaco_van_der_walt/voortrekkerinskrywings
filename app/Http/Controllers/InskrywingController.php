<?php namespace App\Http\Controllers;

use Input;
use Form;
use Validator;
use Redirect;
use Session;
use Response;
use Request;
use Log;
use Auth;
use View;
use App\Groep;
use App\Kamp;
use App\Rol;
use App\Persoon;
use App\Vorm;
use App\Inskrywing;
use App\Kommando;
use App\VoortrekkerBetrokkenheid;
use DateTime;

class InskrywingController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}



	public function view_inskrywing($id)
    {

        $inskrywing = Inskrywing::find($id);

        if($inskrywing===null)
        {
            Log::error("2700 || Kon nie die inskrywing kry nie! |", ['veroorsaak_deur' => Auth::user()->toArray(), "opsoek_na_inskrywing" => $id]);
            return Redirect::back()->withErrors("Kon nie die inskrywing vind nie");
        }

        $persoon = Persoon::find($inskrywing->persoon_id);
        $kamp = Kamp::find($inskrywing->kamp_id);

        $rolle = $inskrywing->persoon->rolle;
        $groepe_string = "";
        $berekende_bedrag = 0;
        foreach($rolle as $rol)
        {
            //Wys net die rolle wat aan die kamp gebind is
            if($rol->groep->kamp_id == $kamp->id)
            {
                $groepe_string .= $rol->groep->groep_naam.": ";
                $groepe_string .= $rol->rol_naam." (R".$rol->inskrywing_koste."-00) <br>";
                $berekende_bedrag = $berekende_bedrag + $rol->inskrywing_koste;
            }
        }

				foreach($inskrywing->opsies as $o)
				{
					$berekende_bedrag = $berekende_bedrag + $o->prys;
				}

        if($persoon===null)
        {
            Log::error("2701 || Kon nie die persoon kry nie! |", ['veroorsaak_deur' => Auth::user()->toArray(), "opsoek_na_persoon_id" => $inskrywing->persoon_id]);
            return Redirect::back()->withErrors("Kon nie die inskrywing vind nie");
        }
        Session::set("inskrywing_id",$inskrywing->id);

        $vorms = Vorm::where('inskrywing_id', $inskrywing->id)->get();

        return View::make('inskrywing')->with(['inskrywing' => $inskrywing, 'persoon' => $persoon, 'vorms'=>$vorms, 'kamp'=>$kamp, 'groepe'=>$groepe_string, 'berekende_bedrag'=>$berekende_bedrag]);
    }



    public function opdateer_inskrywing()
    {
        $rules = array(
                        'betaal_bedrag' => 'numeric',
                        'betaal' => 'Required|in:j,n'
                );
         //Check if Input passes the rules
        $v = Validator::make(Input::all(), $rules);
        if( $v->passes() ) {

            $inskrywing = Inskrywing::find(Session::get('inskrywing_id'));
            if($inskrywing===null)
            {
                Log::error("2705 || Kon nie die inskrywing kry nie! |", ['veroorsaak_deur' => Auth::user()->toArray(), 'opsoek_na_inskrywing'=>Session::get('inskrywing_id')]);
                return Redirect::back()->withErrors("Kon nie die inskrywing vind nie");
            }

            $inskrywing_backup = $inskrywing;
            if(Input::get('betaal') === 'j')
            {
                $inskrywing->betaal= true;
            }
            else
            {
                $inskrywing->betaal = false;
            }

            $inskrywing->betaal_bedrag = Input::get('betaal_bedrag');
            $inskrywing->nota = Input::get('nota');
            $inskrywing->save();

            if(Input::file("files")[0] !== null)
            {
                $files = Input::file('files');
                $file_count = count($files);
                $uploadcount=0;
                $kamp = Kamp::find(Session::get('paneel_kamp'));

                    foreach($files as $file)
                        {
                            $destinationPath = 'uploads/'.$kamp->kamp_naam;
                            $dt = new DateTime();
                            $filename = $inskrywing->id."_".$dt->getTimestamp()."_".$file->getClientOriginalName();
                            $upload_success = $file->move($destinationPath, $filename);
                            $uploadcount ++;

                            $vorm = new Vorm();
                            $vorm->inskrywing_id = $inskrywing->id;
                            $vorm->file_name = $file->getClientOriginalName();
                            $vorm->file_path = $destinationPath."/".$filename;
                            $vorm->save();
                        }
            }

            Log::notice('2750 || Inskrywing opgedateer! |', ['veroorsaak_deur' => Auth::user()->toArray(), 'nuwe_inskrywing_rekord' => $inskrywing->toArray(), 'ou_inskrywing_rekord' => $inskrywing_backup->toArray()]);
                     return Redirect::back()->with('success', "Inskrywing is opgedateer");

        } else {
             //Invalid input! Redirecting back...
            return redirect()->back()->withInput()->withErrors($v);
        }

    }

        public function getDelete()
        {
            $inskrywing = Inskrywing::find(Session::get('inskrywing_id'));
            if($inskrywing===null)
            {
                Log::error("2725 || Kon nie die inskrywing kry nie! |", ['veroorsaak_deur' => Auth::user()->toArray(), 'opsoek_na_inskrywing'=>Session::get('inskrywing_id')]);
                return Redirect::back()->withErrors("Kon nie die inskrywing vind nie");
            }

            $persoon = Persoon::find($inskrywing->persoon_id);

            if($persoon===null)
            {
                Log::error("2726 || Kon nie die persoon kry nie! |", ['veroorsaak_deur' => Auth::user()->toArray(), 'opsoek_na_persoon'=>$inskrywing->persoon_id]);
                return Redirect::back()->withErrors("Kon nie die inskrywing vind nie");
            }

            //Remove all Rolle attached to this camp
            $rolle = $persoon->rolle;
            $kamp_id = Session::get('paneel_kamp');
            foreach($rolle as $rol)
            {
                if($rol->groep->kamp_id == $kamp_id)
                {
                    $persoon->rolle()->detach($rol->id);
                }
            }

						//Remove all ekstras
						$opsies = $inskrywing->opsies;
						foreach($opsies as $o)
						{
							$inskrywing->opsies()->detach($o);
						}

            //Now remove the files
            $vorms = Vorm::where('inskrywing_id', $inskrywing->id)->get();
            foreach($vorms as $vorm)
            {
                $vorm->delete();
            }

            //Now delete the inskrywing...
            $inskrywing->delete();
            $persoon->save();

            Log::notice('2728 || Inskrywing geskrap! |', ['veroorsaak_deur' => Auth::user()->toArray(), 'inskrywing' => $inskrywing->toArray()]);

            return Redirect::to('kamp/paneel/'.$kamp_id)->with('success', $persoon->noemnaam." ".$persoon->van." se inskrywing is geskrap");

        }

        public function nuwe_inskrywing()
        {
            $kamp = Kamp::find(Session::get('paneel_kamp'));
            if($kamp===null)
            {
                Log::error("2725 || Kon nie die kamp kry nie! |", ['veroorsaak_deur' => Auth::user()->toArray(), 'Nuwe inskrywing nie vanuit Kamp Paneel begin nie']);
                return Redirect::back()->withErrors("Kon nie die kamp vind nie");
            }
            else
            {
               Session::set('inskrywing_kamp', $kamp->id);
               if(Session::has('inskrywing_persoon'))
               {
                 $persoon = Persoon::where('id', Session::get('inskrywing_persoon'))->with('kommando')->get();
                 if(Session::has('inskrywing_persoon_opgedateer'))
                 {
                    if(Session::has('inskrywing_betrokkenheid'))
                    {
                        if(Session::has('inskrywing_rol'))
                        {
                            $betrokkenheid = Session::get('inskrywing_betrokkenheid');
                            $rol = Session::get('inskrywing_rol');
                            $groep = Groep::find($rol->groep_id);
                            return View::make('nuwe_inskrywing_bevestig')->with(['persoon' => $persoon, 'kamp'=> $kamp, 'betrokkenheid' => $betrokkenheid, 'groep' => $groep, 'rol' => $rol]);
                        }

                        else
                        {
                            //We have Kamp, Persoon, Persoon edit and Betrokkenheid. Now we need to set the Groep
                            $session_betrokkenheid = Session::get('inskrywing_betrokkenheid');

                            //Find all Groepe that has a Rol where the Persoon kan join
                            $rolle = Kamp::find(Session::get('inskrywing_kamp'))->rolle()->get();
                            $groepe = '';
                            foreach($rolle as $rol)
                            {
                                $betrokkenheid_beperkings = $rol->voortrekker_betrokkenhede()->get();
                                foreach($betrokkenheid_beperkings as $beperking)
                                {
                                    $beperking_id = $beperking->id;
                                    if($beperking_id === $session_betrokkenheid->id)
                                    {
                                       $groep = $rol->groep()->get();
                                       $groepe[$groep[0]->id]['groep_id'] =$groep[0]->id;
                                       $groepe[$groep[0]->id]['groep_naam'] =$groep[0]->groep_naam;
                                       $groepe[$groep[0]->id]['rolle'][$rol->id]['rol_id'] = $rol->id;
                                       $groepe[$groep[0]->id]['rolle'][$rol->id]['rol_naam'] = $rol->rol_naam;
                                    }
                                }
                            }

                            if ($groepe === '')
                            {
                                return "Geen groepe beskikbaar vir die Inskrywing nie!"; //TODO
                            }

                            return View::make('nuwe_inskrywing_rolle')->with(['persoon' => $persoon, 'kamp'=> $kamp, 'groepe' => $groepe]);
                        }

                    }
                    else
                    {
                    //We have the Persoon and Persoon has been updated. Now we can Select Rol

                    $betrokkenhede = VoortrekkerBetrokkenheid::all();
                    return View::make('nuwe_inskrywing_betrokkenheid')->with(['persoon' => $persoon, 'kamp'=> $kamp, 'betrokkenhede' => $betrokkenhede]);
                    }

                 }
                 else
                 {
                    //We have in Session Kamp and Persoon. Now we need to find edit the Persoon!
                    $persoon = Persoon::where('id', Session::get('inskrywing_persoon'))->with('kommando')->get();
                    return View::make('nuwe_inskrywing_edit_persoon')->with(['persoon' => $persoon, 'kommando' => $kommando = Kommando::orderBy('kommando_naam')->get(), 'kamp' =>$kamp]);
                 }

               }
               else
               {
                 //Don't have the Persoon to add yet - but we have Kamp!
                 return View::make('nuwe_inskrywing_begin')->with(['kamp' => $kamp]);
               }
            }

        }

        public function ajax_persone_inskrywing()
        {

        $return['recordsTotal'] = Persoon::count();

        $flashdata = [];
        $return['data'] = [];
        $i = 0;
        foreach (Persoon::orderBy('van')->with('kommando')->get() as $persoon) {
            $return['data'][$i] = [$persoon->registrasie_nommer];
            $return['data'][$i][] = [$persoon->voortrekker_registrasie_nommer];
            $return['data'][$i][] = [$persoon->noemnaam];
            $return['data'][$i][] = [$persoon->van];
            $return['data'][$i][] = [$persoon->geboorte_datum];
            if($persoon->kommando)
            {
               $return['data'][$i][] = [$persoon->kommando->kommando_naam];
            }
            else
            {
                $return['data'][$i][] ="";
            }
            $return['data'][$i][] = Inskrywing::where('persoon_id', $persoon->id)->count();

            $reeds_ingeskryf = Inskrywing::where('persoon_id', $persoon->id)->where('kamp_id', Session::get('inskrywing_kamp'))->count();
            if( $reeds_ingeskryf > 0)
            {
              $return['data'][$i][] =  'Reeds Ingeskryf!';
            }
            else
            {
                $return['data'][$i][] =  '<a href="'.url("inskrywing/nuut/$i").'" class="btn btn-xs bg-green"><i class="fa fa-user-plus"> Skryf In</i></a>';
            }

            $flashdata[$i] = $persoon->id;
            $i++;
        }

        Session::set('inskrywing_persone', $flashdata);

        return response()->json($return);
        }

        public function kanseleer_inskrywing()
        {
            //Forget all Session Variables
            Session::forget('inskrywing_persoon');
            Session::forget('inskrywing_persoon_opgedateer');
            Session::forget('inskrywing_betrokkenheid');
            Session::forget('inskrywing_rol');
            Session::forget('inskrywing_kamp');
            Session::forget('merge_persoon_id');
            Session::forget('verwerk_inskrywing');
            Session::forget('publieke_inskrywing_betrokkenheid');
            Session::forget('publieke_inskrywing_rol');
            return Redirect::to('kamp/paneel/'.Session::get('paneel_kamp'))->withErrors("Inskrywing Gekanseleer");
        }

        public function nuwe_inskrywing_persoon($id)
        {
            $persoon = Persoon::find(Session::get("inskrywing_persone.$id", false));
            if($persoon===null)
            {
                Log::error("2725 || Kon nie die persoon kry nie! |", ['veroorsaak_deur' => Auth::user()->toArray(), 'Nuwe inskrywing kon nie die persoon in die flashdata kry nie!']);
                return Redirect::back()->withErrors("Kon nie die persoon vind nie!");
            }
            else
            {
                $reeds_ingeskryf = Inskrywing::where('persoon_id', $persoon->id)->where('kamp_id', Session::get('inskrywing_kamp'))->count();
                if( $reeds_ingeskryf > 0)
                {
                    Log::error("2725 || Die persoon is reeds ingeskryf! |", ['veroorsaak_deur' => Auth::user()->toArray(), 'Die Persoon is reeds ingeskryf']);
                    return Redirect::back()->withErrors("Kon nie die persoon vind nie!");
                }
                else
                {
                    Session::set('inskrywing_persoon',$persoon->id);
                    return Redirect::to('inskrywing/nuut');
                }

            }
        }

        public function opdateer_persoon()
        {
            if (Session::has('inskrywing_persoon'))
                        {
                            //Validate the new input
                            $rules = array(
                                    'van' => 'Required',
                                    'noemnaam' => 'Required',
                                    'geboorte_datum' => 'Required|date',
                                    'geslag' => 'Required|in:m,f',
                                    'id_nommer' => 'digits:13',
                                    'kontak_nommer_1' => 'numeric',
                                    'kontak_nommer_2' => 'numeric',
                                    'epos_adres' => 'email',
                                    'ouer_voog_id_nommer_1' => 'digits:13',
                                    'ouer_voog_id_nommer_2' => 'digits:13',
                                    'ouer_voog_kontak_nommer_1' => 'numeric',
                                    'ouer_voog_kontak_nommer_2' => 'numeric',
                                    'ouer_voog_epos_adres_1' => 'email',
                                    'ouer_voog_epos_adres_2' => 'email',
                                    'huisdokter_kontak_nommer_1' => 'numeric',
                                    'huisdokter_kontak_nommer_2' => 'numeric'

                            );
                            //Check if Input passes the rules
                            $v = Validator::make(Input::all(), $rules);
                            if( $v->passes() ) {
                                //All Input Valid. Let's update the record!

                                $id = Session::get('inskrywing_persoon');
                                $persoon = Persoon::find($id);
                                $persoon_backup = $persoon;

                                //begin setting!
                                $persoon->van = Input::get('van');
                                $persoon->voorname = Input::get('voorname');
                                $persoon->noemnaam = Input::get('noemnaam');
                                $persoon->geboorte_datum = Input::get('geboorte_datum');
                                $persoon->geslag = Input::get('geslag');
                                $persoon->id_nommer = Input::get('id_nommer');
                                $persoon->geslag = Input::get('geslag');
                                $persoon->kontak_nommer_1 = Input::get('kontak_nommer_1');
                                $persoon->kontak_nommer_2 = Input::get('kontak_nommer_2');
                                $persoon->epos_adres = Input::get('epos_adres');
                                $persoon->voertuig_registrasie_nommer = Input::get('voertuig_registrasie_nommer');
                                $persoon->woonadres = Input::get('woonadres');
                                $persoon->posadres = Input::get('posadres');

                                if(Input::get('geregistreerde_voortrekker') === 'on')
                                    {
                                       $persoon->voortrekker = true;
                                    }
                                else $persoon->voortrekker = false;

                                $persoon->voortrekker_registrasie_nommer = Input::get('voortrekker_registrasie_nommer');
                                $persoon->voortrekker_rang = Input::get('voortrekker_rang');

                                if(Input::get('kommando') != null)
                                {
                                   $kommando = Kommando::where('id', Input::get('kommando'))->firstOrFail();
                                   $persoon->kommando_id = $kommando->id;
                                }

                                $persoon->ouer_voog_naam_van_1 = Input::get('ouer_voog_naam_van_1');
                                $persoon->ouer_voog_id_nommer_1 = Input::get('ouer_voog_id_nommer_1');
                                $persoon->ouer_voog_kontak_nommer_1 = Input::get('ouer_voog_kontak_nommer_1');
                                $persoon->ouer_voog_epos_adres_1 = Input::get('ouer_voog_epos_adres_1');

                                $persoon->ouer_voog_naam_van_2 = Input::get('ouer_voog_naam_van_2');
                                $persoon->ouer_voog_id_nommer_2 = Input::get('ouer_voog_id_nommer_2');
                                $persoon->ouer_voog_kontak_nommer_2 = Input::get('ouer_voog_kontak_nommer_2');
                                $persoon->ouer_voog_epos_adres_2 = Input::get('ouer_voog_epos_adres_2');

                                if(Input::get('mediese_fonds') === 'on')
                                {
                                    $persoon->mediese_fonds = true;
                                }
                                else $persoon->mediese_fonds = false;

                                $persoon->mediese_fonds_naam = Input::get('mediese_fonds_naam');
                                $persoon->mediese_fonds_lid_nommer = Input::get('mediese_fonds_lid_nommer');
                                $persoon->huis_dokter_naam_van = Input::get('huis_dokter_naam_van');
                                $persoon->huis_dokter_kontak_nommer_1 = Input::get('huis_dokter_kontak_nommer_1');
                                $persoon->huis_dokter_kontak_nommer_2 = Input::get('huis_dokter_kontak_nommer_2');
                                $persoon->siekte_allergie = Input::get('siekte_allergie');

                                $persoon->save();

                                Log::notice('2000 || Persoon opgedateer! |', ['veroorsaak_deur' => Auth::user()->toArray(), 'nuwe_persoon_rekord' => $persoon->toArray(), 'ou_persoon_rekord' => $persoon_backup->toArray()]);
                                Session::set('inskrywing_persoon_opgedateer', true);
                                return Redirect::to('inskrywing/nuut');
                            }
                            else {
                                //Invalid input! Redirecting back...
                                return redirect()->back()->withInput()->withErrors($v);
                            }

                        }
                    else {
                        Log::warning("2110 || Kon nie 'n persoon in die flashdata vind om in te skryf nie! |", ['veroorsaak_deur' => Auth::user()->toArray()]);
                        return Redirect::to('persone/tabel')->withErrors("Die Inskrywing was onsuksesvol!");
                    }

                    Session::forget('inskrywing_persoon');
                    Session::forget('inskrywing_persoon_opgedateer');
                    return view('persone_table')->withErrors("Die Inskrywing was onsuksesvol!");

        }

        public function nuwe_inskrywing_betrokkenheid($id)
        {
            //Check if we have a valid flow of Inskrywing so far
            $session_kamp = Session::get('inskrywing_kamp');
            $session_persoon = Session::get('inskrywing_persoon');
            $session_persoon_opgedateer = Session::get('inskrywing_persoon_opgedateer');
            if(($session_kamp == false) || ($session_persoon == false) || ($session_persoon_opgedateer == false))
            {
                Log::warning("2110 || Inskrywing Proses was nie korrek voltooi nie! |", ['veroorsaak_deur' => Auth::user()->toArray()]);
                        return Redirect::to('persone/tabel')->withErrors("Die Inskrywing was onsuksesvol!");
            }
            else
            {
                $betrokkenheid = VoortrekkerBetrokkenheid::find($id);
                if($betrokkenheid == false)
                {
                   Log::warning("2110 || Kon nie die betrokkenheid in databasis kry vir 'n inskrywing nie! |", ['veroorsaak_deur' => Auth::user()->toArray()]);
                        return Redirect::to('persone/tabel')->withErrors("Die Inskrywing was onsuksesvol!");
                }

                Session::set('inskrywing_betrokkenheid', $betrokkenheid);
                return Redirect::to('inskrywing/nuut');
            }


        }

        public function nuwe_inskrywing_rol($id)
        {
            //Check if flow is valid
            $session_kamp = Session::get('inskrywing_kamp');
            $session_persoon = Session::get('inskrywing_persoon');
            $session_persoon_opgedateer = Session::get('inskrywing_persoon_opgedateer');
            $session_betrokkenheid = Session::get('inskrywing_betrokkenheid');
            if(($session_kamp == false) || ($session_persoon == false) || ($session_persoon_opgedateer == false) || ($session_betrokkenheid == false))
            {
                Log::warning("2110 || Inskrywing Proses was nie korrek voltooi nie! |", ['veroorsaak_deur' => Auth::user()->toArray()]);
                        return Redirect::to('inskrywing/kanseleer')->withErrors("Die Inskrywing was onsuksesvol!");
            }
            else
            {
               $rol = Rol::find($id);
               if($rol == false)
                {
                   Log::warning("2110 || Kon nie die rol in databasis kry vir 'n inskrywing nie! |", ['veroorsaak_deur' => Auth::user()->toArray()]);
                        return Redirect::to('inskrywing/kanseleer')->withErrors("Die Inskrywing was onsuksesvol!");
                }
                Session::set('inskrywing_rol', $rol);
               return Redirect::to('inskrywing/nuut');
            }


        }

        public function nuwe_inskrywing_bevestig()
        {

            $persoon = Persoon::find(Session::get('inskrywing_persoon'));
            $rol = Session::get('inskrywing_rol');
            $betrokkenheid = Session::get('inskrywing_betrokkenheid');

            $exists = $persoon->rolle->contains($rol);

            if($exists)
            {
              return Redirect::back()->withErrors($persoon->noemnaam." ".$persoon->van." het reeds die rol ".$rol->rol_naam);
            }
            else
            {


              //Check if Inskrywing exists
              $check_inskrywing = Inskrywing::where('persoon_id', $persoon->id)->where('kamp_id', $rol->groep->kamp_id)->first();
              if($check_inskrywing === null)
              {
                //Skep die Inskrywing
                $inskrywing = new Inskrywing;
                $inskrywing->persoon_id = $persoon->id;
                $inskrywing->kamp_id = $rol->groep->kamp_id;
                $inskrywing->voortrekker_betrokkenheid_id = $betrokkenheid->id;
                $persoon->rolle()->attach($rol);
                $inskrywing->save();
              }

                //Forget all Session Variables
                Session::forget('inskrywing_persoon');
                Session::forget('inskrywing_persoon_opgedateer');
                Session::forget('inskrywing_betrokkenheid');
                Session::forget('inskrywing_rol');
                Session::forget('inskrywing_kamp');

              return Redirect::to('inskrywing/view/'.$inskrywing->id)->with('success', $persoon->noemnaam." ".$persoon->van." vervul nou die rol ".$rol->rol_naam);
            }
        }

        public function getEmails($id)
        {

            $return['recordsTotal'] = Persoon::count();

            $return['data'] = [];
            $kamp_id = $id;
            $return_string = "";

            foreach(Inskrywing::where('kamp_id', $kamp_id)->get() as $inskrywing)
            {
                if($inskrywing->persoon->epos_adres)
                {
                   $string = $inskrywing->persoon->noemnaam." ".$inskrywing->persoon->van.",".$inskrywing->persoon->epos_adres."<br>";
                    $return_string .= $string;
                }

                if(($inskrywing->persoon->ouer_voog_naam_van_1) && ($inskrywing->persoon->ouer_voog_epos_adres_1))
                {
                    $string = $inskrywing->persoon->ouer_voog_naam_van_1.",".$inskrywing->persoon->ouer_voog_epos_adres_1."<br>";
                    $return_string .= $string;
                }

                if(($inskrywing->persoon->ouer_voog_naam_van_2) && ($inskrywing->persoon->ouer_voog_epos_adres_2))
                {
                    $string = $inskrywing->persoon->ouer_voog_naam_van_2.",".$inskrywing->persoon->ouer_voog_epos_adres_1."<br>";
                    $return_string .= $string;
                }

            }

            return $return_string;
        }

}

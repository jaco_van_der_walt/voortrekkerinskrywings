<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PubliekeInskrywing extends Model {

    use SoftDeletes;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'publieke_inskrywings';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

    protected $guarded = ['id'];

    protected $hidden = ['created_at', 'updated_at'];

     protected $dates = ['deleted_at'];


    public function kamp()
    {
    	 return $this->belongsTo('App\Kamp', 'kamp_id');
    }

    public function groep()
    {
        return $this->belongsTo('App\Groep', 'groep_id');
    }

    public function voortrekkerBetrokkenheid(){
        return $this->belongsTo('App\VoortrekkerBetrokkenheid');
    }

    public function opsies(){
      return $this->belongsToMany('App\Opsie', 'publieke_inskrywing_ekstras', 'publieke_inskrywing_id', 'opsie_id');
    }


}

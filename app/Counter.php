<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Counter extends Model {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'counters';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

    protected $guarded = ['id'];

    protected $hidden = ['created_at', 'updated_at'];
}

@extends('layouts.master')

@section('head')
@parent
<title>Inskrywing</title>
@stop


@section('content')
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <a href={{URL('kamp/paneel/'.$kamp->id)}}><button type="submit" class="btn btn-primary "><i class="fa fa-arrow-circle-left"></i></button></a> {{$kamp->kamp_naam}}: Inskrywing
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">

                @if($errors->has())
                   @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Waarskuwing!</b> {{$error}}
                        </div>
                  @endforeach
                @endif

                @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissable">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Sukses!</b> {{Session::get('success')}}
                        </div>
                @endif


                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- form start -->
                           <!-- <form role="form" action="{{URL('inskrywing/edit')}}" method="POST"> -->
                           {!! Form::open(array('url'=>'inskrywing/edit','method'=>'POST', 'files'=>true)) !!}
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><b>{{$persoon->noemnaam}} {{$persoon->van}}</b></h3>
                                </div><!-- /.box-header -->

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <div class="box-body">

                                        <div class=" row form-group">
                                            <div class="col-xs-2">
                                                <label>Verwysing</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="col-xs-11">
                                                    <label>{{$inskrywing->verwysing}}</label>
                                                </div>
                                            </div>
                                        </div>

                                        @foreach($inskrywing->opsies as $o)
                                          <div class=" row form-group">
                                              <div class="col-xs-2">
                                                  <label>{{$o->ekstra->naam}}</label>
                                              </div>
                                              <div class="col-xs-4">
                                                  <div class="col-xs-11">
                                                      <label>{{$o->naam}} (R{{$o->prys}}-00)</label>
                                                  </div>
                                              </div>
                                          </div>
                                        @endforeach



                                        <div class=" row form-group">
                                            <div class="col-xs-2">
                                                <label>Kursusgroep</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="col-xs-11">

                                                    <label>{!! $groepe !!}</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class=" row form-group">
                                            <div class="col-xs-2">
                                                <label>Berekende bedrag verskuldig</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="col-xs-11">

                                                    <label>R{{$berekende_bedrag}}-00</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class=" row form-group">
                                            <div class="col-xs-2">
                                                <label>Volledige Betaling Ontvang</label>
                                            </div>
                                            <div class="col-xs-1">
                                                <label>
                                                @if(Input::old('betaal'))
                                                    @if(Input::old('betaal') === 'j')
                                                        <input type="radio" name="betaal" id="optionsRadios1" value="j" checked>
                                                    @else
                                                        <input type="radio" name="betaal" id="optionsRadios1" value="j">
                                                    @endif
                                                @else
                                                    @if($inskrywing->betaal == true)
                                                        <input type="radio" name="betaal" id="optionsRadios1" value="j" checked>
                                                    @else
                                                        <input type="radio" name="betaal" id="optionsRadios1" value="j">
                                                    @endif
                                                @endif
                                                    Ja
                                                </label>
                                            </div>
                                            <div class="col-xs-1">
                                                <label>
                                                @if(Input::old('betaal'))
                                                    @if(Input::old('betaal') === '0')
                                                        <input type="radio" name="betaal" id="optionsRadios2" value="n" checked>
                                                    @else
                                                        <input type="radio" name="betaal" id="optionsRadios2" value="n">
                                                    @endif
                                                @else
                                                     @if($inskrywing->betaal == false)
                                                        <input type="radio" name="betaal" id="optionsRadios2" value="n" checked>
                                                    @else
                                                        <input type="radio" name="betaal" id="optionsRadios2" value="n">
                                                    @endif
                                                @endif
                                                    Nee
                                                </label>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-xs-2">
                                                <label>Bedrag Ontvang/Verskuldig</label>
                                            </div>
                                            <div class="col-xs-4">
                                                @if(Input::old('betaal_bedrag'))

                                                <div class="col-xs-11">
                                                    <input type="number" class="form-control" name="betaal_bedrag" value="{{{Input::old('betaal_bedrag')}}}" >
                                                </div>
                                                @else
                                                <div class="row">
                                                 <input type="number" class="form-control" name="betaal_bedrag" value="{{{$inskrywing->betaal_bedrag}}}" >
                                                </div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-xs-2">
                                                <label>Betaling Verwysing/Nota</label>
                                            </div>
                                            <div class="col-xs-4">
                                                @if(Input::old('nota'))
                                                    <textarea class="form-control" rows="4" name="nota">{{{Input::old('nota')}}}</textarea>
                                                @else
                                                    <textarea class="form-control" rows="4" name="nota">{{{$inskrywing->nota}}}</textarea>
                                                @endif
                                            </div>

                                        </div>



                            </div>


                                </div>

                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><b>Vorms</b></h3>
                                </div><!-- /.box-header -->

                                <div class="box-body">
                                <div class="row form-group">
                                    <div class="col-xs-2">
                                        <label>Vorms</label>
                                    </div>

                                    <div class="col-xs-10">
                                    @foreach($vorms as $vorm)
                                     <p><a href="{{URL($vorm->file_path)}}">{{$vorm->file_name}}</a></p>
                                    @endforeach
                                    </div>
                                </div>

                                 <div class="row form-group">
                                    <div class="col-xs-2">
                                        <label>Stoor nuwe Vorms</label>
                                    </div>
                                    <div class="col-xs-10">
                                        <input type="file" name="files[]" multiple>

                                    </div>
                                </div>
                            </div>






                            </div>

                            <div class="box-footer">
                                        <button type="submit" class="btn btn-primary pull-right" style="margin-right: 5px;">Opdateer</button>
                                        <button class="btn btn-danger pull-left" style="margin-right: 5px;" data-toggle="modal" data-target="#deleteModal"><i class="fa  fa-trash-o"></i>  Skrap Inskrywing</button>
                            </div>

                            {!! Form::close() !!}
                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->

<div id="deleteModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Skrap Inskrywing vir {{{$persoon->noemnaam}}} {{{$persoon->van}}}</h4>
      </div>
      <div class="modal-body">
        <p>Is jy seker jy wil die inskrywing vir {{{$persoon->noemnaam}}} {{{$persoon->van}}} skrap?<br><br>Om die inskrywing te skrap verwyder slegs hierdie persoon se rol en enige vorms van hierdie kamp af. Die persoon se data bly steeds gestoor.</p>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary pull-left" data-dismiss="modal">Kanseleer</button>
        <a href={{URL('inskrywing/delete')}}><button class="btn btn-danger pull-right"><i class="fa  fa-trash-o"></i>  Skrap Inskrywing</button></a>
      </div>
    </div>

  </div>
</div>



@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>
        <!-- InputMask -->
        <script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.date.extensions.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.extensions.js')}}" type="text/javascript"></script>
@stop

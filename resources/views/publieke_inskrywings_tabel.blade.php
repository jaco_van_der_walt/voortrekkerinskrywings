@extends('layouts.master')

@section('head')
@parent
<title>Aanlyn Inskrywings</title>

<script src="{{URL('//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css')}}" type="text/css"></script>
@stop


@section('content')
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <a href={{URL('kamp/paneel/'.$kamp_id)}}><button type="submit" class="btn btn-primary "><i class="fa fa-arrow-circle-left"></i></button></a>
                        Aanlyn Inskrywings
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">

                @if($errors->has())
                   @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Waarskuwing!</b> {{$error}}
                        </div>
                  @endforeach
                @endif

                @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissable">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Sukses!</b> {{Session::get('success')}}
                        </div>
                @endif

                <div class="callout callout-info">
                    <h4>Aanlyn Inskrywings</h4>
                    <p>Die volgende inskrywings is deur die aanlyn portaal ontvang. Hierdie inskrywings moet eers verwerk word deur 'n Administrateur voordat 'n persoon ingeskryf is vir die kamp. Dink aan die inskrywings op hierdie bladsy as die papier vorms wat ouers ingevul het. Die Administrateur moet eers deur die inskrywing lees en verwerk om enige probleme aan te spreek.</p>
                </div>

<div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Aanlyn Inskrywings Ontvang</h3>                                    
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="inskrywings" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Noem Naam</th>
                                                <th>Van</th>
                                                <th>Geboorte Datum</th>
                                                <th>Kommando</th>
                                                <th>Betrokkenheid</th>
                                                <th>Groep</th>
                                                <th>Verwerk</th>
                                                <th>Aksies</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>#</th>
                                                <th>Noem Naam</th>
                                                <th>Van</th>
                                                <th>Geboorte Datum</th>
                                                <th>Kommando</th>
                                                <th>Betrokkenheid</th>
                                                <th>Groep</th>
                                                <th>Verwerk</th>
                                                <th>Aksies</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->

                                <div class="box-footer col-md-12">
                                    <div class="col-md-12">
                                    </div>
                                </div>

                            </div><!-- /.box -->
                        </div>
                    </div>


                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>

        <!-- DATA TABES SCRIPT -->
        <script src="{{URL('//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('assets/js/plugins/datatables/dataTables.bootstrap.js')}}" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>

        <!-- page script -->
        <script type="text/javascript">
            $(function() {
                $('#inskrywings').DataTable( {
                    "sAjaxSource": "{{{URL('ajax/publiek/inskrywings')}}}",
                    "sServerMethod": "GET",
                    "order": [[ 0, "desc" ]]
                } );
                
            });
        </script>


@stop
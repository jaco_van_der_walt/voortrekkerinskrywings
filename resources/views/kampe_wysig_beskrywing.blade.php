@extends('layouts.master')

@section('head')
@parent
<title>{{$kamp->kamp_naam}}</title>
@stop


@section('content')
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><a href={{URL('kamp/paneel/'.$kamp->id)}}><button type="submit" class="btn btn-primary "><i class="fa fa-arrow-circle-left"></i></button></a>
        {{$kamp->kamp_naam}}</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        @if($errors->has())
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Waarskuwing!</b> {{$error}}
                </div>
            @endforeach
        @endif

        @if(Session::has('success'))
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Sukses!</b> {{Session::get('success')}}
            </div>
        @endif

        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title"><b>Kampbeskrywing</b></h3>
                    </div><!-- /.box-header --> 

                    <div class='box-body pad'>
                    {!! Form::open(array('url'=>'kampbeskrywing/wysig','method'=>'POST')) !!}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <textarea name="beskrywing_inhoud" class="textarea" placeholder="Tik die kamp se beskrywing hier in" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea><br>
                        <button type="submit" class="btn btn-success" style="margin: 5px;"><i class="fa fa-floppy"></i>  Stoor</button>
                    {!! Form::close() !!}
                    </div>
                </div>
            </div><!-- /.box --> 
        </div>    
    </section><!-- /.content -->
</aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="{{URL::asset('assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}" type="text/javascript"></script>
        <script type="text/javascript">
            $(function() {
                $(".textarea").wysihtml5();
                $(".textarea").data("wysihtml5").editor.setValue("{!! $kamp->kamp_beskrywing !!}");
            });
        </script>
@stop
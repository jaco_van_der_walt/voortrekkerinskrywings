@extends('layouts.public_inskrywing')

@section('head')
@parent
<title>Inskrywing</title>
@stop


@section('content')
<aside class="right-side stretch">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <a href={{URL('publiek/inskrywing/terug')}} class="btn-sm btn-primary pull-left" style="margin-left: 5px; margin-bottom: 5px;"><i class="fa fa-arrow-left"></i></a> &nbsp;
                        Inskrywing: {{$kamp->kamp_naam}} - {{$persoon[0]->noemnaam}} {{$persoon[0]->van}}
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">

                    @if($errors->has())
                       @foreach ($errors->all() as $error)
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Waarskuwing!</b> {{$error}}
                            </div>
                      @endforeach
                    @endif

                    <div class="row">
                        <div class="col-md-12">

                        @if(Session::get('publiek_inskrywing_soort') === 'jeuglid')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="callout callout-info">
                                    <h4>Vrywaringsbrief</h4>
                                    <p>Hierdie Vrywaringsbrief word deur jou as ouer of wettige voog van {{$persoon[0]->noemnaam}} {{$persoon[0]->van}} aanvaar. 'n Afskrif van hierdie Vrywaringsvorm sal na jou epos adres gestuur word sodra die inskrywing verwerk en aanvaar is.</p>
                                </div>
                            </div>
                        </div>
                        @else
                        <div class="row">
                            <div class="col-md-12">
                                <div class="callout callout-info">
                                    <h4>Vrywaringsbrief</h4>
                                    <p>Alle Volwassenese moet hierdie Vrywaringsbrief teken en aanvaar. 'n Afskrif van hierdie Vrywaringsvorm sal na jou epos adres gestuur word sodra die inskrywing verwerk en aanvaar is.</p>
                                </div>
                            </div>
                        </div>
                        @endif

                        <!-- Vrywaring -->
                        <div class="box box-primary">
                        <div class="box-header">
                            @if($persoon[0]->voorname == '')
                                <h4 class="box-title"><strong>Vrywaring: {{$persoon[0]->noemnaam}} {{$persoon[0]->van}}</strong></h4>
                            @else
                                <h4 class="box-title"><strong>Vrywaring: {{$persoon[0]->voorname}} ({{$persoon[0]->noemnaam}}) {{$persoon[0]->van}}</strong></h4>
                            @endif
                            
                        </div>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        @if(Session::get('publiek_inskrywing_soort') === 'jeuglid')
                                            {!! $kamp->kamp_vrywaring_jeug !!}
                                        @else
                                            {!! $kamp->kamp_vrywaring_volwasse !!}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>




                        <form role="form" action="{{URL('publiek/inskrywing/vrywaring')}}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title"><b>Geteken Deur:</b></h3>
                            </div>
                            <div class="box-body">
                                    
                                    <div class="row form-group">
                                        <div class="col-xs-3">
                                            @if(Session::get('publiek_inskrywing_soort') === 'jeuglid')
                                                <label>Volle Naam en Van (Ouer/Voog)</label>
                                            @else
                                                <label>Volle Naam en Van</label>
                                            @endif
                                        </div>
                                            <div class="col-xs-6">
                                                <input id="vrn_input" type="text" class="form-control" name="geteken_deur" value="{{Input::old('geteken_deur')}}" > 
                                            </div>   
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-3">
                                            @if(Session::get('publiek_inskrywing_soort') === 'jeuglid')
                                                <label>ID Nommer (Ouer/Voog)</label>
                                            @else
                                                <label>ID Nommer</label>
                                            @endif
                                        </div>
                                            <div class="col-xs-6">
                                                <input id="vrn_input" type="text" class="form-control" name="geteken_deur_id_nommer" value="{{Input::old('geteken_deur_id_nommer')}}" > 
                                            </div>   
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-3">
                                            @if(Session::get('publiek_inskrywing_soort') === 'jeuglid')
                                                <label>Epos Adres (Ouer/Voog)</label>
                                            @else
                                                <label>Epos Adres</label>
                                            @endif
                                        </div>
                                            <div class="col-xs-6">
                                                <input id="vrn_input" type="email" class="form-control" name="geteken_deur_email" value="{{Input::old('geteken_deur_email')}}" > 
                                            </div>   
                                    </div>   
                            </div>
                        </div>

                        <!-- footer --> 
                        <div class="box-footer col-md-12">
                            <div class="row">
                                    <a href="{{URL('publiek/inskrywing/terug')}}" class="btn btn-warning pull-left" style="margin-right: 5px;"><i class="fa fa-arrow-circle-left"></i> Terug </a>
                                    <button id="submit" type="submit" class="btn btn-success pull-right">Teken Vrywaring <i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                        </div>
                        <!-- /footer -->
                        </div>
                    </div> 
                    </form>


                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>


@stop
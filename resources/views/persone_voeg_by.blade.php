@extends('layouts.master')

@section('head')
@parent
<title>Persone</title>
@stop


@section('content')
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <a href={{URL('persone/tabel')}}><button type="submit" class="btn btn-primary "><i class="fa fa-arrow-circle-left"></i></button></a> Persone
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">

                @if($errors->has())
                   @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Waarskuwing!</b> {{$error}}
                        </div>
                  @endforeach
                @endif


                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- form start -->
                           <form role="form" action="{{URL('persone/voegby/nuut')}}" method="POST">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><b>Persoonlike Inligting</b></h3>
                                </div><!-- /.box-header -->
                                
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                
                                    <div class="box-body">

                                    <!-- Van -->
                                        <div class="row form-group">
                                            <div class="col-xs-2">
                                                <label>Van <span>*</span></label>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="van" value="{{Input::old('van')}}"> 
                                            </div>   
                                        </div>

                                    <!-- Voorname -->
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Voorname</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="voorname" value="{{Input::old('voorname')}}" > 
                                            </div>   
                                        </div>

                                    <!-- Noemnaam -->
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Noemnaam <span>*</span></label>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="noemnaam" value="{{Input::old('noemnaam')}}"> 
                                            </div>   
                                        </div>

                                    <!-- Date dd/mm/yyyy -->
                                    <div class="row form-group">
                                        <div class="col-xs-2">
                                                <label>Geboortedatum <span>*</span></label>
                                         </div>
                                        <div class="col-xs-4">
                                            <input id="datemask" name="geboorte_datum" type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask value="{{Input::old('geboorte_datum')}}"/>
                                        </div><!-- /.input group -->
                                    </div><!-- /.form group -->

                                    <!-- Geslag -->
                                        <!-- radio -->
                                        <div class=" row form-group"> 

                                            <div class="col-xs-2">
                                                <label>Geslag <span>*</span></label>
                                            </div>    
                                            <div class="col-xs-1">
                                                <label>
                                                    @if(Input::old('geslag') === 'm')
                                                        <input type="radio" name="geslag" id="optionsRadios1" value="m" checked>
                                                    @else
                                                        <input type="radio" name="geslag" id="optionsRadios1" value="m">
                                                    @endif
                                                    Manlik
                                                </label>
                                            </div>
                                            <div class="col-xs-1">
                                                <label>
                                                    @if(Input::old('geslag') === 'f')
                                                    <input type="radio" name="geslag" id="optionsRadios2" value="f" checked>
                                                    @else
                                                    <input type="radio" name="geslag" id="optionsRadios2" value="f">
                                                    @endif
                                                    Vroulik
                                                </label>
                                            </div>
                                        </div>

                                    <!-- ID Nommer -->
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>ID Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="id_nommer" value="{{Input::old('id_nommer')}}" > 
                                            </div>   
                                        </div>

                                    <!-- Kontak Nommer -->
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Kontak Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="kontak_nommer_1" value="{{Input::old('kontak_nommer_1')}}"> 
                                            </div>
                                            
                                            <div class="col-xs-1">
                                                <label>Alternatiewe Kontak Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="kontak_nommer_2" value="{{Input::old('kontak_nommer_2')}}"> 
                                            </div>     
                                        </div>

                                    <!-- Epos Adres -->
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Epos Adres</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="epos_adres" value="{{Input::old('epos_adres')}}" > 
                                            </div>   
                                        </div>


                                        <!-- Adres -->
                                        <div class="row form-group">
                                            <div class="col-xs-2">
                                                <label>Woonadres</label>
                                            </div>

                                            <div class="col-xs-4">
                                                <textarea class="form-control" rows="4" name="woonadres">{{Input::old('woonadres')}}</textarea>
                                            </div>

                                            <div class="col-xs-1">
                                                <label>Posadres</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <textarea class="form-control" rows="4" name="posadres">{{Input::old('posadres')}}</textarea>
                                            </div>
                                        </div>      

                                    <!-- Voertuig Registrasie Nommer -->
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Voertuig Registrasie Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="voertuig_registrasie_nommer" value="{{Input::old('voertuig_registrasie_nommer')}}" > 
                                            </div>   
                                        </div>


                                    </div>
                                
                            </div><!-- /.box --> 

                            <!-- Die Voortrekkers --> 
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><b>Die Voortrekkers</b></h3>
                                </div><!-- /.box-header -->

                                <div class="box-body">

                                        <div class="row form-group">
                                            <div class="col-xs-2">
                                                <label>Geregistreerde Voortrekker?</label>
                                            </div>

                                            <div class="col-xs-3">
                                                <label>
                                                    @if(Input::old('geregistreerde_voortrekker') === 'on')
                                                        <input type="checkbox" checked="true" name="geregistreerde_voortrekker" checked/>
                                                    @else
                                                        <input type="checkbox" checked="true" name="geregistreerde_voortrekker" checked/>
                                                    @endif
                                                    Ja
                                                </label>                                                
                                            </div>
                                        </div>



                                    <!-- Voortrekker Registrasie Nommer -->
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Voortrekker Registrasie Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <input type="text" class="form-control" name="voortrekker_registrasie_nommer" value="{{Input::old('voortrekker_registrasie_nommer')}}" > 
                                            </div>   
                                        </div>            
                                    

                                        <!-- Rang -->
                                        <div class="row form-group">
                                                 <div class="col-xs-2">
                                                    <label>Voortrekker Rang</label>
                                                </div>
                                                <div class="col-xs-4">
                                                    <input type="text" class="form-control" name="rang" value="{{Input::old('rang')}}"> 
                                                </div>              
                                        </div>

                                        <!-- Kommando -->
                                        <div class="row form-group">
                                                 <div class="col-xs-2">
                                                    <label>Kommando</label>
                                                </div>
                                                <div class="col-xs-4">
                                                    <select name="kommando" class="form-control">
                                                        <option></option>
                                                        @foreach($kommando as $k)
                                                            @if(Input::old("kommando") == $k->id)
                                                                <option value="{{{$k->id}}}" selected>{{{$k->kommando_naam}}}</option>
                                                            @else
                                                                <option value="{{{$k->id}}}">{{{$k->kommando_naam}}}</option>
                                                            @endif
                                                        @endforeach

                                                    </select>
                                                </div>       
                                        </div>
                                    </div>
                                </div>

                            <!-- Ouers/Voog --> 
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><b>Ouers/Voog</b></h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                        <!-- Ouer/Voog #1 -->
                                        <h4><b>Ouer/Voog #1</b></h4>
                                        <div class="row form-group">
                                                 <div class="col-xs-2">
                                                    <label>Ouer/Voog Naam & Van</label>
                                                </div>
                                                <div class="col-xs-4">
                                                    <input type="text" class="form-control" name="ouer_voog_naam_van_1" value="{{Input::old('ouer_voog_naam_van_1')}}"> 
                                                </div>       
                                        </div>

                                        <div class="row form-group">
                                                 <div class="col-xs-2">
                                                    <label>Ouer/Voog ID Nommer</label>
                                                </div>
                                                <div class="col-xs-4">
                                                    <input type="text" class="form-control" name="ouer_voog_id_nommer_1" value="{{Input::old('ouer_voog_id_nommer_1')}}"> 
                                                </div>       
                                        </div>

                                        <div class="row form-group">
                                                 <div class="col-xs-2">
                                                    <label>Ouer/Voog Kontak Nommer</label>
                                                </div>
                                                <div class="col-xs-4">
                                                    <input type="text" class="form-control" name="ouer_voog_kontak_nommer_1" value="{{Input::old('ouer_voog_kontak_nommer_1')}}"> 
                                                </div>

                                                 <div class="col-xs-2">
                                                    <label>Ouer/Voog Epos Adres</label>
                                                </div>
                                                <div class="col-xs-4">
                                                    <input type="text" class="form-control" name="ouer_voog_epos_adres_1" value="{{Input::old('ouer_voog_epos_adres_1')}}"> 
                                                </div>         
                                        </div>

                                        <!-- Ouer/Voog #2 -->
                                        <h4><b>Ouer/Voog #2</b></h4>
                                        <div class="row form-group">
                                                 <div class="col-xs-2">
                                                    <label>Ouer/Voog Naam & Van</label>
                                                </div>
                                                <div class="col-xs-4">
                                                    <input type="text" class="form-control" name="ouer_voog_naam_van_2" value="{{Input::old('ouer_voog_naam_van_2')}}"> 
                                                </div>       
                                        </div>

                                        <div class="row form-group">
                                                 <div class="col-xs-2">
                                                    <label>Ouer/Voog ID Nommer</label>
                                                </div>
                                                <div class="col-xs-4">
                                                    <input type="text" class="form-control" name="ouer_voog_id_nommer_2" value="{{Input::old('ouer_voog_id_nommer_2')}}"> 
                                                </div>       
                                        </div>

                                        <div class="row form-group">
                                                 <div class="col-xs-2">
                                                    <label>Ouer/Voog Kontak Nommer</label>
                                                </div>
                                                <div class="col-xs-4">
                                                    <input type="text" class="form-control" name="ouer_voog_kontak_nommer_2" value="{{Input::old('ouer_voog_kontak_nommer_2')}}"> 
                                                </div>

                                                 <div class="col-xs-2">
                                                    <label>Ouer/Voog Epos Adres</label>
                                                </div>
                                                <div class="col-xs-4">
                                                    <input type="text" class="form-control" name="ouer_voog_epos_adres_2" value="{{Input::old('ouer_voog_epos_adres_2')}}"> 
                                                </div>         
                                        </div>
                                       


                                    </div>
                                </div>


                            <!-- Mediese Inligting --> 
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><b>Mediese Inligting</b></h3>
                                </div><!-- /.box-header -->

                                <div class="box-body"> 

                                    <!-- Mediese Fonds Check -->
                                    <div class="row form-group">
                                            <div class="col-xs-2">
                                                <label>Mediese Fonds?</label>
                                            </div>

                                            <div class="col-xs-3">
                                                <label>
                                                    @if(Input::old('mediese_fonds') === 'on')
                                                        <input type="checkbox" checked="true" name="mediese_fonds" checked/>
                                                    @else
                                                        <input type="checkbox" checked="true" name="mediese_fonds" checked/>
                                                    @endif
                                                    Ja
                                                </label>                                                
                                            </div>
                                        </div>

                                        <!-- Mediese Fonds Naam -->
                                        <div class="row form-group">
                                                 <div class="col-xs-2">
                                                    <label>Mediese Fonds Naam</label>
                                                </div>
                                                <div class="col-xs-4">
                                                    <input type="text" class="form-control" name="mediese_fonds_naam" value="{{Input::old('mediese_fonds_naam')}}"> 
                                                </div>              
                                        </div>

                                        <!-- Mediese Fonds Lidmaat Nommer -->
                                        <div class="row form-group">
                                                 <div class="col-xs-2">
                                                    <label>Mediese Fonds Lidmaat Nommer</label>
                                                </div>
                                                <div class="col-xs-4">
                                                    <input type="text" class="form-control" name="mediese_fonds_lid_nommer" value="{{Input::old('mediese_fonds_lid_nommer')}}">
                                                </div>              
                                        </div>


                                        <!-- Huisdokter -->
                                        <h4><b>Huisdokter</b></h4>
                                        <div class="row form-group">
                                                 <div class="col-xs-2">
                                                    <label>Huisdokter Naam & Van</label>
                                                </div>
                                                <div class="col-xs-4">
                                                    <input type="text" class="form-control" name="huis_dokter_naam_van" value="{{Input::old('huis_dokter_naam_van')}}"> 
                                                </div>       
                                        </div>


                                        <div class="row form-group">
                                                 <div class="col-xs-2">
                                                    <label>Huisdokter Kontak Nommer</label>
                                                </div>
                                                <div class="col-xs-4">
                                                    <input type="text" class="form-control" name="huis_dokter_kontak_nommer_1" value="{{Input::old('huis_dokter_kontak_nommer_1')}}"> 
                                                </div>

                                                 <div class="col-xs-2">
                                                    <label>Huisdokter Alternatiewe Kontak Nommer</label>
                                                </div>
                                                <div class="col-xs-4">
                                                    <input type="text" class="form-control" name="huis_dokter_kontak_nommer_2" value="{{Input::old('huis_dokter_kontak_nommer_2')}}"> 
                                                </div>         
                                        </div>

                                        <!-- Mediese Toestand & Allergie -->
                                        <div class="row form-group">
                                            <div class="col-xs-2">
                                                    <label>Siektetoestande of Allergieë</label>
                                                </div>
                                            <div class="col-xs-4">
                                                <textarea class="form-control" rows="4" name="siekte_allergie">{{Input::old('siekte_allergie')}}</textarea>
                                            </div>

                                        </div>  

                                </div>
                            </div>


                                </div>
                            </div>

                            <div class="box-footer">
                                        <button type="submit" class="btn bg-olive pull-right" style="margin-bottom: 15px;"><i class="fa fa-paper-plane"></i> Stoor</button>
                                        </form>
                                        <a href={{URL('persone/tabel')}}><button class="btn btn-primary pull-left" style="margin-bottom: 15px;">Kanselleer</button></a>
                                        
                            </div>
                            
                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>
        <!-- InputMask -->
        <script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.date.extensions.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.extensions.js')}}" type="text/javascript"></script>


        <script type="text/javascript">
            //Datemask
                $("#datemask").inputmask("yyyy/mm/dd", {"placeholder": "yyyy/mm/dd"});
        </script>



@stop
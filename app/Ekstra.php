<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ekstra extends Model
{
    protected $table = 'ekstras';

    public function opsies()
    {
      return $this->hasMany('App\Opsie', 'ekstra_id');
    }

    public function kamp()
    {
       return $this->belongsTo('App\Kamp', 'kamp_id');
    }
}

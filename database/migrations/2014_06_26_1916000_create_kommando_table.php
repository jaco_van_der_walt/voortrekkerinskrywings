<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKommandoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('kommando', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('kommando_naam');
			$table->string('stad_dorp')->nullable();
			$table->enum('provinsie', ['Gauteng', 'KwaZulu-Natal', 'Limpopo', 'Mpumalanga', 'Noordwes', 'Noord-Kaap', 'Oos-Kaap', 'Vrystaat'])->nullable();
			$table->string('kontak_persoon_naam_van')->nullable();
			$table->string('kontak_persoon_epos')->nullable();
			$table->string('kontak_persoon_telefoon')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('kommando');
	}

}
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKampeOrganisasiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kampe_organisasies', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('kamp_id')->unsigned();
            $table->integer('organisasie_id')->unsigned();

            $table->foreign('kamp_id')->references('id')->on('kampe');
            $table->foreign('organisasie_id')->references('id')->on('organisasies');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kampe_organisasies');
    }
}

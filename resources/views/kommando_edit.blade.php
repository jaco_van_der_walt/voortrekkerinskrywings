@extends('layouts.master')

@section('head')
@parent
<title>Kommando</title>
@stop


@section('content')
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Wysig {{{$kommando->kommando_naam}}}
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Persone</a></li>
                        <li class="active">Voeg By</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                @if($errors->has())
                   @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Waarskuwing!</b> {{$error}}
                        </div>
                  @endforeach
                @endif


                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- form start -->
                           <form role="form" action="{{URL('kommando/edit')}}" method="POST">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><b>Kommando Inligting</b></h3>
                                </div><!-- /.box-header -->
                                
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                
                                    <div class="box-body">

                                    <!-- Naam -->
                                        <div class="row form-group">
                                            <div class="col-xs-2">
                                                <label>Kommando Naam <span>*</span></label>
                                            </div>
                                            <div class="col-xs-4">
                                                @if(Input::old('kommando_naam'))
                                                    <input type="text" class="form-control" name="kommando_naam" value="{{Input::old('kommando_naam')}}"> 
                                                @else
                                                    <input type="text" class="form-control" name="kommando_naam" value="{{$kommando->kommando_naam}}"> 
                                                @endif
                                            </div>   
                                        </div>

                                    <!-- Stad/Dorp -->
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Stad/Dorp</label>
                                            </div>
                                            <div class="col-xs-4">
                                                @if(Input::old('stad_dorp'))
                                                    <input type="text" class="form-control" name="stad_dorp" value="{{Input::old('stad_dorp')}}" > 
                                                @else
                                                    <input type="text" class="form-control" name="stad_dorp" value="{{$kommando->stad_dorp}}" > 
                                                @endif
                                            </div>   
                                        </div>

                                    <!-- Provinsie -->
                                        <div class="row form-group">
                                            <div class="col-xs-2">
                                                <label>Provinsie</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <select name="provinsie" class="form-control">
                                                    
                                                    <option></option>
                                                    
                                                    <option 
                                                    @if(Input::old('provinsie') === "Gauteng" ||
                                                           $kommando->provinsie === "Gauteng")
                                                        selected 
                                                    @endif
                                                    >Gauteng</option>

                                                    <option 
                                                    @if(Input::old('provinsie') === "KwaZulu-Natal" ||
                                                           $kommando->provinsie === "KwaZulu-Natal")
                                                        selected 
                                                    @endif
                                                    >KwaZulu-Natal</option>

                                                    <option 
                                                    @if(Input::old('provinsie') === "Limpopo" ||
                                                           $kommando->provinsie === "Limpopo")
                                                        selected 
                                                    @endif
                                                    >Limpopo</option>

                                                    <option 
                                                    @if(Input::old('provinsie') === "Mpumalanga" ||
                                                           $kommando->provinsie === "Mpumalanga")
                                                        selected 
                                                    @endif
                                                    >Mpumalanga</option>

                                                    <option 
                                                    @if(Input::old('provinsie') === "Noordwes" ||
                                                           $kommando->provinsie === "Noordwes")
                                                        selected 
                                                    @endif
                                                    >Noordwes</option>

                                                    <option 
                                                    @if(Input::old('provinsie') === "Noord-Kaap" ||
                                                           $kommando->provinsie === "Noord-Kaap")
                                                        selected 
                                                    @endif
                                                    >Noord-Kaap</option>

                                                    <option 
                                                    @if(Input::old('provinsie') === "Oos-Kaap" ||
                                                           $kommando->provinsie === "Oos-Kaap")
                                                        selected 
                                                    @endif
                                                    >Oos-Kaap</option>

                                                    <option 
                                                    @if(Input::old('provinsie') === "Wes-Kaap" ||
                                                           $kommando->provinsie === "Wes-Kaap")
                                                        selected 
                                                    @endif
                                                    >Wes-Kaap</option>

                                                    <option 
                                                    @if(Input::old('provinsie') === "Vrystaat" ||
                                                           $kommando->provinsie === "Vrystaat")
                                                        selected 
                                                    @endif
                                                    >Vrystaat</option>

                                                </select>
                                            </div>
                                        </div>
                            </div>
                                
                            </div><!-- /.box --> 

                            <!-- Die Voortrekkers --> 
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><b>Kontak Persoon</b></h3>
                                </div><!-- /.box-header -->

                                <div class="box-body">



                                    <!-- Kontak Persoon Naam & Van -->
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Kontak Persoon Naam & Van</label>
                                            </div>
                                            <div class="col-xs-4">
                                                @if(Input::old('kontak_persoon_naam_van'))
                                                <input type="text" class="form-control" name="kontak_persoon_naam_van" value="{{Input::old('kontak_persoon_naam_van')}}" > 
                                                @else
                                                <input type="text" class="form-control" name="kontak_persoon_naam_van" value="{{$kommando->kontak_persoon_naam_van}}" >
                                                @endif
                                            </div>   
                                        </div>            
                                    

                                        <!-- Kontak Persoon Epos Adres -->
                                        <div class="row form-group">
                                                 <div class="col-xs-2">
                                                    <label>Kontak Persoon Epos Adres</label>
                                                </div>
                                                <div class="col-xs-4">
                                                    @if(Input::old('kontak_persoon_epos'))
                                                    <input type="text" class="form-control" name="kontak_persoon_epos" value="{{Input::old('kontak_persoon_epos')}}"> 
                                                    @else
                                                    <input type="text" class="form-control" name="kontak_persoon_epos" value="{{$kommando->kontak_persoon_epos}}"> 
                                                    @endif
                                                </div>              
                                        </div>

                                        <!-- Kontak Persoon Telefoon Nommer -->
                                        <div class="row form-group">
                                                 <div class="col-xs-2">
                                                    <label>Kontak Persoon Telefoon Nommer</label>
                                                </div>
                                                <div class="col-xs-4">
                                                    @if(Input::old('kontak_persoon_telefoon'))
                                                    <input type="text" class="form-control" name="kontak_persoon_telefoon" value="{{Input::old('kontak_persoon_telefoon')}}"> 
                                                    @else
                                                    <input type="text" class="form-control" name="kontak_persoon_telefoon" value="{{$kommando->kontak_persoon_telefoon}}"> 
                                                    @endif
                                                </div>       
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>

<div class="box-footer col-md-12">
    <div class="col-md-4">
        <a href={{URL('persone')}}><button class="btn btn-primary">Kanseleer</button></a>
    </div>
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <button type="submit" class="btn btn-warning pull-right"><i class="fa fa-edit"></i>  Opdateer</button>
        <button class="btn btn-danger pull-right" style="margin-right: 5px;" data-toggle="modal" data-target="#deleteModal"><i class="fa  fa-trash-o"></i>  Skrap</button>   
    </div>
</div>
</form>

<div id="deleteModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Skrap {{{$kommando->kommando_naam}}}</h4>
      </div>
      <div class="modal-body">
        <p>Skrap {{{$kommando->kommando_naam}}}?</p>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary pull-left" data-dismiss="modal">Kanseleer</button>
        <a href={{URL('kommando/delete')}}><button class="btn btn-danger pull-right"><i class="fa  fa-trash-o"></i>  Skrap</button></a>
      </div>
    </div>

  </div>
</div>
</form>
</div><!-- /.row (main row) -->
</section><!-- /.content -->
</aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>
        <!-- InputMask -->
        <script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.date.extensions.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.extensions.js')}}" type="text/javascript"></script>


        <script type="text/javascript">
            //Datemask
                $("#datemask").inputmask("yyyy/mm/dd", {"placeholder": "yyyy/mm/dd"});
        </script>



@stop
<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>Teken in</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="{{{URL::asset('assets/css/bootstrap.min.css')}}}" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="{{{URL::asset('assets/css/font-awesome.min.css')}}}" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="{{{URL::asset('assets/css/AdminLTE.css')}}}" rel="stylesheet" type="text/css" />
        <!-- Custom CSS -->
         <link href="{{{URL::asset('assets/css/custom.css')}}}" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="bg-black">

        @if (count($errors) > 0)
        <div class="box-body auth_alerts">
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <b>Waarskuwing: </b> {{ $error }}
                </div>
            @endforeach
        </div>
        @endif

        <div class="form-box" id="login-box">
            <div class="header">Teken In</div>
            <form role="form" method="POST" action="{{ url('/auth/login') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" type="checkbox" name="remember" checked="true">
                <div class="body bg-gray">
                    <div class="form-group">
                        <input type="text" name="email" class="form-control" placeholder="E-Mail Address" value="{{old('email')}}"/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Password"/>
                    </div>          
                </div>
                <div class="footer">                                                               
                    <button type="submit" class="btn bg-olive btn-block">Teken In</button>       
                </div>
            </form>
        </div>


        <!-- jQuery 2.0.2 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="{{{URL::asset('assets/js/bootstrap.min.js')}}}" type="text/javascript"></script>        

    </body>
</html>
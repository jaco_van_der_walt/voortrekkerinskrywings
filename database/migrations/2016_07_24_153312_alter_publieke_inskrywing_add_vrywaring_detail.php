<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPubliekeInskrywingAddVrywaringDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('publieke_inskrywings', function ($table) {
            $table->string('vrywaring_geteken_deur')->nullable();
            $table->string('vrywaring_geteken_deur_id')->nullable();
            $table->string('vrywaring_geteken_deur_epos')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('publieke_inskrywings', function ($table) {
            $table->dropColumn('vrywaring_geteken_deur');
            $table->dropColumn('vrywaring_geteken_deur_id');
            $table->dropColumn('vrywaring_geteken_deur_epos');
        });
    }
}

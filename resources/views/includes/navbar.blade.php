            <a href="{{URL('admin')}}" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Kamp Admin
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">

                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                       
                        
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span>{{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}<i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="{{URL::asset('assets/img/avatar_wapen.png')}}" class="img-circle" alt="User Image" />
                                    <p>
                                        {{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}
                                        <small>Kamp Administrateur</small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div >
                                        <a href="{{url('/auth/logout')}}" class="btn btn-default btn-flat pull-right">Teken Uit</a>
                                        @if(Auth::user()->super_admin)
                                        <a href="{{url('superadmin')}}" class="btn btn-default btn-flat">Stelsel Administrateur</a>
                                        @endif
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
<!DOCTYPE html>
<html>
    <head>
            @section('head')
            @include('includes.head')
            @show
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            @include('includes.navbar')
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                @include('includes.sidebar')
            </aside>


            <!-- Right side column. Contains the navbar and content of the page -->
            @yield('content')
            
        </div><!-- ./wrapper -->

        @section('plugins')
        @include('includes.plugins')
        @show

    </body>
</html>

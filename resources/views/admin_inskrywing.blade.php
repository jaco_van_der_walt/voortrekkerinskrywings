@extends('layouts.master')

@section('head')
@parent
<title>{{$kamp->kamp_naam}}: Inskrywing</title>
<script src="{{URL('//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css')}}" type="text/css"></script>
<!-- jQuery 2.0.2 -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<!-- jQuery UI 1.10.3 -->
<script src="http://homestead.app/assets/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="http://homestead.app/assets/js/bootstrap.min.js" type="text/javascript"></script>  
<!-- Steps -->
<script src="{{URL::asset('assets/js/plugins/steps/jquery.steps.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('assets/css/steps/jquery_steps.css')}}" type="text/css"></script> 

<script type="text/javascript">


$("#example-basic").steps({
    headerTag: "h3",
    bodyTag: "section",
    transitionEffect: "slideLeft",
    autoFocus: true
});

                        
</script>     

@stop


@section('content')
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                         <a href={{URL('kamp/paneel/'.$kamp->id)}}><button type="submit" class="btn btn-primary "><i class="fa fa-arrow-circle-left"></i></button></a> {{$kamp->kamp_naam}}: Inskrywing
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">

                @if($errors->has())
                   @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Waarskuwing!</b> {{$error}}
                        </div>
                  @endforeach
                @endif

                @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissable">
                            <i class="fa fa-check"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Sukses!</b> {{Session::get('success')}}
                        </div>
                @endif


                    <!-- Small boxes (Stat box) -->
                                        


                <div id="example-basic">
    <h3>Sleutelbord</h3>
    <section>
        <p>Probeer die sleutelbord deur die links of regs pyltjie te druk!</p>
    </section>
    <h3>Nagevolge</h3>
    <section>
        <p>Wonderful transition effects.</p>
        <p>Wonderlike oorgang effekte.</p>
    </section>
    <h3>Pager</h3>
    <section>
        <p>Die volgende en voorige knoppies help jou om deur die inhoud te navigeer.</p>
    </section>
</div>

                            



                        <div class="box-footer">
                        </div>

                    </div><!-- /.row (main row) -->
  

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>

         <!-- DATA TABES SCRIPT -->
        <script src="{{URL('//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
        <script src="{{URL::asset('assets/js/plugins/datatables/dataTables.bootstrap.js')}}" type="text/javascript"></script>


       

@stop
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPubliekeInskrywingAddKoste extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('publieke_inskrywings', function ($table) {
            $table->float('koste')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('publieke_inskrywings', function ($table) {
            $table->dropColumn('koste');
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePubliekeInskrywingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publieke_inskrywings', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('matched_persoon_id')->unsigned()->nullable();
            $table->integer('kamp_id')->unsigned();
            $table->integer('groep_id')->unsigned()->nullable();
            $table->integer('voortrekker_betrokkenheid_id')->unsigned()->nullable();
            $table->string('inskrywing_soort');
            $table->boolean('bevestig_ouderdom')->default(false);
            $table->boolean('teken_vrywaring')->default(false);
            $table->boolean('admin_verwerk')->deafault(false);
            $table->string('verwysing')->nullable();
            $table->string('van');
            $table->string('voorname')->nullable();
            $table->string('noemnaam');
            $table->date('geboorte_datum');
            $table->char('geslag', 1);
            $table->string('id_nommer')->nullable();
            $table->string('kontak_nommer_1')->nullable();
            $table->string('kontak_nommer_2')->nullable();
            $table->string('epos_adres')->nullable();
            $table->string('voertuig_registrasie_nommer')->nullable();
            $table->boolean('voortrekker');
            $table->string('voortrekker_registrasie_nommer')->nullable();
            $table->string('voortrekker_rang')->nullable();
            $table->integer('kommando_id')->unsigned()->nullable();
            $table->string('ouer_voog_naam_van_1')->nullable();
            $table->string('ouer_voog_naam_van_2')->nullable();
            $table->string('ouer_voog_id_nommer_1')->nullable();
            $table->string('ouer_voog_id_nommer_2')->nullable();
            $table->string('ouer_voog_kontak_nommer_1')->nullable();
            $table->string('ouer_voog_kontak_nommer_2')->nullable();
            $table->string('ouer_voog_epos_adres_1')->nullable();
            $table->string('ouer_voog_epos_adres_2')->nullable();
            $table->string('posadres')->nullable();
            $table->string('woonadres')->nullable();
            $table->boolean('mediese_fonds');
            $table->string('mediese_fonds_naam')->nullable();
            $table->string('mediese_fonds_lid_nommer')->nullable();
            $table->string('huis_dokter_naam_van')->nullable();
            $table->string('huis_dokter_kontak_nommer_1')->nullable();
            $table->string('huis_dokter_kontak_nommer_2')->nullable();
            $table->string('siekte_allergie')->nullable();
            $table->string('notas')->nullable();


            $table->foreign('kommando_id')->references('id')->on('kommando');
            $table->foreign('kamp_id')->references('id')->on('kampe');
            $table->foreign('groep_id')->references('id')->on('groepe');
            $table->foreign('voortrekker_betrokkenheid_id')->references('id')->on('voortrekker_betrokkenheid');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('publieke_inskrywings');
    }
}

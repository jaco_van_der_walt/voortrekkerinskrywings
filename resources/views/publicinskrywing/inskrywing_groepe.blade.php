@extends('layouts.public_inskrywing')

@section('head')
@parent
<title>Inskrywing</title>



@stop


@section('content')
<aside class="right-side stretch">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                    <a href={{URL('publiek/inskrywing/terug')}} class="btn-sm btn-primary pull-left" style="margin-left: 5px; margin-bottom: 5px;"><i class="fa fa-arrow-left"></i></a> &nbsp;
                        Inskrywing: {{$kamp->kamp_naam}} - {{$persoon[0]->noemnaam}} {{$persoon[0]->van}}
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">

                    @if($errors->has())
                       @foreach ($errors->all() as $error)
                            <div class="alert alert-danger alert-dismissable">
                                <i class="fa fa-ban"></i>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <b>Waarskuwing!</b> {{$error}}
                            </div>
                      @endforeach
                    @endif

                    <div class="row">
                        <div class="col-md-12">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="callout callout-info">
                                    <h4>Kies Kursus of Divisie</h4>
                                    <p>By watse kursusgroep, divisie of groep wil {{$persoon[0]->noemnaam}} {{$persoon[0]->van}} betrokke wees tydens {{$kamp->kamp_naam}}?</p>
                                </div>
                            </div>
                        </div>

                        <div class="box box-primary">
                            <div class="box-header">
                                <h3 class="box-title"><b>Groepe Beskikbaar</b></h3>
                            </div>
                            <div class="box-body">
                                <div class="row">


                                    @foreach($groepe as $groep)
                                        @if($beskikbaar[$groep['groep_id']] < 1)
                                        <div class="col-md-4">
                                            <!-- Primary tile -->
                                            <div class="box box-solid bg-red">
                                                <div class="box-header">
                                                    <h4 class="box-title"><strong>{{$groep['groep_naam']}}</strong></h4>
                                                    {{$groep['groep_beskrywing']}}
                                                </div>
                                                <div class="box-body" style="padding-top: 0px;">
                                                  <p style="margin: 0px;"><em>{{$groep['groep_beskrywing']}}</em> &nbsp</p>
                                                    <div class = "row">
                                                        <div class="col-md-12">
                                                            <h4>Geen plekke tans beskikbaar!</h4>
                                                        </div>
                                                    </div>
                                                </div><!-- /.box-body -->
                                            </div><!-- /.box -->
                                        </div><!-- /.col -->
                                        @else
                                        <a href="{{URL('publiek/inskrywing/groep',[$groep['groep_id']])}}">
                                        <div class="col-md-4">
                                            <!-- Primary tile -->
                                            <div class="box box-solid bg-light-blue">
                                                <div class="box-header">
                                                    <h4 class="box-title"><strong>{{$groep['groep_naam']}}</strong></h4>
                                                </div>
                                                <div class="box-body" style="padding-top: 0px;">
                                                  <p style="margin: 0px;"><em>{{$groep['groep_beskrywing']}}</em> &nbsp</p>
                                                    <div class = "row">
                                                        <div class="col-md-3">
                                                            <h4>R{{$groep['groep_koste']}}</h4>
                                                        </div>
                                                        <div class="col-md-9 text-right">
                                                            <h4>Plekke beskikbaar: {{$beskikbaar[$groep['groep_id']]}}</h4>
                                                        </div>
                                                    </div>
                                                </div><!-- /.box-body -->
                                            </div><!-- /.box -->
                                        </div><!-- /.col -->
                                        </a>
                                        @endif

                                    @endforeach
                                </div>

                            </div>
                        </div>

                        <!-- footer -->
                        <div class="box-footer col-md-12">
                            <div class="row">
                                <a href="{{URL('publiek/inskrywing/terug')}}" class="btn btn-warning pull-left" style="margin-right: 5px;"><i class="fa fa-arrow-circle-left"></i> Terug </a>
                            </div>
                        </div>
                        <!-- /footer -->

                    </div>




                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
        <!-- AdminLTE App -->
        <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>


@stop

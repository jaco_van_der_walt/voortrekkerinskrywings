<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<style>
			table, td, th {
			    border: 1px solid black;
			    width: 100%;
			    border-collapse: collapse;
			    padding: 4px;
			    word-wrap:break-word;
			}

			.heading {
				text-align: left;
			}

			body { 
				font-family: DejaVu Sans, sans-serif;
				font-size:11px; 
			}
		</style>
	</head>
	<body>
		<div>
		<row>
			<h2>{{$kamp->kamp_naam}}: Siektes en Allergië</h2>
		</row>
			<table>
				<tr> 
					<td class="heading">
						<b>Van</b>
					</td>
					<td class="heading">
						<b>Noemnaam</b>
					</td>
					<td class="heading">
						<b>Geslag</b>
					</td>
					<td class="heading">
						<b>Betrokkenheid</b>
					</td>
					<td class="heading">
						<b>Geboortedatum</b>
					</td>
					<td class="heading">
						<b>Kommando</b>
					</td>
					<td class="heading">
						<b>Siekte/Allergië</b>
					</td>

				</tr>
				@foreach($inskrywings as $inskrywing)
					<tr>
						<td>
							{{$inskrywing->persoon->van}}
						</td>
						<td>
							{{$inskrywing->persoon->noemnaam}}
						</td>
						<td>
							@if($inskrywing->persoon->geslag == 'm')
							 ♂
							@elseif($inskrywing->persoon->geslag == 'f')
							 ♀
							@else
								{{$inskrywing->persoon->geslag}}
							@endif
						</td>
						<td>
							{{$inskrywing->voortrekkerBetrokkenheid->betrokkenheid}}
						</td>
						<td>
							{{$inskrywing->persoon->geboorte_datum}}
						</td>
						<td>
							@if($inskrywing->persoon->kommando)
								{{$inskrywing->persoon->kommando->kommando_naam}}
							@endif
						</td>
						<td>
							{{$inskrywing->persoon->siekte_allergie}}
						</td>
						
					</tr>
				@endforeach
			</table>
			<h4>Totaal: {{$count}}</h4>
		</div>
	</body>
</html>
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersoneTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('persone', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('registrasie_nommer');
			$table->string('van');
			$table->string('voorname')->nullable();
			$table->string('noemnaam');
			$table->date('geboorte_datum');
			$table->enum('geslag', ['m', 'f']);
			$table->string('id_nommer')->nullable();
			$table->string('kontak_nommer_1')->nullable();
			$table->string('kontak_nommer_2')->nullable();
			$table->string('epos_adres')->nullable();
			$table->string('voertuig_registrasie_nommer')->nullable();
			$table->boolean('voortrekker');
			$table->string('voortrekker_registrasie_nommer')->nullable();
			$table->string('voortrekker_rang')->nullable();
			$table->integer('kommando_id')->unsigned()->nullable();
			$table->string('ouer_voog_naam_van_1')->nullable();
			$table->string('ouer_voog_naam_van_2')->nullable();
			$table->string('ouer_voog_id_nommer_1')->nullable();
			$table->string('ouer_voog_id_nommer_2')->nullable();
			$table->string('ouer_voog_kontak_nommer_1')->nullable();
			$table->string('ouer_voog_kontak_nommer_2')->nullable();
			$table->string('ouer_voog_epos_adres_1')->nullable();
			$table->string('ouer_voog_epos_adres_2')->nullable();
			$table->string('posadres')->nullable();
			$table->string('woonadres')->nullable();
            $table->boolean('mediese_fonds');
			$table->string('mediese_fonds_naam')->nullable();
			$table->string('mediese_fonds_lid_nommer')->nullable();
			$table->string('huis_dokter_naam_van')->nullable();
			$table->string('huis_dokter_kontak_nommer_1')->nullable();
			$table->string('huis_dokter_kontak_nommer_2')->nullable();
			$table->string('siekte_allergie')->nullable();
			$table->string('notas')->nullable();


			$table->foreign('kommando_id')->references('id')->on('kommando');

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('persone');
	}

}
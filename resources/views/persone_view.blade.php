@extends('layouts.master') @section('head') @parent
<title>Persone</title>@stop @section('content')
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
         <h1>
                {{{$persoon->noemnaam}}} {{{$persoon->van}}} <small class="pull-right">{{$persoon->registrasie_nommer}}</small>
            </h1>

    </section>
    <!-- Main content -->
    <section class="content">@if($errors->has()) @foreach ($errors->all() as $error)
        <div class="alert alert-danger alert-dismissable"> <i class="fa fa-ban"></i>

            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <b>Waarskuwing!</b> {{$error}}</div>@endforeach @endif
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                         <h3 class="box-title"><b>Persoonlike Inligting</b></h3>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Registrasie Nommer -->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Registrasie Nommer</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$persoon->registrasie_nommer}}</label>
                            </div>
                        </div>
                        <!-- Van -->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Van  
                                </label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$persoon->van}}</label>
                            </div>
                        </div>
                        <!-- Voorname -->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Voorname</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$persoon->voorname}}</label>
                            </div>
                        </div>
                        <!-- Noemnaam -->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Noemnaam  
                                </label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$persoon->noemnaam}}</label>
                            </div>
                        </div>
                        <!-- Date dd/mm/yyyy -->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Geboortedatum  
                                </label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$persoon->geboorte_datum}}</label>
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                        <!-- Geslag -->
                        <!-- radio -->
                        <div class=" row form-group">
                            <div class="col-xs-5">
                                <label>Geslag  
                                </label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$persoon->geslag}}</label>
                            </div>
                        </div>
                        <!-- ID Nommer -->
                        @if($persoon->id_nommer)
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>ID Nommer</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$persoon->id_nommer}}</label>
                            </div>
                        </div>
                        @endif

                        @if($persoon->kontak_nommer_1)
                        <!-- Kontak Nommer -->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Kontak Nommer</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$persoon->kontak_nommer_1}}</label>
                            </div>
                        </div>
                        @endif

                        @if($persoon->kontak_nommer_2)
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Alternatiewe Kontak Nommer</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$persoon->kontak_nommer_2}}</label>
                            </div>
                        </div>
                        @endif

                        @if($persoon->epos_adres)
                        <!-- Epos Adres -->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Epos Adres</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$persoon->epos_adres}}</label>
                            </div>
                        </div>
                        @endif

                        @if($persoon->woonadres)
                        <!-- Adres -->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Woonadres</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$persoon->woonadres}}</label>
                            </div>
                        </div>
                        @endif

                        @if($persoon->posadres)
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Posadres</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$persoon->posadres}}</label>
                            </div>
                        </div>
                        @endif

                        <!-- Voertuig Registrasie Nommer -->
                         @if($persoon->voertuig_registrasie_nommer)
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Voertuig Registrasie Nommer</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$persoon->voertuig_registrasie_nommer}}</label>
                            </div>
                        </div>
                        @endif

                    </div>
                </div>
                <!-- /.box -->
                <!-- Die Voortrekkers -->
                <div class="box box-primary">
                    <div class="box-header">
                         <h3 class="box-title"><b>Die Voortrekkers</b></h3>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Voortrekker Registrasie Nommer -->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Voortrekker Registrasie Nommer</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$persoon->voortrekker_registrasie_nommer}}</label>
                            </div>
                        </div>

                        @if($persoon->voortrekker_rang)
                        <!-- Rang -->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Voortrekker Rang</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$persoon->voortrekker_rang}}</label>
                            </div>
                        </div>
                        @endif

                        <!-- Kommando-->
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Kommando</label>
                            </div>
                            <div class="col-xs-7">
                                @if($persoon->kommando)
                                    <label>{{$persoon->kommando->kommando_naam}}</label>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Ouers/Voog -->
                <div class="box box-primary">
                    <div class="box-header">
                         <h3 class="box-title"><b>Ouers/Voog</b></h3>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- Ouer/Voog #1 -->
                         <h4><b>Ouer/Voog #1</b></h4>

                        @if($persoon->ouer_voog_naam_van_1)
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Ouer/Voog Naam & Van</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$persoon->ouer_voog_naam_van_1}}</label>
                            </div>
                        </div>
                        @endif

                        @if($persoon->ouer_voog_id_nommer_1)
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Ouer/Voog ID Nommer</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$persoon->ouer_voog_id_nommer_1}}</label>
                            </div>
                        </div>
                        @endif

                        @if($persoon->ouer_voog_kontak_nommer_1)
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Ouer/Voog Kontak Nommer</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$persoon->ouer_voog_kontak_nommer_1}}</label>
                            </div>
                        </div>
                        @endif

                        @if($persoon->ouer_voog_epos_adres_1)
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Ouer/Voog Epos Adres</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$persoon->ouer_voog_epos_adres_1}}</label>
                            </div>
                        </div>
                        @endif

                         <h4><b>Ouer/Voog #2</b></h4>

                         @if($persoon->ouer_voog_naam_van_2)
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Ouer/Voog Naam & Van</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$persoon->ouer_voog_naam_van_2}}</label>
                            </div>
                        </div>
                        @endif

                        @if($persoon->ouer_voog_id_nommer_2)
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Ouer/Voog ID Nommer</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$persoon->ouer_voog_id_nommer_2}}</label>
                            </div>
                        </div>
                        @endif

                        @if($persoon->ouer_voog_kontak_nommer_2)
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Ouer/Voog Kontak Nommer</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$persoon->ouer_voog_kontak_nommer_2}}</label>
                            </div>
                        </div>
                        @endif

                        @if($persoon->ouer_voog_epos_adres_2)
                        <div class="row form-group">
                            <div class="col-xs-5">
                                <label>Ouer/Voog Epos Adres</label>
                            </div>
                            <div class="col-xs-7">
                                <label>{{$persoon->ouer_voog_epos_adres_2}}</label>
                            </div>
                        </div>
                        @endif

                    </div>
                </div>
            <!-- Mediese Inligting -->
            <div class="box box-primary">
                <div class="box-header">
                     <h3 class="box-title"><b>Mediese Inligting</b></h3>

                </div>
                <!-- /.box-header -->
                <div class="box-body">


                    <!-- Mediese Fonds Naam -->
                    @if($persoon->mediese_fonds_naam)
                    <div class="row form-group">
                        <div class="col-xs-5">
                            <label>Mediese Fonds Naam</label>
                        </div>
                        <div class="col-xs-7">
                            <label>{{$persoon->mediese_fonds_naam}}</label>
                        </div>
                    </div>
                    @endif

                    <!-- Mediese Fonds Lidmaat Nommer -->
                    @if($persoon->mediese_fonds_lid_nommer)
                    <div class="row form-group">
                        <div class="col-xs-5">
                            <label>Mediese Fonds Lidmaat Nommer</label>
                        </div>
                        <div class="col-xs-7">
                            <label>{{$persoon->mediese_fonds_lid_nommer}}</label>
                        </div>
                    </div>
                    @endif

                    <!-- Huisdokter -->
                    <h4><b>Huisdokter</b></h4>

                    @if($persoon->huis_dokter_naam_van)
                    <div class="row form-group">
                        <div class="col-xs-5">
                            <label>Huisdokter Naam & Van</label>
                        </div>
                        <div class="col-xs-7">
                            <label>{{$persoon->huis_dokter_naam_van}}</label>
                        </div>
                    </div>
                    @endif

                    @if($persoon->huis_dokter_kontak_nommer_1)
                    <div class="row form-group">
                        <div class="col-xs-5">
                            <label>Huisdokter Kontak Nommer</label>
                        </div>
                        <div class="col-xs-7">
                            <label>{{$persoon->huis_dokter_kontak_nommer_1}}</label>
                        </div>
                    </div>
                    @endif

                    @if($persoon->huis_dokter_kontak_nommer_2)
                    <div class="row form-group">
                        <div class="col-xs-5">
                            <label>Huisdokter Alternatiewe Kontak Nommer</label>
                        </div>
                        <div class="col-xs-7">
                            <label>{{$persoon->huis_dokter_kontak_nommer_2}}</label>
                        </div>
                    </div>
                    @endif

                    <!-- Mediese Toestand & Allergie -->
                    <div class="row form-group">
                        <div class="col-xs-5">
                            <label>Siektetoestande of Allergieë</label>
                        </div>
                        <div class="col-xs-7">
                            <label>{{$persoon->siekte_allergie}}</label>
                        </div>
                    </div>
                </div>
            </div>

        <!-- Mediese Inligting -->
            <div class="box box-primary">
                <div class="box-header">
                     <h3 class="box-title"><b>Kamp Geskiedenis</b></h3>

                </div>
                <!-- /.box-header -->
                <div class="box-body">

                @if($inskrywings)
                    @foreach($inskrywings as $inskrywing)
                    <div class="row form-group">
                        <div class="col-xs-3">
                            <label>{{$inskrywing}}</label>
                        </div>
                        <div class="col-xs-3">
                            @if($groepe[$inskrywing])
                                <label>{{$groepe[$inskrywing]}}</label>
                            @endif
                        </div>
                        <div class="col-xs-3">
                            @if($rolle[$inskrywing])
                                <label>{{$rolle[$inskrywing]}}</label>
                            @endif
                        </div>

                    </div>
                    @endforeach
                @endif


                </div>
            </div>
        </div>
        <div class="box-footer col-md-12">

        </div>
        </div>
        <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
</aside>
<!-- /.right-side -->@stop @section('plugins') @parent
<!-- AdminLTE App -->
<script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>
<!-- InputMask -->
<script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.date.extensions.js')}}" type="text/javascript"></script>
<script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.extensions.js')}}" type="text/javascript"></script>
<script type="text/javascript">
    //Datemask
    $("#datemask").inputmask("yyyy/mm/dd", {
        "placeholder": "yyyy/mm/dd"
    });
</script>@stop
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rolle', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('groep_id')->unsigned();
			$table->string('rol_naam');
			$table->timestamps();

			$table->foreign('groep_id')->references('id')->on('groepe');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rolle');
	}

}
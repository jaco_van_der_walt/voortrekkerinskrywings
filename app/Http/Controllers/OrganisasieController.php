<?php

namespace App\Http\Controllers;

use App\Persoon;
use App\Kamp;
use App\User;
use Hash;
use Auth;
use Redirect;
use App\Inskrywing;
use Illuminate\Http\RedirectResponse;
use \Rap2hpoutre\LaravelLogViewer;
use Input;
use Validator;
use Session;
use Log;
use App\Organisasie;

class OrganisasieController extends Controller
{
    public function ajax_organisasies()
    {
        $super_user = Auth::user();
        if($super_user->super_admin)
        {
            $return['recordsTotal'] = Organisasie::count();

            $return['data'] = [];
            $i = 0;
            foreach (Organisasie::orderBy('organisasie_naam')->get() as $o) {
                $return['data'][$i] = [$o->organisasie_naam];                
                $return['data'][$i][] =  '';
                $i++;
            }

            return response()->json($return);
        }
        else 
        {
            return Redirect::to('admin')->withErrors("U het nie die nodige toeganregte tot die skakel nie!");
        }
    }
}

@extends('layouts.master')

@section('head')
@parent
<title>Publieke Inskrywing</title>
@stop


@section('content')
<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <a href={{URL('publiek/inskrywing/admin/tabel')}}><button type="submit" class="btn btn-primary "><i class="fa fa-arrow-circle-left"></i></button></a> Kies 'n persoon
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">

                @if($errors->has())
                   @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <i class="fa fa-ban"></i>
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <b>Waarskuwing!</b> {{$error}}
                        </div>
                  @endforeach
                @endif

                <div class="callout callout-info">
                    <h4>Bevestig of die Persoon reeds in die databasis is</h4>
                    <p>Hieronder volg 'n lys van persone wat reeds in die databasis bestaan wat moontlik dieselfde persoon is wat wil inskryf. Kliek op 'n naam om die data in die databasis aan die regterkant van die skerm te vertoon. Die linkerkant wys die persoon se data soos dit ingevul is tydens die aanlyn proses. Kies die persoon wat by die inskrywing pas en kliek "Gebruik hierdie Persoon". Indien geen persoon in die databasis by die inskrywing pas nie, kliek "Skep nuwe Persoon" om 'n nuwe inskrywing in die databasis te maak.<br><br><strong>Maak asseblief seker dat jy nie persone dupliseer nie</strong></p>
                </div>



                    <form role="form" action="{{URL('publiek/inskrywing/admin/merge')}}" method="POST">
                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><b>Moontlike Persone reeds in Databasis</b></h3>
                                </div>
                                <div class="box-body">
                                    @foreach($moontlike_persone as $persoon)
                                        <a class="btn" onclick="load_persoon({{$persoon->id}})" id="{{$persoon->id}}">{{$persoon->noemnaam}} {{$persoon->van}}</a>
                                        
                                    @endforeach
                                </div>
                            </div>
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><b>Persoonlike Inligting</b></h3>
                                </div>
                                
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="selected_persoon" id="selected_persoon">
                                
                                    <div class="box-body">

                                    <!-- Van -->
                                        <div class="row form-group">
                                            <div class="col-xs-2">
                                                <label>Van</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->van}}</p>
                                            </div>   
                                            <div class="col-xs-4 alert-info">
                                                <label id="van"></label>
                                            </div>  
                                        </div>

                                    <!-- Voorname -->
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Voorname</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->voorname}}</p>
                                            </div>
                                            <div class="col-xs-4 alert-info">
                                                <label id="voorname"></label>
                                            </div>
                                        </div>

                                    <!-- Noemnaam -->
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Noemnaam</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->noemnaam}}</p>
                                            </div>   
                                            <div class="col-xs-4 alert-info">
                                                <label id="noemnaam"></label>
                                            </div>  
                                        </div>

                                    <!-- Date dd/mm/yyyy -->
                                    <div class="row form-group">
                                        <div class="col-xs-2">
                                                <label>Geboortedatum</label>
                                         </div>
                                        <div class="col-xs-4">
                                                <p>{{$inskrywing->geboorte_datum}}</p>
                                        </div>
                                        <div class="col-xs-4 alert-info">
                                                <label id="geboorte_datum"></label>
                                        </div>
                                    </div>

                                    <!-- Geslag -->
                                        <div class=" row form-group"> 

                                            <div class="col-xs-2">
                                                <label>Geslag</label>
                                            </div>    
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->geslag}}</p>
                                            </div>
                                            <div class="col-xs-4 alert-info">
                                                <label id="geslag"></label>
                                            </div>
                                        </div>

                                    <!-- ID Nommer -->
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>ID Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->id_nommer}}</p>
                                            </div>   
                                            <div class="col-xs-4 alert-info">
                                                <label id="id_nommer"></label>
                                            </div>
                                        </div>

                                    <!-- Kontak Nommer -->
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Kontak Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->kontak_nommer_1}}</p>
                                            </div>
                                            <div class="col-xs-4 alert-info">
                                                <label id="kontak_nommer_1"></label>
                                            </div>
                                        </div>

                                    <!-- Alternatiewe Kontak Nommer -->
                                        <div class="row form-group">
                                            <div class="col-xs-2">
                                                <label>Alternatiewe Kontak Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->kontak_nommer_2}}</p>
                                            </div>    
                                            <div class="col-xs-4 alert-info">
                                                <label id="kontak_nommer_2"></label>
                                            </div>    
                                        </div>

                                    <!-- Epos Adres -->
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Epos Adres</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->epos_adres}}</p>
                                            </div>   
                                            <div class="col-xs-4 alert-info">
                                                <label id="epos_adres"></label>
                                            </div>  
                                        </div>

                                        <!-- Woonadres -->
                                        <div class="row form-group">
                                            <div class="col-xs-2">
                                                <label>Woonadres</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->woonadres}}</p>
                                            </div>
                                            <div class="col-xs-4 alert-info">
                                                <label id="woonadres"></label>
                                            </div>
                                        </div>

                                        <!-- Posadres -->
                                        <div class="row form-group">
                                            <div class="col-xs-2">
                                                <label>Posadres</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->posadres}}</p>
                                            </div>
                                            <div class="col-xs-4 alert-info">
                                                <label id="posadres"></label>
                                            </div>
                                        </div>      

                                    <!-- Voertuig Registrasie Nommer -->
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Voertuig Registrasie Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->voertuig_registrasie_nommer}}</p>
                                            </div>   
                                            <div class="col-xs-4 alert-info">
                                                <label id="voertuig_registrasie_nommer"></label>
                                            </div>   
                                        </div>
                                    </div>
                            </div><!-- /.box --> 

                            <!-- Die Voortrekkers --> 
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><b>Die Voortrekkers</b></h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                        <div class="row form-group">
                                            <div class="col-xs-2">
                                                <label>Geregistreerde Voortrekker?</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->voortrekker}}</p>                                              
                                            </div>
                                            <div class="col-xs-4 alert-info">
                                                <label id="voortrekker"></label>                                              
                                            </div>
                                        </div>

                                    <!-- Voortrekker Registrasie Nommer -->
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Voortrekker Registrasie Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->voortrekker_registrasie_nommer}}</p>
                                            </div>   
                                            <div class="col-xs-4 alert-info">
                                                <label id="voortrekker_registrasie_nommer"></label>
                                            </div>  
                                        </div>            

                                        <!-- Rang -->
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Voortrekker Rang</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->voortrekker_rang}}</p>
                                            </div>   
                                            <div class="col-xs-4 alert-info">
                                                <label id="voortrekker_rang"></label>
                                            </div>           
                                        </div>

                                        <!-- Kommando -->
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Kommando</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->kommando_naam}}</p>
                                            </div>    
                                            <div class="col-xs-4 alert-info">
                                                <label id="kommando_naam"></label>
                                            </div>    
                                        </div>

                                        <!-- Skoolgraad -->
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Skoolgraad</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->skoolgraad}}</p>
                                            </div>    
                                            <div class="col-xs-4 alert-info">
                                                <label id="skoolgraad"></label>
                                            </div>    
                                        </div>
                                    </div>
                                </div>

                            <!-- Ouers/Voog --> 
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><b>Ouers/Voog</b></h3>
                                </div><!-- /.box-header -->
                                <div class="box-body">
                                        <!-- Ouer/Voog #1 -->
                                        <h4><b>Ouer/Voog #1</b></h4>
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Ouer/Voog Naam & Van</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->ouer_voog_naam_van_1}}</p>
                                            </div>   
                                            <div class="col-xs-4 alert-info">
                                                <label id="ouer_voog_naam_van_1"></label>
                                            </div>     
                                        </div>

                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Ouer/Voog ID Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->ouer_voog_id_nommer_1}}</p>
                                            </div>    
                                            <div class="col-xs-4 alert-info">
                                                <label id="ouer_voog_id_nommer_1"></label>
                                            </div>    
                                        </div>

                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Ouer/Voog Kontak Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->ouer_voog_kontak_nommer_1}}</p>
                                            </div>
                                            <div class="col-xs-4 alert-info">
                                                <label id="ouer_voog_kontak_nommer_1"></label>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Ouer/Voog Epos Adres</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->ouer_voog_pos_adres_1}}</p>
                                            </div>  
                                            <div class="col-xs-4 alert-info">
                                                <label id="ouer_voog_pos_adres_1"></label>
                                            </div>        
                                        </div>

                                        <!-- Ouer/Voog #2 -->
                                        <h4><b>Ouer/Voog #2</b></h4>
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Ouer/Voog Naam & Van</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->ouer_voog_naam_van_2}}</p>
                                            </div>   
                                            <div class="col-xs-4 alert-info">
                                                <label id="ouer_voog_naam_van_2"></label>
                                            </div>     
                                        </div>

                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Ouer/Voog ID Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->ouer_voog_id_nommer_2}}</p>
                                            </div>   
                                            <div class="col-xs-4 alert-info">
                                                <label id="ouer_voog_id_nommer_2"></label>
                                            </div>     
                                        </div>

                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Ouer/Voog Kontak Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->ouer_voog_kontak_nommer_2}}</p>
                                            </div>
                                            <div class="col-xs-4 alert-info">
                                                <label id="ouer_voog_kontak_nommer_2"></label>
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Ouer/Voog Epos Adres</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->ouer_voog_epos_adres_2}}</p>
                                            </div>   
                                            <div class="col-xs-4 alert-info">
                                                <label id="ouer_voog_epos_adres_2"></label>
                                            </div>        
                                        </div>
                                    </div>
                                </div>

                            <!-- Mediese Inligting --> 
                            <div class="box box-primary">
                                <div class="box-header">
                                    <h3 class="box-title"><b>Mediese Inligting</b></h3>
                                </div><!-- /.box-header -->
                                <div class="box-body"> 
                                    <!-- Mediese Fonds Check -->
                                    <div class="row form-group">
                                            <div class="col-xs-2">
                                                <label>Mediese Fonds?</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->mediese_fonds}}</p>                                              
                                            </div>
                                            <div class="col-xs-4 alert-info">
                                                <label id="mediese_fonds"></label>                                              
                                            </div>
                                        </div>

                                        <!-- Mediese Fonds Naam -->
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Mediese Fonds Naam</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->mediese_fonds_naam}}</p>  
                                            </div>     
                                            <div class="col-xs-4 alert-info">
                                                <label id="mediese_fonds_naam"></label>  
                                            </div>           
                                        </div>

                                        <!-- Mediese Fonds Lidmaat Nommer -->
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Mediese Fonds Lidmaat Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->mediese_fonds_lid_nommer}}</p>  
                                            </div>    
                                            <div class="col-xs-4 alert-info">
                                                <label id="mediese_fonds_lid_nommer"></label>  
                                            </div>            
                                        </div>

                                        <!-- Huisdokter -->
                                        <h4><b>Huisdokter</b></h4>
                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Huisdokter Naam & Van</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->huis_dokter_naam_van}}</p>  
                                            </div>   
                                            <div class="col-xs-4 alert-info">
                                                <label id="huis_dokter_naam_van"></label>  
                                            </div>      
                                        </div>

                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Huisdokter Kontak Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->huis_dokter_kontak_nommer_1}}</p>  
                                            </div>
                                            <div class="col-xs-4 alert-info">
                                                <label id="huis_dokter_kontak_nommer_1"></label>  
                                            </div>
                                        </div>

                                        <div class="row form-group">
                                             <div class="col-xs-2">
                                                <label>Huisdokter Alternatiewe Kontak Nommer</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->huis_dokter_kontak_nommer_2}}</p>  
                                            </div>  
                                            <div class="col-xs-4 alert-info">
                                                <label id="huis_dokter_kontak_nommer_2"></label>  
                                            </div>        
                                        </div>

                                        <!-- Mediese Toestand & Allergie -->
                                        <div class="row form-group">
                                            <div class="col-xs-2">
                                                <label>Siektetoestande of Allergieë</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <p>{{$inskrywing->siekte_allergie}}</p>  
                                            </div>
                                            <div class="col-xs-4 alert-info">
                                                <label id="siekte_allergie"></label>  
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="row form-group">
                            <div class = "col-md-12">
                                <a href={{URL('publiek/inskrywing/admin/tabel')}} class="btn btn-primary" style="margin-bottom: 15px;">Kanselleer</a>
                                <button id="submit" type="submit" class="btn btn-success pull-right">Gebruik Hierdie Persoon <i class="fa fa-arrow-circle-right"></i></button> 
                                <a href={{URL('publiek/inskrywing/admin/skep/persoon',$inskrywing->id)}} class="btn btn-warning pull-right" style="margin-bottom: 15px; margin-right: 50px;">Skep nuwe persoon</a> 
                            
                            </div> 
                        </div>
                    </form>
                    </div><!-- /.row (main row) -->
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
@stop


@section('plugins')
@parent
    <!-- AdminLTE App -->
    <script src="{{URL::asset('assets/js/AdminLTE/app.js')}}" type="text/javascript"></script>
    <!-- InputMask -->
    <script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.date.extensions.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets/js/plugins/input-mask/jquery.inputmask.extensions.js')}}" type="text/javascript"></script>


    <script type="text/javascript">
        var selected = -1;
        function load_persoon($id)
        {
            $('#'+$id).toggleClass('btn-success');
            $('#'+selected).toggleClass('btn-success');
            selected = $id;

            $.get("{{URL('ajax/persoon')}}/"+$id, 
                function( data ) 
                {
                    $('#van').html(data.van);
                    $('#voorname').html(data.voorname);
                    $('#noemnaam').html(data.noemnaam);
                    $('#geboorte_datum').html(data.geboorte_datum);
                    $('#geslag').html(data.geslag);
                    $('#id_nommer').html(data.id_nommer);
                    $('#kontak_nommer_1').html(data.kontak_nommer_1);
                    $('#kontak_nommer_2').html(data.kontak_nommer_2);
                    $('#epos_adres').html(data.epos_adres);
                    $('#woonadres').html(data.woonadres);
                    $('#posadres').html(data.posadres);
                    $('#voertuig_registrasie_nommer').html(data.voertuig_registrasie_nommer);
                    $('#voortrekker').html(data.voortrekker);
                    $('#voortrekker_registrasie_nommer').html(data.voortrekker_registrasie_nommer);
                    $('#voortrekker_rang').html(data.voortrekker_rang);
                    $('#kommando_naam').html(data.kommando_naam);
                    $('#ouer_voog_naam_van_1').html(data.ouer_voog_naam_van_1);
                    $('#ouer_voog_id_nommer_1').html(data.ouer_voog_id_nommer_1);
                    $('#ouer_voog_kontak_nommer_1').html(data.ouer_voog_kontak_nommer_1);
                    $('#ouer_voog_pos_adres_1').html(data.ouer_voog_pos_adres_1);
                    $('#ouer_voog_naam_van_2').html(data.ouer_voog_naam_van_2);
                    $('#ouer_voog_id_nommer_2').html(data.ouer_voog_id_nommer_2);
                    $('#ouer_voog_kontak_nommer_2').html(data.ouer_voog_kontak_nommer_2);
                    $('#ouer_voog_epos_adres_2').html(data.ouer_voog_epos_adres_2);
                    $('#mediese_fonds').html(data.mediese_fonds);
                    $('#mediese_fonds_naam').html(data.mediese_fonds_naam);
                    $('#mediese_fonds_lid_nommer').html(data.mediese_fonds_lid_nommer);
                    $('#huis_dokter_naam_van').html(data.huis_dokter_naam_van);
                    $('#huis_dokter_kontak_nommer_1').html(data.huis_dokter_kontak_nommer_1);
                    $('#huis_dokter_kontak_nommer_2').html(data.huis_dokter_kontak_nommer_2);
                    $('#siekte_allergie').html(data.siekte_allergie);

                    document.getElementById("selected_persoon").value = data.id;
                });
        }

    </script>



@stop